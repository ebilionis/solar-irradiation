#!/bin/env python
# vim: set syntax=python
"""
A script that extract patches from satellite data.

Author:
    Ilias Bilionis

Date:
    1/27/2014
"""


import os
import sys
import warnings
import glob
import pandas as pd
import numpy as np
import tables as pt
from datetime import datetime
try:
    # This will work if the module is properly installed
    import solar
except:
    # This will work if everything is in place
    this_file = os.path.abspath(__file__)
    script_dir = os.path.split(this_file)[0]
    solar_dir = os.path.split(script_dir)[0]
    sys.path.insert(0, solar_dir)
    import solar



# The mask we are interested in (CONUS scan relevant only)
DATA_MASK = ['0102', '0132', '0202', '0232', '0332', '0402', '0432', '0502', '0632',
             '0732', '0802', '0832', '0932', '1002', '1032', '1102', '1132', '1232',
             '1302', '1332', '1402', '1602', '1632', '1702', '1832', '1902', '1932',
             '2002', '2032', '2132', '2302', '2332']


# Put here all the attributes you want to extract from the satellite
ATTRIBUTES = ['solar_zenith_angle',
              'cloud_phase',
              'insolation',
              'insolation_diffuse',
              'wind_speed_10m_nwp',
              'wind_speed_cloud_top_nwp',
              'wind_direction_cloud_top_nwp',
              'refl_0_65um_nom',
              'cloud_transmission_0_65um_nom',
              'cld_reff_dcomp',
              'cloud_water_path'
              ]


patch_dtype = {'name': pt.StringCol(itemsize=16),
               'id': pt.UInt32Col(),
               'date': pt.StringCol(itemsize=26),
               'rows': pt.UInt32Col(),
               'cols': pt.UInt32Col(),
               'latitude': pt.Float64Col(),
               'longitude': pt.Float64Col()}

clavrx_dtype = {'name': pt.StringCol(itemsize=20),
                'id': pt.UInt32Col(),
                'date': pt.StringCol(itemsize=26),
                'sun_earth_distance': pt.Float32Col(),
                'filename': pt.StringCol(itemsize=43)}


def add_patch(patches, box_rows, box_cols, longitude, latitude, tol=1e-6):
    """
    Add a patch to the file (if not already there).
    """
    m = [p for p in patches.where('(abs(longitude - %f) < %f) & (abs(latitude - %f) < %f) & (box_rows == %d) & (box_cols == %d)' % (longitude, tol, latitude, tol, box_rows, box_cols))]
    if m == []:
        name = 'patch_' + str(patches.nrows)
        patches.row['name'] = name
        patches.row['id'] = patches.nrows
        patches.row['date'] = datetime.now()
        patches.row['rows'] = box_rows
        patches.row['cols'] = box_cols
        patches.row['latitude'] = latitude
        patches.row['longitude'] = longitude
        patches.row.append()
        patches.flush()
    else:
        name = m[0]['name']
    return name


def create_patch_group(fd, patch_name):
    """
    Create a group for the patch data if not already there.
    """
    try:
        patch_data = fd.get_node('/data/' + patch_name)
    except:
        try:
            fd.get_node('/data')
        except:
            fd.create_group('/', 'data')
        patch_data = fd.create_group('/data', patch_name)
    return patch_data


def add_data(fd, patch_name, clavrx_fd, attr):
    """
    Add data to a patch group.
    """
    # Pick the compression filter (full compression is 9).
    # Here we are using zlib
    filter = pt.Filters(complevel=9)
    # Write longitude and latitude only ones
    lons = clavrx_fd.get_patch('longitude')
    lats = clavrx_fd.get_patch('latitude')
    try:
        fd.get_node('/data/' + patch_name + '/longitude')
    except:
        clons = fd.create_carray('/data/' + patch_name, 'longitude', pt.Float32Atom(),
                                shape=(lons.shape[0], lons.shape[1]), filters=filter)
        clons[:,:] = lons
        clats = fd.create_carray('/data/' + patch_name, 'latitude', pt.Float32Atom(),
                                shape=(lats.shape[0], lats.shape[1]), filters=filter)
        clats[:,:] = lats
    # Write table containing info about files read so far
    try:
        records = fd.get_node('/data/' + patch_name + '/records')
    except:
        records = fd.create_table('/data/' + patch_name, 'records', clavrx_dtype)
    # Check if the record exists, if yes skip the file
    date = clavrx_fd.datetime
    m = list(records.where('date == \'%s\'' % str(date)))
    if m == []:
        # Write the file
        name = ('goes13_' + str(date.year) + '_' + str(date.timetuple().tm_yday) + '_'
                + str(date.hour).zfill(2) + str(date.minute).zfill(2))
        records.row['name'] = name
        records.row['id'] = records.nrows
        records.row['filename'] = clavrx_fd.f.attributes()['FILENAME']
        records.row['sun_earth_distance'] = clavrx_fd.f.attributes()['SUN_EARTH_DISTANCE']
        records.row['date'] = date
        records.row.append()
        records.flush()
        # Create a group for all the data
        fd.create_group('/data/' + patch_name, name)
        # Write the attributes one by one
        for t in attr:
            d = clavrx_fd.get_patch(t)
            dtype = str(d.dtype)
            atom_class = dtype[0].upper() + dtype[1:] + 'Atom'
            ct = fd.create_carray('/data/' + patch_name + '/' + name,
                                  t, getattr(pt, atom_class)(),
                                  shape=(d.shape[0], d.shape[1]), filters=filter)
            ct[:, :] = d
    else:
        print 'Skipping.'


def main(box_rows, box_cols, lat, lon, CLAVRx_DIR, output_dir):
    """
    The main function.

    :param box_rows:    The number of rows of the box.
    :type box_rows:     int
    :param box_cols:    The number of columns of the box.
    :type box_cols:     int
    :param lat:         The latitude of the site.
    :type lat:          float
    :param lon:         The longitude of the site.
    :type lon:          float
    """
    # An object representing the observer.
    observer = solar.Observer(latitude=lat, longitude=lon)
    # Get a list of all the data files we have in place (we do not check the mask)
    with open(os.path.join(CLAVRx_DIR, '.filelist'), 'r') as fd:
        lof = fd.readlines()
    lof = [os.path.join(CLAVRx_DIR, f.strip()) for f in lof]
    # This should sort the data in chronological order
    lof.sort()
    fd = solar.CLAVRxFile(lof[0], observer=observer)
    filename = os.path.join(output_dir, 'sat_' + str(box_rows) + 'x' +
                                         str(box_cols) + '_patch.h5')
    if os.path.exists(filename) and pt.is_pytables_file(filename):
        out_fd = pt.open_file(filename, mode='a')
        patches = out_fd.root.patches
    else:
        out_fd = pt.open_file(filename, mode='w')
        patches = out_fd.create_table('/', 'patches', patch_dtype,
                                      'Description of patches')
    # Look it up to see if there already is a patch like the one we want to create
    patch_name = add_patch(patches, box_rows, box_cols,
                           observer.longitude, observer.latitude)
    # Create group for the patch if not already there
    patch_group = create_patch_group(out_fd, patch_name)
    add_data(out_fd, patch_name, fd, ATTRIBUTES)
    index = [fd.datetime]
    data = [[fd.get_at_observer(ATTRIBUTES[i]) for i in range(len(ATTRIBUTES))]]
    for filename in lof[1:]:
        print filename
        try:
            fd.swap_file(filename)
            add_data(out_fd, patch_name, fd, ATTRIBUTES)
        except:
            print 'Corrupted file:', filename
            continue


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description='Extract patches from satellite data.',
                                formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--box-rows', dest='box_rows', type=int, default=200,
                        help='specify the number of rows of the box.')
    parser.add_argument('--box-cols', dest='box_cols', type=int, default=200,
                        help='specify the number of columns of the box.')
    parser.add_argument('--lat', dest='lat', type=float, default=41.711841,
                        help='specify the latitude of the site.')
    parser.add_argument('--lon', dest='lon', type=float, default=-87.976492,
                        help='specify the longitude of the site.')
    parser.add_argument('--clavrx-dir', dest='clavrx_dir', type=str,
                        default=(os.environ['CLAVRx_DIR']
                                 if os.environ.has_key('CLAVRx_DIR') else None),
                        help=('specify the directory containing the CLAVRx data.'
                              ' If it is not specified, then we look at the'
                              ' environment variable CLAVRx_DIR. If the latter'
                              ' is not specified either, then I look at '
                              ' "$HOME/data/CLAVRx".'))
    parser.add_argument('--output-dir', dest='output_dir', type=str,
                        default=os.path.abspath('.'),
                        help='specify the output directory.')
    args = parser.parse_args()
    main(args.box_rows, args.box_cols, args.lat, args.lon, args.clavrx_dir,
         args.output_dir)
