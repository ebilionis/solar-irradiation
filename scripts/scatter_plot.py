"""
Do a scatter plot.
"""

import pandas as pd
import tables as pt
import os
import sys
import numpy as np
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import *
try:
    import solar
except:
    sys.path.insert(0, os.path.abspath(os.path.join(os.path.split(__file__)[0], '..')))
from solar.dates import get_good_dates_with_lag
from solar.dates import str_to_datetime
from solar.dates import get_closest_date
from solar.input_output import extract_patch_data
from solar.input_output import load_pickled_obj
import cPickle as pickle
import warnings
warnings.filterwarnings("ignore")
import matplotlib as mlt
mlt.use('Agg')
import matplotlib.pyplot as plt


# Load the reduction map
red_map_file = sys.argv[1]
with open(red_map_file, 'rb') as fd:
    red_map = pickle.load(fd)
input_file = sys.argv[2]
out_dir = sys.argv[3]


f = pt.open_file(input_file, 'r')
clearness_indices, dates = extract_patch_data(f, 'clearness_index', final_date='2014-03-03 16:00')
f.close()
lag_idx = get_good_dates_with_lag(dates, 2)
Z = red_map.transform(clearness_indices)
X = np.array([np.hstack([Z[j, :] for j in lag_idx[i, :]])
              for i in xrange(lag_idx.shape[0])])
for i in xrange(red_map.n_components):
    for j in xrange(red_map.n_components):
        x1 = X[:, i]
        x2 = X[:, j + red_map.n_components]
        plt.plot(x1, x2, '.')
        plt.xlabel('$x_{%d}(t-1)$' % (i + 1), fontsize=30)
        plt.ylabel('$x_{%d}(t)$' % (j + 1), fontsize=30)
        plt.locator_params(nbins=6)
        plt.setp(plt.axes().get_xticklabels(), fontsize=30)
        plt.setp(plt.axes().get_yticklabels(), fontsize=30)
        plt.tight_layout()
        png_file = os.path.join(out_dir, 'scatter_%d_%d.png' % (i + 1, j + 1))
        print 'Writing:', png_file
        plt.savefig(png_file)
        plt.clf()
