"""
Make a movie of the predictions.

Author:
    Ilias Bilionis
"""


import tables as pt
import os
from utils import plot_map
import subprocess
import matplotlib.pyplot as plt
import numpy as np


def main(input_filename, prefix, sample, delay, do_mean, do_std,
         lower, upper, num_levels):
    """
    The main function.
    """
    f = pt.open_file(input_filename, mode='r')
    lat = f.root.latitude()[:]
    f.root.latitude.umount()
    lon = f.root.longitude()[:]
    f.root.latitude.umount()
    sample_name = f.root.sample_record.cols.name[sample]
    png_files = []
    levels = np.linspace(lower, upper, num_levels)
    for row in f.root.prediction_record.iterrows():
        print row['name']
        if do_mean:
            k = f.get_node('/statistics/' + row['name']).clearness_index_mean[:]
            title = 'mean'
        elif do_std:
            k = f.get_node('/statistics/' + row['name']).clearness_index_std[:]
            title = 'std'
        else:
            pred_g = f.get_node('/samples/' + sample_name + '/' + row['name'])
            k = pred_g.clearness_index[:]
            title = sample_name
        png_file = prefix + '_' + title
        plot_map(lon, lat, k, levels)
        plt.title(title + ': ' + row['date'], fontsize=16)
        png_file += '_' + row['name'] + '.png'
        png_files.append(png_file)
        plt.savefig(png_file)
        plt.clf()
    f.close()
    # Create movie
    movie_file = prefix + '_' + title + '.gif'
    print 'Creating movie:', movie_file
    cmd = ['convert', '-delay', str(delay)] + png_files + [movie_file]
    subprocess.call(cmd)


if __name__ == '__main__':
    from argparse import ArgumentParser as Parser
    from argparse import ArgumentDefaultsHelpFormatter as HelpFormatter
    parser = Parser(description='Continue the training of the GP using MCMC.',
                    formatter_class=HelpFormatter)
    parser.add_argument('-i', dest='input_filename', type=str,
                        help='specify the pickled file containing the PCA.')
    parser.add_argument('--prefix', dest='prefix', type=str,
                        help='specify the output prefix.')
    parser.add_argument('--sample', dest='sample', type=int, default=0,
                        help='specify the sample number to plot.')
    parser.add_argument('--delay', dest='delay', type=int, default=20,
                        help='specify the delay between frames.')
    parser.add_argument('--do-mean', dest='do_mean', action='store_true',
                        help='do the mean.')
    parser.add_argument('--do-std', dest='do_std', action='store_true',
                        help='do the standard deviation.')
    parser.add_argument('--lower', dest='lower', type=float, default=0.,
                        help='specify the lowest contour level.')
    parser.add_argument('--upper', dest='upper', type=float, default=1.3,
                        help='specify the highest contour level.')
    parser.add_argument('--levels', dest='levels', type=int, default=20,
                        help='specify the number of contour levels.')
    args = parser.parse_args()
    if args.input_filename is None:
        sys.stderr.write('You have to specify the input filename.\n')
        quit()
    if args.do_mean and args.do_std:
        sys.stderr.write('You may specify only of of the --do-mean or --do-std options.')
        quit()
    if args.prefix is None:
        args.prefix = os.path.splitext(args.input_filename)[0] + '_ahead'
    main(args.input_filename, args.prefix, args.sample, args.delay,
         args.do_mean, args.do_std, args.lower, args.upper, args.levels)
