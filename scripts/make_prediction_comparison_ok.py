"""
Compare the predictions with ANL ground observations.

Author:
    Ilias Bilionis
"""


import tables as pt
import os
import sys
from utils import plot_map
import subprocess
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from utils import str_to_datetime
from utils import get_closest_date
try:
    import solar
except:
    sys.path.insert(0, os.path.abspath(os.path.join(os.path.split(__file__)[0], '..')))
from solar import Observer
from solar import ExtraterrestrialIrradiance
from solar import ClearSkyIrradiance
from solar import fetch_no_qc_data
from solar.utils import get_error_bars
from utils import create_and_write_float32_array
from utils import date_to_str


def main(input_filenames, sat_obs_filename, out_dir, sample, do_mean):
    """
    The main function.
    """
    # Find out the input filenames
    tmp = input_filenames.split(',')
    sat_input_filename = tmp[0]
    ground_input_filename = None if len(tmp) == 1 else tmp[1]
    # DO EVERYTHING YOU NEED WITH THE SATELLITE DATA
    sat_f = pt.open_file(sat_input_filename, mode='a')
    lon = sat_f.root.longitude()[:]
    sat_f.root.longitude.umount()
    lat = sat_f.root.latitude()[:]
    sat_f.root.latitude.umount()
    shape = lat.shape
    i0 = shape[0] / 2
    j0 = shape[0] / 2
    b_index = sat_f.root.prediction_record.cols.date[:]
    index = str_to_datetime(sat_f.root.prediction_record.cols.date[:])
    # Check if we have already computed the statistics
    try:
        I_sat_m = sat_f.root.statistics.insolation_median[:]
        I_sat_p_05 = sat_f.root.statistics.insolation_percentile_05[:]
        I_sat_p_95 = sat_f.root.statistics.insolation_percentile_95[:]
        clear_sky_insolation = sat_f.root.statistics.clear_sky_insolation[:]
    except:
        print 'I did not find point statistics in:', sat_input_filename
        print 'I am computing them from scratch.'
        # Initialize the model for extraterrestrial insolation
        ext_I = ExtraterrestrialIrradiance()
        # Initialize the clear sky model
        cls_model = ClearSkyIrradiance()
        obs = Observer(longitude=lon[i0, j0], latitude=lat[i0, j0])
        z, r = obs.compute_many(index)
        clear_sky_insolation = cls_model(ext_I(r), z)[0]
        I_samples = []
        for row_s in sat_f.root.sample_record.iterrows():
            sys.stdout.write('Doing ' + row_s['name'] + ' out of '
                             + str(sat_f.root.sample_record.nrows) + '\r')
            sys.stdout.flush()
            I_single = []
            for row_p in sat_f.root.prediction_record.iterrows():
                k_p = sat_f.get_node('/samples/' + row_s['name'] + '/' +
                                     row_p['name']).clearness_index[i0, j0]
                I_single.append(k_p)
            I_single = np.array(I_single) * clear_sky_insolation
            I_samples.append(I_single)
        I_samples = np.array(I_samples)
        I_sat_m = np.percentile(I_samples, 50, axis=0)[:, None]
        I_sat_p_05 = np.percentile(I_samples, 5, axis=0)[:, None]
        I_sat_p_95 = np.percentile(I_samples, 95, axis=0)[:, None]
        statistics_group = sat_f.get_node('/statistics')
        filter = pt.Filters(complib='zlib', complevel=9)
        try:
            sat_f.remove_node('/statistics/clear_sky_insolation')
        except:
            pass
        try:
            sat_f.remove_node('/statistics/insolation_median')
        except:
            pass
        try:
            sat_f.remove_node('/statistics/insolation_percentile_05')
        except:
            pass
        try:
            sat_f.remove_node('/statistics/insolation_percentile_95')
        except:
            pass
        create_and_write_float32_array(sat_f, statistics_group,
                                       'clear_sky_insolation',
                                       clear_sky_insolation[:, None], filter)
        create_and_write_float32_array(sat_f, statistics_group,
                                       'insolation_median',
                                       I_sat_m, filter)
        create_and_write_float32_array(sat_f, statistics_group,
                                       'insolation_percentile_05',
                                       I_sat_p_05, filter)
        create_and_write_float32_array(sat_f, statistics_group,
                                       'insolation_percentile_95',
                                       I_sat_p_95, filter)
        sys.stdout.write('\n')
    # At this point we should have I_sat_m, I_sat_p_05, I_sat_p_95
    sat_f.close()
    # EXTRACT REAL SATELLITE OBSERVATIONS AT THOSE TIMES
    sat_obs_filename = '/pvfs/ebilionis/data/insolation_ok_400x400.h5'
    sat_obs_f = pt.open_file(sat_obs_filename, mode='r')
    sat_dates = str_to_datetime(sat_obs_f.root.records.cols.date[:])
    sat_idx = []
    for p_date in index:
        sat_idx.append(get_closest_date(sat_dates, p_date))
    sat_idx = list(set(sat_idx))
    I_sat_obs = []
    for i in sat_idx:
        sat_name = sat_obs_f.root.records.cols.name[i]
        I_sat_obs.append(sat_obs_f.get_node('/data/' + sat_name).insolation[i0, j0])
    sat_f.close()
#    df = pd.DataFrame(I_sat_obs, index=sat_dates[sat_idx],
#                      columns=['Satellite Observations'])
    df = pd.DataFrame(I_sat_obs, index=sat_dates[sat_idx],
                      columns=['Satellite Observations'])
#    df.index = df.index.tz_localize('UTC')
    ax = df.plot(style='m*', markersize=10)
#
    # Now let us get the ground based predictions (if there)
#    ground_f = pt.open_file(ground_input_filename, mode='r')
#    I_ground_m = ground_f.root.statistics.insolation_median[:]
#    I_ground_p_05 = ground_f.root.statistics.insolation_percentile_05[:]
#    I_ground_p_95 = ground_f.root.statistics.insolation_percentile_95[:]
#    ground_f.close()
#    sys.stdout.write('Fetching ground observations data...')
#    df_I_ground = fetch_no_qc_data().resample('30T', how='mean')
#    df_I_ground.columns = ['Ground Measurements']
#    sys.stdout.write('\n')
#    ax = df_I_ground.plot(style='b+', markersize=10)
    # Plot satellite prediction
    #index = index.tz_convert('CST')
    df = pd.DataFrame(I_sat_m, index=index, columns=['PCA/GP (Satellite) Median'])
#    print df
    ax = df.plot(style='r', ax=ax, linewidth=2)
    plt.fill_between(df.index.values, I_sat_p_05[:, 0], I_sat_p_95[:, 0], alpha=0.2,
                     facecolor='red')
    #print df.index
    #print df.index.values
    #print index.values
    #print df.index
    #print index
    # Plot the ground based prediction
#    df = pd.DataFrame(I_ground_m, index=index, columns=['GP (Ground) Median'])
#    ax = df.plot(style='g', ax=ax, linewidth=2)
#    plt.fill_between(df.index.values, I_ground_p_05[:,0], I_ground_p_95[:,0],
#                     alpha=0.2, facecolor='green')
    df = pd.DataFrame(clear_sky_insolation, index=index, columns=['Clear Sky Model'])
    df.plot(style='k', ax=ax, linewidth=2)
    png_file = os.path.join(out_dir, date_to_str(index[0]) + '_comparison.png')
    print 'Writing', png_file
    plt.savefig(png_file)


if __name__ == '__main__':
    from argparse import ArgumentParser as Parser
    from argparse import ArgumentDefaultsHelpFormatter as HelpFormatter
    parser = Parser(description='Make a comparison plot of various models.',
                    formatter_class=HelpFormatter)
    parser.add_argument('-i', dest='input_filenames', type=str,
                        help='specify the input filenames (comma separated list).')
    parser.add_argument('--out-dir', dest='out_dir', type=str,
                        help='specify the output out_dir.')
    parser.add_argument('--sample', dest='sample', type=int, default=0,
                        help='specify the sample number to plot.')
    parser.add_argument('--do-mean', dest='do_mean', action='store_true',
                        help='do the mean.')
    parser.add_argument('--sat-data-filename', dest='sat_data_filename', type=str,
                        help='specify the satellite data filename.')
    args = parser.parse_args()
    if args.input_filenames is None:
        sys.stderr.write('You have to specify the input filename.\n')
        quit()
    if args.do_mean and args.do_std:
        sys.stderr.write('You may specify only of of the --do-mean or --do-std options.')
        quit()
    if args.out_dir is None:
        args.out_dir = os.path.split(args.input_filenames.split(',')[0])[0]
    main(args.input_filenames, args.sat_data_filename, args.out_dir, args.sample,
         args.do_mean)
