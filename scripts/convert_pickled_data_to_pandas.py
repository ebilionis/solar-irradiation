"""
Convert emil's pickled data to a pandas time series.
"""


import pandas as pd
import cPickle as pickle
import numpy as np
import sys
import os


def read_pickled_data(filename):
    """
    Read the pickled data from ``filename``.
    :returns:   A dictionary describing the data.
    """
    data = {}
    with open(filename, 'rb') as fd:
        data['julian'] = np.array(pickle.load(fd), 'float64')
        data['unix'] = (data['julian'] - 2440587.5) * 86400
        data['year'] = np.array(pickle.load(fd), 'int16')
        data['month'] = np.array(pickle.load(fd), 'uint8')
        data['day'] = np.array(pickle.load(fd), 'uint8')
        data['hour'] = np.array(pickle.load(fd), 'uint16')
        data['minute'] = np.array(pickle.load(fd), 'uint8')
        data['irradiance'] = np.array(pickle.load(fd), 'float32')
        idx = (data['irradiance'] == 99999)
        data['irradiance'][idx] = 'NaN'
    return data


if __name__ == '__main__':
    if not len(sys.argv) == 2:
        sys.stderr.write('Provide the pickled data.\n')
        sys.exit(0)
    #data = read_pickled_data(sys.argv[1])
    #my_pickled_file = os.path.splitext(sys.argv[1])[0] + '.pickle'
    #with open(my_pickled_file, 'wb') as fd:
    #    pickle.dump(data, fd, protocol=pickle.HIGHEST_PROTOCOL)
    #quit()
    with open(sys.argv[1], 'rb') as fd:
        data = pickle.load(fd)
    print data['unix']
    #t = pd.to_datetime(data['unix'], unit='s')
    t = pd.DatetimeIndex(
    [pd.datetime(data['year'][i], data['month'][i], data['day'][i],
                 data['hour'][i],
                 data['minute'][i])
     for i in xrange(data['year'].shape[0])], tz='Etc/GMT+6')
    df = pd.DataFrame(data['irradiance'], index=t, columns=['Irradiance'])
#    df['Irradiance'] = df['Irradiance'].interpolate(method='linear')
    df['julian'] = data['julian']
    print df
    my_pandas_file = os.path.splitext(sys.argv[1])[0] + '.pd'
    df.to_pickle(my_pandas_file)
    my_hdf5_file = os.path.splitext(sys.argv[1])[0] + '.h5'
    df.to_hdf(my_hdf5_file, 'solar')
