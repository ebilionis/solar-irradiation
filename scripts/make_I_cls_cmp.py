#!/bin/env python
# vim: set syntax=python
"""
Make a movie of the satellite data.

Author:
    Ilias Bilionis
"""

import tables as pt
import matplotlib.pyplot as plt
import os
import numpy as np
from solar.input_output import extract_patch_data
from utils import plot_map


if __name__ == '__main__':
    f = pt.open_file('data/insolation_ok_400x400.h5', mode='r')
    lat = f.root.latitude[:]
    lon = f.root.longitude[:]
    I, dates = extract_patch_data(f, 'insolation',
                                  start_date='2014-03-01 16:00',
                                  final_date='2014-03-01 23:30')
    C, dates = extract_patch_data(f, 'clearness_index',
                                  start_date='2014-03-01 16:00',
                                  final_date='2014-03-01 23:30')
    for i in xrange(I.shape[0]):
        fig = plt.figure()
        ax1 = fig.add_subplot(121)
        ax2 = fig.add_subplot(122)
        plot_map(lon, lat, I[i, :].reshape(lat.shape),
                 levels=np.linspace(0, 800, 20),
                 ax=ax1)
        plot_map(lon, lat, C[i, :].reshape(lat.shape),
                 levels=np.linspace(0, 1.3, 20),
                 ax=ax2)
        png_figure = 'I_cls_cmp_' + str(i).zfill(2) + '.png'
        print 'Writing:', png_figure
        plt.savefig(png_figure)
        plt.clf()
