"""
Make the prediction plots.

Author:
    Ilias Bilionis

Date:
    5/28/2014

"""

import warnings
warnings.filterwarnings("ignore")
from sklearn.decomposition import *
import tables as pt
import pandas as pd
import numpy as np
import cPickle as pickle
import os
import sys
try:
    import solar
except:
    sys.path.insert(0, os.path.abspath(os.path.join(os.path.split(__file__)[0], '..')))
import solar
from solar.input_output import extract_patch_data
from solar.input_output import load_pickled_obj
from solar.input_output import read_netcdf_data
from solar.dates import get_closest_date
import matplotlib as mlt
mlt.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.dates as dates
from utils import plot_map
from utils import date_to_str


def main(**kwargs):
    """
    The main function.
    """
    png_prefix = kwargs['out_dir'] + '/'
    # GROUND
    I_ground, g_lon, g_lat = read_netcdf_data(kwargs['ground_insolation'])
    g_dates = I_ground.index
    g_state = load_pickled_obj(kwargs['ground_data'])
    I_g = g_state['I']
    g_p_dates = g_state['dates']
    i_start = get_closest_date(g_dates, g_p_dates[0])
    i_end = get_closest_date(g_dates, g_p_dates[-1])
    I_g_obs = I_ground[i_start:i_end]
    I_g_m = np.percentile(I_g, 50, axis=0)
    I_g_05 = np.percentile(I_g, 5, axis=0)
    I_g_95 = np.percentile(I_g, 95, axis=0)

    # SATELITE
    s_state = load_pickled_obj(kwargs['satelite_data'])
    I_s = s_state['I']
    p_dates = s_state['dates']
    m_k = s_state['mean_k']
    s_k = s_state['std_k']
    Z_s = s_state['Z']

    # Statistics of the PCA coefficients
    Z_m = np.percentile(Z_s, 50, axis=0)
    Z_05 = np.percentile(Z_s, 5, axis=0)
    Z_95 = np.percentile(Z_s, 95, axis=0)
    # Statistics of the solar irradiation at the center of the field
    I_m = np.percentile(I_s, 50, axis=0)
    I_05 = np.percentile(I_s, 5, axis=0)
    I_95 = np.percentile(I_s, 95, axis=0)

    # Observed data
    f = pt.open_file(kwargs['satelite_insolation'], mode='r')
    lat = f.root.latitude[:]
    lon = f.root.longitude[:]
    patch_shape = lat.shape
    i0 = lat.shape[0] / 2
    j0 = lat.shape[1] / 2

    # Load the reduction map
    reduction_map = load_pickled_obj(kwargs['reduction_map_file'])

    # Observed clearness index
    K_obs, dates_obs = extract_patch_data(f, 'clearness_index',
                                          start_date=p_dates[0],
                                          final_date=p_dates[-1])
    Z_obs = reduction_map.transform(K_obs)

    force = kwargs['force']
    # Create one figure for each PCA coefficient
    for i in range(Z_m.shape[1]):
        png_file = png_prefix + 'Z_' + str(i) + '.png'
        if os.path.exists(png_file) and not force:
            print 'Found:', png_file
            continue
        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.plot(p_dates.to_pydatetime(), Z_m[:, i], 'b', linewidth=2)
        ax.fill_between(p_dates.to_pydatetime(), Z_05[:, i], Z_95[:, i], color='grey', alpha=0.4)
        ax.plot(dates_obs.to_pydatetime(), Z_obs[:, i], 'r+', markersize=10, markeredgewidth=2)
        ax.legend(['Prediction for $x_{%d}$' % (i + 1),
                    'Observed $x_{%d}$' % (i + 1)],
                   loc='best')
#        ax.locator_params(axis='x', nbins=4)
        ax.xaxis.set_major_locator(dates.HourLocator(interval=2))
        ax.xaxis.set_major_formatter(dates.DateFormatter('%H:%M'))
        plt.setp(ax.get_legend().get_texts(), fontsize=30)
        plt.title(p_dates[0].to_pydatetime().strftime('%Y-%m-%d %H:%M UTC'), fontsize=30)
        plt.xlabel('Time (t)', fontsize=30)
        plt.ylabel('Reduced Variable ($x_{%d}(t)$)' % (i + 1), fontsize=30)
        plt.setp(ax.get_xticklabels(), fontsize=30)
        plt.setp(ax.get_yticklabels(), fontsize=30)
        plt.tight_layout()
        print 'Writing:', png_file
        plt.savefig(png_file)
        del fig
        plt.clf()
    # Comparison Plot
    png_file = png_prefix + 'compare.png'
    if os.path.exists(png_file) and not force:
        print 'Found:', png_file
    else:
        dt = p_dates[0].to_pydatetime() - g_p_dates[0].to_pydatetime()
        leg_txt = []
        if dt.total_seconds() < 30 * 60:
            plt.plot(g_p_dates.to_pydatetime(), I_g_m, 'g--', linewidth=2)
            plt.fill_between(g_p_dates.to_pydatetime(), I_g_05, I_g_95, color='green', alpha=0.4)
            plt.plot(I_g_obs.index.to_pydatetime(), I_g_obs, 'g+', markersize=10, markeredgewidth=2)
            leg_txt = ['Ground model', 'Ground observations']

        # Observed insolation
        I_obs, dates_obs = extract_patch_data(f, 'insolation',
                                              start_date=p_dates[0],
                                              final_date=p_dates[-1])
        I_obs = np.array([I_obs[i, :].reshape(patch_shape)[i0, j0]
                         for i in range(I_obs.shape[0])])
        f.close()
        plt.plot(p_dates.to_pydatetime(), I_m, 'r', linewidth=2)
        plt.fill_between(p_dates.to_pydatetime(), I_05, I_95, color='red', alpha=0.4)
        plt.plot(dates_obs[:-2].to_pydatetime(), I_obs[:-2], 'r.', markersize=10, markeredgewidth=2)
        leg_txt += ['Satellite model', 'Satellite observations']
        leg = plt.legend(leg_txt,
                    loc='best')
        plt.title(p_dates[0].to_pydatetime().strftime('%Y-%m-%d %H:%M UTC'))
#        ax.locator_params(axis='x', nbins=4)
        plt.setp(leg.get_texts(), fontsize=16)
        plt.xlabel('Time (t)', fontsize=16)
        plt.ylabel('Irradiance $(W/m^2)$', fontsize=16)
        plt.axes().xaxis.set_major_locator(dates.HourLocator(interval=2))
        plt.axes().xaxis.set_major_formatter(dates.DateFormatter('%H:%M'))
        plt.setp(plt.axes().get_xticklabels(), fontsize=16)
        plt.setp(plt.axes().get_yticklabels(), fontsize=16)
        plt.tight_layout()
        print 'Writing:', png_file
        plt.savefig(png_file)
        plt.clf()
    levels = np.linspace(0, 1.2, 20)
    for i in range(m_k.shape[0]):
        png_file = png_prefix + date_to_str(p_dates[i]) + '_k_mean.png'
        p_title = p_dates[i].to_pydatetime().strftime('%Y-%m-%d %H:%M UTC')
        if os.path.exists(png_file) and not force:
            print 'Found:', png_file
        else:
            plot_map(lon, lat, m_k[i, :].reshape(lat.shape), levels=levels,
                     fontsize=kwargs['colorbar_fontsize'])
            plt.title(p_title + ': Mean', fontsize=30)
            print 'Writing:', png_file
            plt.savefig(png_file)
            plt.clf()
        png_file = png_prefix + date_to_str(p_dates[i]) + '_k_std.png'
        if os.path.exists(png_file) and not force:
            print 'Found:', png_file
        else:
            plot_map(lon, lat, s_k[i, :].reshape(lat.shape), levels=0.4 * levels,
                     fontsize=kwargs['colorbar_fontsize'])
            plt.title(p_title + ': Std.',
                      fontsize=30)
            print 'Writing:', png_file
            plt.savefig(png_file)
            plt.clf()
    # Plot the observed clearness index at the dates we have
    for i in range(K_obs.shape[0]):
        png_file = png_prefix + date_to_str(dates_obs[i]) + '_k_obs.png'
        p_title = dates_obs[i].to_pydatetime().strftime('%Y-%m-%d %H:%M UTC')
        if os.path.exists(png_file) and not force:
            print 'Found:', png_file
        else:
            plot_map(lon, lat, K_obs[i, :].reshape(lat.shape), levels=levels,
                     fontsize=kwargs['colorbar_fontsize'])
            plt.title(p_title + ': Obs.', fontsize=30)
            print 'Writing:', png_file
            plt.savefig(png_file)
            plt.clf()
        # Plot difference with mean prediction
        png_file = png_prefix + date_to_str(dates_obs[i]) + '_err.png'
        if os.path.exists(png_file) and not force:
            print 'Found:', png_file
        else:
            idx = p_dates.get_loc(dates_obs[i])
            plot_map(lon, lat, np.abs((m_k[idx, :] - K_obs[i, :])).reshape(lat.shape),
                     levels=0.4 * levels,
                     fontsize=kwargs['colorbar_fontsize'])
            plt.title(p_title + ': Err. (f)', fontsize=30)
            print 'Writing:', png_file
            plt.savefig(png_file)
            plt.clf()
    # Plot the reduced version of the clearness index
    for i in range(K_obs.shape[0]):
        if isinstance(reduction_map, FactorAnalysis):
            W = reduction_map.components_.T
            mu = reduction_map.mean_
            y = np.dot(Z_obs[i, :], W.T) + mu
        else:
            y = reduction_map.inverse_transform(Z_obs[i, :][None, :])
        png_file = png_prefix + date_to_str(dates_obs[i]) + '_k_obs_r.png'
        p_title = dates_obs[i].to_pydatetime().strftime('%Y-%m-%d %H:%M UTC')
        if os.path.exists(png_file) and not force:
            print 'Found:', png_file
        else:
            plot_map(lon, lat, y.reshape(lat.shape), levels=levels,
                     fontsize=kwargs['colorbar_fontsize'])
            plt.title(p_title + ': Obs.', fontsize=30)
            print 'Writing:', png_file
            plt.savefig(png_file)
            plt.clf()
        # Plot difference with mean prediction
        png_file = png_prefix + date_to_str(dates_obs[i]) + '_err_r.png'
        if os.path.exists(png_file) and not force:
            print 'Found:', png_file
        else:
            idx = p_dates.get_loc(dates_obs[i])
            plot_map(lon, lat, np.abs((m_k[idx, :] - y)).reshape(lat.shape),
                     levels=0.4 * levels, fontsize=kwargs['colorbar_fontsize'])
            plt.title(p_title + ': Err. (r)', fontsize=30)
            print 'Writing:', png_file
            plt.savefig(png_file)
            plt.clf()



if __name__ == '__main__':
    from argparse import ArgumentParser as Parser
    from argparse import ArgumentDefaultsHelpFormatter as HelpFormatter
    parser = Parser(description='Make prediction plots.')
    parser.add_argument('--ground-insolation', dest='ground_insolation', type=str,
                        help='specify the ground insolation file')
    parser.add_argument('--ground-data', dest='ground_data', type=str,
                        help='specify the file containing the ground prediction.')
    parser.add_argument('--satelite-insolation', dest='satelite_insolation',
                        type=str,
                        help='specify the satelite insolation file')
    parser.add_argument('--satelite-data', dest='satelite_data', type=str,
                        help='specify the file containing the satelite data')
    parser.add_argument('--reduction-map-file', dest='reduction_map_file', type=str,
                        help='specify the reduction map file')
    parser.add_argument('--out-dir', dest='out_dir', type=str,
                        help='specify the output directory')
    parser.add_argument('--force', dest='force', action='store_true',
                        help='force replacement of existing plots.')
    parser.add_argument('--colorbar-fontsize', dest='colorbar_fontsize',
                        type=int, help='specify the fontsize of the colobar.',
                        default=24)
    args = parser.parse_args()
    main(**args.__dict__)
