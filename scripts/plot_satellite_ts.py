"""
Plot satellite time series.
"""


from solar import Observer
from solar import ExtraterrestrialIrradiance
from solar import ClearSkyIrradiance
from solar import fetch_month
import math

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


data = fetch_month(2013, 10)
sat_data = pd.read_pickle('./data/sat_point.pd')
data.irradiation.plot()
obs = Observer()
z, r = obs.compute_many(sat_data.index)
E_ext = ExtraterrestrialIrradiance()
E = ClearSkyIrradiance()
I0 = E_ext(r)
z = np.radians(sat_data.solar_zenith_angle)
I_g, I_bn, I_d = E(I0, z, p=88.5, aod_700=0.01, w=0.21)
I_d = I_g - I_bn
#sat_data.plot()
#plt.show()
ci = sat_data.cloud_probability
kt = 1. - ci
#sat_data['sat_model'] = kt * I_g  + (1. - kt) * I_d
#sat_data['I_g'] = I_g
#sat_data['I_bn'] = I_bn
#sat_data['I_d'] = I_d
#sat_data.sat_model.plot()
#sat_data.I_g.plot()
#sat_data.I_bn.plot()
#sat_data.I_d.plot()
sat_data.insolation.plot()
plt.show()
