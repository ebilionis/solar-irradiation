"""
A script that simply probes our google api so that it is available offline.

Author:
    Ilias Bilionis

Date:
    5/26/2014

"""

import tables as pt
import sys
import os
try:
    import solar
except:
    sys.path.insert(0, os.path.abspath(os.path.join(os.path.split(__file__)[0], '..')))
from solar import Observer

if not len(sys.argv) == 2:
    print 'Usage:'
    print '\t', sys.argv[0], ' <insolation_file>'

insolation_file=sys.argv[1]
assert os.path.exists(insolation_file)

f = pt.open_file(insolation_file, mode='a')
longitude = f.root.longitude[:]
latitude = f.root.latitude[:]
elevation = f.root.surface_elevation[:]
i0 = longitude.shape[0] / 2
j0 = longitude.shape[1] / 2
lon = longitude[i0, j0]
lat = latitude[i0, j0]

# This is what calls the google api and makes it available offline
observer = Observer(latitude=lat, longitude=lon)
