#!/usr/bin/env python
"""
Add the solar altitude to the h5 data set.
"""


import pandas as pd
import cPickle as pickle
import numpy as np
import ephem as em
import sys
import os


if __name__ == '__main__':
    if not len(sys.argv) == 2:
        sys.stderr.write('Provide the pickled data.\n')
        sys.exit(0)
    # The position of Lemont, IL
    g = em.Observer()
    g.lat = '41.6688'
    g.lon = '-87.9888'
    g.elev = 219.456
    data = pd.read_hdf(sys.argv[1], 'solar')
    alpha = np.zeros(data.shape[0])
    count = 0
    for (i, E) in data.iterrows():
        g.date = i.tz_convert('UTC')
        print g.date
        s = em.Sun(g)
        alpha[count] = float(s.alt)
        count += 1
    data['alpha'] = alpha
    out_name = os.path.splitext(sys.argv[1])[0] + '_w_alpha.h5'
    data.to_hdf(out_name, 'solar')
