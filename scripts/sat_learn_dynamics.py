"""
Test the Gaussian process idea on the available data.

Author:
    Ilias Bilionis

"""


import warnings
warnings.filterwarnings("ignore")
import tables as pt
import numpy as np
import cPickle as pickle
from sklearn.decomposition import *
import os
import sys
import pymcmc as pm
import GPy
try:
    import solar
except:
    sys.path.insert(0, os.path.abspath(os.path.join(os.path.split(__file__)[0], '..')))
from solar.input_output import extract_patch_data
from solar.input_output import dump_pickled_obj
from solar.satellite_models import MultioutputGaussianProcess
from solar.satellite_models import IndependentOutputGaussianProcess
from utils import init_mpi


def main(**kwargs):
    """
    The main function.
    """
    # Initialize MPI
    comm, rank, size = init_mpi(kwargs['use_mpi'])
    kwargs['comm'] = comm
    model = eval(kwargs['gp_model'] + '(**kwargs)')
    model.fit(start_date=kwargs['start_date'], final_date=kwargs['final_date'])
    if rank == 0:
        print str(model)
    output_filename = kwargs['output_file']
    if rank == 0:
        print 'Writing:', output_filename
    dump_pickled_obj(model, output_filename, comm)


if __name__ == '__main__':
    from argparse import ArgumentParser as Parser
    from argparse import ArgumentDefaultsHelpFormatter as HelpFormatter
    parser = Parser(description='Train a GP based on a PCA decomposition.',
                    formatter_class=HelpFormatter)
    parser.add_argument('--reduction-map', dest='reduction_map_file', type=str,
                        help='specify the pickled file containing the reduction map.')
    parser.add_argument('-i', dest='insolation_file', type=str,
                        help='specify the file containing the insolation data.')
    parser.add_argument('-o', dest='output_file', type=str,
                        help='specify the output file.')
    parser.add_argument('--prefix', dest='prefix', type=str,
                        help='specify the output prefix.')
    parser.add_argument('--lag', dest='lag', type=int, default=2,
                        help=('specify the lag to use in order to build the'
                              ' data'))
    parser.add_argument('--start-date', dest='start_date', type=str,
                        help='specify the first date we start training.')
    parser.add_argument('--final-date', dest='final_date', type=str,
                        help='specify the last date we train on.')
    parser.add_argument('--max-iter', dest='max_iters', type=int,
                        default=5000,
                        help='specify the number of optimization iterations.')
    parser.add_argument('--kernel', dest='kernel', type=str, default='rbf',
                        help='specify the covariance kernel to be used.')
    parser.add_argument('--use-mpi', dest='use_mpi', action='store_true',
                        help='enable mpi.')
    parser.add_argument('--gp', dest='gp_model', type=str,
                        default='MultioutputGaussianProcess',
                        help='specify the GP model you want to use')
    parser.add_argument('--degree', dest='degree', type=int,
                        default=1,
                        help='specify the degree of the polynomials you want to use'
                             ' for the mean function of the GP')
    args = parser.parse_args()
    main(**args.__dict__)
