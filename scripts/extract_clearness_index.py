"""
A script that extracts (it actually computes) the clearness index.

Author:
    Ilias Bilionis
"""

import tables as pt
import numpy as np
import sys
import os
from utils import str_to_datetime
from utils import create_and_write_float32_array
try:
    import solar
except:
    sys.path.insert(0, os.path.abspath(os.path.join(os.path.split(__file__)[0], '..')))
from solar import Observer


def main(input_filename, hours_after_sunrise, hours_before_sunset,
         complib, complevel):
    """
    The main function.

    :param input_filename:  The HDF5 input file.
    :type input_filename:   str
    :param hours_after_sunrise:     Samples will be kept only if they are
                                    ``hours_after_sunrise`` fractional hours after the
                                    sunrise.
    :type hours_after_sunrise:      float
    :param hours_before_sunset:     Sampels will be kept only if they are
                                    ``hourse_before_sunset`` fractional hours before
                                    the sunset.
    :type hours_before_sunset:      float
    :param complib:     Compression library
    :type complib:      str
    :param complevel: Compression level
    :type complvel:   int
    """
    # Initialize compression filter
    filter = pt.Filters(complib=complib, complevel=complevel)
    # Open the input file
    f = pt.open_file(input_filename, mode='a')
    longitude = f.root.longitude[:]
    latitude = f.root.latitude[:]
    elevation = f.root.surface_elevation[:]
    i0 = longitude.shape[0] / 2
    j0 = longitude.shape[1] / 2
    lon = longitude[i0, j0]
    lat = latitude[i0, j0]
    # Initialize an Observer object
    observer = Observer(latitude=lat, longitude=lon)
    # Get the indices of all dates and convert them to datetime objects
    dates = str_to_datetime(f.root.records.cols.date[:], timezone='UTC')
    # Compute the fractional times of sunrise and sunset
    good_days = observer.is_inside_day(dates,
                                       hours_after_sunrise=hours_after_sunrise,
                                       hours_before_sunset=hours_before_sunset)
    good_idx = np.arange(dates.shape[0])[good_days]
    for i in good_idx:
        row = f.root.records[i]
        sys.stdout.write('Extracting ' + row['date'] + '\r')
        sys.stdout.flush()
        patch_data = getattr(f.root.data, row['name'])
        insolation = patch_data.insolation[:]
        # Skip record if no insolation data is found
        if insolation.flatten().sum() <= 0.:
            sys.stdout.write('\n*** SKIPPING: BAD INSOLATION ***\n')
            continue
        # Compute clear sky insolation
        z = patch_data.solar_zenith_angle[:]
        r = row['sun_earth_distance']
        clear_sky_insolation = observer.evaluate_clear_sky_model(solar_zenith_angle=z,
                                                                 sun_earth_distance=r)
        clearness_index = insolation / clear_sky_insolation
#        clearness_index[clearness_index >= 1.] = 1.
        # Write the data:
        try:
            f.remove_node('/data/' + row['name'] + '/clear_sky_insolation')
        except:
            pass
        try:
            f.remove_node('/data/' + row['name'] + '/clearness_index')
        except:
            pass
        create_and_write_float32_array(f, patch_data,
                                       'clear_sky_insolation', clear_sky_insolation,
                                       filter)
        create_and_write_float32_array(f, patch_data, 'clearness_index',
                                       clearness_index, filter)
    sys.stdout.write('\n')
    f.close()


if __name__ == '__main__':
    from argparse import ArgumentParser as Parser
    from argparse import ArgumentDefaultsHelpFormatter as HelpFormatter
    parser = Parser(description='Extract clearness index from satellite data.',
                                formatter_class=HelpFormatter)
    parser.add_argument('-i', dest='input_filename', type=str,
                        help=('specify the HDF5 input file containing information'
                              ' about the satellite patches.'))
    parser.add_argument('--hours-after-sunrise', type=float, default=2.,
                        help=('Samples will be kept only if they are this many'
                              ' fractional hours after the sunrise.'))
    parser.add_argument('--hours-before-sunset', type=float, default=2.,
                        help=('Samples will be kept only if the ara this many'
                              ' fractional hours before the sunset.'))
    parser.add_argument('--complib', dest='complib', type=str, default='zlib',
                        help=('specify the compression library (\'zlib\', \'lzo\','
                              ' \'bzip2\', \'blosc:blosclz\', \'blosc:lz4\','
                              ' \'blosc:lz4hc\', \'blosc:snappy\', '
                              ' \'blosc:zlib\').'))
    parser.add_argument('--complevel', dest='complevel', type=int, default=9,
                        help='specify the compression level (0-9).')
    args = parser.parse_args()
    if args.input_filename is None:
        sys.stderr.write('You have to specify the input filename.\n')
        quit()
    main(args.input_filename, args.hours_after_sunrise,
         args.hours_before_sunset, args.complib,
         args.complevel)
