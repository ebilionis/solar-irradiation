"""
Test the PCA reconstruction at a particular date/time.

Author:
    Ilias Bilionis
"""


import tables as pt
import os
import numpy as np
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
import cPickle as pickle
from utils import plot_map
from utils import str_to_datetime
from utils import get_closest_date
from utils import date_to_str


def main(input_file, date, prefix, lower, upper, levels):
    """
    The main function.
    """
    with open(input_file, 'rb') as fd:
        pca = pickle.load(fd)
    f = pt.open_file(pca.input_filename, mode='r')
    lat = f.root.latitude[:]
    lon = f.root.longitude[:]
    records = f.root.records
    dates = str_to_datetime(records.cols.date[:])
    i = get_closest_date(dates, date)
    print 'You requsted the clearness index at:', date
    print 'The closest I could find was:', dates[i]
    data = getattr(f.get_node('/data/' + records.cols.name[i]), 'clearness_index')[:]
    k_reconst = pca.inverse_transform(pca.transform(data.flatten()[None, :])).reshape(lat.shape)
    f.close()
    plot_map(lon, lat, k_reconst, np.linspace(lower, upper, levels))
    plt.title('clearness index reconst.: ' + str(dates[i]), fontsize=16)
    png_file = (prefix + '_clearness_index_reconstruction_'
                + date_to_str(dates[i]) + '.png')
    print 'Writing figure:', png_file
    plt.savefig(png_file)


if __name__ == '__main__':
    from argparse import ArgumentParser as Parser
    from argparse import ArgumentDefaultsHelpFormatter as HelpFormatter
    parser = Parser(description='Test the PCA reconstruction.',
                    formatter_class=HelpFormatter)
    parser.add_argument('-i', dest='input_filename', type=str,
                        help='specify the pickled file containing the PCA.')
    parser.add_argument('--date', dest='date', type=str,
                         help='specify the date.')
    parser.add_argument('--prefix', dest='prefix', type=str,
                        help='specify the output prefix.')
    parser.add_argument('--lower', dest='lower', type=float, default=0.,
                        help='specify the lower bound for the contour levels.')
    parser.add_argument('--upper', dest='upper', type=float, default=1.5,
                        help='specify the upper bound for the contour levels.')
    parser.add_argument('--levels', dest='levels', type=int, default=20,
                        help='specify the number of the contour levels.')
    args = parser.parse_args()
    if args.input_filename is None:
        sys.stderr.write('You have to specify the input filename.\n')
        quit()
    if args.prefix is None:
        args.prefix = os.path.splitext(args.input_filename)[0]
    main(args.input_filename, args.date, args.prefix, args.lower, args.upper,
         args.levels)
