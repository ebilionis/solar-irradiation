"""
Compare the satellite estimate for Argonne to ground measurments.

Author:
    Ilias Bilionis
"""

import tables as pt
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from utils import str_to_datetime
from solar.dates import get_closest_date
import sys
try:
    import solar
except:
    sys.path.insert(0, os.path.abspath(os.path.join(os.path.split(__file__)[0], '..')))
from solar import fetch_no_qc_data


if __name__ == '__main__':
    f = pt.open_file('./data/insolation_anl_400x400.h5', mode='r')
    lat = f.root.latitude[:]
    i0 = lat.shape[0] / 2
    j0 = lat.shape[1] / 2
    all_dates = str_to_datetime(f.root.records.cols.date)
    idx = get_closest_date(all_dates, '2014-02-01 00:00')
    I_sat = np.array([f.get_node('/data/' + g).insolation[i0, j0]
                      for g in f.root.records.cols.name[idx:]])
    df_I_sat = pd.DataFrame(I_sat, index=all_dates[idx:],
                            columns=['Satellite Irradiance'])
    f.close()
    df_I_ground = solar.fetch_year(2014)
    ax = df_I_ground.plot()
    df_I_sat.plot(ax=ax)
    plt.show()
    a = raw_input('press enter...')
