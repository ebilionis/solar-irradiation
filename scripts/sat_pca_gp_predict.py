"""
Make predictions using the gp model.

Author:
    Ilias Bilionis
"""


import warnings
warnings.filterwarnings("ignore")
import tables as pt
import numpy as np
import cPickle as pickle
from sklearn.decomposition import PCA
import os
import sys
import GPy
try:
    import solar
except:
    sys.path.insert(0, os.path.abspath(os.path.join(os.path.split(__file__)[0], '..')))
import solar
from solar.satellite_models import MultioutputGaussianProcess
from solar.ground_models import GroundModel
from solar.input_output import extract_patch_data
from solar.input_output import load_pickled_obj
from solar.dates import get_closest_date
from utils import init_mpi
from utils import date_to_str
from utils import plot_map
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable

def main(input_filename, ground_filename, prefix, start_date, num_ahead, num_samples, use_mpi):
    """
    The main function.
    """
    comm, rank, size = init_mpi(use_mpi)
    model = load_pickled_obj(input_filename, comm=comm)
    ground_model = load_pickled_obj(ground_filename)
    if os.path.isdir(prefix):
        png_prefix = prefix + '/'
    else:
        png_prefix = prefix + '_'
    # Ground
    I_g, g_p_dates = ground_model.sample(start_date, num_ahead=num_ahead,
                                         num_samples=num_samples)
    i_start = get_closest_date(ground_model.dates, g_p_dates[0])
    i_end = get_closest_date(ground_model.dates, g_p_dates[-1])
    I_g_obs = ground_model.I_ground[i_start:i_end]
    I_g_m = np.percentile(I_g, 50, axis=0)
    I_g_05 = np.percentile(I_g, 5, axis=0)
    I_g_95 = np.percentile(I_g, 95, axis=0)

    # Satellite
    I_s, p_dates, m_k, s_k, Z_s = model.sample(start_date, num_ahead=num_ahead,
                                               num_samples=num_samples)
    # Statistics of the PCA coefficients
    Z_m = np.percentile(Z_s, 50, axis=0)
    Z_05 = np.percentile(Z_s, 5, axis=0)
    Z_95 = np.percentile(Z_s, 95, axis=0)
    # Statistics of the solar irradiation at the center of the field
    I_m = np.percentile(I_s, 50, axis=0)
    I_05 = np.percentile(I_s, 5, axis=0)
    I_95 = np.percentile(I_s, 95, axis=0)
    # Observed data
    f = pt.open_file(model.input_filename, mode='r')
    lat = f.root.latitude[:]
    lon = f.root.longitude[:]
    # Observed clearness index
    K_obs, dates_obs = extract_patch_data(f, 'clearness_index',
                                          start_date=p_dates[0],
                                          final_date=p_dates[-1])
    Z_obs = model.pca.transform(K_obs)
    Z_obs = model.scaler.transform(Z_obs)
    # Create one figure for each PCA coefficient
    for i in range(Z_m.shape[1]):
        fig = plt.figure()
        ax = fig.add_subplot(111)
        ax.plot(p_dates, Z_m[:, i], 'b', linewidth=2)
        ax.fill_between(p_dates, Z_05[:, i], Z_95[:, i], color='grey', alpha=0.2)
        ax.plot(dates_obs, Z_obs[:, i], 'r+', markersize=10)
        ax.legend(['Mean prediction for $x_%d$' % (i + 1),
                    'Observed value for $x_%d$' % (i + 1)],
                   loc='best')
        plt.title(start_date, fontsize=16)
        plt.tight_layout()
        png_file = png_prefix + 'Z_' + str(i) + '.png'
        print 'Writing:', png_file
        plt.savefig(png_file)
        del fig
        plt.clf()
    # Comparison Plot
    plt.plot(g_p_dates, I_g_m, 'g--', linewidth=2)
    plt.fill_between(g_p_dates, I_g_05, I_g_95, color='green', alpha=0.2)
    plt.plot(I_g_obs.index, I_g_obs, 'g+', markersize=10)

    # Observed insolation
    I_obs, dates_obs = extract_patch_data(f, 'insolation',
                                          start_date=p_dates[0],
                                          final_date=p_dates[-1])
    I_obs = np.array([I_obs[i, :].reshape(model.patch_shape)[model.i0, model.j0]
                     for i in range(I_obs.shape[0])])
    f.close()
    plt.plot(p_dates, I_m, 'r', linewidth=2)
    plt.fill_between(p_dates, I_05, I_95, color='red', alpha=0.2)
    plt.plot(dates_obs, I_obs, 'r.', markersize=10)
    plt.legend(['Ground model',
                'Ground observations', 
                'Satellite model',
                'Satellite observations'],
                loc='best')
    plt.title(start_date, fontsize=16)
    png_file = png_prefix + 'compare.png'
    print 'Writing:', png_file
    plt.savefig(png_file)
    plt.clf()
    levels = np.linspace(0, 1.2, 20)
    for i in range(m_k.shape[0]):
        plot_map(lon, lat, m_k[i, :].reshape(lat.shape), levels=levels)
        plt.title(str(p_dates[i]) + ': Clearness Index Mean Prediction', fontsize=16)
        png_file = png_prefix + date_to_str(p_dates[i]) + '_k_mean.png'
        print 'Writing:', png_file
        plt.savefig(png_file)
        plt.clf()
        plot_map(lon, lat, s_k[i, :].reshape(lat.shape), levels=0.2 * levels)
        plt.title(str(p_dates[i]) + ': Clearness Index Std. of Prediction', fontsize=16)
        png_file = png_prefix + date_to_str(p_dates[i]) + '_k_std.png'
        print 'Writing:', png_file
        plt.savefig(png_file)
        plt.clf()
    # Plot the observed clearness index at the dates we have
    for i in range(K_obs.shape[0]):
        plot_map(lon, lat, K_obs[i, :].reshape(lat.shape), levels=levels)
        plt.title(str(dates_obs[i]) + ': Observed Clearness Index', fontsize=16)
        png_file = png_prefix + date_to_str(dates_obs[i]) + '_k_obs.png'
        print 'Writing:', png_file
        plt.savefig(png_file)
        plt.clf()


if __name__ == '__main__':
    from argparse import ArgumentParser as Parser
    from argparse import ArgumentDefaultsHelpFormatter as HelpFormatter
    parser = Parser(description='Plot several things from a PCA decomposition.')
    parser.add_argument('-i', dest='input_filename', type=str,
                        help='specify the pickled file containing the satellite.')
    parser.add_argument('-g', dest='ground_filename', type=str,
                        help='specify the pickled file containing the ground model.')
    parser.add_argument('--prefix', dest='prefix', type=str,
                        help='specify the output prefix.')
    parser.add_argument('--start-date', dest='start_date', type=str,
                        help='specify the first date we start training.')
    parser.add_argument('--num-ahead', dest='num_ahead', type=int, default=50,
                        help='specify how many steps ahead you want to predict.')
    parser.add_argument('--num-samples', dest='num_samples', type=int,
                        default=100,
                        help=('specify how many MC samples you want to take to'
                              ' compute the error bars.'))
    parser.add_argument('--use-mpi', dest='use_mpi', action='store_true',
                        help='enable mpi.')
    args = parser.parse_args()
    if args.input_filename is None:
        sys.stderr.write('You have to specify the input filename.\n')
        quit()
    if args.prefix is None:
        args.prefix = os.path.splitext(args.input_filename)[0]
    main(args.input_filename, args.ground_filename, args.prefix, args.start_date, args.num_ahead,
         args.num_samples, args.use_mpi)
