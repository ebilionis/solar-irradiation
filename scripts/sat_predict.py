"""
Make predictions using the gp model.

Author:
    Ilias Bilionis
"""


import warnings
warnings.filterwarnings("ignore")
import tables as pt
import numpy as np
from sklearn.decomposition import *
import os
import sys
import traceback
import GPy
try:
    import solar
except:
    sys.path.insert(0, os.path.abspath(os.path.join(os.path.split(__file__)[0], '..')))
import solar
from solar.input_output import load_pickled_obj
from solar.input_output import dump_pickled_obj
from utils import init_mpi


def main(**kwargs):
    """
    The main function.
    """
    comm, rank, size = init_mpi(kwargs['use_mpi'])
    try:
        model = load_pickled_obj(kwargs['satelite_model_file'], comm=comm)
        model.comm = comm
        model.reduction_map_file = kwargs['reduction_map_file']
        model.insolation_file = kwargs['insolation_file']
        output_file = kwargs['output_file']
        start_date = kwargs['start_date']
        num_ahead = kwargs['num_ahead']
        num_samples = kwargs['num_samples']
        state = model.sample(start_date, num_ahead=num_ahead,
                             num_samples=num_samples)
        dump_pickled_obj(state, output_file, comm=comm)
    except Exception as e:
        print str(e)
        traceback.print_exc()
        if comm is not None:
            comm.Abort(1)


if __name__ == '__main__':
    from argparse import ArgumentParser as Parser
    from argparse import ArgumentDefaultsHelpFormatter as HelpFormatter
    parser = Parser(description='Make forecasts and plot the results.')
    parser.add_argument('-i', dest='insolation_file', type=str,
                        help='specify the file containing the insolation.')
    parser.add_argument('--reduction-map', dest='reduction_map_file', type=str,
                        help='specify the file containing the reduction map.')
    parser.add_argument('--satelite-model', dest='satelite_model_file', type=str,
                        help='specify the file containing the satellite model.')
    parser.add_argument('-o', dest='output_file', type=str,
                        help='specify the data output file.')
    parser.add_argument('--start-date', dest='start_date', type=str,
                        help='specify the first date we start training.')
    parser.add_argument('--num-ahead', dest='num_ahead', type=int, default=50,
                        help='specify how many steps ahead you want to predict.')
    parser.add_argument('--num-samples', dest='num_samples', type=int,
                        default=100,
                        help=('specify how many MC samples you want to take to'
                              ' compute the error bars.'))
    parser.add_argument('--use-mpi', dest='use_mpi', action='store_true',
                        help='enable mpi.')
    args = parser.parse_args()
    main(**args.__dict__)
