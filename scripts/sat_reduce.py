"""
Perform dimensionality reduction on the satellite data.

Author:
    Ilias Bilionis
"""

import warnings
warnings.filterwarnings("ignore")
import tables as pt
import numpy as np
from sklearn.decomposition import *
import cPickle as pickle
import os
import sys
import math
from scipy.special import logit
try:
    import solar
except:
    sys.path.insert(0, os.path.abspath(os.path.join(os.path.split(__file__)[0], '..')))
from solar.input_output import extract_patch_data


def main(**kwargs):
    #input_filename, output_filename, num_comp, start_date, final_date):
    """
    The main function.
    """
    f = pt.open_file(kwargs['input_filename'], mode='r')
#    lon = f.root.longitude[:]
#    lat = f.root.latitude[:]
#    h = f.root.elevation[:]
    X, dates = extract_patch_data(f, 'clearness_index',
                                  start_date=kwargs['start_date'],
                                  final_date=kwargs['final_date'])
# The clearness index as an image on the very first date:
#  k = X[0,:].rehspae(lon.shape)
    f.close()
    reduction_map = eval(kwargs['reduction_map'] +
                         '(n_components=kwargs[\'num_comp\'], copy=False)')
    reduction_map.fit(X)
    with open(kwargs['output_filename'], 'wb') as fd:
        pickle.dump(reduction_map, fd, protocol=pickle.HIGHEST_PROTOCOL)


if __name__ == '__main__':
    from argparse import ArgumentParser as Parser
    from argparse import ArgumentDefaultsHelpFormatter as HelpFormatter
    parser = Parser(description=('Perform dimensionality reduction on the'
                                 ' satellite data.'),
                    formatter_class=HelpFormatter)
    parser.add_argument('-i', dest='input_filename', type=str,
                        help='specify the input HDF5 file.')
    parser.add_argument('--num-comp', dest='num_comp', type=int,
                        default=10,
                        help='specify the number of components.')
    parser.add_argument('--start-date', dest='start_date', type=str,
                        help='specify the first date we start training.')
    parser.add_argument('--final-date', dest='final_date', type=str,
                        help='specify the last date we train on.')
    parser.add_argument('-o', dest='output_filename', type=str,
                        help='specify the output file (pickled object).')
    parser.add_argument('--reduction-map', dest='reduction_map', type=str,
                        default='FactorAnalysis',
                        help='specify the reduction method you wish to use '
                             ' (PCA, FactorAnalysis, etc.)')
    args = parser.parse_args()
    if args.input_filename is None:
        sys.stderr.write('You have to specify the input filename.\n')
        quit()
    if args.output_filename is None:
        args.output_filename = (os.path.splitext(args.input_filename)[0] +
                                '_' + args.reduction_map + '_' + str(args.num_comp) + '.pcl')
    main(**args.__dict__)
