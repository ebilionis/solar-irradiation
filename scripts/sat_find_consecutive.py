"""
Finds consecutive images in an HDF5 file.

Author:
    Ilias Bilionis
"""


import tables as pt
import numpy as np
import sys
from utils import str_to_datetime
from utils import get_good_dates_with_lag


def main(input_filename, lag):
    """
    The main function.
    """
    inf = pt.open_file(input_filename, mode='a')
    dates = str_to_datetime(inf.root.records.cols.date)
    inf.close()
    print 'Using a lag of:', lag
    sys.stdout.write('Computing lags...')
    lag_idx = get_good_dates_with_lag(dates, lag)
    sys.stdout.write(' Done!\n')
    print 'Number of good data points:', lag_idx.shape[0]
    print 'Here are the indices of the lags:'
    print lag_idx


if __name__ == '__main__':
    from argparse import ArgumentParser as Parser
    from argparse import ArgumentDefaultsHelpFormatter as HelpFormatter
    parser = Parser(description=('Perform dimensionality reduction on the'
                                 ' satellite data.'),
                    formatter_class=HelpFormatter)
    parser.add_argument('-i', dest='input_filename', type=str,
                        help='specify the input HDF5 file.')
    parser.add_argument('--lag', dest='lag', type=int, default=2,
                        help=('specify the lag to use in order to build the'
                              ' data'))
    args = parser.parse_args()
    if args.input_filename is None:
        sys.stderr.write('You have to specify the input filename.\n')
        quit()
    main(args.input_filename, args.lag)
