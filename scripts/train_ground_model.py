"""
Train a recursive GP on the ground measeruments at the OK site.

Author:
    Ilias Bilionis
"""


import warnings
warnings.filterwarnings("ignore")
import tables as pt
import pandas as pd
import numpy as np
import cPickle as pickle
import GPy
import sys
import os
try:
    import solar
except:
    sys.path.insert(0, os.path.abspath(os.path.join(os.path.split(__file__)[0], '..')))
from solar.ground_models import GroundModel


def main(netcdf_filename, start_date, final_date, num_obs, lag, max_iter, output_file, kernel):
    """
    The main function.
    """
    model = GroundModel(netcdf_filename, lag=lag, max_iter=max_iter, kernel=kernel)
    model.fit(num_obs, start_date=start_date, final_date=final_date)
    print str(model.model)
    print 'Writing:', output_file
    with open(output_file, 'wb') as fd:
        pickle.dump(model, fd,
                    protocol=pickle.HIGHEST_PROTOCOL)


if __name__ == '__main__':
    from argparse import ArgumentParser as Parser
    from argparse import ArgumentDefaultsHelpFormatter as HelpFormatter
    parser = Parser(description='Train a recursive GP on the ground observations.',
                    formatter_class=HelpFormatter)
    parser.add_argument('--netcdf-filename', dest='netcdf_filename', type=str, default=2013,
                        help='specify the training data.')
    parser.add_argument('-o', dest='output_file', type=str,
                        help='specify the output file.')
    parser.add_argument('--start-date', dest='start_date', type=str,
                        help='specify the first date we start training.')
    parser.add_argument('--final-date', dest='final_date', type=str,
                        help='specify the last date we train on.')
    parser.add_argument('--lag', dest='lag', type=int, default=2,
                        help='specify the lag to be used for the recursive GP.')
    parser.add_argument('--max-iter', dest='max_iter', type=int, default=1000,
                        help='specify the maximum number of iterations for'
                             ' the likelihood maximization algorithm.')
    parser.add_argument('--num-obs', dest='num_obs', type=int, default=100,
                        help='specify the number of observations to use')
    parser.add_argument('--kernel', dest='kernel', type=str, default='exponential',
                        help='specify the covariance kernel to be used.')
    args = parser.parse_args()
    main(args.netcdf_filename, args.start_date, args.final_date, args.num_obs,
         args.lag, args.max_iter, args.output_file, args.kernel)
