"""
Extract a PCA-GP model from an MCMC chain.
"""


import warnings
warnings.filterwarnings("ignore")
import tables as pt
import numpy as np
import cPickle as pickle
import os
import sys
import GPy


def main(gp_filename, mcmc_prefix, prefix, chain, step):
    """
    The main function.
    """
    # Load the gp model
    with open(gp_filename, 'rb') as fd:
        model = pickle.load(fd)
    # Get the number of components
    num_comp = len(model['gp'])
    # Loop over all mcmc db
    for i in xrange(num_comp):
        mcmc_comp_filename = mcmc_prefix + '_' + str(i) + '_db.h5'
        print 'Doing:', mcmc_comp_filename
        f = pt.open_file(mcmc_comp_filename, mode='r')
        chain_counter = f.root.mcmc.chain_counter
        chain_i = np.arange(chain_counter.nrows)[chain]
        print 'Extracting data from chain', chain_i
        chain_data = f.get_node('/mcmc/data/chain_' + str(chain_i))
        step_i = np.arange(chain_data.nrows)[step]
        print 'Extracting data from step', step_i
        params = chain_data.cols.params[:][step_i, :]
        model['gp'][i]._set_params_transformed(params)
        f.close()
    # Save the new model
    gp_output_filename = prefix + '.pcl'
    print 'Writing final model in', gp_output_filename
    with open(gp_output_filename, 'wb') as fd:
        pickle.dump(model, fd, protocol=pickle.HIGHEST_PROTOCOL)


if __name__ == '__main__':
    from argparse import ArgumentParser as Parser
    from argparse import ArgumentDefaultsHelpFormatter as HelpFormatter
    parser = Parser(description='Extract a PCA-GP model from an MCMC chain.',
                    formatter_class=HelpFormatter)
    parser.add_argument('-i', dest='gp_filename', type=str,
                        help='specify the gp model filename.')
    parser.add_argument('--mcmc-prefix', dest='mcmc_prefix', type=str,
                        help=('specify the mcmc db filename (without the final part'
                              ' that specifies the pca component).'))
    parser.add_argument('--prefix', dest='prefix', type=str,
                        help='specify the output prefix.')
    parser.add_argument('--chain', dest='chain', type=int, default=-1,
                        help='specify the mcmc chain number.')
    parser.add_argument('--step', dest='step', type=int, default=-1,
                        help='specify the mcmc step number within a chain.')
    args = parser.parse_args()
    if args.gp_filename is None:
        sys.stderr.write('You have to specify both the gp filename.')
        quit()
    if args.mcmc_prefix is None:
        args.mcmc_prefix = os.path.splitext(args.gp_filename)[0] + '_mcmc'
    if args.prefix is None:
        args.prefix = os.path.splitext(args.gp_filename)[0] + '_mcmc'
    main(args.gp_filename, args.mcmc_prefix, args.prefix, args.chain, args.step)
