"""
Plot satellite time series.
"""


from solar import Observer
from solar import ExtraterrestrialIrradiance
from solar import ClearSkyIrradiance
from solar import fetch_month
import math
import itertools
import pandas as pd
import numpy as np
import GPy
import pymcmc as pm
import matplotlib.pyplot as plt


VAR_NAMES = [
#'surface_elevation',
#'refl_3_75um_nom',
'cld_height_opaque', #IN?
'k_index_nwp',
'total_precipitable_water_nwp',
#'glint_mask',
#'cld_opd_acha', #IN?
#'cld_reff_dcomp_qf',
#'relative_azimuth_angle',
#'temp_13_3um_nom',
'surface_temperature_retrieved',
'cld_reff_acha', #ANC?
'cld_height_h2o',
#'acha_info',
#'cld_opd_dcomp',
'cld_height_acha', #IN?
'cld_height_base_acha', #IN?
'cloud_fraction', #IN?
#'cld_opd_dcomp_qf',
#'surface_type',
#'solar_azimuth_angle',
'surface_air_temperature_nwp',
'cloud_type',
#'refl_0_65um_nom_unnormalized',
'wind_direction_10m_nwp',
#'cld_beta_acha_qf',
#'acha_inversion_flag',
#'cld_press_acha',
#'land_class',
#'cld_opd_nlcomp', # IN?
#'acha_quality',
#'insolation',
#'temp_3_75um_nom',
#'latitude',
#'cloud_transmission_0_65um_nom',
#'refl_0_65um_nom',
'surface_relative_humidity_nwp',
#'cld_opd_dcomp_unc',
#'snow_class', # IN?
'total_ozone',
#'rain_rate',
#'coast_mask',
'wind_direction_cloud_top_nwp',
'cloud_probability',
'solar_zenith_angle',
#'dcomp_info',
'surface_temperature_background',
#'packed_pixel_meta_data',
#'cld_height_uncer_acha',
'surface_pressure_background',
#'cld_temp_acha_qf',
#'dcomp_quality',
#'refl_0_65um_nom_stddev_3x3',
#'nlcomp_info',
'wind_speed_cloud_top_nwp',
#'cld_temp_uncer_acha',
#'refl_0_65um_nom_dark',
#'pixel_sst_unmasked',
#'cld_height_top_acha',
#'cld_emiss_acha', #IN?
#'cld_reff_dcomp_unc',
'cloud_water_path',
#'temp_11_0um_nom_stddev_3x3',
#'insolation_diffuse',
#'cloud_fraction_uncertainty',
#'cloud_mask',
#'cld_reff_nlcomp_unc',
#'longitude',
'wind_speed_10m_nwp',
'cloud_phase',
#'bad_pixel_mask',
#'nlcomp_quality',
#'cld_opd_nlcomp_unc',
#'cld_emiss_acha_qf',
#'temp_11_0um_nom',
#'cld_reff_nlcomp', #ANC?
#'cld_temp_acha',
#'temp_6_7um_nom',
#'acha_processing_order',
#'sensor_zenith_angle',
'cld_reff_dcomp']



def get_X_Y(sat_data, var_names=VAR_NAMES):
    """
    Get x and y from satellite data.
    """
    X = np.array([np.array(sat_data[name]) for name in var_names]).T
    for i, name in itertools.izip(xrange(X.shape[1]), var_names):
        if name == 'solar_zenith_angle':
            X[:, i] = np.cos(np.radians(X[:, i]))
            X[X[:, i] <= 0., i] = 0.
    Y = np.array(sat_data.insolation)[:, None]
    return X, Y


#data = fetch_month(2013, 10)
sat_data = pd.read_hdf('./data/sat_point.h5', 'anl')
sat_data.cld_reff_dcomp.plot()
a = raw_input('Press enter')
quit()
X, Y = get_X_Y(sat_data['11-2013'])
X_m = np.mean(X, axis=0)
X_s = np.std(X, axis=0)
X_s[X_s == 0.0] = 1.
X = (X - X_m) / X_s
Y_m = np.mean(Y, axis=0)
Y_s = np.std(Y, axis=0)
Y = (Y - Y_m) / Y_s
num_train = 200
X_train = X[:num_train, :]
Y_train = Y[:num_train, :]
#k = (GPy.kern.linear(X.shape[1], ARD=True)
#     + GPy.kern.rbf(X.shape[1], ARD=True))
k = GPy.kern.exponential(X.shape[1], ARD=True)
m = GPy.models.GPRegression(X_train, Y_train, kernel=k)
for name in m._get_param_names():
    m.set_prior(name, pm.UninformativeScalePrior())
#m.optimize_restarts(num_restarts=1, messages=True)
m.optimize(messages=True)
mcmc = pm.MetropolisHastings(pm.GPyModel(m, compute_grad=True), db_filename='sat.h5')
mcmc.proposal.dt = 0.05
model_state, prop_state = mcmc.db.get_states(-1, -1)
#mcmc.sample(10, init_model_state=model_state, init_proposal_state=prop_state)
mcmc.sample(1000000, num_thin=1000, num_burn=10000)
print str(m)
params = m._get_params()
fig, ax = plt.subplots()
ax.bar(np.arange(X.shape[1]) * 10, 1. / params[1:X.shape[1] + 1], color='r')
ax.set_xticks(np.arange(X.shape[1]) * 10)
ax.set_xticklabels([name[:3] + name[-3:] for name in VAR_NAMES])
ax.set_xlabel('Variable name', fontsize=16)
ax.set_ylabel('Inverse length scale', fontsize=16)
plt.show()
a = raw_input('Press enter...')
I = 1. / params[1:X.shape[1] + 1]
idx = np.argsort(I)
for i in idx:
    print VAR_NAMES[i], '\t', I[i]
X, Y = get_X_Y(sat_data['10-2013'])
X = (X - X_m) / X_s
Y = (Y - Y_m) / Y_s
Y_p, var, _025pm, _975pm = m.predict(X)
Y_p = Y_p * Y_s + Y_m
plt.plot(Y_p)
#plt.plot(_025pm, 'r')
#plt.plot(_975pm, 'r')
plt.plot(Y * Y_s + Y_m)
plt.show()
a = raw_input('Press enter...')
