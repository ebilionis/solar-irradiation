import warnings
warnings.filterwarnings("ignore")
import tables as pt
import pandas as pd
import numpy as np
import cPickle as pickle
import GPy
import sys
import os
from sklearn.preprocessing import StandardScaler
try:
    import solar
except:
    sys.path.insert(0, os.path.abspath(os.path.join(os.path.split(__file__)[0], '..')))
from solar.input_output import extract_patch_data
from solar.dates import get_good_dates_with_lag
from solar.dates import str_to_datetime
from solar.dates import get_closest_date
import matplotlib.pyplot as plt


with open('data/insolation_ok_400x400_pca_16.pcl', 'rb') as fd:
    pca = pickle.load(fd)
lag = 2
final_date = '2014-2-25 18:30'
f = pt.open_file('data/insolation_ok_400x400.h5', mode='r')
k, dates = extract_patch_data(f, 'clearness_index', final_date=final_date)
f.close()

lag_idx = get_good_dates_with_lag(dates, lag)
Z = pca.transform(k)
##scaler = StandardScaler().fit(Z)
#s_Z = scaler.transform(Z)
s_Z = Z
X = np.array([np.hstack([s_Z[j, :] for j in lag_idx[i, :-1]])
                          for i in xrange(lag_idx.shape[0])])
Y = np.array([s_Z[lag_idx[i, -1], :] for i in xrange(lag_idx.shape[0])])
H = np.hstack([X, X**2])
B = np.linalg.lstsq(H, Y)[0]
#np.save('B', B)
#B = np.load('B.npy')

# Unused data:
num_ahead=6
starting_date = '2014-03-07 19:00'
f = pt.open_file('data/insolation_ok_400x400.h5', mode='r')
dates = str_to_datetime(f.root.records.cols.date[:])
lag_idx = get_good_dates_with_lag(dates, lag - 1)
l_num_start = get_closest_date(dates[lag_idx[:, 0]], starting_date)
g_num_start = lag_idx[l_num_start, 0]
dt_starting_date = dates[lag_idx[l_num_start, 0]]
prediction_dates = pd.date_range(dt_starting_date, freq='30min',
                                         periods=num_ahead + lag)
k, u_dates = extract_patch_data(f, 'clearness_index', 
                              start_date=dates[g_num_start],
                              final_date=dates[g_num_start+lag])
Z = pca.transform(k)
#s_Z = scaler.transform(Z)
#Y = np.array([s_Z[lag_idx[i, -1], :] for i in xrange(lag_idx.shape[0])])
x0 = np.hstack([Z[0, :], Z[1, :]])[None, :]
num_comp = x0.shape[1] / lag
Y = [x0[0, :num_comp], x0[0, num_comp:]]
for i in xrange(num_ahead):
    h0 = np.hstack([x0, x0**2])
    y0 = np.dot(h0, B)
    Y.append(y0)
    x0 = np.hstack([x0[:, num_comp:], y0])
Y = np.vstack(Y)
k_obs, dates_obs = extract_patch_data(f, 'clearness_index',
                                      start_date=prediction_dates[0],
                                      final_date=prediction_dates[-1])
Z = pca.transform(k_obs)
for i in xrange(num_comp):
    plt.plot(prediction_dates, Y[:, i], linewidth=2)
    plt.plot(dates_obs, Z[:, i], '+', linewidth=2, markersize=10)
    plt.show()
    a = raw_input('press enter...')
f.close()
