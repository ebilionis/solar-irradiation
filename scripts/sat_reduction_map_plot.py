"""
Plot several things for the PCA-reduced clearness index.

Author:
    Ilias Bilionis
"""


import matplotlib as mlt
mlt.use('Agg')
import matplotlib.pyplot as plt
import cPickle as pickle
from sklearn.decomposition import *
import sys
import os
import numpy as np
import tables as pt
import pandas as pd
from datetime import datetime
import math
import tables as pt
import itertools
from utils import plot_map
from utils import str_to_datetime
try:
    import solar
except:
    sys.path.insert(0, os.path.abspath(os.path.join(os.path.split(__file__)[0], '..')))
from solar.input_output import extract_patch_data

def main(**kwargs):
    """
    The main function.
    """
    # Load the PCA
    with open(kwargs['reduction_map_file'], 'rb') as fd:
        reduction_map = pickle.load(fd)
    prefix = kwargs['prefix']
    num_eigen = kwargs['num_eigen']
    num_trace = kwargs['num_trace']
    num_comp_trace = kwargs['num_comp_trace']
    # Plot the energy only if we can...
    if hasattr(reduction_map, 'explained_variance_ratio_'):
        energy_png = prefix + '_energy.png'
        if not os.path.exists(energy_png) or kwargs['force']:
            energy = np.cumsum(reduction_map.explained_variance_ratio_)
            plt.plot(np.arange(1, energy.shape[0] + 1), energy, linewidth=2)
            plt.plot(np.arange(1, energy.shape[0] + 1),
                     np.ones(energy.shape), '--r', linewidth=2)
            plt.plot(np.ones(50) * 16, np.linspace(0, energy[15], 50),
                     '-.g', linewidth=2)
            plt.xlabel('$R$', fontsize=16)
            plt.ylabel('$\pi_R$', fontsize=16)
            plt.ylim([0., 1.1])
            plt.savefig(energy_png)
            plt.clf()
        else:
            print 'I found:', energy_png
    # Plot the eigenvectors
    original_dim = reduction_map.components_.shape[1]
    # Open the HDF5 file to get latitude and longitude
    inf = pt.open_file(kwargs['input_file'], mode='r')
    lat = inf.root.latitude[:]
    lon = inf.root.longitude[:]
    if num_eigen is None:
        num_eigen = reduction_map.n_components
    for i in xrange(num_eigen):
        eig_png = prefix + '_eigen_' + str(i).zfill(max(2, len(str(num_eigen)))) + '.png'
        if not os.path.exists(eig_png) or kwargs['force']:
            data = reduction_map.components_[i, :].reshape(lat.shape)
            plot_map(lon, lat, data, fontsize=kwargs['eigen_colorbar_fontsize'])
            plt.savefig(eig_png)
            plt.clf()
        else:
            print 'I found:', eig_png
    # Plot time evolution of particular reduced dimensions
    evolution_png = prefix + '_evolution.png'
    if not os.path.exists(evolution_png) or kwargs['force']:
        records = inf.get_node('/records')
        index = []
        Z = []
        if num_trace is None:
            num_trace = records.nrows
        if num_comp_trace is None:
            num_comp = num_eigen
        X, index = extract_patch_data(inf, 'clearness_index',
                                      final_date='2014-03-01 16:00')
        X = X[:num_trace, :]
        index = index[:num_trace]
        Z = reduction_map.transform(X)[:, :num_comp_trace]
        Z /= np.std(Z, axis=0)
        fig = plt.figure()
        ax = fig.add_subplot(111)
        df = pd.DataFrame(index=index, data=Z,
                          columns=['$x_{%d}(t)$' % i
                                   for i in xrange(1, num_comp_trace + 1)])
        df.plot(style='-*', markersize=10, ax=ax)
        plt.setp(ax.get_xticklabels(), fontsize=kwargs['evolution_fontsize'])
        plt.setp(ax.get_yticklabels(), fontsize=kwargs['evolution_fontsize'])
        ax.set_xlabel('Time (t)', fontsize=kwargs['evolution_fontsize'])
        ax.set_ylabel('Reduced variables ($x(t)$)',
                      fontsize=kwargs['evolution_fontsize'])
        plt.setp(ax.get_legend().get_texts(), fontsize=kwargs['evolution_fontsize'])
        plt.tight_layout()
        plt.savefig(evolution_png)
        inf.close()
    else:
        print 'I found:', evolution_png


if __name__ == '__main__':
    from argparse import ArgumentParser as Parser
    from argparse import ArgumentDefaultsHelpFormatter as HelpFormatter
    parser = Parser(description='Plot several things from a reduction map.')
    parser.add_argument('-i', dest='input_file', type=str,
                        help='specify the file containing the insolation.')
    parser.add_argument('--reduction-map', dest='reduction_map_file', type=str,
                        help='specify the file containing the reduction map.')
    parser.add_argument('--prefix', dest='prefix', type=str,
                        help='specify the output prefix.')
    parser.add_argument('--num-eigen', dest='num_eigen', type=int,
                        help='specify the number of eigenfunctions to plot.')
    parser.add_argument('--num-trace', dest='num_trace', type=int,
                        help='specify the length of the trace to plot.')
    parser.add_argument('--num-comp-trace', dest='num_comp_trace', type=int,
                        help='specify the number of components whose trace to plot.')
    parser.add_argument('--eigen-colorbar-fontsize', dest='eigen_colorbar_fontsize',
                        type=int, help='specify the fontsize of the colobar.',
                        default=24)
    parser.add_argument('--evolution-fontsize', dest='evolution_fontsize',
                        type=int, help='specify the fontsize of the evolution graph',
                        default=16)
    parser.add_argument('--force', dest='force', action='store_true',
                        help='rewrite existing graphs.')
    args = parser.parse_args()
    if args.prefix is None:
        args.prefix = os.path.splitext(args.reduction_map_file)[0]
    main(**args.__dict__)
