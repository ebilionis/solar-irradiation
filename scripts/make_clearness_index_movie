#!/bin/env python
# vim: set syntax=python
"""
Make a movie of the satellite data.

Author:
    Ilias Bilionis
"""

import tables as pt
import matplotlib.pyplot as plt
import mpl_toolkits.basemap as bm
import os
import numpy as np


def plot(lon, lat, data):
    """
    Plot the data on a map.
    """
    m = bm.Basemap(projection='mill',
                   llcrnrlon=lon.min(), urcrnrlon=lon.max(),
                   llcrnrlat=lat.min(), urcrnrlat=lat.max(),
                   resolution='l')
    m.drawcoastlines()
    m.drawstates()
    m.drawcountries()
    x, y = m(lon, lat)
    m.contourf(x, y, data, levels=np.linspace(0, 1.4, 50))
    m.colorbar()


if __name__ == '__main__':
    f = pt.open_file('data/sat_400x400_patch.h5', mode='r')
    lon = f.root.data.patch_0.longitude[:]
    lat = f.root.data.patch_0.latitude[:]
    count = 0
    for row in f.get_node('/data/patch_0/records').iterrows():
        date = row['date']
        print date
        patch_data = getattr(f.root.data.patch_0, row['name'])
        insolation = getattr(patch_data, 'insolation')[:]
        cls_insolation = getattr(patch_data, 'clear_sky_insolation')[:]
        if insolation.flatten().sum() == 0.:
            print 'Skiping'
            continue
        k = insolation / cls_insolation
        plot(lon, lat, k)
        plt.title(date)
        plt.savefig(os.path.join('video',
                    'clearness_index' + '_' + row['name'] + '.png'))
        plt.clf()
