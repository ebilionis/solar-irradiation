"""
Plot the evolution of the PCA coefficients.

Author:
    Ilias Bilionis
"""

import tables as pt
import os
from utils import plot_map
import matplotlib.pyplot as plt
import numpy as np


def main(input_filename, prefix, sample, do_mean, do_std):
    """
    The main function.
    """
    f = pt.open_file(input_filename, mode='r')
    # Collect all the PCA coefficients for all samples
    x = []
    for row_s in f.root.sample_record.iterrows():
        x_ss = f.get_node('/samples/' + row_s['name']).pca[:]
        print row_s['name']
        print x_ss
    f.close()


if __name__ == '__main__':
    from argparse import ArgumentParser as Parser
    from argparse import ArgumentDefaultsHelpFormatter as HelpFormatter
    parser = Parser(description='Continue the training of the GP using MCMC.',
                    formatter_class=HelpFormatter)
    parser.add_argument('-i', dest='input_filename', type=str,
                        help='specify the pickled file containing the PCA.')
    parser.add_argument('--prefix', dest='prefix', type=str,
                        help='specify the output prefix.')
    parser.add_argument('--sample', dest='sample', type=int, default=0,
                        help='specify the sample number to plot.')
    parser.add_argument('--do-mean', dest='do_mean', action='store_true',
                        help='do the mean.')
    parser.add_argument('--do-std', dest='do_std', action='store_true',
                        help='do the standard deviation.')
    args = parser.parse_args()
    if args.input_filename is None:
        sys.stderr.write('You have to specify the input filename.\n')
        quit()
    if args.do_mean and args.do_std:
        sys.stderr.write('You may specify only of of the --do-mean or --do-std options.')
        quit()
    if args.prefix is None:
        args.prefix = os.path.splitext(args.input_filename)[0] + '_ahead'
    main(args.input_filename, args.prefix, args.sample,
         args.do_mean, args.do_std)
