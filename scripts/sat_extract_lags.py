"""
Extract lag-L groups from satellite data.

Author:
    Ilias Bilionis

"""

import tables as pt
from utils import str_to_datetime
from utils import discover_groups_of_lags


def extract_lags(records, lag):
    # Get all dates as datetime objects
    dates = str_to_datetime(records.cols.date[:])
    # Now discover groups of a particular lag
    disover_groups_of_lags(dates, lag)


def main(lag, input_filename):
    f = pt.open_file(input_filename, mode='r')
    try:
        records = f.get_node('/records')
        extract_lags(records, lag)
    except Exception as e:
        f.close()
        raise e
    f.close()


if __name__ == '__main__':
    from argparse import ArgumentParser as Parser
    from argparse import ArgumentDefaultsHelpFormatter as Formatter
    
    parser = Parser(description='Extract groups of lags from satellite data',
                    formatter_class=Formatter)
    parser.add_argument('-l', dest='lag', type=int, default=2,
                        help='Specify the lag you want to extract.')
    parser.add_argument('-i', dest='input_filename', type=str,
                        default='data/sat_400x400_clean.h5',
                        help='Specify the input file to look at.')
    args = parser.parse_args()
    main(args.lag, args.input_filename)
