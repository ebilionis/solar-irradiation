# Make a single prediction
gp_prefix=${1}
num_start=${2}
num_ahead=${3}
num_samples=${4}
gp_filename="${gp_prefix}.pcl"
png_dir="${gp_prefix}_${num_start}_${num_ahead}"
if [ ! -d ${png_dir} ]
then
    cmd="mkdir ${png_dir}"
    echo ${cmd}
    eval ${cmd}
fi
png_prefix="${png_dir}/pred"
echo $png_prefix
cmd="python scripts/sat_pca_gp_predict.py -i ${gp_filename} --num-start=${num_start} \
    --num-ahead=${num_ahead} --num-samples=${num_samples} \
    --prefix=${png_prefix}"
echo ${cmd}
eval ${cmd}
