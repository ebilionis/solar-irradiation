"""
Train the GP using MCMC.

Author:
    Ilias Bilionis

"""


import warnings
warnings.filterwarnings("ignore")
import numpy as np
import cPickle as pickle
import os
import sys
import itertools
import GPy
import pymcmc as pm
import itertools
from utils import init_mpi
from utils import load_pickled_obj
from utils import dump_pickled_obj


def main(input_filename, num_mcmc, num_thin, use_mpi):
    """
    The main function.
    """
    # Initialize MPI
    comm, rank, size = init_mpi(use_mpi)
    # Load the model
    model = load_pickled_obj(input_filename, comm)
    prefix = os.path.splitext(input_filename)[0] + '_mcmc'
    num_comp = len(model['gp'])
    my_num_comp = num_comp / size
    my_comp = []
    my_dbs = []
    for c in xrange(my_num_comp * rank, my_num_comp * (rank + 1)):
        m = model['gp'][c]
        print rank, 'is doing', c
        mcmc_model = pm.GPyModel(m, compute_grad=True)
        proposal = pm.MALAProposal(dt=0.1)
        mcmc_db_filename = prefix + '_' + str(c) + '_db.h5'
        my_dbs.append(mcmc_db_filename)
        mcmc = pm.MetropolisHastings(mcmc_model, proposal=proposal,
                                     db_filename=mcmc_db_filename)
        mcmc.sample(num_mcmc, num_thin=num_thin, verbose=(rank == 0))
        my_comp.append(m)
    if use_mpi:
        gps = comm.gather(my_comp)
        dbs = comm.gather(my_dbs)
        if rank == 0:
            model['gp'] = []
            model['mcmc_db'] = []
            for gp, db in itertools.izip(gps, dbs):
                model['gp'] += gp
                model['mcmc_db'] += db
    dump_pickled_obj(model, prefix + '.pcl', comm)


if __name__ == '__main__':
    from argparse import ArgumentParser as Parser
    from argparse import ArgumentDefaultsHelpFormatter as HelpFormatter
    parser = Parser(description='Continue the training of the GP using MCMC.',
                    formatter_class=HelpFormatter)
    parser.add_argument('-i', dest='input_filename', type=str,
                        help='specify the pickled file containing the PCA.')
    parser.add_argument('--num-mcmc', dest='num_mcmc', type=int,
                        default=100000,
                        help='specify the number of MCMC steps.')
    parser.add_argument('--num-thin', dest='num_thin', type=int, default=10,
                        help='specify the frequency of the MCMC output.')
    parser.add_argument('--use-mpi', dest='use_mpi', action='store_true',
                        help='enable mpi.')
    args = parser.parse_args()
    if args.input_filename is None:
        sys.stderr.write('You have to specify the input filename.\n')
        quit()
    main(args.input_filename, args.num_mcmc, args.num_thin, args.use_mpi)
