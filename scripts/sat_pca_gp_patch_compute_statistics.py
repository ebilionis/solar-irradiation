"""
Compute statistics from the HDF5 file.

Author:
    Ilias Bilionis
"""


import tables as pt
import os
import sys
import numpy as np
import itertools
from utils import create_and_write_float32_array


def main(input_filename, complib, complevel):
    """
    The main function.
    """
    # Initialize the compression filter for the output
    filter = pt.Filters(complib=complib, complevel=complevel)
    # Open the input file
    f = pt.open_file(input_filename, mode='a')
    # Figure out the shape of the patch
    shape = f.root.latitude().shape
    f.root.latitude.umount()
    # Initialize some arrays
    s = np.zeros(shape, dtype='float32')
    s2 = np.zeros(shape, dtype='float32')
    # Loop over each time step
    for prediction in f.root.prediction_record.iterrows():
        msg = 'Processing: ' + prediction['name'] + ' --> '
        s = np.zeros(shape, dtype='float32')
        s2 = np.zeros(shape, dtype='float32')
        for sample in f.root.sample_record.iterrows():
            sys.stdout.write(msg + sample['name'] + '\r')
            sys.stdout.flush()
            g = f.get_node('/samples/' + sample['name'] + '/' + prediction['name'])
            k = g.clearness_index[:]
            s += k
            s2 += k ** 2.
        num_samples = f.root.sample_record.nrows
        s /= num_samples
        s2 /= num_samples
        s2 -= s ** 2.
        s2 = np.sqrt(s2)
        try:
            f.remove_node('/statistics/' + prediction['name'], recursive=True)
        except:
            pass
        g = f.create_group('/statistics', prediction['name'])
        create_and_write_float32_array(f, g,
                                       'clearness_index_mean', s, filter)
        create_and_write_float32_array(f, g,
                                       'clearness_index_std', s2, filter)
    sys.stdout.write('\n')
    f.close()


if __name__ == '__main__':
    from argparse import ArgumentParser as Parser
    from argparse import ArgumentDefaultsHelpFormatter as HelpFormatter
    parser = Parser(description='Continue the training of the GP using MCMC.',
                    formatter_class=HelpFormatter)
    parser.add_argument('-i', dest='input_filename', type=str,
                        help='specify the pickled file containing the PCA.')
    parser.add_argument('--complib', dest='complib', type=str, default='zlib',
                        help=('specify the compression library (\'zlib\', \'lzo\','
                              ' \'bzip2\', \'blosc:blosclz\', \'blosc:lz4\','
                              ' \'blosc:lz4hc\', \'blosc:snappy\', '
                              ' \'blosc:zlib\').'))
    parser.add_argument('--complevel', dest='complevel', type=int, default=9,
                        help='specify the compression level (0-9).')
    args = parser.parse_args()
    if args.input_filename is None:
        sys.stderr.write('You have to specify the input filename.\n')
        quit()
    main(args.input_filename, args.complib, args.complevel)
