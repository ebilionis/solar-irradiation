"""
Train a recursive GP on the ground measeruments.

Author:
    Ilias Bilionis
"""


import warnings
warnings.filterwarnings("ignore")
import tables as pt
import pandas as pd
import numpy as np
import cPickle as pickle
import GPy
import sys
try:
    import solar
except:
    sys.path.insert(0, os.path.abspath(os.path.join(os.path.split(__file__)[0], '..')))
from solar import fetch_year
from solar import fetch_month
from solar import Observer
from solar import ExtraterrestrialIrradiance
from solar import ClearSkyIrradiance
import matplotlib.pyplot as plt
from utils import get_good_dates_with_lag


def main(year, num_obs, lag, max_iter, prefix, kernel):
    """
    The main function.
    """
    # Initialize some models
    obs = Observer(timezone=-6)
    cls_model = ClearSkyIrradiance()
    I_ext = ExtraterrestrialIrradiance()
    # Fetch the data for the training year
    I_ground = fetch_year(year).irradiation.resample('30T', how='mean')
    # Get rid of observations very early in the morning or very late
    # in the afternoon
    dates = I_ground.index
    sunrise_ftime, sunset_ftime, r = obs.compute_many_sunrise_and_sunset(dates)
    dates_ftime = dates.hour + dates.minute / 60.
    good_dates = ((dates_ftime >= sunrise_ftime + 2.) *
                  (dates_ftime <= sunset_ftime - 2.))
    I_ground = I_ground[good_dates]
    # Drop any NaN's
    I_ground = I_ground.dropna()
    # Find the indices of the dates that have a particular lag
    lag_idx = get_good_dates_with_lag(I_ground.index, lag)
    # Compute the clearness index on these points
    z, r = obs.compute_many(I_ground.index)
    I_cls = cls_model(I_ext(r), z)[0]
    k = I_ground / I_cls
    # Construct X and Y
    Z = I_ground.values
    X = np.array([np.hstack([k[j] for j in lag_idx[i, :-1]])
                  for i in xrange(lag_idx.shape[0])])
    Y = np.array([k[lag_idx[i, -1]] for i in xrange(lag_idx.shape[0])])[:, None]
    # Scale the data
    X_m = np.mean(X, axis=0)
    X_s = np.std(X, axis=0)
    X = (X - X_m) / X_s
    Y_m = np.mean(Y, axis=0)
    Y_s = np.std(Y, axis=0)
    Y = (Y - Y_m) / Y_s
    # Sub-sample the data radnomly
    sub_idx = np.random.permutation(np.arange(X.shape[0]))[:num_obs]
    X = X[sub_idx, :]
    Y = Y[sub_idx, :]
    # Initialize the model
    model = {'X_m': X_m,
             'X_s': X_s,
             'Y_m': Y_m,
             'Y_s': Y_s,
             'gp': []}
    # Train the GP by maximizing the likelihood
    k = getattr(GPy.kern, kernel)(X.shape[1], ARD=True)
    m = GPy.models.GPRegression(X, Y, kernel=k)
    m.optimize(messages=True, max_iters=max_iter)
    model['gp'].append(m)
    print 'Trained model:'
    print str(m)
    output_file = (prefix + '_' + str(year) + '_' + str(lag) + '_'
                   + str(num_obs) + '_' + kernel + '.pcl')
    print 'Writing:', output_file
    with open(output_file, 'wb') as fd:
        pickle.dump(model, fd,
                    protocol=pickle.HIGHEST_PROTOCOL)


if __name__ == '__main__':
    from argparse import ArgumentParser as Parser
    from argparse import ArgumentDefaultsHelpFormatter as HelpFormatter
    parser = Parser(description='Train a recursive GP on the ground observations.',
                    formatter_class=HelpFormatter)
    parser.add_argument('--year', dest='year', type=int, default=2013,
                        help='specify the training year.')
    parser.add_argument('--prefix', dest='prefix', type=str,
                        help='specify the output prefix.')
    parser.add_argument('--num-obs', dest='num_obs', type=int, default=200,
                        help='specify the number of observations to use.')
    parser.add_argument('--lag', dest='lag', type=int, default=2,
                        help='specify the lag to be used for the recursive GP.')
    parser.add_argument('--max-iter', dest='max_iter', type=int, default=1000,
                        help='specify the maximum number of iterations for'
                             ' the likelihood maximization algorithm.')
    parser.add_argument('--kernel', dest='kernel', type=str, default='exponential',
                        help='specify the covariance kernel to be used.')
    args = parser.parse_args()
    if args.prefix is None:
        args.prefix = './results/anl_gp'
    main(args.year, args.num_obs, args.lag, args.max_iter, args.prefix, args.kernel)
