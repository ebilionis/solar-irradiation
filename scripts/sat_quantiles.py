"""
Makes the quantile plots.

Author:
    Ilias Bilionis

Date:
    5/29/2014

"""


import warnings
warnings.filterwarnings("ignore")
import tables as pt
import numpy as np
import cPickle as pickle
from sklearn.decomposition import *
import os
import sys
import GPy
try:
    import solar
except:
    sys.path.insert(0, os.path.abspath(os.path.join(os.path.split(__file__)[0], '..')))
import solar
from solar.satellite_models import MultioutputGaussianProcess
from solar.ground_models import GroundModel
from solar.input_output import extract_patch_data
from solar.input_output import load_pickled_obj
from solar.input_output import dump_pickled_obj
from solar.dates import get_good_dates_with_lag
from solar.dates import str_to_datetime
from solar.dates import get_closest_date
from utils import init_mpi


def main(**kwargs):
    """
    The main function.
    """
    comm, rank, size = init_mpi(kwargs['use_mpi'])
    try:
        model = load_pickled_obj(kwargs['satelite_model_file'], comm=comm)
        model.comm = comm
        model.reduction_map_file = kwargs['reduction_map_file']
        model.insolation_file = kwargs['insolation_file']
        output_file = kwargs['output_file']
        start_date = kwargs['start_date']
        num_ahead = kwargs['num_ahead']
        num_samples = kwargs['num_samples']

        # Get the dates from the file
        if rank == 0:
            f = pt.open_file(kwargs['insolation_file'], mode='r')
            cli, dates = extract_patch_data(f, 'clearness_index', start_date=start_date)
            f.close()
        else:
            dates = None
        if comm is not None:
            dates = comm.bcast(dates)
        lag_idx = get_good_dates_with_lag(dates, model.lag)
        l_num_start = get_closest_date(dates[lag_idx[:, 0]], start_date)
        g_num_start = lag_idx[l_num_start, 0]

        # Loop over the good dates
        res = []
        for i in xrange(l_num_start, lag_idx.shape[0]):
            try:
                if rank == 0:
                    print 'Trying:', dates[lag_idx[i, 0]], dates[lag_idx[i, 1]], dates[lag_idx[i, 2]]
                state = model.sample(dates[lag_idx[i, 0]], num_ahead=num_ahead,
                                     num_samples=num_samples)
            except:
                continue
            if rank == 0:
                I_m = np.percentile(state['I'], 50, axis=0)[-1]
                I_std = np.std(state['I'], axis=0)[-1]
                f = pt.open_file(kwargs['insolation_file'], mode='r')
                I_obs, dates_obs = extract_patch_data(f, 'insolation',
                                                      start_date=state['dates'][-1],
                                                      final_date=state['dates'][-1])
                f.close()
                I_obs = I_obs[0, :].reshape(model.patch_shape)[model.i0, model.j0]
                res.append((I_m - I_obs) / I_std)
                dump_pickled_obj(res, output_file)
    except Exception as e:
        print str(e)
        traceback.print_exc()
        if comm is not None:
            comm.Abort(1)


if __name__ == '__main__':
    from argparse import ArgumentParser as Parser
    from argparse import ArgumentDefaultsHelpFormatter as HelpFormatter
    parser = Parser(description='Perform goodness of fit tests.')
    parser.add_argument('-i', dest='insolation_file', type=str,
                        help='specify the file containing the insolation.')
    parser.add_argument('--reduction-map', dest='reduction_map_file', type=str,
                        help='specify the file containing the reduction map.')
    parser.add_argument('--satelite-model', dest='satelite_model_file', type=str,
                        help='specify the file containing the satellite model.')
    parser.add_argument('-o', dest='output_file', type=str,
                        help='specify the output file.')
    parser.add_argument('--start-date', dest='start_date', type=str,
                        help='specify the first date we start training.')
    parser.add_argument('--num-ahead', dest='num_ahead', type=int, default=1,
                        help='specify how many steps ahead you want to predict.')
    parser.add_argument('--num-samples', dest='num_samples', type=int,
                        default=1024,
                        help=('specify how many MC samples you want to take to'
                              ' compute the error bars.'))
    parser.add_argument('--use-mpi', dest='use_mpi', action='store_true',
                        help='enable mpi.')
    args = parser.parse_args()
    main(**args.__dict__)
