"""
Train ARMA models.

Author:
    Ilias Bilionis

"""
import matplotlib.pyplot as plt
from statsmodels.tsa.ar_model import AR
#import statsmodels.api as sm
import pandas as pd
import sys
try:
    import solar
except:
    sys.path.insert(0, os.path.abspath(os.path.join(os.path.split(__file__)[0], '..')))
from solar import Observer
from solar import ExtraterrestrialIrradiance
from solar import ClearSkyIrradiance
from solar import fetch_no_qc_data
from solar.utils import get_error_bars
#from utils import get_closest_date



if __name__ == '__main__':
    obs = Observer(timezone=-6)
    # Load data to train on
    #data = pd.read_hdf('./data/data_w_alpha.h5', 'solar')['2011']
    data = fetch_no_qc_data()
    #data.index = data.index.tz_convert('UTC')
    I_obs = data.irradiance.resample('30T', how='mean')
    # Compute the fractional times of sunrise and sunset
    dates = I_obs.index
    sunrise_ftime, sunset_ftime, r = obs.compute_many_sunrise_and_sunset(dates)
    # Find the indices of the dates that are between sunrise and sunset
    dates_ftime = dates.hour + dates.minute / 60.
    good_dates = ((dates_ftime >= sunrise_ftime + 2.) *
                  (dates_ftime <= sunset_ftime - 2.))
    I_obs = I_obs[good_dates]
    #I_obs.plot()
    #plt.show()
    # Initialize the model for extraterrestrial insolation
    ext_I = ExtraterrestrialIrradiance()
    # Initialize the clear sky model
    cls_model = ClearSkyIrradiance()
    z, r = obs.compute_many(I_obs.index)
    extraterrestrial_insolation = ext_I(r)
    I_cls = cls_model(extraterrestrial_insolation, z)[0]
    #ax = I_obs.plot()
    #plt.plot(I_obs.index.values, I_cls)
    #plt.show()
    k = I_obs / I_cls
    print k
    #k.plot()
    #plt.show()
    k.index = k.index.tz_convert('UTC')
    #quit()
    ar = AR(k, missing='drop', freq='30T')
    out = ar.fit(disp=True, maxlag=1, ic=None, method='mle', full_output=True)
    df_p = out.predict(start='2014-02-01 17:00+00:00', end='2014-02-01 20:30+00:00', dynamic=True)
                     #  end='2014-03-01 15:00-06:00', dynamic=True)
    print df_p
    z, r = obs.compute_many(df_p.index.tz_convert('Etc/GMT+6'))
    I_ext = ext_I(r)
    I_cls = cls_model(I_ext, z)[0]
    df_p *= I_cls
    print df_p
    print I_cls
    ax = I_obs.plot(style='+')
    df_p.plot(ax=ax)
    plt.show()
