"""
Get a picture of the observed clearness index at a particular date/time.

Author:
    Ilias Bilionis
"""

import tables as pt
import os
import numpy as np
import matplotlib.pyplot as plt
from utils import plot_map
from utils import str_to_datetime
from utils import get_closest_date
from utils import date_to_str


def main(input_file, date, prefix, name, lower, upper, levels):
    """
    The main function.
    """
    f = pt.open_file(input_file, mode='r')
    lat = f.root.latitude[:]
    lon = f.root.longitude[:]
    records = f.root.records
    dates = str_to_datetime(records.cols.date[:])
    i = get_closest_date(dates, date)
    print 'You requsted the clearness index at:', date
    print 'The closest I could find was:', dates[i]
    data = getattr(f.get_node('/data/' + records.cols.name[i]), name)[:]
    f.close()
    plot_map(lon, lat, data, np.linspace(lower, upper, levels))
    plt.title(' '.join(name.split('_')) + ': ' + str(dates[i]), fontsize=16)
    png_file = prefix + '_' + name + '_' + date_to_str(dates[i]) + '.png'
    print 'Writing figure:', png_file
    plt.savefig(png_file)


if __name__ == '__main__':
    from argparse import ArgumentParser as Parser
    from argparse import ArgumentDefaultsHelpFormatter as HelpFormatter
    parser = Parser(description='Get a picture of the observed data.',
                    formatter_class=HelpFormatter)
    parser.add_argument('-i', dest='input_filename', type=str,
                        help='specify the pickled file containing the PCA.')
    parser.add_argument('--date', dest='date', type=str,
                         help='specify the date.')
    parser.add_argument('--prefix', dest='prefix', type=str,
                        help='specify the output prefix.')
    parser.add_argument('--name', dest='name', type=str, default='clearness_index',
                        help='specify the name of the variable you want to plot.')
    parser.add_argument('--lower', dest='lower', type=float, default=0.,
                        help='specify the lower bound for the contour levels.')
    parser.add_argument('--upper', dest='upper', type=float, default=1.5,
                        help='specify the upper bound for the contour levels.')
    parser.add_argument('--levels', dest='levels', type=int, default=20,
                        help='specify the number of the contour levels.')
    args = parser.parse_args()
    if args.input_filename is None:
        sys.stderr.write('You have to specify the input filename.\n')
        quit()
    if args.prefix is None:
        args.prefix = os.path.splitext(args.input_filename)[0]
    main(args.input_filename, args.date, args.prefix, args.name, args.lower, args.upper,
         args.levels)
