"""
Subsample an HDF5 dataset.

Author:
    Ilias Bilionis
"""


import tables as pt
import sys
import os
from utils import create_and_write_float32_array


def sub_sample_float32_array(outf, where, name, data, lr, ur, lc, uc, filter):
    """
    Subsample array.
    """
    create_and_write_float32_array(outf, where, name, data[lr:ur, lc:uc],
                                   filter)


def main(input_filename, box_rows, box_cols, outfput_filename,
         complib, complevel):
    """
    The main function.
    """
    # Initialize compression filter
    filter = pt.Filters(complib=complib, complevel=complevel)
    # Open input file
    inf = pt.open_file(input_filename, mode='r')
    # Read original box rows
    original_box_rows = inf.root.latitude.shape[0]
    assert original_box_rows >= box_rows
    original_box_cols = inf.root.latitude.shape[1]
    assert original_box_cols >= box_cols
    # Middle of box rows
    mr = original_box_rows / 2
    # Middle of box cols
    mc = original_box_cols / 2
    # Lower bound for rows
    lr = mr - box_rows / 2
    # Upper bound for rows
    ur = mr + box_rows / 2
    # Upper bound for columns
    lc = mc - box_rows / 2
    # Lower bound for columns
    uc = mc + box_rows / 2
    # Open outfput file
    outf = pt.open_file(outfput_filename, mode='w')
    # Copy longitude and latitude
    sub_sample_float32_array(outf, outf.root, 'latitude', inf.root.latitude,
                             lr, ur, lc, uc,
                             filter)
    sub_sample_float32_array(outf, outf.root, 'longitude', inf.root.longitude,
                             lr, ur, lc, uc,
                             filter)
    # Copy some things right away
    inf.copy_node('/records', outf.root)
    inf.copy_node('/data', outf.root)
    # Copy the rest of the data
    for row in inf.root.records.iterrows():
        g = inf.copy_node('/data/' + row['name'], outf.root.data)
        sub_sample_float32_array(outf, g, 'insolation',
                                 getattr(inf.root.data, row['name']).insolation,
                                 lr, ur, lc, uc, filter)
        sub_sample_float32_array(outf, g, 'clear_sky_insolation',
                                getattr(inf.root.data, row['name']).clear_sky_insolation,
                                 lr, ur, lc, uc, filter)
        sub_sample_float32_array(outf, g, 'clearness_index',
                                 getattr(inf.root.data, row['name']).clearness_index,
                                 lr, ur, lc, uc, filter)
    inf.close()
    outf.close()


if __name__ == '__main__':
    from argparse import ArgumentParser as Parser
    from argparse import ArgumentDefaultsHelpFormatter as HelpFormatter
    parser = Parser(description='Subsample the satellite data.',
                    formatter_class=HelpFormatter)
    parser.add_argument('-i', dest='input_filename', type=str,
                        help='specify the input HDF5 file.')
    parser.add_argument('-o', dest='outfput_filename', type=str,
                        help='specify the outfput file (pickled object).')
    parser.add_argument('--box-rows', dest='box_rows', type=int, default=100,
                        help='specify the number of rows of the box.')
    parser.add_argument('--box-cols', dest='box_cols', type=int, default=100,
                        help='specify the number of columns of the box.')
    parser.add_argument('--complib', dest='complib', type=str, default='zlib',
                        help=('specify the compression library (\'zlib\', \'lzo\','
                              ' \'bzip2\', \'blosc:blosclz\', \'blosc:lz4\','
                              ' \'blosc:lz4hc\', \'blosc:snappy\', '
                              ' \'blosc:zlib\').'))
    parser.add_argument('--complevel', dest='complevel', type=int, default=9,
                        help='specify the compression level (0-9).')
    args = parser.parse_args()
    if args.input_filename is None:
        sys.stderr.write('You have to specify the input filename.\n')
        quit()
    if args.outfput_filename is None:
        base, in_filename = os.path.split(args.input_filename)
        args.outfput_filename = os.path.join(base, ('sat_' + str(args.box_rows) + 'x'
                                + str(args.box_cols) + '_'
                                + '_'.join(in_filename.split('_')[-2:])))
    main(args.input_filename, args.box_rows, args.box_cols, args.outfput_filename,
         args.complib, args.complevel)
