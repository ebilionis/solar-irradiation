"""
Plot the parameters of the GP model.

Author:
    Ilias Bilionis

Date:
    4/4/2014

"""


import warnings
warnings.filterwarnings("ignore")
import sys
import os
try:
    import solar
except:
    sys.path.insert(0, os.path.abspath(os.path.join(os.path.split(__file__)[0], '..')))
import solar
from solar.input_output import load_pickled_obj
import matplotlib.pyplot as plt
import sys
import cPickle as pickle


def main(input_filename):
    """
    The main function.
    """
    model = load_pickled_obj(input_filename)
    print str(model)


if __name__ == '__main__':
    from argparse import ArgumentParser as Parser
    from argparse import ArgumentDefaultsHelpFormatter as HelpFormatter
    parser = Parser(description='Plot several things from a PCA decomposition.')
    parser.add_argument('-i', dest='input_filename', type=str,
                        help='specify the pickled file containing the satellite.')
    args = parser.parse_args()
    if args.input_filename is None:
        sys.stderr.write('You have to specify the input filename.\n')
        quit()
    main(args.input_filename)
