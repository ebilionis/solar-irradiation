"""
Makes the quantile plots.

Author:
    Ilias Bilionis

Date:
    5/30/2014

"""


import tables as pt
import numpy as np
import cPickle as pickle
import os
import sys
import matplotlib as mlt
mlt.use('Agg')
import matplotlib.pyplot as plt
import scipy.stats as stats
import statsmodels.api as sm


error_file = sys.argv[1]
prefix = os.path.splitext(error_file)[0]

with oppen(error_file, 'rb') as fd:
    errors = pickle.load(fd)

raise NotImplementedError('Not sure if I need this script.')

sm.qqplot(res, line='45')
ax = plt.axes()
ax.set_title('Quantile-Quantile Plot').set_fontsize(16)
plt.tight_layout()
plt.savefig(prefix + '_qqplot.png')

with open(prefix + '_shapiro.txt', 'w') as fd:
    fd.write(str(stats.shapiro(errors)) + '\n')
