"""
Several utilities that are relevant to more than one scripts.

Author:
    Ilias Bilionis

"""


__all__ = ['str_to_datetime', 'discover_groups_of_lags',
           'create_and_write_float32_array', 'extract_h5_data',
           'init_mpi', 'load_pickled_obj', 'dump_pickled_obj',
           'take_sample', 'take_sample_path', 'take_many_sample_paths',
           'read_netcdf_data', 'naivify_dates']


from datetime import timedelta
from datetime import datetime
import numpy as np
import pandas as pd
import tables as pt
import os
import cPickle as pickle
import math
import sys
import warnings
from netCDF4 import Dataset
import pytz
warnings.filterwarnings("ignore")
import GPy


def _str_to_datetime(sdate):
    """
    Convert the string representation of time to a datetime object.

    This version works only with one string.
    It gets rid of the miliseconds and the seconds.
    """
    sdate = ':'.join(sdate.split(':')[:-1])
    return datetime.strptime(sdate.split('.')[0], '%Y-%m-%d %H:%M')


def str_to_datetime(sdate, timezone='UTC'):
    """
    Convert many dates to datetime objects.
    """
    if not hasattr(sdate, '__getitem__'):
        return _str_to_datetime(sdate)
    else:
        d = pd.DatetimeIndex([_str_to_datetime(s) for s in sdate])
        if timezone is None:
            return d
        else:
            return d.tz_localize(timezone)


def date_to_str(date):
    """
    Return a nice string representation of a datetime object.
    """
    return (str(date.year)
            + '_' + str(date.month).zfill(2)
            + '_' + str(date.day).zfill(2)
            + '_' + str(date.hour).zfill(2)
            + str(date.minute).zfill(2))


def discover_groups_of_lags(dates, lag, interval=30):
    """
    Discover groups of lags in available data with a particular interval
    (in minutes).
    """
    # Make sure the dates are sorted
    idx = np.argsort(dates)
    raise NotImplementedError('TODO')


def create_and_write_float32_array(outf, group, name, data, filter):
    """
    Create and write a Float32 carray.
    """
    data_w = outf.create_carray(group._v_pathname,
                                name, pt.Float32Atom(), shape=data.shape,
                                filters=filter)
    data_w[:, :] = data


def plot_map(lon, lat, data, levels=None, ax=None, fontsize=16):
    """
    Plot the data on a map.
    """
    import matplotlib.pyplot as plt
    import mpl_toolkits.basemap as bm
    m = bm.Basemap(projection='mill',
                   llcrnrlon=lon.min(), urcrnrlon=lon.max(),
                   llcrnrlat=lat.min(), urcrnrlat=lat.max(),
                   resolution='l', ax=ax)
    m.drawcoastlines()
    m.drawstates()
    m.drawcountries()
    x, y = m(lon, lat)
    if levels is None:
        m.contourf(x, y, data)
    else:
        data[data >= levels[-1]] = levels[-1]
        m.contourf(x, y, data, levels)
    c = m.colorbar(format='%.2f')
    plt.setp(c.ax.get_yticklabels(), fontsize=fontsize)
    plt.tight_layout()


def extract_h5_data(fin, name, num_obs):
    X = []
    count = 0
    num_obs = fin.root.records.nrows if num_obs == -1 else num_obs
    print 'number of observations:', num_obs
    for g in fin.root.records.cols.name:
        sys.stdout.write('extracting: ' + g + '\r')
        try:
            x = getattr(fin.get_node('/data/' + g), name)[:].flatten()
        except:
            continue
        if np.isnan(x).any() or np.isinf(x).any():
            print 'found nan'
            continue
        sys.stdout.flush()
        X.append(x)
        count += 1
        if count == num_obs:
            break
    sys.stdout.write('\n')
    print 'final number of observations extracted:', count
    X = np.vstack(X)
    return X


def init_mpi(use_mpi):
    """
    Initialize mpi (if requested).
    """
    if use_mpi:
        import mpi4py.MPI as mpi
        comm = mpi.COMM_WORLD
        rank = comm.Get_rank()
        size = comm.Get_size()
        return comm, rank, size
    else:
        return None, 0, 1


def load_pickled_obj(input_filename, comm):
    """
    Load pickled object and make sure that everybody has it.
    """
    rank = 0 if comm is None else comm.Get_rank()
    if rank == 0:
        with open(input_filename, 'rb') as fd:
            obj = pickle.load(fd)
    else:
        obj = None
    if comm is not None:
        obj = comm.bcast(obj)
    return obj


def dump_pickled_obj(obj, output_filename, comm):
    """
    Dump an object to a file.
    """
    rank = 0 if comm is None else comm.Get_rank()
    if rank == 0:
        with open(output_filename, 'wb') as fd:
            pickle.dump(obj, fd, protocol=pickle.HIGHEST_PROTOCOL)


# Taking samples from the PCA-GP model
def take_sample(Z_init, model):
    """
    Take a single sample of the next step.
    """
    num_comp = model['Y_m'].shape[0]
    X_init = (Z_init - model['X_m']) / model['X_s']
    Y = np.array([gp.posterior_samples(X_init, 1)[0, 0] for gp in model['gp']])
    return Y * model['Y_s'] + model['Y_m']


def take_sample_path(Z_init, lag, num_ahead, model):
    """
    Take a sample path.
    """
    num_comp= model['X_m'].shape[0] / lag
    Y_p = []
    for i in xrange(num_ahead):
        y = take_sample(Z_init, model)[None, :]
        Y_p.append(y)
        Z_init = np.hstack([Z_init[:, num_comp:], y])
    return np.vstack(Y_p)


def take_many_sample_paths(Z_init, lag, num_ahead, num_samples, model, comm=None,
                           gather=False):
    """
    Take many sample paths.
    """
    if comm is not None:
        rank = comm.Get_rank()
        size = comm.Get_size()
    else:
        rank = 0
        size = 1
    my_num_samples = num_samples / size
    num_samples = my_num_samples * size
    if rank == 0:
        print 'Taking a total of ', num_samples, 'samples.'
    my_Y = []
    for i in xrange(my_num_samples):
        if rank == 0:
            sys.stdout.write('sample: ' +
                              str((i + 1) * size).zfill(len(str(num_samples))) + '\r')
            sys.stdout.flush()
        my_Y.append(take_sample_path(Z_init, lag, num_ahead, model))
    if rank == 0:
        sys.stdout.write('\n')
    my_Y = np.array(my_Y)
    if gather and comm is not None:
        Y = comm.gather(my_Y)
        if rank == 0:
            Y = np.vstack(Y)
    else:
        Y = my_Y
    return Y


def read_netcdf_data(filename, timezone='Etc/GMT+6'):
    """
    Read insolation data from a netcdf file.
    """
    with Dataset(filename, 'r') as fd:
        t0 = datetime.fromtimestamp(fd.variables['base_time'][:], pytz.utc)
        index = pd.DatetimeIndex([t0 + timedelta(seconds=dt)
                 for dt in fd.variables['time_offset'][:]]).tz_convert(timezone)
        I_g_max = fd.variables['down_short_hemisp_max'][:][:, None]
        lat = fd.variables['lat'][:][0]
        lon = fd.variables['lon'][:][0]
        df = pd.DataFrame(I_g_max, index=index, columns=['insolation']).resample('30T',
                                                                     how='mean')

    return df, lon, lat


def naivify_dates(dates):
    """
    Make dates UTC but timezone ignorant.
    """
    # Change time to UTC for comparison reasons
    utc_dates = dates.tz_convert('UTC')
    # Make the index naive with regards to the timezone
    utc_dates = pd.DatetimeIndex([i.replace(tzinfo=None) for i in utc_dates])
    return utc_dates
