"""
Use a recursive GP to predict into the future.

Author:
    Ilias Bilionis

"""


import warnings
warnings.filterwarnings("ignore")
import tables as pt
import pandas as pd
import numpy as np
import cPickle as pickle
import GPy
import itertools
import sys
import os
try:
    import solar
except:
    sys.path.insert(0, os.path.abspath(os.path.join(os.path.split(__file__)[0], '..')))
from solar import fetch_no_qc_data
from solar import Observer
from solar import ExtraterrestrialIrradiance
from solar import ClearSkyIrradiance
import matplotlib.pyplot as plt
from utils import get_good_dates_with_lag
from utils import get_closest_date
from utils import date_to_str
from utils import take_sample_path
from utils import create_and_write_float32_array
from utils import read_netcdf_data
from utils import is_inside_day


# Data type for prediction_records
prediction_records_dtype = {'id': pt.Int32Col(),
                            'name': pt.StringCol(itemsize=26),
                            'date': pt.StringCol(itemsize=26)}


# Data type for the sample records
sample_records_dtype = {'id': pt.Int32Col(),
                        'name': pt.StringCol(itemsize=13)}


def main(input_filename, starting_date, num_samples, num_ahead, prefix,
         ground_data_filename,
         complib, complevel):
    """
    The main function.
    """
    filter = pt.Filters(complib=complib, complevel=complevel)
    # Get the most recent observed data
    if ground_data_filename is None:
        I_ground = fetch_no_qc_data().irradiance.resample('30T', how='mean')
        lon = None
        lat = None
    else:
        I_ground, lon, lat = read_netcdf_data(ground_data_filename, timezone='Etc/GMT+6')
    # Initialize some models
    if lon is not None and lat is not None:
        obs = Observer(longitude=lon, latitude=lat, timezone=-6)
    else:
        obs = Observer(timezone=-6)
    cls_model = ClearSkyIrradiance()
    I_ext = ExtraterrestrialIrradiance()
    # Load the recursive GP model
    with open(input_filename, 'rb') as fd:
        model = pickle.load(fd)
    # Figure out the lag
    lag = model['X_m'].shape[0]
    # Get only the dates that are after sunrise and before sunset
    dates = I_ground.index
    sunrise_ftime, sunset_ftime, r = obs.compute_many_sunrise_and_sunset(dates)
    good_dates = is_inside_day(dates, sunrise_ftime, sunset_ftime,
                               return_good_dates=True)
    I_ground = I_ground[good_dates]
    # Drop any NaN's
    I_ground = I_ground.dropna()
    dates = I_ground.index
    # Change time to UTC for comparison reasons
    utc_dates = I_ground.index.tz_convert('UTC')
    # Make the index naive with regards to the timezone
    utc_dates = pd.DatetimeIndex([i.replace(tzinfo=None) for i in utc_dates])
    # Construct the lags between dates
    lag_idx = get_good_dates_with_lag(utc_dates, lag - 1)
    l_num_start = get_closest_date(utc_dates[lag_idx[:, 0]], starting_date)
    g_num_start = lag_idx[l_num_start, 0]
    print 'Requested date:', starting_date
    print 'Closest date found in dataset:', utc_dates[g_num_start]
    print 'So, as a starting point I will use the tuple:'
    print '\t', utc_dates[lag_idx[l_num_start, :]]
    print 'Local time:', I_ground.index[g_num_start]
    dt_starting_date = utc_dates[g_num_start]
    output_filename = prefix + '_' + date_to_str(dt_starting_date) + '.h5'
    # Here is the starting point
    I_start = I_ground[g_num_start:g_num_start+lag]
    # Get the clearness index there
    z, r = obs.compute_many(I_start.index.tz_convert('Etc/GMT+6'))
    I_cls = cls_model(I_ext(r), z)[0]
    X = np.array(I_start.insolation) / I_cls
    X = X[None, :]
    # The prediction dates
    prediction_dates = pd.date_range(dt_starting_date, freq='30T',
                                     periods=num_ahead + lag, tz='UTC')[lag:]
    # Compute clear sky model on those dates
    z, r = obs.compute_many(prediction_dates.tz_convert('Etc/GMT+6'))
    I_cls = cls_model(I_ext(r), z)[0]
    f = pt.open_file(output_filename, mode='w')
    # Right the predictions table
    prediction_records = f.create_table('/', 'prediction_record',
                                        prediction_records_dtype,
                                        'Prediction Record')
    for i in xrange(len(prediction_dates)):
        date = prediction_dates[i]
        row = prediction_records.row
        row['id'] = prediction_records.nrows
        row['name'] = 'prediction_' + date_to_str(date)
        row['date'] = str(date)
        row.append()
    prediction_records.flush()
    sample_records = f.create_table('/', 'sample_record',
                                    sample_records_dtype,
                                    'Sample Record')
    samples_group = f.create_group('/', 'samples')
    statistics_group = f.create_group('/', 'statistics')
    I_p_all = []
    k_p_all = []
    for i in xrange(num_samples):
        sys.stdout.write('Taking sample ' + str(i + 1).zfill(len(str(num_samples)))
                         + ' out of ' + str(num_samples) + '\r')
        sys.stdout.flush()
        row = sample_records.row
        sid = sample_records.nrows
        row['id'] = sid
        row['name'] = 'sample_' + str(sid).zfill(6)
        Y = take_sample_path(X, lag, num_ahead, model)
        I_p = Y * I_cls[:, None]
        I_p_all.append(I_p)
        k_p_all.append(Y)
        sample_g = f.create_group('/samples', row['name'])
        create_and_write_float32_array(f, sample_g,
                                       'clearness_index',
                                       Y, filter)
        create_and_write_float32_array(f, sample_g,
                                       'insolation',
                                       I_p, filter)
        row.append()
        sample_records.flush()
    sys.stdout.write('\n')
    I_p_all = np.array(I_p_all).reshape((num_samples, num_ahead))
    k_p_all = np.array(k_p_all).reshape((num_samples, num_ahead))
    # Compute the statistics
    print 'Computing the statistics...'
    m = np.percentile(I_p_all, 50, axis=0)[:, None]
    l = np.percentile(I_p_all, 5, axis=0)[:, None]
    u = np.percentile(I_p_all, 95, axis=0)[:, None]
    create_and_write_float32_array(f, statistics_group,
                                   'insolation_median', m, filter)
    create_and_write_float32_array(f, statistics_group,
                                   'insolation_percentile_05', l, filter)
    create_and_write_float32_array(f, statistics_group,
                                   'insolation_percentile_95', u, filter)
    m = np.percentile(k_p_all, 50, axis=0)[:, None]
    l = np.percentile(k_p_all, 5, axis=0)[:, None]
    u = np.percentile(k_p_all, 95, axis=0)[:, None]
    create_and_write_float32_array(f, statistics_group,
                                   'clearness_index_median', m, filter)
    create_and_write_float32_array(f, statistics_group,
                                   'clearness_index_percentile_05', l, filter)
    create_and_write_float32_array(f, statistics_group,
                                   'clearness_index_percentile_95', u, filter)
    f.close()
    print 'Writing:', output_filename


if __name__ == '__main__':
    from argparse import ArgumentParser as Parser
    from argparse import ArgumentDefaultsHelpFormatter as HelpFormatter
    parser = Parser(description='Make predictions using a recursive GP mdoel.',
                    formatter_class=HelpFormatter)
    parser.add_argument('-i', dest='input_filename',
                        help='specify the input filename.')
    parser.add_argument('--starting-date', dest='starting_date', type=str,
                        help='specify the starting date.')
    parser.add_argument('--prefix', dest='prefix', type=str,
                        help='specify the output prefix.')
    parser.add_argument('--num-samples', dest='num_samples', type=int,
                        default=100,
                        help='specify the number of samples to take.')
    parser.add_argument('--num-ahead', dest='num_ahead', type=int,
                        default=24,
                        help='specify the number of steps ahead to forecast.')
    parser.add_argument('--netcdf-filename', dest='netcdf_filename', type=str,
                        help='specify the file containing the observed ground insolation.')
    parser.add_argument('--complib', dest='complib', type=str, default='zlib',
                        help=('specify the compression library (\'zlib\', \'lzo\','
                              ' \'bzip2\', \'blosc:blosclz\', \'blosc:lz4\','
                              ' \'blosc:lz4hc\', \'blosc:snappy\', '
                              ' \'blosc:zlib\').'))
    parser.add_argument('--complevel', dest='complevel', type=int, default=9,
                        help='specify the compression level (0-9).')
    args = parser.parse_args()
    if args.input_filename is None:
        sys.stderr.write('You have to specify an input filename.\n')
        sys.exit(1)
    if args.prefix is None:
        args.prefix = os.path.splitext(args.input_filename)[0] + '_predict'
    main(args.input_filename, args.starting_date, args.num_samples,
         args.num_ahead, args.prefix,
         args.netcdf_filename, args.complib, args.complevel)
