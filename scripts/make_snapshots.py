"""
Make snapshots of insolations.

Author:
    Ilias Bilionis

Date:
    3/13/2014

"""


import glob
import numpy as np
import itertools
from utils import plot_map
import matplotlib.pyplot as plt
from datetime import datetime
import os


def read_data(filename, var_name, types='float32'): 
    """ 
    Read ``var_name`` from ``file_name``. 
    """ 
    nf = np.load(filename) 
    if not isinstance(var_name, list): 
        var_name = [var_name] 
        types = [types] 
    if len(var_name) != len(types): 
        types = types[0] * len(var_name) 
    data = [] 
    for n, t in itertools.izip(var_name, types): 
        if t != 'object': 
            data.append(np.array(nf[n] * nf[n + '_scale_factor'] + nf[n + '_add_offset'], 
                        dtype=t)) 
        else: 
            data.append(nf[n].item()) 
    if len(data) == 1: 
        return data[0] 
    else: 
        return data


DATA_DIR = 'data/snapshots'

lat = read_data(DATA_DIR + '/latitude.npz', 'latitude', 'float32')
lon = read_data(DATA_DIR + '/longitude.npz', 'longitude', 'float32')
print lat, lon
lof = glob.glob(DATA_DIR + '/goes*.npz')
lof.sort()
for l in lof:
    try:
        insolation, date = read_data(l, ['insolation', 'date'], ['float32', 'object'])
    except:
        continue
    if np.sum(insolation.flatten()) <= 1.:
        continue
    date = datetime(date.year, date.month, date.day, date.hour, date.minute)
    png_file = os.path.splitext(l)[0] + '.png'
    print 'Writing:', png_file
    plt.clf()
    plot_map(lon, lat, insolation)
    plt.title(str(date), fontsize=16)
    plt.savefig(png_file)
