"""
Give us all the dates on which we can do predictions.

Author:
    Ilias Bilionis

Date:
    5/30/2014

"""


import warnings
warnings.filterwarnings("ignore")
import tables as pt
import numpy as np
import cPickle as pickle
from sklearn.decomposition import *
import os
import sys
import GPy
try:
    import solar
except:
    sys.path.insert(0, os.path.abspath(os.path.join(os.path.split(__file__)[0], '..')))
import solar
from solar.satellite_models import MultioutputGaussianProcess
from solar.ground_models import GroundModel
from solar.input_output import extract_patch_data
from solar.input_output import load_pickled_obj
from solar.input_output import dump_pickled_obj
from solar.dates import get_good_dates_with_lag
from solar.dates import str_to_datetime
from solar.dates import get_closest_date
from solar import Observer
from utils import init_mpi


def main(**kwargs):
    """
    The main function.
    """
    insolation_file = kwargs['insolation_file']
    start_date = kwargs['start_date']
    lag = kwargs['lag']
    hours_after_sunrise = kwargs['hours_after_sunrise']
    hours_before_sunset = kwargs['hours_before_sunset']

    # Get the dates from the file
    f = pt.open_file(insolation_file, mode='r')
    longitude = f.root.longitude[:]
    latitude = f.root.latitude[:]
    elevation = f.root.surface_elevation[:]
    i0 = longitude.shape[0] / 2
    j0 = longitude.shape[1] / 2
    lon = longitude[i0, j0]
    lat = latitude[i0, j0]
    # Initialize an Observer object
    observer = Observer(latitude=lat, longitude=lon)
    # Get the indices of all dates and convert them to datetime objects
    dates = str_to_datetime(f.root.records.cols.date[:], timezone='UTC')
    # Compute the fractional times of sunrise and sunset
    good_days = observer.is_inside_day(dates,
                                       hours_after_sunrise=hours_after_sunrise,
                                       hours_before_sunset=hours_before_sunset)
    dates = dates[good_days]
    f.close()
    lag_idx = get_good_dates_with_lag(dates, lag - 1)
    l_num_start = get_closest_date(dates[lag_idx[:, 0]], start_date)
    g_num_start = lag_idx[l_num_start, 0]

    # Loop over the good dates
    for i in xrange(l_num_start, lag_idx.shape[0]):
        sys.stdout.write('%s  '
                        % dates[lag_idx[i, 0]].to_pydatetime().strftime('%Y-%m-%d_%H:%M'))


if __name__ == '__main__':
    from argparse import ArgumentParser as Parser
    from argparse import ArgumentDefaultsHelpFormatter as HelpFormatter
    parser = Parser(description='Perform goodness of fit tests.')
    parser.add_argument('-i', dest='insolation_file', type=str,
                        help='specify the file containing the insolation.')
    parser.add_argument('--lag', dest='lag', type=int,
                        help='specify the lag you want.')
    parser.add_argument('--start-date', dest='start_date', type=str,
                        help='specify the first date we start training.')
    parser.add_argument('--hours-after-sunrise', type=float, default=2.,
                        help=('Samples will be kept only if they are this many'
                              ' fractional hours after the sunrise.'))
    parser.add_argument('--hours-before-sunset', type=float, default=2.,
                        help=('Samples will be kept only if the ara this many'
                              ' fractional hours before the sunset.'))
    args = parser.parse_args()
    main(**args.__dict__)
