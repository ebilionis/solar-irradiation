"""
Performs a goodness of fit test on point predictions.

Author:
    Ilias Bilionis
"""


import warnings
warnings.filterwarnings("ignore")
import tables as pt
import numpy as np
import cPickle as pickle
from sklearn.decomposition import *
import os
import sys
import GPy
try:
    import solar
except:
    sys.path.insert(0, os.path.abspath(os.path.join(os.path.split(__file__)[0], '..')))
import solar
from solar.satellite_models import MultioutputGaussianProcess
from solar.ground_models import GroundModel
from solar.input_output import extract_patch_data
from solar.input_output import load_pickled_obj
from solar.dates import get_good_dates_with_lag
from solar.dates import str_to_datetime
from solar.dates import get_closest_date
from utils import init_mpi
from utils import date_to_str
from utils import plot_map
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
import scipy.stats as stats


def main(input_filename, prefix, start_date, num_ahead, num_samples, use_mpi):
    """
    The main function.
    """
    e_data_file = prefix + '_err.npy'
    png_file = prefix + '_qqplot.png'
    shapiro_file = prefix + '_shapiro.txt'
    comm, rank, size = init_mpi(use_mpi)
    if os.path.exists(e_data_file):
        err = np.load(e_data_file)
        print 'Found:', e_data_file
    else:
        model = load_pickled_obj(input_filename, comm=comm)
        print 'Observer:'
        print 'lat:', model.observer.latitude
        print 'lon:', model.observer.longitude
        print 'ele:', model.observer.elevation
        f = pt.open_file(model.input_filename, mode='r')
        dates = str_to_datetime(f.root.records.cols.date)
        f.close()
        lag_idx = get_good_dates_with_lag(dates, model.lag)
        l_num_start = get_closest_date(dates[lag_idx[:, 0]], start_date)
        g_num_start = lag_idx[l_num_start, 0]
        res = []
        all_err = []
        all_I = []
        for i in xrange(l_num_start, lag_idx.shape[0]):
            try:
                print 'Trying:', dates[lag_idx[i, 0]], dates[lag_idx[i, 1]], dates[lag_idx[i, 2]]
                I_s, p_dates = model.sample(dates[lag_idx[i, 0]], num_ahead=num_ahead,
                                           num_samples=num_samples)[0:2]
            except:
                continue
            I_m = np.percentile(I_s, 50, axis=0)[-1]
            I_std = np.std(I_s, axis=0)[-1]
            f = pt.open_file(model.input_filename, mode='r')
            I_obs, dates_obs = extract_patch_data(f, 'insolation',
                                                  start_date=p_dates[-1],
                                                  final_date=p_dates[-1])
            f.close()
            I_obs = I_obs[0, :].reshape(model.patch_shape)[model.i0, model.j0]
            res.append((I_m - I_obs) / I_std)
            all_I.append(I_obs)
            all_err.append(I_m - I_obs)
        all_I = np.array(all_I)
        all_err = np.array(all_err)
        RMSE = np.linalg.norm(all_err) / np.sqrt(1. * all_err.shape[0])
        R2 = 1. - RMSE ** 2. / np.var(all_I)
        np.savetxt(e_data_file.split('.')[0] + '_RMSE.txt', [RMSE])
        np.savetxt(e_data_file.split('.')[0] + '_R2.txt', [R2])
        np.save(e_data_file.split('.')[0], res)
    if os.path.exists(png_file):
        print 'Found:', png_file
    else:
        stats.probplot(res, dist='norm', plot=plt)
        plt.savefig(png_file)
    if os.path.exists(shapiro_file):
        print 'Found:', shapiro_file
    else:
        with open(shapiro_file, 'w') as fd:
            fd.write(str(stats.shapiro(res)) + '\n')


if __name__ == '__main__':
    from argparse import ArgumentParser as Parser
    from argparse import ArgumentDefaultsHelpFormatter as HelpFormatter
    parser = Parser(description='Perform goodness of fit tests.')
    parser.add_argument('-i', dest='input_filename', type=str,
                        help='specify the pickled file containing the satellite.')
    parser.add_argument('--prefix', dest='prefix', type=str,
                        help='specify the output prefix.')
    parser.add_argument('--start-date', dest='start_date', type=str,
                        help='specify the first date we start training.')
    parser.add_argument('--num-ahead', dest='num_ahead', type=int, default=50,
                        help='specify how many steps ahead you want to predict.')
    parser.add_argument('--num-samples', dest='num_samples', type=int,
                        default=100,
                        help=('specify how many MC samples you want to take to'
                              ' compute the error bars.'))
    parser.add_argument('--use-mpi', dest='use_mpi', action='store_true',
                        help='enable mpi.')
    args = parser.parse_args()
    if args.input_filename is None:
        sys.stderr.write('You have to specify the input filename.\n')
        quit()
    if args.prefix is None:
        args.prefix = os.path.splitext(args.input_filename)[0]
    main(**args.__dict__)
