"""
Compute forecasting errors.

We will only use forecasts that have already been completed.

Author:
    Ilias Bilionis

Date:
    5/30/2014

"""


import warnings
warnings.filterwarnings("ignore")
from datetime import datetime
import tables as pt
import numpy as np
import cPickle as pickle
import os
import sys
import pandas as pd
try:
    import solar
except:
    sys.path.insert(0, os.path.abspath(os.path.join(os.path.split(__file__)[0], '..')))
import solar
from solar.input_output import extract_patch_data
from solar.input_output import load_pickled_obj
from solar.input_output import dump_pickled_obj
from solar.dates import get_good_dates_with_lag
from solar.dates import str_to_datetime
from solar.dates import get_closest_date
from solar.utils import compute_crps
from solar import Observer
import matplotlib as mlt
mlt.use('Agg')
import matplotlib.pyplot as plt
import scipy.stats as stats
import statsmodels.api as sm
from statsmodels.distributions.empirical_distribution import ECDF


def main(**kwargs):
    """
    The main function.
    """
    insolation_file = kwargs['insolation_file']
    start_date = kwargs['start_date']
    lag = kwargs['lag']
    hours_after_sunrise = kwargs['hours_after_sunrise']
    hours_before_sunset = kwargs['hours_before_sunset']
    output_dir = kwargs['output_dir']
    prediction_file = kwargs['prediction_file']
    # This index corresponds to num_ahead = 1 at the moment
    # the method has not been generalized
    idx = kwargs['num_ahead']

    # Get the dates from the file
    f = pt.open_file(insolation_file, mode='r')
    longitude = f.root.longitude[:]
    latitude = f.root.latitude[:]
    elevation = f.root.surface_elevation[:]
    i0 = longitude.shape[0] / 2
    j0 = longitude.shape[1] / 2
    lon = longitude[i0, j0]
    lat = latitude[i0, j0]
    # Initialize an Observer object
    observer = Observer(latitude=lat, longitude=lon)
    # Get the indices of all dates and convert them to datetime objects
    dates = str_to_datetime(f.root.records.cols.date[:], timezone='UTC')
    # Compute the fractional times of sunrise and sunset
    good_days = observer.is_inside_day(dates,
                                       hours_after_sunrise=hours_after_sunrise,
                                       hours_before_sunset=hours_before_sunset)
    dates = dates[good_days]
    f.close()
    lag_idx = get_good_dates_with_lag(dates, lag)
    l_num_start = get_closest_date(dates[lag_idx[:, 0]], start_date)
    g_num_start = lag_idx[l_num_start, 0]

    # Loop over the good dates
    residuals = []
    all_mad = []
    all_crps = []
    for i in xrange(l_num_start, lag_idx.shape[0]):
        if not dates[lag_idx[i, 0]].to_pydatetime().strftime('%H%M') == '1600':
            continue
        nice_date = dates[lag_idx[i, 0]].to_pydatetime().strftime('%Y_%m_%d_%H%M')
        f_name = os.path.join(os.path.join(output_dir, nice_date), prediction_file)
        if os.path.exists(f_name):
            print f_name
            with open(f_name, 'rb') as fd:
                data = pickle.load(fd)
            I = np.array(data['I'])[:, idx]
            f = pt.open_file(insolation_file, mode='r')
            I_obs, d_obs = extract_patch_data(f, 'insolation',
                                       start_date=data['dates'][idx],
                                       final_date=data['dates'][idx])
            print d_obs[0], data['dates'][idx]
            print d_obs[0] == data['dates'][idx]
            if not d_obs[0] == data['dates'][idx]:
                print 'Not the same'
                return
            f.close()
            I_obs = I_obs[0, :].reshape(longitude.shape)[i0, j0]
            fig = plt.figure()
            ax = fig.add_subplot(111)
            print I
            ax.hist(I, bins=10, normed=True, alpha=0.5)
            ax.plot([I_obs], [0], 'ro', markersize=10)
            leg = plt.legend(['Observation', 'Predictive distribution'] , loc='best')
            plt.setp(leg.get_texts(), fontsize=16)
            ax.set_xlim(0., 1400.)
            ax.set_ylim(0., 0.005)
            ax.set_title(
                data['dates'][idx].to_pydatetime().strftime('%Y-%m-%d %H:%M UTC'),
                         fontsize=16)
            ax.set_xlabel('Irradiance ($W/m^2$)', fontsize=16)
            ax.set_ylabel('Probability', fontsize=16)
            plt.setp(ax.get_xticklabels(), fontsize=16)
            plt.setp(ax.get_yticklabels(), fontsize=16)
            plt.tight_layout()
            plt.savefig(os.path.join(output_dir,
                        'p_dist_' + str(idx) + '_' + nice_date + '.png'))
            del fig
            ecdf = ECDF(I)
            residuals.append(ecdf(I_obs))
            mad, crps = compute_crps(I, I_obs)
            all_mad.append(mad)
            all_crps.append(crps)

    residuals = np.vstack(residuals).flatten()
    all_mad = np.vstack(all_mad).flatten()
    all_crps = np.vstack(all_crps).flatten()
    print 'Saving files'
    prefix = 'sat_' + str(idx)
    np.savetxt(os.path.join(output_dir, prefix + '_all_crps.txt'), all_crps)
    print os.path.join(output_dir, prefix + '_all_crps.txt')
    np.savetxt(os.path.join(output_dir, prefix + '_all_mad.txt'), all_mad)
    np.savetxt(os.path.join(output_dir, prefix + '_mean_crps.txt'), [np.mean(all_crps)])
    np.savetxt(os.path.join(output_dir, prefix + '_mean_mad.txt'), [np.mean(all_mad)])
    fig = plt.figure()
#    ax = fig.add_subplot(111)
#    ax.hist(residuals)
    sm.qqplot(residuals, line='45',
              dist=stats.distributions.uniform)#, fit=False)
    ax = plt.axes()
    ax.set_title('Quantile-Quantile Plot: '
                 + data['dates'][idx].to_pydatetime().strftime('%Y-%m-%d %H:%M')
                ).set_fontsize(16)
    ax.get_xaxis().get_label().set_fontsize(16)
    ax.get_yaxis().get_label().set_fontsize(16)
    plt.setp(ax.get_xticklabels(), fontsize=16)
    plt.setp(ax.get_yticklabels(), fontsize=16)
    plt.tight_layout()
    plt.savefig(os.path.join(output_dir, prefix + '_qqplot.png'))

#    with open(os.path.join(output_dir, 'sat_shapiro.txt'), 'w') as fd:
#        fd.write(str(stats.shapiro(residuals)) + '\n')


if __name__ == '__main__':
    from argparse import ArgumentParser as Parser
    from argparse import ArgumentDefaultsHelpFormatter as HelpFormatter
    parser = Parser(description='Perform goodness of fit tests.')
    parser.add_argument('-i', dest='insolation_file', type=str,
                        help='specify the file containing the insolation.')
    parser.add_argument('--lag', dest='lag', type=int, default=2,
                        help='specify the lag you want.')
    parser.add_argument('--num-ahead', dest='num_ahead', type=int, default=1,
                        help='the number of steps ahead.')
    parser.add_argument('--start-date', dest='start_date', type=str,
                        help='specify the first date we start training.')
    parser.add_argument('--output-dir', dest='output_dir', type=str,
                        help='specify the output directory (this is where we look for completed'
                             ' forecast data')
    parser.add_argument('--prediction-file', dest='prediction_file', type=str,
                        help='specify the prediction filename', default='satelite_prediction.pcl')
    parser.add_argument('--hours-after-sunrise', type=float, default=2.,
                        help=('Samples will be kept only if they are this many'
                              ' fractional hours after the sunrise.'))
    parser.add_argument('--hours-before-sunset', type=float, default=2.,
                        help=('Samples will be kept only if the ara this many'
                              ' fractional hours before the sunset.'))
    args = parser.parse_args()
    main(**args.__dict__)
