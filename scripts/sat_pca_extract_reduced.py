"""
Extraxt the reduced variables from the satellite data and store them
in another file.

Author:
    Ilias Bilionis
"""


import cPickle as pickle
from sklearn.decomposition import PCA
import sys
import os
import numpy as np
import tables as pt



if not len(sys.argv) == 3:
    print 'Usage:'
    print 'python', sys.argv[0], ' <patch_data_file> <pca_file>'
    quit()


tmp = os.path.splitext(os.path.basename(sys.argv[2]))[0].split('_')
num_obs = int(tmp[2])
num_comp = int(tmp[3])
with open(sys.argv[2], 'rb') as fd:
    pca = pickle.load(fd)

f = pt.open_file(sys.argv[1], mode='r')
records = f.get_node('/records')
prev_z = None
prev_date = None
X = []
Y = []
dates = []
for row in records.iterrows():
    data_group = f.get_node('/data/' + row['name'])
    k = data_group.clearness_index[:].flatten()
    z = pca.transform(np.atleast_2d(k))
    if row['consecutive']:
        X.append(prev_z)
        Y.append(z)
        dates.append(row['date'])
        print prev_date, '--->', row['date']
    prev_z = z
    prev_date = row['date']
f.close()
X = np.vstack(X)
Y = np.vstack(Y)
num_consecutive = X.shape[0]
num_comp = X.shape[1]
f_w = pt.open_file('data/gp_' + str(num_consecutive) + '_' + str(num_comp) + '.h5',
                   mode='w')
filter = pt.Filters(complevel=9)
X_c = f_w.create_carray('/', 'X', pt.Float32Atom(), shape=X.shape, filters=filter)
X_c[:, :] = X
Y_c = f_w.create_carray('/', 'Y', pt.Float32Atom(), shape=Y.shape, filters=filter)
Y_c[:, :] = Y
f_w.close()
