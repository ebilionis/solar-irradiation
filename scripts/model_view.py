"""
View a pca GP model.

Author:
    Ilias Bilionis
"""
import tables as pt
import numpy as np
from sklearn.decomposition import PCA
import cPickle as pickle
import sys
import os
import pandas as pd
import matplotlib.pyplot as plt
try:
    import solar
except:
    sys.path.insert(0, os.path.abspath(os.path.join(os.path.split(__file__)[0], '..')))
import solar


def main(input_filename):
    """
    The main function.
    """
    # Read the model.
    with open(input_filename, 'rb') as fd:
        model = pickle.load(fd)
    R = model.num_comp
    columns = '|c|' + 'c' * R + '|'
    columns_1 = '|c|' + ('c' * (2 * R + 1)) + '|'
    column_names_1 = (['\(r\)'] + 
                      ['\(v_{r,%d}\)' % i for i in range(2 * R + 1)])
    columns_2 = '|c|' + ('c' * (2 * R + 2)) + '|'
    column_names_2 = (['\(r\)', '\(s_r^2\)'] + 
                      ['\(\ell_{r,%d}\)' % i for i in range(1, 2 * R + 1)] + 
                      ['\(\sigma_r^2\)'])
    table = (r'\begin{longtable}{' + columns + r"""}
\hline
""" + 'Parameter&' + '&'.join([str(i+1) for i in xrange(R)]) + r"""\\
\hline
""")
    table_1 = (
r"""\begin{tabular}{""" + columns_1 + r"""}
\hline
""" + '&'.join(column_names_1) + r"""\\
\hline
"""
)
    table_2 = (
r"""
\begin{tabular}{""" + columns_2 + r"""}
\hline
""" + '&'.join(column_names_2) + r"""\\
\hline
""")
    for i in xrange(R):
        m = model.model[i]
        p = m._get_params()
        print '%e' % m._get_params_transformed()[-1]
        line_prefix = str(i + 1) + '&'
        line_1 = (line_prefix + 
                  '&'.join(['%.2f' % p[j] for j in xrange(2 * R + 1)])
                  + '\\\\\n')
        table_1 += line_1
        line_2 = (line_prefix +
                  '&'.join(['%.2f' % p[j] for j in xrange(2 * R + 1, 4 * R + 3)])
                  + '\\\\\n')
        table_2 += line_2
    table_1 += (r"""\hline
\end{tabular}
""")
    table_2 += (r"""\hline
\end{tabular}
""")
    # Write the results to files
    with open('table_1.tex', 'w') as fd:
        fd.write(table_1)
    with open('table_2.tex', 'w') as fd:
        fd.write(table_2)
    row_names = column_names_1[1:] + column_names_2[1:]
    for i in xrange(len(row_names)):
        p = [m._get_params()[i] for m in model.model]
        line = row_names[i] + '&' + '&'.join(['%.2f' % p[j] if p[j] < 10. else '*' for j in xrange(R)]) + '\\\\\n'
        table += line
    table += (r"""\hline
\end{longtable}
""")
    with open('table.tex', 'w') as fd:
        fd.write(table)


if __name__ == '__main__':
    from argparse import ArgumentParser as Parser
    from argparse import ArgumentDefaultsHelpFormatter as HelpFormatter
    parser = Parser(description='Plot several things from a PCA decomposition.')
    parser.add_argument('-i', dest='input_filename', type=str,
                        help='specify the pickled file containing the PCA.')
    args = parser.parse_args()
    if args.input_filename is None:
        sys.stderr.write('You have to specify the input filename.\n')
        quit()
    main(args.input_filename)
