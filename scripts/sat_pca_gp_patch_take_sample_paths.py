"""
Predict the clearness index starting at a particular date.

Author:
    Ilias Bilionis

"""


import warnings
warnings.filterwarnings("ignore")
import tables as pt
import numpy as np
import cPickle as pickle
import os
import sys
import itertools
import GPy
import pymcmc as pm
import itertools
import pandas as pd
from utils import init_mpi
from utils import load_pickled_obj
from utils import dump_pickled_obj
from utils import str_to_datetime
from utils import extract_h5_data
from utils import get_good_dates_with_lag
from utils import get_closest_date
from utils import take_sample_path
from utils import create_and_write_float32_array
from utils import date_to_str


# Data type for prediction_records
prediction_records_dtype = {'id': pt.Int32Col(),
                            'name': pt.StringCol(itemsize=26),
                            'date': pt.StringCol(itemsize=26)}


# Data type for the sample records
sample_records_dtype = {'id': pt.Int32Col(),
                        'name': pt.StringCol(itemsize=13)}


def add_sample(Y, k, shape, outf, filter):
    num_ahead = Y.shape[0]
    prediction_record = outf.get_node('/prediction_record')
    sample_records = outf.get_node('/sample_record')
    sid = sample_records.nrows
    sample_records.row['id'] = sid
    sname = 'sample_' + str(sid).zfill(6)
    sample_records.row['name'] = sname
    sample_records.row.append()
    sample_records.flush()
    sample_g = outf.create_group('/samples', sname)
    create_and_write_float32_array(outf, sample_g, 'pca',
                                   Y, filter)
    for i, row in itertools.izip(xrange(num_ahead),
                                 prediction_record.iterrows()):
        predict_g = outf.create_group(sample_g, row['name'])
        create_and_write_float32_array(outf, predict_g,
                                       'clearness_index',
                                        k[i, :].reshape(shape),
                                        filter)


def initialize_output_file(inf, output_filename):
    """
    Initialize the output file.
    """
    print 'Initializing output file:', output_filename
    outf = pt.open_file(output_filename, mode='a')
    # Create links of latitude and longitude
    try:
        lon = outf.get_node('/longitude')
    except:
        lon = outf.create_external_link('/', 'longitude',
                                        os.path.abspath(inf.filename)
                                        + ':/longitude')
        print 'Created link \'/longitude\' to \'%s\'.' % lon.target
    try:
        lat = outf.get_node('/latitude')
    except:
        lat = outf.create_external_link('/', 'latitude',
                                        os.path.abspath(inf.filename)
                                        + ':/latitude')
        print 'Created link \'/latitude\' to \'%s\'.' % lat.target
    try:
        prediction_records = outf.get_node('/prediction_record')
    except:
        prediction_records = outf.create_table('/', 'prediction_record',
                                               prediction_records_dtype,
                                               'Prediction Record')
    try:
        sample_records = outf.get_node('/sample_record')
    except:
        sample_records = outf.create_table('/', 'sample_record',
                                           sample_records_dtype,
                                           'Sample Record')
    try:
        samples = outf.get_node('/samples')
    except:
        samples = outf.create_group('/', 'samples')
    try:
        pca = outf.get_node('/statistics')
    except:
        statistics = outf.create_group('/', 'statistics')
    return outf


def main(input_filename, use_mpi, prefix, starting_date, num_samples,
         num_ahead, complib, complevel):
    """
    The main function.
    """
    # Initialize MPI
    comm, rank, size = init_mpi(use_mpi)
    # Load the model
    model = load_pickled_obj(input_filename, comm)
    # Load the PCA model
    pca = load_pickled_obj(model['pca'], comm)
    # Read the observed days we have so far
    if rank == 0:
        inf = pt.open_file(pca.input_filename, mode='r')
        lat = inf.root.latitude[:]
        lon = inf.root.longitude[:]
        dates = str_to_datetime(inf.root.records.cols.date[:], timezone=None)
    else:
        dates = None
        lat = None
        lon = None
    if use_mpi:
        dates = comm.bcast(dates)
        lat = comm.bcast(lat)
        lon = comm.bcast(lon)
    # Make sure each CPU has a different seed
    np.random.seed(seed=((rank + 1) * 314159))
    # Initialize the compression filter for the output
    if rank == 0:
        filter = pt.Filters(complib=complib, complevel=complevel)
    # Figure out the lag
    lag = model['X_m'].shape[0] / pca.n_components
    # Construct the lags between dates
    lag_idx = get_good_dates_with_lag(dates, lag - 1)
    l_num_start = get_closest_date(dates[lag_idx[:, 0]], starting_date)
    g_num_start = lag_idx[l_num_start, 0]
    if rank == 0:
        print 'Requested date:', starting_date
        print 'Closest date found in dataset:', dates[g_num_start]
        print 'So, as a starting point I will use the pair:'
        print '\t', dates[lag_idx[l_num_start, 0]], ',', dates[lag_idx[l_num_start, 1]]
    # Figure out what the output filename will be
    dt_starting_date = dates[lag_idx[l_num_start, 0]]
    output_filename = (prefix + '_' + date_to_str(dt_starting_date)
                       + '.h5')
    if rank == 0:
        outf = initialize_output_file(inf, output_filename)
    # Read data at that point
    if rank == 0:
        # Read the data at that date
        data = []
        data = np.vstack([getattr(inf.get_node('/data/' + g),
                          'clearness_index')[:].flatten()
            for g in inf.root.records.cols.name[g_num_start:g_num_start+lag]])
        Z = pca.transform(data)
    else:
        Z = None
    if use_mpi:
        Z = comm.bcast(Z)
    # Figure out the prediction dates
    prediction_dates = pd.date_range(dt_starting_date, freq='30min',
            periods=num_ahead + lag)[lag:]
    # Write the prediction_record
    if rank == 0:
        prediction_record = outf.get_node('/prediction_record')
        if not prediction_record.nrows == num_ahead:
            for i in xrange(num_ahead):
                row = prediction_record.row
                row['id'] = i
                row['name'] = ('prediction_'
                               + date_to_str(prediction_dates[i]))
                row['date'] = str(prediction_dates[i])
                row.append()
            prediction_record.flush()
    # Construct X
    X = np.hstack([Z[j, :] for j in xrange(lag)])[None, :]
    # Make the predictions
    my_num_samples = num_samples / size
    num_samples = my_num_samples * size
    if rank == 0:
        print 'Taking a total of ', num_samples, 'samples.'
    for i in xrange(my_num_samples):
        if rank == 0:
            sys.stdout.write('sample: ' +
                    str((i + 1) * size).zfill(len(str(num_samples))) + '\r')
            sys.stdout.flush()
        # Get the PCA coefficients
        Y = take_sample_path(X, lag, num_ahead, model)
        # Get the Clearness index
        k = pca.inverse_transform(Y)
        # Get the insolation

        # The root should write everything to a file at this point
        if not rank == 0:
            comm.send(obj=Y, dest=0, tag=rank * 2)
            comm.send(obj=k, dest=0, tag=rank * 2 + 1)
        else:
            add_sample(Y, k, lat.shape, outf, filter)
            if use_mpi:
                for i in xrange(1, size):
                    Y = comm.recv(source=i, tag=i * 2)
                    k = comm.recv(source=i, tag=i * 2 + 1)
                    add_sample(Y, k, lat.shape, outf, filter)
    # Finalize the computation of each sample
    if rank == 0:
        inf.close()
        outf.close()


if __name__ == '__main__':
    from argparse import ArgumentParser as Parser
    from argparse import ArgumentDefaultsHelpFormatter as HelpFormatter
    parser = Parser(description='Continue the training of the GP using MCMC.',
                    formatter_class=HelpFormatter)
    parser.add_argument('-i', dest='input_filename', type=str,
                        help='specify the pickled file containing the PCA.')
    parser.add_argument('--use-mpi', dest='use_mpi', action='store_true',
                        help='enable mpi.')
    parser.add_argument('--prefix', dest='prefix', type=str,
                        help='specify the output prefix.')
#    parser.add_argument('--num-start', dest='num_start', type=int,
#                        help='specify the starting point.')
    parser.add_argument('--starting-date', dest='starting_date', type=str,
                         help='specify the starting date.')
    parser.add_argument('--num-ahead', dest='num_ahead', type=int, default=50,
                        help='specify how many steps ahead you want to predict.')
    parser.add_argument('--num-samples', dest='num_samples', type=int,
                        default=200,
                        help=('specify how many MC samples you want to take to'
                              ' compute the error bars.'))
    parser.add_argument('--complib', dest='complib', type=str, default='zlib',
                        help=('specify the compression library (\'zlib\', \'lzo\','
                              ' \'bzip2\', \'blosc:blosclz\', \'blosc:lz4\','
                              ' \'blosc:lz4hc\', \'blosc:snappy\', '
                              ' \'blosc:zlib\').'))
    parser.add_argument('--complevel', dest='complevel', type=int, default=9,
                        help='specify the compression level (0-9).')
    args = parser.parse_args()
    if args.input_filename is None:
        sys.stderr.write('You have to specify the input filename.\n')
        quit()
    if args.prefix is None:
        args.prefix = os.path.splitext(args.input_filename)[0] + '_predict'
    main(args.input_filename, args.use_mpi, args.prefix, args.starting_date,
         args.num_samples, args.num_ahead, args.complib, args.complevel)
