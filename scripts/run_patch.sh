# Run everything for a single patch
# Read input
prefix="data/sat_${1}x${1}_patch_clean"
input_filename="${prefix}.h5"
num_comp=${2}
# Construct PCA
pca_prefix="${prefix}_pca_${num_comp}"
pca_filename="${pca_prefix}.pcl"
cmd="python scripts/sat_pca_reduce.py -i ${input_filename} --num-comp=${num_comp}\
 -o ${pca_filename}"
echo ${cmd}
if [ ! -f ${pca_filename} ]
then
    eval ${cmd}
fi
# Train the GP
gp_prefix="${pca_prefix}"
cmd="python scripts/sat_pca_gp_train.py -i ${pca_filename} --prefix=${gp_prefix}"
echo ${cmd}
gp_filename="${gp_prefix}_gp.pcl"
if [ ! -f ${gp_filename} ]
then
    eval ${cmd}
fi
# Make a typical prediction
num_ahead=65
num_samples=500
for num_start in 485 575 570 585
do
    cmd="sh scripts/predict_single.sh ${gp_prefix}_gp ${num_start} ${num_ahead} ${num_samples}"
    echo ${cmd}
    pwd
    eval ${cmd}
done
