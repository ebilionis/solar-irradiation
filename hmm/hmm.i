/* File: hmm.i */
%module core

%{
#define SWIG_FILE_WITH_INIT
#include "hmm.hpp"
%}

%include "numpy.i"
%init %{
import_array();
%}

%apply (int DIM1, double* IN_ARRAY1)
       {(int N_x, double* x),
        (int K_pi, double* pi),
        (int N_c, double* in_c),
        (int M, double* loglike_per_iter)};
%apply (int DIM1, double* INPLACE_ARRAY1)
       {(int N_c, double* c),
        (int K_pi, double* in_out_pi),
        (int K_f, double* f),
        (int K_alpha, double* alpha_init),
        (int K_alpha_old, double* alpha_old),
        (int K_alpha_new, double* alpha_new)};
%apply (int DIM1, int DIM2, double* IN_ARRAY2)
       {(int K_A_1, int K_A_2, double* A),
        (int K_B, int P_B, double* B),
        (int N_F, int K_F, double* F),
        (int N_alpha, int K_alpha, double* alpha),
        (int N_beta, int K_beta, double* beta)};
%apply (int DIM1, int DIM2, double* INPLACE_ARRAY2)
       {(int K_A_1, int K_A_2, double* in_out_A),
        (int K_B, int P_B, double* in_out_B),
        (int N_alpha, int K_alpha, double* in_alpha),
        (int N_beta, int K_beta, double* in_beta)};
%apply (int DIM1, int* IN_ARRAY1)
       {(int N_x, int* x),
        (int N_z, int* z)};

double logsumexp(int N_x, double* x);
void forward(int K_pi, double* pi,
             int K_A_1, int K_A_2, double* A,
             int N_F, int K_F, double* F,
             int N_c, double* c,
             int N_alpha, int K_alpha, double* alpha);
void backward(int K_A_1, int K_A_2, double* A,
              int N_F, int K_F, double* F,
              int N_c, double* in_c,
              int N_beta, int K_beta, double* beta);
double loglike(int N_c, double* in_c);
void update_pi(int N_alpha, int K_alpha, double* in_alpha,
               int N_beta, int K_beta, double* in_beta,
               int K_pi, double* in_out_pi);
void update_A(int N_F, int K_F, double* F,
              int N_alpha, int K_alpha, double* in_alpha,
              int N_beta, int K_beta, double* in_beta,
              int N_c, double* c,
              int K_A_1, int K_A_2, double* in_out_A);
void update_B(int N_x, int* x,
              int N_alpha, int K_alpha, double* in_alpha,
              int N_beta, int K_beta, double* in_beta,
              int K_B, int P_B, double* in_out_B);
void baum_welch(int N_x, int* x,
                int K_pi, double* pi,
                int K_A_1, int K_A_2, double* in_out_A,
                int K_B, int P_B, double* in_out_B,
                int N_alpha, int K_alpha, double* alpha,
                int N_beta, int K_beta, double* beta,
                int N_c, double* c,
                int N_F, int K_F, double* F);
int train_discrete_HMM(int N_x, int* x,
                       int max_iter,
                       double tol,
                       int verbose,
                       int K_pi, double* in_out_pi,
                       int K_A_1, int K_A_2, double* in_out_A,
                       int K_B, int P_B, double* in_out_B,
                       int N_alpha, int K_alpha, double* alpha,
                       int N_beta, int K_beta, double* beta,
                       int N_c, double* c,
                       int N_F, int K_F, double* F,
                       int M, double* loglike_per_iter);
void viterbi(int K_pi, double* pi,
             int K_A_1, int K_A_2, double* A,
             int N_F, int K_F, double* F,
             int N_z, int* z);
double forward_initialize(int K_pi, double* pi,
                          int K_f, double* f,
                          int K_alpha, double* alpha_init);
double forward_step(int K_A_1, int K_A_2, double* A,
                    int K_f, double* f,
                    int K_alpha_old, double* alpha_old,
                    int K_alpha_new, double* alpha_new);
