#ifndef HMM_IS_INCLUDED
#define HMM_IS_INCLUDED
// C++ Code for HMM
// Ilias Bilionis, Argonne National Laboratory, 2013
//
// All matrices are assumed to be C-like
// For numerical stability, we will work with logarithms
// of probabilities.
#include <cassert>
#ifdef HAS_MKL
#include <mkl_cblas.h>
#else
#include <cblas.h>
#endif
#include <algorithm>
#include <functional>
#include <numeric>
#include <cmath>
#include <iostream>


#define HMM_DEBUG

#define iamax cblas_idamax
#define axpy cblas_daxpy
#define copy cblas_dcopy
#define scal cblas_dscal
#define gemv cblas_dgemv


#define LOG_ZERO_PROBABILITY -INFINITY
#define ZERO_PROBABILITY 1e-16


using std::transform;
using std::multiplies;
using std::accumulate;
using std::fill_n;
using std::cout;
using std::endl;
using std::max_element;


// Compute the logarithm of the sum of the exponentials of a vector of positive
// numbers. It uses the Log-Sum-Exp trick.
// Parameters
// ----------
// + N      (in) the dimensions of the vector
// + x      (in, vec N) the vector
inline
double logsumexp(const int N, const double* x)
{
    const double A = *std::max_element(x, x + N);
    double s = 0.;
    for(int i=0; i<N; i++) s += exp(x[i] - A);
    return A + log(s);
}


// Scale a probablity vector and return the normalization constant.
// Parameters
// ----------
// + K      (in) number of states
// + x      (in/out, vec K) the probability vector
inline double normalize(int K, double* x)
{
    const double c = accumulate(x, x + K, 0.);
    scal(K, 1. /  c, x, 1);
    return c;
}


// Scale a transition matrix.
// Parameters
// ----------
// + N      (in) number of rows
// + K      (in) number of columns
// + A      (in/out, mat N x K) the transmition matrix
inline void normalize(int N, int K, double* A)
{
    double* A_j = A;
    for(int i=0; i<N; i++) {
        normalize(K, A_j);
        A_j += K;
    }
}

// The initial step of the forward algorithm
// 
// Parameters
// ----------
// + K          (in, int) number of states
// + pi         (in, vec K) initial probabilities
// + f          (in, vec K) probability of observation conditioned on each stae
// + alpha_init (out, vec K) the initial alpha
inline
double forward_initialize(const int K,
                          const double* pi,
                          const double* f,
                          double* alpha_init)
{
    transform(pi, pi + K, f, alpha_init, multiplies<double>());
    return normalize(K, alpha_init);
}


// Python wrapper for forward_initialize()
double forward_initialize(const int K_pi, const double* pi,
                          const int K_f, const double* f,
                          const int K_alpha, double* alpha_init)
{
    const int K = K_pi;
#ifdef HMM_DEBUG
    assert(K == K_f);
    assert(K == K_alpha);
#endif
    return forward_initialize(K, pi, f, alpha_init);
}


// A single step of the forward algorithm.
// It does not apply to the very first step.
// For the very first step, using forward_initialize().
//
// Parameters
// ----------
// + K          (in, int) number of states
// + A          (in, mat K x K) transmission matrx
// + f          (in, vec K) probability of observation conditioned on each stae
// + alpha_old  (in, vec K) the old alpha
// + alpha_new  (out, vec K) the new alpha
//
// Returns
// -------
// The normalization constant of the new alpha's.
inline
double forward_step(const int K,
                    const double* A,
                    const double* f,
                    const double* alpha_old,
                    double* alpha_new)
{
    fill_n(alpha_new, K, 0.);
    gemv(CblasRowMajor,CblasTrans, K, K, 1., A, K, alpha_old, 1, 0.,
         alpha_new, 1);
    transform(f, f + K, alpha_new, alpha_new, multiplies<double>());
    return normalize(K, alpha_new);
}

// Python wrapper for forward_step()
double forward_step(const int K_A_1, const int K_A_2, const double* A,
                    const int K_f, const double* f,
                    const int K_alpha_old, const double* alpha_old,
                    const int K_alpha_new, double* alpha_new)
{
    const int K = K_A_1;
#ifdef HMM_DEBUG
    assert(K == K_A_2);
    assert(K == K_f);
    assert(K == K_alpha_old);
    assert(K == K_alpha_new);
#endif
    return forward_step(K, A, f, alpha_old, alpha_new);
}


// The forward algorithm.
// Parameters
// ----------
// + N      (in, int) number of samples
// + K      (in, int) number of states
// + pi     (in, vec K) initial probabilities for hidden states
// + A      (in, mat K x K) transmission matrx
// + F      (in, mat N x K) matrix of probabilities of each observation
// + c      (out, vec N) normalization constants
// + alpha  (out, mat N x K) matrix of the alphas
inline
void forward(const int N, const int K,
             const double* pi,
             const double* A,
             const double* F,
             double* c,
             double* alpha)
{
    c[0] = forward_initialize(K, pi, F, alpha);
    for(int n=1; n<N; n++) {
        c[n] = forward_step(K, A, F + K, alpha, alpha + K);
        F += K;
        alpha += K;
    }
}

// Python wrapper for _forward
void forward(const int K_pi, const double* pi,
             const int K_A_1, const int K_A_2, const double* A,
             const int N_F, const int K_F, const double* F,
             const int N_c, double* c,
             const int N_alpha, const int K_alpha, double* alpha)
{
    const int K = K_pi;
    const int N = N_F;
#ifdef HMM_DEBUG
    assert(K == K_A_1);
    assert(K == K_A_2);
    assert(K == K_F);
    assert(N == N_c);
    assert(N == N_alpha);
    assert(K == K_alpha);
#endif
    forward(N, K, pi, A, F, c, alpha);
}


// The backward algorithm.
// Parameters
// ----------
// + N      (in) number of samples
// + K      (in) number of states
// + A      (in, mat K x K) transition matrix
// + F      (in, mat N x K) matrix of probabilities of each observation
// + c      (in, vec N) normalization constants
// + beta   (out, mat N x K) matrix of the betas
//
// Preconditions
// -------------
// + c must be filled using _forward()
inline
void backward(const int N, const int K,
              const double* A,
              const double* F,
              const double* c,
              double* beta)
{
    double* beta_n = beta + (N - 1) * K;
    fill_n(beta_n, K, 1.);
    scal(K, 1., beta_n, 1);
    const double* beta_prev = beta_n;
    const double* F_prev = F + (N - 1) * K;
    for(int n=N - 2; n>=0; n--) {
        beta_n -= K;
        fill_n(beta_n, K, 0.);
        for(int k=0; k<K; k++) {
            for(int j=0; j<K; j++) 
                beta_n[k] += (beta_prev[j] * F_prev[j] * A[k * K + j]);
        }
        scal(K, 1. / c[n + 1], beta_n, 1);
        beta_prev = beta_n;
        F_prev -= K;
    }
}

// The python wrapper for the backward algorithm
void backward(const int K_A_1, const int K_A_2, const double* A,
              const int N_F, const int K_F, const double* F,
              const int N_c, const double* in_c,
              const int N_beta, const int K_beta, double* beta)
{
    const int N = N_F;
    const int K = K_A_1;
#ifdef HMM_DEBUG
    assert(K == K_A_2);
    assert(N == N_F);
    assert(K == K_F);
    assert(N == N_c);
    assert(N == N_beta);
    assert(K == K_beta);
#endif
    backward(N, K, A, F, in_c, beta);
}


// Compute the logarithm of the likelihood.
// Parameters
// ----------
// + N      (in) number of observations
// + c      (in, vec N) normalization constants
// Preconditons:
// + c must have been computed using forward()
inline double loglike(const int N, const double* c)
{
    double res = 0.;
    for(int n=0; n<N; n++) res += log(c[n]);
    return res;
}


// Update the initial probabilities vector.
// Parameters
// ----------
// + K      (in) number of states
// + alpha  (in, mat N x K) the alphas
// + beta   (in, mat N x K) the betas
// + pi     (in/out, vec K) initial probabilities
//
// Preconditions
// -------------
// + alpha has been calculated using forward() using pi
// + beta has been calculated using backward() using pi and alpha
inline void update_pi(const int K,
                      const double* alpha,
                      const double* beta,
                      double* pi)
{
    transform(alpha, alpha + K, beta, pi, multiplies<double>());
    normalize(K, pi);
}

// Python wrapper for update_pi()
void update_pi(const int N_alpha, const int K_alpha, const double* alpha,
               const int N_beta, const int K_beta, const double* beta,
               const int K_pi, double* pi)
{
    const int K = K_alpha;
#ifdef HMM_DEBUG
    assert(N_alpha >= 1);
    assert(K == K_alpha);
    assert(n_beta >= 1);
    assert(K == K_beta);
    assert(K == K_pi);
#endif
    update_pi(K, alpha, beta, pi);
}


// Update the transmission matrix
// Parameters
// ----------
// + N      (in) number of samples
// + K      (in) number of states
// + F      (in, mat N x K) matrix of probabilities of each observation
// + alpha  (in, mat N x K) the alphas
// + beta   (in, mat N x K) the betas
// + c      (in, vec N) normalization constants
// + A      (in/out, mat K x K) transition matrix
// 
// Preconditions
// -------------
// + A is a valid transimition matrix
// + alpha and c have been calculated using forward() with A
// + beta  has been calculated using backward() with A, alpha and c
inline void update_A(const int N, const int K,
                     const double* F,
                     const double* alpha,
                     const double* beta,
                     const double* c,
                     double* A)
{
    for(int j=0; j<K; j++) {
        for(int k=0; k<K; k++) {
            double s = 0.;
            for(int n=1; n<N; n++) {
                s += (alpha[(n - 1) * K + j] * beta[n * K + k]
                      * F[n * K + k]) / c[n];
            }
            A[j * K + k] *= s;
        }
    }
    normalize(K, K, A);
}

// Python wrapper for update_A
void update_A(const int N_F, const int K_F, const double* F,
              const int N_alpha, const int K_alpha, const double* alpha,
              const int N_beta, const int K_beta, const double* beta,
              const int N_c, const double* c,
              const int K_A_1, const int K_A_2, double* A)
{
    const int N = N_F;
    const int K = K_F;
#ifdef HMM_DEBUG
    assert(N == N_alpha);
    assert(K == K_beta);
    assert(N == N_c);
    assert(K == K_A_1);
    assert(K == K_A_2);
#endif
    update_A(N, K, F, alpha, beta, c, A);
}


// Update discrete emission probabilities
// Parameters
// ----------
// + N      (in) number of samples
// + K      (in) number of states
// + P      (in) number of symbols
// + x      (in, vec N) observed data
// + alpha  (in, mat N x K) the alphas
// + beta   (in, mat N x K) the betas
// + B      (in/out, mat K x P) emission probabilities
// 
// Preconditions
// -------------
// + x[i] < P
// + B is a valid transimition matrix
// + alpha and c have been calculated using forward() with F from x and B
// + beta  has been calculated using backward() with F from x and B and alpha
inline void update_B(const int N, const int K, const int P,
                     const int* x,
                     const double* alpha,
                     const double* beta,
                     double* B)
{
    fill_n(B, K * P, 0.);
    for(int k=0; k<K; k++)
        for(int n=0; n<N; n++)
            B[k * P + x[n]] += alpha[n * K + k] * beta[n * K + k];
    normalize(K, P, B);
}


// Python wrapper for update_B()
void update_B(const int N_x, const int * x,
              const int N_alpha, const int K_alpha, const double* alpha,
              const int N_beta, const int K_beta, const double* beta,
              const int K_B, const int P_B, double* B)
{
    const int N = N_x;
    const int K = K_alpha;
    const int P = P_B;
#ifdef HMM_DEBUG
    assert(N == N_alpha);
    assert(N == N_beta);
    assert(K == K_B);
    for(int i=0; i<N; i++){
        assert(x[i] >= 0);
        assert(x[i] < P);
    }
#endif
    update_B(N, K, P, x, alpha, beta, B);
}


// Perform a single iteration of the Baum-Welch algorithm.
// Parameters
// ----------
// + N      number of samples
// + K      number of states
// + P      number of symbols
// + x      (in, vec N) observations
// + pi     (in/out, vec K) initial probabilities
// + A      (in/out, vec K x K) transmission probabilities
// + B      (in/out, vec K x P) emission probabilities
// + alpha  (out, vec N x K) the alphas
// + beta   (out, vec N x K) the betas
// + c      (out, vec N) the normalization constants
// + F      (out, vec N x K) probabilities of observations
//
// Preconditions
// -------------
// + x[i] < P
// + pi is a valid probability density
// + A is a valid transition matrix
// + B is a valid emission matrix
inline void baum_welch(const int N, const int K, const int P,
                       const int* x,
                       double* pi,
                       double* A,
                       double* B,
                       double* alpha,
                       double* beta,
                       double* c,
                       double* F)
{
    for(int n=0; n<N; n++)
        for(int k=0; k<K; k++) F[n * K + k] = B[k * P + x[n]];
    forward(N, K, pi, A, F, c, alpha);
    backward(N, K, A, F, c, beta);
    update_pi(K, alpha, beta, pi);
    update_A(N, K, F, alpha, beta, c, A);
    update_B(N, K, P, x, alpha, beta, B);
}
                
// Python wrapper for baum_welch
void baum_welch(const int N_x, const int* x,
                const int K_pi, double* pi,
                const int K_A_1, const int K_A_2, double* A,
                const int K_B, const int P_B, double* B,
                const int N_alpha, const int K_alpha, double* alpha,
                const int N_beta, const int K_beta, double* beta,
                const int N_c, double* c,
                const int N_F, const int K_F, double* F)
{
    const int N = N_x;
    const int K = K_pi;
    const int P = P_B;
#ifdef HMM_DEBUG
    assert(K == K_A_1);
    assert(K == K_A_2);
    assert(K == K_B);
    assert(N == N_alpha);
    assert(K == K_alpha);
    assert(N == N_beta);
    assert(K == K_beta);
    assert(N == N_c);
    assert(N == N_F);
    for(int i=0; i<N; i++){
        assert(x[i] >= 0);
        assert(x[i] < P);
    }
#endif
    baum_welch(N, K, P, x, pi, A, B, alpha, beta, c, F);
}

// Iterate Baum-Welch until convergence for a discrete emission HMM.
// Parameters
// ----------
// + N          (in) number of samples
// + K          (in) number of states
// + P          (in) number of symbols
// + x          (in, vec N) observations
// + max_iter   (in) maximum number of iterations
// + verbose    (in) verbosity level (0 print nothing, 1 print sth, etc.)
// + tol        (in) convergence tolerance
// + pi         (in/out, vec K) initial probabilities
// + A          (in/out, vec K x K) transmission probabilities
// + B          (in/out, vec K x P) emission probabilities
// + alpha      (out, vec N x K) the alphas
// + beta       (out, vec N x K) the betas
// + c          (out, vec N) the normalization constants
// + F          (out, vec N x K) probabilities of observations
// + loglike    (out, vec max_iter) log likelihood at each iteration
//
// Returns
// -------
// + iter       (out) number of iterations actually performed
//
// Preconditions
// -------------
// + x[i] < P
// + pi is a valid probability density
// + A is a valid transition matrix
// + B is a valid emission matrix
inline int train_discrete_HMM(const int N, const int K, const int P,
                              const int* x,
                              const int max_iter,
                              const double tol,
                              const int verbose,
                              double* pi,
                              double* A,
                              double* B,
                              double* alpha,
                              double* beta,
                              double* c,
                              double* F,
                              double* loglike_per_iter)
{
    int iter = 0;
    double loglike_prev = -1e99;
    double loglike_cur;
    while(iter < max_iter) {
        baum_welch(N, K, P, x, pi, A, B, alpha, beta, c, F);
        loglike_cur = loglike(N, c);
        loglike_per_iter[iter++] = loglike_cur;
        if(loglike_cur < loglike_prev) {
            cout << "*** ERROR: Loglikelihood decreased!" << endl;
            break;
        }
        if(iter > 1 &&
           fabs((loglike_cur - loglike_prev) / loglike_prev) < tol) {
            if(verbose >= 1)
                cout << "*** CONVERGED!" << endl;
            break;
        }
        if(verbose >= 1)
            cout << iter << ": " << loglike_cur << endl;
        loglike_prev = loglike_cur;
    }
    return iter;
}


// Python wrapper for train_discrete_HMM()
int train_discrete_HMM(const int N_x, const int* x,
                       const int max_iter,
                       const double tol,
                       const int verbose,
                       const int K_pi, double* pi,
                       const int K_A_1, const int K_A_2, double* A,
                       const int K_B, const int P_B, double* B,
                       const int N_alpha, const int K_alpha, double* alpha,
                       const int N_beta, const int K_beta, double* beta,
                       const int N_c, double* c,
                       const int N_F, const int K_F, double* F,
                       const int M, double* loglike_per_iter)
{
    const int N = N_x;
    const int K = K_pi;
    const int P = P_B;
#ifdef HMM_DEBUG
    assert(max_iter >= 1);
    assert(tol > 0.);
    assert(K == K_A_1);
    assert(K == K_A_2);
    assert(K == K_B);
    assert(N == N_alpha);
    assert(K == K_alpha);
    assert(N == N_beta);
    assert(K == K_beta);
    assert(N == N_c);
    assert(N == N_F);
    assert(K == K_F);
    assert(M >= max_iter);
#endif
    return train_discrete_HMM(N, K, P, x, max_iter, tol, verbose, pi, A, B,
                              alpha, beta, c, F, loglike_per_iter);
}


// Compute the logarithm with correction,
// seeting the value to -INFINITY if x
// is smaller than ZERO_PROBABILITY
inline double ln(const double x)
{
    return x <= ZERO_PROBABILITY? LOG_ZERO_PROBABILITY : log(x);
}


// Return the argmax of a sequence
template<class scalar_type>
inline int argmax(const scalar_type* start, const scalar_type* end)
{
    return static_cast<int>(max_element(start, end) - start);
}


// The Viterbi algorithm for the computation of the most likely sequence
// of hidden states given a sequence of observations.
inline void viterbi(const int N, const int K,
                    const double* pi, const double* A, const double* F,
                    int* z)
{
    double* omega = new double[N * K];
    double* lnA = new double[K * K];
    int* pred = new int[N * K];
    for(int j=0; j<K; j++)
    for(int k=0; k<K; k++)
        lnA[j * K + k] = ln(A[j * K + k]);
    for(int k=0; k<K; k++)
        omega[k] = ln(pi[k]) + ln(F[k]);
    for(int n=1; n<N; n++) {
        for(int k=0; k<K; k++) {
            double max_omega_prev = LOG_ZERO_PROBABILITY;
            for(int j=0; j<K; j++) {
                double tmp = omega[(n - 1) * K + j] + lnA[j * K + k];
                if(tmp > max_omega_prev) {
                    pred[n * K + k] = j;
                    max_omega_prev = tmp;
                }
            }
            omega[n * K + k] = max_omega_prev + ln(F[n * K + k]);
        }
    }
    z[N - 1] = argmax(omega + (N - 1) * K, omega + N * K); 
    for(int n=N-2; n>=0; n--)
        z[n] = pred[(n + 1) * K + z[n + 1]];
    delete omega;
    delete lnA;
    delete pred;
}

// Python wrapper for the viterbi algorithm
void viterbi(const int K_pi, const double* pi,
             const int K_A_1, const int K_A_2, const double* A,
             const int N_F, const int K_F, const double* F,
             const int N_z, int* z)
{
    const int N = N_F;
    const int K = K_pi;
#ifdef HMM_DEBUG
    assert(K == K_A_1);
    assert(K == K_A_2);
    assert(K == K_F);
    assert(N == N_z);
#endif
    viterbi(N, K, pi, A, F, z);
}

#endif
