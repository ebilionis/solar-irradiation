"""
Python Hidden Markov Model (HMM) package.

Author:
    Ilias Bilionis
    Mathematics and Computer Science Division
    Argonne National Laboratory
    Lemont, IL, USA
    December 2013
"""


__all__ = ['core', 'DiscreteHMM']

import core
from discrete_hmm import DiscreteHMM
