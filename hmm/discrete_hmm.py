"""
Implementation of a class for Discrete HMM's.
"""

__all__ = ['DiscreteHMM']


import core
import numpy as np


class DiscreteHMM(object):
    """
    A discrete HMM model.

    :param num_states:  The number of hidden states.
    :type num_states:   int
    :param num_symbols: The number of emission symbols.
    :type num_symbols:  int
    """

    # number of hidden states
    _num_states = None

    # number of symbols
    _num_symbols = None

    # initial probabilties
    _pi = None

    # transmission probabilities
    _A = None

    # emission probabilities
    _B = None

    # alpha forward table
    _alpha = None

    # beta backward table
    _beta = None

    # normalization constants of alpha table
    _c = None

    # observed data
    _X = None

    @property
    def num_states(self):
        """Get the number of hidden states."""
        return self._num_states

    @property
    def num_symbols(self):
        """Get the number of symbols."""
        return self._num_symbols

    @property
    def pi(self):
        """Get the initial probabilities."""
        return self._pi

    @pi.setter
    def pi(self, value):
        """Set the initial probabilities."""
        value = np.array(value)
        assert value.ndim == 1
        assert (value >= 0).all()
        value = value / value.sum()
        self._pi = value

    @property
    def A(self):
        """Get the transmission probabilities."""
        return self._A

    @A.setter
    def A(self, value):
        """Set the transmission probabilities."""
        value = np.array(value)
        assert value.ndim == 2
        assert (value >= 0).all()
        value /= value.sum(axis=1).reshape((value.shape[0], 1))
        self._A = value

    @property
    def B(self):
        """Get the emission probabilities."""
        return self._B

    @B.setter
    def B(self, value):
        """Set the emission probabilities."""
        value = np.array(value)
        assert value.ndim == 2
        assert (value >= 0).all()
        value /= value.sum(axis=1).reshape((value.shape[0], 1))
        self._B = value

    @property
    def alpha(self):
        """Get the alpha forward table."""
        return self._alpha

    @property
    def beta(self):
        """Get the beta backward table."""
        return self._beta

    @property
    def c(self):
        """Get the normalization constants of the alpha table."""
        return self._c

    @property
    def loglike(self):
        """
        Get the logarithm of the likelihood of the observed data.

        Precondition
        ------------
        The self.forward() has been run.
        """
        assert self.c is not None
        return np.log(self.c).sum()

    @property
    def X(self):
        """Get the observed data."""
        return self._X

    @X.setter
    def X(self, value):
        """Set the observed data."""
        value = np.array(value, dtype='int32')
        assert value.ndim == 1
        assert (value >= 0).all()
        assert (value < self.num_symbols).all()
        self._X = value
        self._forward()

    @property
    def F(self):
        """
        Get the probabilities of the observed data.

        Precondition
        ------------
        The observed data must have been set.
        """
        assert self.X is not None
        return self.B[:, self.X].T

    def __init__(self, pi, A, B):
        """
        Initialize the object.
        """
        self.pi = pi
        self.A = A
        self.B = B
        self._num_states = self.pi.shape[0]
        self._num_symbols = self.B.shape[1]
        # Sanity check
        assert self.num_states == self.A.shape[0]
        assert self.num_states == self.A.shape[1]
        assert self.num_states == self.B.shape[0]

    def _forward(self):
        """
        Run the forward algorithm that computes the alpha table.

        Precondition
        ------------
        The data have been set already.
        """
        assert self.X is not None
        self._c = np.ndarray(self.X.shape[0])
        self._alpha = np.ndarray((self.X.shape[0], self.num_states))
        core.forward(self.pi, self.A, self.F, self.c, self.alpha)

    def _backward(self):
        """
        Run the backward algorithm that computes the beta table.

        Precondition
        ------------
        The forward algorithm must have been run already.
        """
        assert self.c is not None
        self._beta = np.ndarray((self.X.shape[0], self.num_states))
        core.backward(self.A, self.F, self.c, self.beta)

    def train(self, X=None, tol=1e-6, max_iter=10000, verbose=1):
        """
        Train the object using maximum likelihood assuming we observed the
        data X. After, we are done, pi, A, B, alpha, beta and c are updated.

        Returns
        -------
        A list of the log likelihood at each iterations of the EM algorithm.
        """
        if X is not None:
            self.X = X
        assert self.X is not None
        N = self.X.shape[0]
        K = self.num_states
        self._alpha = np.ndarray((N, K))
        self._beta = np.ndarray((N, K))
        self._c = np.ndarray(N)
        self._X = X
        loglike_per_iter = np.ndarray(max_iter)
        it = core.train_discrete_HMM(self.X, max_iter, tol, verbose, self.pi,
                                     self.A, self.B, self.alpha, self.beta,
                                     self.c, self.F, loglike_per_iter)
        return loglike_per_iter[:it]

    def decode(self, X=None):
        """
        Return the most probable sequence of Hidden states.
        """
        if X is None:
            X = self._X
        else:
            X = np.array(X, dtype='int32')
            assert (X >= 0).all()
            assert (X < self.num_symbols).all()
        Z = np.ndarray(X.shape[0], dtype='int32')
        F = self.B[:, X].T
	A = np.copy(self.A)
        for i in xrange(1000):
            A = np.dot(A.T, A)
            A = A / A.sum(axis=1).reshape((A.shape[0], 1))
        pi = np.copy(A[0, :])
        core.viterbi(pi, self.A, F, Z)
        return Z

    def get_A(self, n):
        """
        Returns the transition matrix from step 0 to n.
        """
        if n == 0:
            return np.eye(self.A.shape[0])
        A = np.copy(self.A)
        for i in xrange(1, n):
            A = np.dot(A.T, A.T).T
            A = A / A.sum(axis=1).reshape((A.shape[0], 1))
        return A

    def step_ahead_prediction(self, X, num_steps):
        """
        Recursively use x_{1:n} observations from X to predict x_{n+1}.
        It reports the probability of each symbol.
        """
        X = np.array(X, dtype='int32')
        assert (X >= 0).all()
        assert (X < self.num_symbols).all()
        N = X.shape[0]
        F = self.B[:, X].T
        alpha = np.ndarray((N, self.num_states))
        c = np.ndarray(N)
        pi = self.get_A(1000)[0, :]
        As = self.get_A(num_steps)
        core.forward(pi, self.A, F, c, alpha)
        XP = np.ndarray((N, self.num_symbols))
        for i in xrange(num_steps):
            XP[i, :] = np.dot(self.B.T, np.dot(self.get_A(i).T, pi)) 
        for i in xrange(num_steps, N):
            XP[i, :] = np.dot(self.B.T, np.dot(As.T, alpha[i - 1, :]))
        return XP
