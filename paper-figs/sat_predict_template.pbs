##!/bin/bash
#PBS -N sat_predict
#PBS -l nodes=1:ppn=8
#PBS -l walltime=00:30:00
#PBS -j oe

cd $PBS_O_WORKDIR

# Load some bash functions
. utils.sh

# The number of cores to be use (template)
NUM_CORES=8

# The scripts directory
SCRIPTS=template

# The processed insolation file
PROCESSED_INSOLATION_FILE=template

# The reduction map
REDUCTION_MAP_FILE=template

# The dynamics file
DYNAMICS_FILE=template

# The start date for predictions
PREDICTION_DATE=template

# Other inputs
NUM_AHEAD=template
NUM_SAMPLES=template

# The output file name (output)
OUTPUT_FILE=template

# Sanity check
check_file $PROCESSED_INSOLATION_FILE
check_file $REDUCTION_MAP_FILE
check_file $DYNAMICS_FILE

# The local insolation file
LOCAL_INSOLATION_FILE=`basename $PROCESSED_INSOLATION_FILE`

# The local reduction map file
LOCAL_REDUCTION_MAP_FILE=`basename $REDUCTION_MAP_FILE`

# The local dynamics file
LOCAL_DYNAMICS_FILE=`basename $DYNAMICS_FILE`

# The local output file
LOCAL_OUTPUT_FILE=`basename $OUTPUT_FILE`

# Copy insolation to master
copy_to_master $PROCESSED_INSOLATION_FILE $LOCAL_INSOLATION_FILE

# Copy reduction map to master
copy_to_master $REDUCTION_MAP_FILE $LOCAL_REDUCTION_MAP_FILE

# Copy the dynamics file
copy_to_master $DYNAMICS_FILE $LOCAL_DYNAMICS_FILE

# Compute the clearness index
mpiexec -n $NUM_CORES \
        python $SCRIPTS/sat_predict.py \
            -i $TMPDIR/$LOCAL_INSOLATION_FILE \
            --reduction-map=$TMPDIR/$LOCAL_REDUCTION_MAP_FILE \
            --satelite-model=$TMPDIR/$LOCAL_DYNAMICS_FILE \
            --start-date="$PREDICTION_DATE" \
            --num-ahead=$NUM_AHEAD \
            --num-samples=$NUM_SAMPLES \
            -o $TMPDIR/$LOCAL_OUTPUT_FILE \
            --use-mpi

# Copy from the master here
copy_from_master $LOCAL_OUTPUT_FILE $OUTPUT_FILE
