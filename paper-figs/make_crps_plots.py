"""
Makes the CRPS plots.

Author:
    Ilias Bilionis

Date:
    8/22/2014

"""

import matplotlib as mlt
mlt.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import os

sat_range = []
sat_crps = []
ground_range = []
ground_crps = []


for i in xrange(2, 18):
    sat_file = 'FactorAnalysis_8/sat_' + str(i) + '_all_crps.txt'
    ground_file = 'FactorAnalysis_8/ground_' + str(i) + '_all_crps.txt'
    if os.path.exists(sat_file):
        print 'Reading', sat_file
        sat_crps.append(np.loadtxt(sat_file))
        sat_range.append(i)
    if os.path.exists(ground_file):
        print 'Reading', ground_file
        ground_crps.append(np.loadtxt(ground_file))
        ground_range.append(i)
sat_crps = np.array(sat_crps)
sat_range = np.array(sat_range)
ground_crps = np.array(ground_crps)
ground_range = np.array(ground_range)
plt.plot(sat_range, np.mean(sat_crps, axis=1), '-ro', linewidth=2, markersize=10,
         markeredgewidth=2)
plt.plot(ground_range[:np.max(sat_range) - 1],
         np.mean(ground_crps[:np.max(sat_range) - 1, :], axis=1), '--g+',
         linewidth=2, markersize=10, markeredgewidth=2)
leg = plt.legend(['Satellite model', 'Ground model'], loc='best')
plt.setp(leg.get_texts(), fontsize=16)
plt.xlabel('Number of steps ahead', fontsize=16)
plt.ylabel('CRPS', fontsize=16)
plt.tight_layout()
plt.savefig('FactorAnalysis_8/crps.png')
