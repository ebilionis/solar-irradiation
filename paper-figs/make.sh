#!/bin/bash
# Make all the figures of the paper
# Author:
#       Ilias Bilionis
# Date:
#       5/26/2014


# Load some functions
. utils.sh

# Some PBS templates we will be using
CLEARNESS_INDEX_PBS_TEMPLATE="./extract_clearness_index_template.pbs"
REDUCE_PBS_TEMPLATE="./reduce_template.pbs"
DYNAMICS_PBS_TEMPLATE="./dynamics_template.pbs"
REDUCTION_PLOTS_PBS_TEMPLATE="./reduction_plots_template.pbs"
GROUND_MODEL_PBS_TEMPLATE="./ground_model_template.pbs"
SATELLITE_PREDICTION_PBS_TEMPLATE="./sat_predict_template.pbs"
GROUND_PREDICTION_PBS_TEMPLATE="./ground_predict_template.pbs"
PREDICTION_PLOTS_PBS_TEMPLATE="./prediction_plots_template.pbs"
SCATTER_PLOTS_PBS_TEMPLATE="./scatter_plots_template.pbs"
SATELLITE_ERRORS_PBS_TEMPLATE="./compute_errors_template.pbs"


print_bar '*'
echo "Data-Driven Model for Solar Irradiation Based on Satelite Observations"
echo "I. Bilionis, E. Constantinescu, M. Anitscu, Solar Energy, 2014"
echo ""
echo "This script produces all the figures of the paper (and  more...)"
echo "It is set up to work on fusion for the moment."
print_bar '*'
echo ""

# Load the input
. input

# PROBE THE GOOGLE API
echo "PROBING GOOGLE API"
print_bar '='
echo "+ checking: SKIP_PROBING=$SKIP_PROBING"
if $SKIP_PROBING
then
    echo "+ nothing to do"
else
    printf "+ probing network before submitting jobs so that we can work offline..."
    run python $SCRIPTS/probe_google_api.py $INSOLATION_FILE > /dev/null 2> /dev/null
    printf ' ***SUCCESS***\n'
fi
print_bar '='
echo ""

# COMPUTE THE CLEARNESS INDEX GIVEN THE INSOLATION FILE
echo "CLEARNESS INDEX COMPUTATION"
print_bar '='
echo "+ checking variable: INSOLATION_FILE=$INSOLATION_FILE"
check_file $INSOLATION_FILE
echo "+ checking variable: PROCESSED_INSOLATION_FILE=$PROCESSED_INSOLATION_FILE"
if [ ! -f $PROCESSED_INSOLATION_FILE ]
then
    echo "+ I am missing: $PROCESSED_INSOLATION_FILE"
    echo "+ I will submit a job to compute it"
    CLEARNESS_INDEX_PBS="extract_clearness_index_$INSOLATION_POSTFIX.pbs"
    echo "+ creating $CLEARNESS_INDEX_PBS"
    cat $CLEARNESS_INDEX_PBS_TEMPLATE | \
        sed "s,\bINSOLATION_FILE=template,INSOLATION_FILE=$INSOLATION_FILE," | 
        sed "s,\bPROCESSED_INSOLATION_FILE=template,\
PROCESSED_INSOLATION_FILE=$PROCESSED_INSOLATION_FILE," | \
        sed "s,\bSCRIPTS=template,SCRIPTS=$SCRIPTS," \
    > $CLEARNESS_INDEX_PBS
    printf "+ submitting $CLEARNESS_INDEX_PBS..."
    CLEARNESS_INDEX_ID=`submit $CLEARNESS_INDEX_PBS`
    printf " job id: $CLEARNESS_INDEX_ID\n"
fi
print_bar '='
echo ""


# REDUCE THE DATA
echo "DATA REDUCTION"
print_bar '='
echo "+ checking: NUM_COMPONENTS=$NUM_COMPONENTS"
echo "+ checking: FINAL_DATE=$FINAL_DATE"
echo "+ checking: $REDUCTION_MAP_FILE"
if [ ! -f $REDUCTION_MAP_FILE ]
then
    echo "+ I did not find the file"
    echo "+ I will submit a job to compute it"
    echo "+ checking variable: $PROCESSED_INSOLATION_FILE"
    if [ ! -f $PROCESSED_INSOLATION_FILE ]
    then
        echo "+ I did not find: $PROCESSED_INSOLATION_FILE"
        echo "+ looking for clearness index job"
        echo "+ I found this: $CLEARNESS_INDEX_ID"
        echo "+ the data reduction job will start right after"
    fi
    REDUCE_PBS="reduce_$NUM_COMPONENTS.pbs" 
    echo "+ creating $REDUCE_PBS"
    cat $REDUCE_PBS_TEMPLATE | \
        sed "s/-N \w*/-N reduce_$NUM_COMPONENTS/" | \
        sed "s,\bPROCESSED_INSOLATION_FILE=template,\
PROCESSED_INSOLATION_FILE=$PROCESSED_INSOLATION_FILE," | \
        sed "s,\bREDUCTION_MAP_FILE=template,REDUCTION_MAP_FILE=$REDUCTION_MAP_FILE," | \
        sed "s/\bREDUCTION_MAP=template/REDUCTION_MAP=$REDUCTION_MAP/" | \
        sed "s/\bNUM_COMPONENTS=template/NUM_COMPONENTS=$NUM_COMPONENTS/" | \
        sed "s/\bFINAL_DATE=template/FINAL_DATE=\"$FINAL_DATE\"/" | \
        sed "s,\bSCRIPTS=template,SCRIPTS=$SCRIPTS," \
    > $REDUCE_PBS 
    printf "+ submitting $REDUCE_PBS..."
    REDUCE_ID=`submit_after $REDUCE_PBS $CLEARNESS_INDEX_ID`
    printf " job id: $REDUCE_ID\n"
else
    echo "+ nothing to do"
fi
print_bar '='
echo ""


echo "LEARNING THE DYNAMICS"
print_bar '='
echo "+ checking: GP_MODEL=$GP_MODEL"
echo "+ checking: KERNEL=$KERNEL"
echo "+ checking: DEGREE=$DEGREE"
echo "+ checking: LAG=$LAG"
echo "+ checking: FINAL_DATE=$FINAL_DATE"
echo "+ checking: $DYNAMICS_FILE"
if [ ! -f $DYNAMICS_FILE ]
then
    echo "+ I did not find the file"
    echo "+ I will submit a job to compute it"
    echo "+ checking variable: $REDUCTION_MAP_FILE"
    if [ ! -f $REDUCTION_MAP_FILE ]
    then
        echo "+ I did not find: $REDUCTION_MAP_FILE"
        echo "+ looking for a reduction job"
        echo "+ I found this: $REDUCE_ID"
        echo "+ the dynamics job will start right after"
    fi
    DYNAMICS_PBS="dynamics_${NUM_COMPONENTS}_$KERNEL.pbs"
    echo "+ creating $DYNAMICS_PBS"
    cat $DYNAMICS_PBS_TEMPLATE | \
        sed "s/nodes=[0-9]*/nodes=$DYNAMICS_NUM_NODES/" | \
        sed "s/NUM_CORES=[0-9]*/NUM_CORES=$DYNAMICS_NUM_CORES/" | \
        sed "s/-N \w*/-N dynamics_$NUM_COMPONENTS_$KERNEL/" | \
        sed "s,\bPROCESSED_INSOLATION_FILE=template,\
PROCESSED_INSOLATION_FILE=$PROCESSED_INSOLATION_FILE," | \
        sed "s,\bREDUCTION_MAP_FILE=template,REDUCTION_MAP_FILE=$REDUCTION_MAP_FILE," | \
        sed "s,\bDYNAMICS_FILE=template,DYNAMICS_FILE=$DYNAMICS_FILE," | \
        sed "s/\bKERNEL=template/KERNEL=$KERNEL/" | \
        sed "s/\bGP_MODEL=template/GP_MODEL=$GP_MODEL/" | \
        sed "s/\bDEGREE=template/DEGREE=$DEGREE/" | \
        sed "s/\bLAG=template/LAG=$LAG/" | \
        sed "s/\bFINAL_DATE=template/FINAL_DATE=\"$FINAL_DATE\"/" | \
        sed "s,\bSCRIPTS=template,SCRIPTS=$SCRIPTS," \
    > $DYNAMICS_PBS
    printf "+ submitting $DYNAMICS_PBS..."
    DYNAMICS_ID=`submit_after $DYNAMICS_PBS $REDUCE_ID`
    printf " job id: $DYNAMICS_ID\n"
else
    echo "+ nothing to do"
fi
print_bar '='
echo ""


echo "REDUCTION MAP PLOTS"
print_bar '='
echo "+ checking: SKIP_REDUCTION_PLOTS=$SKIP_REDUCTION_PLOTS"
if $SKIP_REDUCTION_PLOTS
then
    echo "+ nothing to do"
else
echo "+ checking: NUM_EIGEN=$NUM_EIGEN"
echo "+ checking: NUM_TRACE=$NUM_TRACE"
echo "+ checking: NUM_COMP_TRACE=$NUM_COMP_TRACE"
echo "+ checking: EIGEN_COLORBAR_FONTSIZE=$EIGEN_COLORBAR_FONTSIZE"
echo "+ checking: EVOLUTION_FONTSIZE=$EVOLUTION_FONTSIZE"
echo "+ checking: $REDUCTION_MAP_FILE"
if [ ! -f $REDUCTION_MAP_FILE ]
then
    echo "+ I did not find: $REDUCTION_MAP_FILE"
    echo "+ looking for a reduction job"
    echo "+ I found this: $REDUCE_ID"
    echo "+ the dynamics job will start right after"
fi
REDUCTION_PLOTS_PBS="reduction_plots_$NUM_COMPONENTS.pbs"
echo "+ creating: $REDUCTION_PLOTS_PBS"
cat $REDUCTION_PLOTS_PBS_TEMPLATE | \
        sed "s/-N \w*/-N reduction_plots_$NUM_COMPONENTS/" | \
        sed "s,\bSCRIPTS=template,SCRIPTS=$SCRIPTS," | \
        sed "s,\bPROCESSED_INSOLATION_FILE=template,\
PROCESSED_INSOLATION_FILE=$PROCESSED_INSOLATION_FILE," | \
        sed "s,\bREDUCTION_MAP_FILE=template,REDUCTION_MAP_FILE=$REDUCTION_MAP_FILE," | \
        sed "s/\bNUM_EIGEN=template/NUM_EIGEN=$NUM_EIGEN/" | \
        sed "s/\bNUM_TRACE=template/NUM_TRACE=$NUM_TRACE/" | \
        sed "s/\bNUM_COMP_TRACE=template/NUM_COMP_TRACE=$NUM_COMP_TRACE/" | \
        sed "s/\bEIGEN_COLORBAR_FONTSIZE=template/\
EIGEN_COLORBAR_FONTSIZE=$EIGEN_COLORBAR_FONTSIZE/" | \
        sed "s/\bEVOLUTION_FONTSIZE=template/\
EVOLUTION_FONTSIZE=$EVOLUTION_FONTSIZE/" \
> $REDUCTION_PLOTS_PBS
printf "+ submiting $REDUCTION_PLOTS_PBS"
REDUCTION_PLOTS_ID=`submit_after $REDUCTION_PLOTS_PBS $REDUCE_ID`
printf " job id: $REDUCTION_PLOTS_ID\n"
fi
print_bar '='
echo ""


echo "TRAINING THE GROUND MODEL"
print_bar '='
echo ""
echo "+ checking: GROUND_INSOLATION_FILE=$GROUND_INSOLATION_FILE"
echo "+ checking: FINAL_DATE=$FINAL_DATE"
echo "+ checking: GROUND_NUM_OBS=$GROUND_NUM_OBS"
echo "+ checking: GROUND_LAG=$GROUND_LAG"
echo "+ checking: GROUND_KERNEL=$GROUND_KERNEL"
echo "+ checking: $GROUND_MODEL_FILE"
if [ ! -f $GROUND_MODEL_FILE ]
then
    echo "+ I did not find: $GROUND_MODEL_FILE"
    echo "+ I will submit a job to compute it"
    GROUND_MODEL_PBS="ground_model_${GROUND_NUM_OBS}_$GROUND_KERNEL.pbs"
    cat $GROUND_MODEL_PBS_TEMPLATE |
        sed "s/-N \w*/-N ground_model_${GROUND_NUM_OBS}_$GROUND_KERNEL/" | \
        sed "s,\bSCRIPTS=template,SCRIPTS=$SCRITPS," | \
        sed "s,\bGROUND_INSOLATION_FILE=template,\
GROUND_INSOLATION_FILE=$GROUND_INSOLATION_FILE," | \
        sed "s/\bFINAL_DATE=template/FINAL_DATE=\"$FINAL_DATE\"/" | \
        sed "s/\bGROUND_NUM_OBS=template/GROUND_NUM_OBS=$GROUND_NUM_OBS/" | \
        sed "s/\bGROUND_LAG=template/GROUND_LAG=$GROUND_LAG/" | \
        sed "s/\bGROUND_KERNEL=template/GROUND_KERNEL=$GROUND_KERNEL/" | \
        sed "s,\bGROUND_MODEL_FILE=template,\
GROUND_MODEL_FILE=$GROUND_MODEL_FILE," \
    > $GROUND_MODEL_PBS
    printf "+ submiting $GROUND_MODEL_PBS..."
    GROUND_MODEL_ID=`submit $GROUND_MODEL_PBS`
    printf " job id: $GROUND_MODEL_ID\n"
else
    echo "+ nothing to do"
fi
print_bar '='
echo ""


echo "PREDICTIONS"
print_bar '='
if $DO_ALL_PREDICTION_DATES
then
    printf "+ generating prediction dates..."
    if [ -f .prediction_dates ]
    then
        out=`cat .prediction_dates`
    else
        out=`python $SCRIPTS/extract_prediction_dates.py \
                    --lag $LAG \
                    -i $PROCESSED_INSOLATION_FILE \
                    --start-date="$FINAL_DATE"`
        echo $out > .prediction_dates
    fi
    printf " found: `echo $out | wc -w`\n"
    declare -a TMP=($out)
    PREDICTION_DATES=()
    for p in "${TMP[@]}"
    do
        PREDICTION_DATES=("${PREDICTION_DATES[@]}" "${p/_/ }")
    done
fi
if [ -z "$PREDICTION_DATES" ]
then
    echo "+ nothing to do (PREDICTION_DATES list is empty)"
fi
for PREDICTION_DATE in "${PREDICTION_DATES[@]}"
do
    echo "$PREDICTION_DATE"
    print_bar '~'
    NICE_PREDICTION_DATE=`nice_date "$PREDICTION_DATE"`
    PREDICTION_DIR="$OUT_DIR/$NICE_PREDICTION_DATE" 
    if [ ! -d $PREDICTION_DIR ]
    then
        echo "+ creating: $PREDICTION_DIR"
        mkdir $PREDICTION_DIR
    fi
    echo "+ doing satelite prediction"
    SATELLITE_PREDICTION_FILE="satelite_prediction.pcl"
    SATELLITE_PREDICTION_FILE_FULL="$PREDICTION_DIR/$SATELLITE_PREDICTION_FILE"
    echo "+ checking: $SATELLITE_PREDICTION_FILE_FULL"
    if [ -f $SATELLITE_PREDICTION_FILE_FULL ]
    then
        echo "+ satelite prediction file exists, I am skiping it"
        SATELLITE_PREDICTION_ID=""
    else
        SATELLITE_PREDICTION_PBS="sat_predict_$NICE_PREDICTION_DATE.pbs"
        cat $SATELLITE_PREDICTION_PBS_TEMPLATE | \
            sed "s/nodes=[0-9]*/nodes=$PREDICTION_NUM_NODES/" | \
            sed "s/\bNUM_CORES=[0-9]*/NUM_CORES=$PREDICTION_NUM_CORES/" | \
            sed "s,\bSCRIPTS=template,SCRIPTS=$SCRIPTS," | \
            sed "s,\bPROCESSED_INSOLATION_FILE=template,\
PROCESSED_INSOLATION_FILE=$PROCESSED_INSOLATION_FILE," | \
            sed "s,\bREDUCTION_MAP_FILE=template,\
REDUCTION_MAP_FILE=$REDUCTION_MAP_FILE," | \
            sed "s,\bDYNAMICS_FILE=template,\
DYNAMICS_FILE=$DYNAMICS_FILE," | \
            sed "s/\bNUM_AHEAD=template/NUM_AHEAD=$NUM_AHEAD/" | \
            sed "s/\bNUM_SAMPLES=template/NUM_SAMPLES=$NUM_SAMPLES/" | \
            sed "s/PREDICTION_DATE=template/\
PREDICTION_DATE=\"$PREDICTION_DATE\"/"  | \
            sed "s,OUTPUT_FILE=template,\
OUTPUT_FILE=$SATELLITE_PREDICTION_FILE_FULL," | \
            sed "s/-N \w*/-N sat_predict_$NICE_PREDICTION_DATE/" \
        > $SATELLITE_PREDICTION_PBS
        printf "+ submiting $SATELLITE_PREDICTION_PBS..."
        SATELLITE_PREDICTION_ID=`submit_after $SATELLITE_PREDICTION_PBS $DYNAMICS_ID`
        printf " job id: $SATELLITE_PREDICTION_ID\n"
    fi
    echo "+ doing ground prediction"
    GROUND_PREDICTION_FILE_FULL="$PREDICTION_DIR/$GROUND_PREDICTION_FILE" 
    echo "+ checking: $GROUND_PREDICTION_FILE_FULL"
    if [ -f $GROUND_PREDICTION_FILE_FULL ]
    then
        echo "+ ground prediction file exists, I am skiping it"
        GROUND_PREDICTION_ID=""
    else
        GROUND_PREDICTION_PBS="ground_predict_$NICE_PREDICTION_DATE.pbs"
        cat $GROUND_PREDICTION_PBS_TEMPLATE | \
            sed "s,\bSCRIPTS=template,SCRIPTS=$SCRIPTS," | \
            sed "s,\bGROUND_INSOLATION_FILE=template,\
GROUND_INSOLATION_FILE=$GROUND_INSOLATION_FILE," | \
            sed "s,\bGROUND_MODEL_FILE=template,\
GROUND_MODEL_FILE=$GROUND_MODEL_FILE," | \
            sed "s/\bNUM_AHEAD=template/NUM_AHEAD=$NUM_AHEAD/" | \
            sed "s/\bNUM_SAMPLES=template/NUM_SAMPLES=$NUM_SAMPLES/" | \
            sed "s/PREDICTION_DATE=template/\
PREDICTION_DATE=\"$PREDICTION_DATE\"/"  | \
            sed "s,OUTPUT_FILE=template,\
OUTPUT_FILE=$GROUND_PREDICTION_FILE_FULL," | \
            sed "s/-N \w*/-N ground_predict_$NICE_PREDICTION_DATE/" \
        > $GROUND_PREDICTION_PBS
        printf "+ submiting $GROUND_PREDICTION_PBS..."
        GROUND_PREDICTION_ID=`submit_after $GROUND_PREDICTION_PBS $GROUND_MODEL_ID`
        printf " job id: $GROUND_PREDICTION_ID\n"
    fi
    echo "+ doing he plots"
    PREDICTION_PLOTS_PBS="prediction_plots_$NICE_PREDICTION_DATE.pbs"
    cat $PREDICTION_PLOTS_PBS_TEMPLATE | \
        sed "s/-N \w*/-N prediction_plots_$NICE_PREDICTION_DATE/" | \
        sed "s,\bSCRIPTS=template,SCRIPTS=$SCRIPTS," | \
        sed "s,\bREDUCTION_MAP_FILE=template,\
REDUCTION_MAP_FILE=$REDUCTION_MAP_FILE," | \
        sed "s,\bPROCESSED_INSOLATION_FILE=template,\
PROCESSED_INSOLATION_FILE=$PROCESSED_INSOLATION_FILE," | \
        sed "s,\bSATELLITE_PREDICTION_FILE=template,\
SATELLITE_PREDICTION_FILE=$SATELLITE_PREDICTION_FILE_FULL," | \
        sed "s,\bGROUND_INSOLATION_FILE=template,\
GROUND_INSOLATION_FILE=$GROUND_INSOLATION_FILE," | \
        sed "s,\bGROUND_PREDICTION_FILE=template,\
GROUND_PREDICTION_FILE=$GROUND_PREDICTION_FILE_FULL," | \
        sed "s/\bCOLORBAR_FONTSIZE=template/COLORBAR_FONTSIZE=$COLORBAR_FONTSIZE/" | \
        sed "s,\bOUTPUT_DIR=template,\
OUTPUT_DIR=$PREDICTION_DIR," \
    > $PREDICTION_PLOTS_PBS
    printf "+ submiting $PREDICTION_PLOTS_PBS..."
    if [ -n "$SATELLITE_PREDICTION_ID" ] || [ -n "$GROUND_PREDICTION_ID" ]
    then
        AFTER="$SATELLITE_PREDICTION_ID:$GROUND_PREDICTION_ID"
    else
        AFTER=""
    fi
    PREDICTION_PLOTS_ID=`submit_after $PREDICTION_PLOTS_PBS $AFTER`
    printf " job id: $PREDICTION_PLOTS_ID\n"
    print_bar '~'
    echo ""
done
print_bar '='
echo ""

# Make the scatter plots
echo "SCATTER PLOTS"
print_bar '='
echo "+ checking: SKIP_SCATTER_PLOTS=$SKIP_SCATTER_PLOTS"
if $SKIP_SCATTER_PLOTS
then
    echo "+ nothing to do"
else
    SCATTER_PLOTS_PBS="scatter_plots.pbs"
    cat $SCATTER_PLOTS_PBS_TEMPLATE | \
        sed "s,\bSCRIPTS=template,SCRIPTS=$SCRIPTS," | \
        sed "s,\bPROCESSED_INSOLATION_FILE=template,\
PROCESSED_INSOLATION_FILE=$PROCESSED_INSOLATION_FILE," | \
        sed "s,\bREDUCTION_MAP_FILE=template,\
REDUCTION_MAP_FILE=$REDUCTION_MAP_FILE," | \
        sed "s,\bOUTPUT_DIR=template,OUTPUT_DIR=$OUT_DIR," \
    > $SCATTER_PLOTS_PBS
    printf "+ submitting $SCATTER_PLOTS_PBS..."
    SCATTER_PLOTS_ID=`submit_after $SCATTER_PLOTS_PBS $REDUCE_ID`
    printf "job id: $SCATTER_PLOTS_ID\n"
fi
print_bar '='
echo ""

# Make the quantile plots
echo "QUANTILE PLOTS"
print_bar '='
echo "+ checking: SKIP_QUANTILE_PLOT=$SKIP_QUANTILE_PLOT"
if $SKIP_QUANTILE_PLOT
then
    echo "+ nothing to do"
else
for n in {2..17}
do
SATELLITE_ERRORS_PBS="compute_errors_$n.pbs" 
cat $SATELLITE_ERRORS_PBS_TEMPLATE | \
    sed "s/-N \w*/-N sat_compute_errors_$n/" | \
    sed "s,\bSCRIPTS=template,SCRIPTS=$SCRIPTS," | \
    sed "s,\bPROCESSED_INSOLATION_FILE=template,\
PROCESSED_INSOLATION_FILE=$PROCESSED_INSOLATION_FILE," | \
    sed "s/\bSTART_DATE=template/\
START_DATE=\"$FINAL_DATE\"/"  | \
    sed "s/\bLAG=template/LAG=$LAG/" | \
    sed "s,\bOUTPUT_DIR=template,OUTPUT_DIR=$OUT_DIR," | \
    sed "s,\bPREDICTION_FILE=template,PREDICTION_FILE=satelite_prediction.pcl," | \
    sed "s,\bNUM_AHEAD=template,NUM_AHEAD=$n," \
> $SATELLITE_ERRORS_PBS
printf "+ submiting $SATELLITE_ERRORS_PBS..."
#SATELLITE_ERRORS_ID=`submit_after_any $SATELLITE_ERRORS_PBS $DYNAMICS_ID`
printf " job id: $SATELLITE_ERRORS_ID\n"
done

for n in {2..17}
do
    GROUND_ERRORS_PBS="ground_compute_errors_$n.pbs" 
    cat $SATELLITE_ERRORS_PBS_TEMPLATE | \
        sed "s/-N \w*/-N ground_compute_errors_$n/" | \
        sed "s,\bSCRIPTS=template,SCRIPTS=$SCRIPTS," | \
        sed "s,\bPROCESSED_INSOLATION_FILE=template,\
PROCESSED_INSOLATION_FILE=$GROUND_INSOLATION_FILE," | \
        sed "s/\bSTART_DATE=template/\
START_DATE=\"$FINAL_DATE\"/"  | \
        sed "s/\bLAG=template/LAG=$LAG/" | \
        sed "s,\bOUTPUT_DIR=template,OUTPUT_DIR=$OUT_DIR," | \
        sed "s,\bPREDICTION_FILE=template,PREDICTION_FILE=ground_prediction.pcl," | \
        sed "s,\NUM_AHEAD=template,NUM_AHEAD=$n," | \
        sed "s/compute_errors\.py/ground_compute_errors\.py/" \
    > $GROUND_ERRORS_PBS
    printf "+ submiting $GROUND_ERRORS_PBS..."
    GROUND_ERRORS_ID=`submit_after_any $GROUND_ERRORS_PBS $GROUND_MODEL_ID`
    printf " job id: $GROUND_ERRORS_ID\n"
done
fi
print_bar '='
echo ""

echo "MOVIES"
print_bar '='
echo "+ checking: SKIP_MOVIES=$SKIP_MOVIES"
if $SKIP_MOVIES
then
    echo "+ nothing to do"
else
   ./make_all_movies.sh $OUT_DIR
   ./submit_all_movie_jobs.sh 
fi
