#!/bin/bash
# Make all movies
OUTPUT_DIR=$1

# Delay between frames
DELAY=100

# Create a file that contains all the commands for computing the movies
cat /dev/null > all_movies_cmds.sh

# Loop over all dates
for DIR in $OUTPUT_DIR/*
do
    if [ -d $DIR ]
    then
        DATE=${DIR##*/}
        echo $DATE
        # Make all the movies
        echo "Making movies for: $DATE"
        cat >> all_movies_cmds.sh <<EOL
convert -delay $DELAY $DIR/*_k_mean.png $DIR/k_mean.gif
convert -delay $DELAY $DIR/*_k_std.png $DIR/k_std.gif
convert -delay $DELAY $DIR/*_err.png $DIR/err.gif
convert -delay $DELAY $DIR/*_err_r.png $DIR/err_r.gif
EOL
    fi
done
