#!/bin/bash
# Submit all movie jobs
if [ ! -d movie_jobs ]
then
    mkdir movie_jobs
fi
counter=1
while read CMD; do
    LAST=`echo $CMD | wc -w`
    output=`echo $CMD | cut -d " " -f $LAST`
    echo $output
    if [ ! -f $output ]
    then
    cat > movie_jobs/job_$counter.pbs <<EOL
##!/bin/sh
#PBS -N movie_job_$counter
#PBS -l nodes=1:ppn=8
#PBS -l walltime=00:02:00
#PBS -j oe

cd \$PBS_O_WORKDIR

$CMD
EOL
    qsub movie_jobs/job_$counter.pbs
    fi
    let counter=counter+1
done < all_movies_cmds.sh
