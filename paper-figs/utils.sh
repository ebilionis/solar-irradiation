#!/bin/bash
# Some generic bash functions needed by the scripts

# An echo that writes on standar error
function echoerr() {
    echo $1 1>&2
}

# Run a command and check its output for failure
function run() {
    echo "RUNNING: $@"
    eval $@
    code=$?
    if [ $code -ne 0 ]
    then
        echoerr "***FAILURE***"
        exit $code        
    else
        echo "***SUCCESS***"
    fi
}

# A function that extracts the real path of a file
function realpath() {
    echo $(cd $(dirname $1); pwd)/$(basename $1)
}

# A fuction that extracts the extension of a file
function extension() {
    filename=`realpath $1`
    echo ${filename##*.}
}

# A function that appends a string to the basename (without the extension)
# of a file
function append() {
    ext=`extension $1`
    base=`basename $1`
    basename_wout_ext=${base%.*}
    new_basename="${basename_wout_ext}_$2.$ext"
    echo $(dirname $1)/$new_basename
}

# A function that swaps the extension of a file
function swap_extension() {
    base=`basename $1`
    basename_wout_ext=${base%.*}
    new_basename="${basename_wout_ext}.$2"
    echo $(dirname $1)/$new_basename
}

# A function that extracts the master node
function rootnode() {
    echo `cat $PBS_NODEFILE | head -1`
}

# A function that copies data to the $TMPDIR of the master node
function copy_to_master() {
    root=`rootnode`
    cmd="scp -rpq $1 $root:$TMPDIR/$2"
    run $cmd
}

# A function that copies data from the $TMPDIR of the master node to GPFS
function copy_from_master() {
    root=`rootnode`
    cmd="scp -rpq $root:$TMPDIR/$1 $2"
    run $cmd
}

# Submit a job and return the id
function submit() {
    out=`qsub $1`
    code=$?
    if [ $code -ne 0 ]
    then
        echoerr "***FAILURE***"
        exit $code
    fi
    echo $out | cut -d "." -f 1
}

# Submit after a job that is running
function submit_after() {
    if [ $# -eq 2 ]
    then
        out=`qsub -W depend=afterok:$2 $1`
    else
        out=`qsub $1`
    fi
    echo $out | cut -d "." -f 1
}

# Submit after a job exits with any status
function submit_after_any() {
    if [ $# -eq 2 ]
    then
        out=`qsub -W depend=afterany:$2 $1`
    else
        out=`qsub $1`
    fi
    echo $out | cut -d "." -f 1
}


# Turn a date to a nice script for use in filenames
function nice_date() {
    echo $1 | sed "s/-/_/g" | sed "s/://" | sed "s/ /_/"
}

# Check if file exists and exit if it does not
function check_file() {
    if [ ! -f $1 ]
    then
        echo "*** FILE NOT FOUND: $1"
        exit 1
    fi
}

# Prints a bar with a particular character
function print_bar() {
    printf "$1%0.s" {1..80}
    printf '\n'
}

# Export these functions
#export -f echoerr
#export -f realpath
#export -f extension
#export -f swap_extension
#export -f append
#export -f rootnode
#export -f copy_to_master
#export -f copy_from_master
#export -f run
#export -f submit
#export -f check_file
#export -f print_bar
