#!/bin/bash
# Uploads data to the repository
. input.sh
DATA_DIR=${REDUCTION_MAP}_$NUM_COMPONENTS
git add -f $DATA_DIR/*
git commit -am "new results"
git push origin fusion
