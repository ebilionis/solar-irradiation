# Run everything required for all kernels given the number of PCA components
# Run it from the main directory of the project like this:
#   $ bash paper-figs/run_all_for_pca.sh <num_comp>

if [ $# -lt 1 ]
then
    echo "This script requires exactly one argument: the number of PCA components"
    exit 1
fi

# Get the number of PCA components you want to do from the command line
num_comp=$1

# List the kernels you want to try here
#declare -a kernels=("rbf"\
#                    "exponential"\
#                    "Matern32"\
#                    "Matern52")
declare -a kernels=("exponential")

# Loop over the kernels
for kernel in "${kernels[@]}"
do
    bash paper-figs/run_all_for_pca_and_kernel.sh $num_comp $kernel
done
