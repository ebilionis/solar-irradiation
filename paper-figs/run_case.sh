# Run everything required to create the figures for a particular number of components
# and a kernel.
# Run it from the main directory of the project like this:
#   $ bash paper-figs/run_all_for_pca_and_kernel.sh <num_comp> <kernel>
#
# The python scripts that are called by this bash script are:
# + ./scripts/sat_reduce.py:             Constructs the reduced representation
# + ./scripts/sat_learn_dynamics.py:     Learns the reduced dynamics.
# + ./scripts/sat_reduction_map_plot.py: Makes the plots related to the reduction map.
# + ./scripts/sat_predict.py:            Makes predictions using the model.

if [ $# -lt 2 ]
then
    echo "This script requires exactly two arguments:"
    echo "  Number of Reduction Map components (1, 2, 3, 4, etc.)"
    echo "  Kernel name (rbf, exponential, Matern32, Matern52, etc.)"
    exit 1
fi

# PARAMETERS READ FROM COMMAND LINE
# ---------------------------------
# Number of components (1, 2, 3, 4, etc.)
num_comp=$1

# The kernel of the recursive GP (rbf, exponential, Matern32, Matern52, etc.)
kernel=$2

# ---------------------------------

# PARAMETERS SPECIFIED HERE
# -------------------------------------------------------------
# Satellite Data Filename
input_filename='data/insolation_ok_400x400.h5'

# End Date of observations (UTC)
final_date="2014-02-25 20:00"

# The reduction map you wish to use (PCA, FactorAnalysis, etc.)
reduction_map="FactorAnalysis"

# Create a directory for the run
out_dir="paper-figs/${reduction_map}_${num_comp}"

# The lag of the dynamic model
lag=2

# The degree of the polynomials you want to use (for the mean)
degree=1

# The model you want to use for the GP
# (IndependentOutputGaussianProcess, MultioutputGaussianProcess, etc.)
gp_model="IndependentOutputGaussianProcess"

# For doing the Reduction Map plots
num_eigen=$num_comp
num_trace=50
num_comp_trace=4

# Prediction dates (All the predictions we can make)
# They must be an array, declared as follows
#declare -a pre_dates=("2014-02-26 16:00" "2014-02-26 19:00"\
#                      "2014-02-27 16:00" "2014-02-27 19:00"\
#                      "2014-02-28 16:00" "2014-02-28 19:00"\ 
#                      "2014-03-01 16:00" "2014-03-01 19:00"\
#                      "2014-03-02 16:00" "2014-03-02 19:00"\
#                      "2014-03-03 16:00" "2014-03-03 19:00"\
#                      "2014-03-04 16:00" "2014-03-04 19:00"\
#                      "2014-03-05 16:00" "2014-03-05 19:00")
#declare -a pre_dates=("2014-03-01 19:00")
declare -a pre_dates=("2014-02-27 16:00" "2014-03-01 16:00")

# The number of samples to use for predictions
num_samples=1000

# The number of steps ahead to look at
num_ahead=6

# The ground based GP model for the comparison plots
# (this must be trained separately at the moment)
gp_ground_model="data/ground_ok_gp_2_350_rbf.pcl"

# -------------------------------------------------------------

# PRINT INFO ABOUT WE ARE GOING TO DO
# -------------------------------------------------------------
echo "Input data: $input_filename"
echo "Number of Reduction Map coefficients: $num_comp"
echo "Final date of observations: $final_date"
echo "I will be writing data here: $out_dir"
echo "I will doing predictions on the following dates/times:"
echo ${pre_dates[@]}
echo "Number of steps ahead: $num_ahead"
echo "Number of samples taken for the statistics: $num_samples"
echo "Ground-based GP model: $gp_ground_model"
# -------------------------------------------------------------

# SANITY CHECK
# -------------------------------------------------------------
# Make sure input file exists
if [ ! -f $input_filename ]
then
    echo "I couldn't fine $input_filename"
    exit 1
fi

# Make sure output directory exists
if [ ! -d $out_dir ]
then
    echo "Creating: $out_dir"
    mkdir $out_dir
fi
# --------------------------------------------------------------

# DO Reduction Map
# --------------------------------------------------------------
# The filename of the reduction map
reduction_map_filename="${out_dir}/${reduction_map}.pcl"

# Construct Reduction Map only if the output filename does not exist
if [ ! -f $reduction_map_filename ]
then
    echo "Doing Reduction Map"
    python scripts/sat_reduce.py -i $input_filename \
        --num-comp=$num_comp \
        --final-date="$final_date" \
        -o $reduction_map_filename
    if [ ! -f $reduction_map_filename ]
    then
        echo "Something went wrong with the Reduction Map..."
        exit 1
    fi
    echo "Reduction Map was written here: $reduction_map_filename"
else
    echo "Skipping construction of Reduction Map since I found this: $reduction_map_filename"
fi
# --------------------------------------------------------------

# DO THE GP
# --------------------------------------------------------------
gp_filename="${out_dir}/${reduction_map}_${gp_model::1}_D=${degree}_gp_L=${lag}_${kernel}.pcl"
if [ ! -f ${gp_filename} ]
then
    echo "Doing the GP"
    python scripts/sat_learn_dynamics.py -i $reduction_map_filename \
        --lag=$lag \
        --kernel=$kernel \
        --final-date="$final_date" \
        --gp=$gp_model \
        --degree=$degree
    if [ ! -f $gp_filename ]
    then
        echo "Something went wrong with the GP..."
        exit 1
    fi
    echo "GP was written here: $gp_filename"
else
    echo "Skipping construction of GP since I found this: $gp_filename"
fi
# --------------------------------------------------------------


# DO THE Reduction Map PLOTS
# --------------------------------------------------------------
echo "Doing the Reduction Map plots:"
echo $reduction_map_filename
python scripts/sat_reduction_map_plot.py -i $reduction_map_filename \
        --num-eigen=$num_eigen \
        --num-trace=$num_trace \
        --num-comp-trace=$num_comp_trace
# --------------------------------------------------------------


# DO THE GP PLOTS
# --------------------------------------------------------------
echo "Doing the GP plots:"
# The list of files that should be there
declare -a png_files=("compare.png")
for start_date in "${pre_dates[@]}"
do
    tmp1=${start_date//-/_}
    tmp2=${tmp1/ /_}
    tmp3=${tmp2//:/}
    prediction_out_dir="${out_dir}/${gp_model::1}_${kernel}_D=${degree}/${tmp3}"
    echo "Doing: $start_date"
    if [ ! -d $prediction_out_dir ]
    then
        echo "Making: $prediction_out_dir"
        mkdir -p $prediction_out_dir
    fi
    echo "Running script that creates the plots."
    python scripts/sat_predict.py -i $gp_filename \
            -g $gp_ground_model \
            --start-date="${start_date}" \
            --num-ahead=${num_ahead} \
            --num-samples=${num_samples} \
            --prefix=${prediction_out_dir}
    if [ $? -ne 0 ]
    then
        echo "Something went wrong with the script that creates the plots."
        exit 1
    fi
done
# --------------------------------------------------------------

# DO THE GOODNESS OF FIT PLOTS
# --------------------------------------------------------------
echo "Doing the goodness of fit plots:"
python scripts/sat_goodness_of_fit.py -i $gp_filename \
        --start-date="${final_date}" \
        --num-ahead=1 \
        --num-samples=100 \
        --prefix=${prediction_out_dir}
if [ $? -ne 0 ]
then
    echo "Something went wrong with the goodness of fit plots."
    exit 1
fi
# --------------------------------------------------------------
