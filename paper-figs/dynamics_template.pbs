##!/bin/sh
#PBS -N dynamics
#PBS -l nodes=1:ppn=8
#PBS -l walltime=2:00:00
#PBS -j oe

cd $PBS_O_WORKDIR

# Load some bash functions
. utils.sh

# The scripts directory
SCRIPTS=template

# The number of cores to be use
NUM_CORES=8

# The processed insolation file
PROCESSED_INSOLATION_FILE=template

# The reduction map file
REDUCTION_MAP_FILE=template

# The dynamics file (output)
DYNAMICS_FILE=template

# The kernel to be used
KERNEL=template

# The gp model to be used
GP_MODEL=template

# The polynomial degree used for the trend
DEGREE=template

# The lag used
LAG=template

# The final date of observations
FINAL_DATE=template

# Sanity check
check_file $PROCESSED_INSOLATION_FILE
check_file $REDUCTION_MAP_FILE

# The local insolation file
LOCAL_INSOLATION_FILE=`basename $PROCESSED_INSOLATION_FILE`

# The local reduction map file
LOCAL_REDUCTION_MAP_FILE=`basename $REDUCTION_MAP_FILE`

# The local dynamics file
LOCAL_DYNAMICS_FILE=`basename $DYNAMICS_FILE`

# Copy insolation to master
copy_to_master $PROCESSED_INSOLATION_FILE $LOCAL_INSOLATION_FILE

# Copy reduction map to master
copy_to_master $REDUCTION_MAP_FILE $LOCAL_REDUCTION_MAP_FILE

# Compute the clearness index
mpiexec -n $NUM_CORES \
     python $SCRIPTS/sat_learn_dynamics.py \
        -i $TMPDIR/$LOCAL_INSOLATION_FILE \
        --reduction-map=$TMPDIR/$LOCAL_REDUCTION_MAP_FILE \
        --kernel=$KERNEL \
        --gp=$GP_MODEL \
        --degree=$DEGREE \
        --lag=$LAG \
        --final-date=\"$FINAL_DATE\" \
        -o $TMPDIR/$LOCAL_DYNAMICS_FILE \
        --use-mpi

# Copy from the master here
copy_from_master $LOCAL_DYNAMICS_FILE $DYNAMICS_FILE
