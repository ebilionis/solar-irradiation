#!/usr/bin/env python
"""
Train an HMM using the Daily data without deseasoning.
"""

import sys
import os
sys.path.insert(0, os.path.abspath('..'))
import matplotlib.pyplot as plt
import scipy
import numpy as np
import solar
import hmm
import cPickle as pickle
import argparse


parser = argparse.ArgumentParser(description='Train an HMM to the solar data.')
parser.add_argument('--num-states', dest='num_states', type=int,
                     help='Specify the number of hidden states')
parser.add_argument('--num-symbols', dest='num_symbols', type=int,
                     help='Specify the number of emmitted symbols')
parser.add_argument('--E0', dest='E0', type=float, default=1361.,
                     help='The extraterrestial irradiance')
parser.add_argument('--how', dest='how', type=str, default='daily',
                     help='Specify how to resample the data')
parser.add_argument('--date', dest='date', type=str, default='1991',
                     help='Specify a range of dates to use in training')
parser.add_argument('--prefix', dest='prefix', type=str, default='solar',
                     help='Specify the prefix of the output files')
parser.add_argument('--out-dir', dest='out_dir', type=str, default='results',
                     help='Specify the output directory')
parser.add_argument('--verbose', dest='verbose', type=int, default=1,
                     help='Specify the verbosity level')
args = parser.parse_args()

# Number of hidden states
K = args.num_states
# Number of symbols
P = args.num_symbols
E0 = args.E0
# Prior for pi
f_pi = 1.
# Prior for A
f_A = 1.
# Prior for B
f_B = 1.
# Tolerance
tol = 1e-10
# The airmass to use
#inverse_airmass_class = solar.IdentityInverseAirMass()
inverse_airmass_class = solar.YoungInverseAirMass.get_fixed()
# Maximum number of iterations
max_iter = 10000
how = args.how
tmp = args.date.split(':')
if len(tmp) == 1:
    date = tmp[0]
else:
    date = tmp

# Load the data
train_data = solar.IrradianceData(num_symbols=P, how=how, E0=E0,
                                  inverse_airmass_class=inverse_airmass_class,
                                  date=date)
X = train_data.discrete_deseasoned

# Make the model
pi = np.random.dirichlet(np.ones(K) * f_pi)
A = np.random.dirichlet(np.ones(K) * f_A, size=(K,))
B = np.random.dirichlet(np.ones(P) * f_B, size=(K,))
model = hmm.DiscreteHMM(pi, A, B)
loglike = model.train(X=X, tol=1e-6, max_iter=max_iter, verbose=args.verbose)
filename = os.path.join(args.out_dir, args.prefix + '_' + how + '_K=' + str(K) + '_P=' + str(P) + '.pickle')
print 'Writing:', filename
with open(filename, 'wb') as fd:
    results = {}
    results['model'] = model
    results['loglike'] = loglike
    results['inverse_airmass'] = train_data.inverse_airmass_class
    results['E0'] = train_data.E0
    results['how'] = train_data.how
    pickle.dump(results, fd)
