"""
Draw some satellite data just to see how they look.
"""


from pyhdf.SD import SD, SDC
import matplotlib.pyplot as plt
from satellite import CLAVRxFile


# Open the hdf file
f = CLAVRxFile('../data/goes13_2013_334_0232.1km.level2.hdf')
f.plot('cloud_fraction')
plt.title('Cloud Fraction')
plt.savefig('conus_cloud_fraction.png')
