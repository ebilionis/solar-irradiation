"""
General implementation of satellite models.

This is an effort to organize things a little bit so that we can try lots of
different things fast.

Author:
    Ilias Bilionis

Date:
    3/12/2014
"""


import pandas as pd
import tables as pt
import os
import sys
import numpy as np
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import *
from observer import Observer
from dates import get_good_dates_with_lag
from dates import str_to_datetime
from dates import get_closest_date
from input_output import extract_patch_data
from input_output import load_pickled_obj
import cPickle as pickle
import warnings
warnings.filterwarnings("ignore")
import GPy
import orthpol
import scipy.stats
import pymcmc as pm


class SatelliteModel(object):

    """
    The base satellite model.

    :param lag:       The lag of the model.
    :type lag:        int
    :param reduction_map:       The PCA we are going to use to reduce the dimensionality
                      of the data.
    :param name:      A name for the model.
    :type name:       str
    """

    # The latitudes of the patch
    _lat = None

    # The longitudes of the patch
    _lon = None

    # An observer object at the centre of the patch
    _observer = None

    # The lag
    _lag = None

    # The MPI communicator
    _comm = None

    # The reduction map file (must be set necessarily through the property)
    _reduction_map_file = None

    # The reduction map itself
    _reduction_map = None

    def _get_h5_data(self, node):
        """
        Get and broadcast H5 data.
        """
        if self.rank == 0:
            f = pt.open_file(self.input_file, 'r')
            data = f.get_node(node)[:]
            f.close()
        else:
            data = None
        if self.use_mpi:
            data = self.comm.bcast(data)
        return data

    @property
    def num_comp(self):
        """
        Get the number of PCA components.
        """
        return self.reduction_map.n_components

    @property
    def latitude(self):
        """
        Get the latitudes of the patch.
        """
        if self._lat is None:
            self._lat = self._get_h5_data('/latitude')
        return self._lat

    @property
    def longitude(self):
        """
        Get the longitudes of the patch.
        """
        if self._lon is None:
            self._lon = self._get_h5_data('/longitude')
        return self._lon

    @property
    def patch_shape(self):
        """
        Get the patch shape.
        """
        return self.latitude.shape

    @property
    def i0(self):
        """
        Get the index of the row of the centre of the patch.
        """
        return self.latitude.shape[0] / 2

    @property
    def j0(self):
        """
        Get the index of the column of the centre of the patch.
        """
        return self.latitude.shape[1] / 2

    @property
    def input_file(self):
        """
        Get the input file. We read it from the reduction_map object.
        """
        return self.insolation_file

    @property
    def observer(self):
        """
        Get an observer object at the centre of the patch.
        """
        if self._observer is None:
            self._observer = Observer(latitude=self.latitude[self.i0, self.j0],
                                      longitude=self.longitude[self.i0, self.j0])
        return self._observer

    @property
    def use_mpi(self):
        """
        Are we using MPI?
        """
        return not self._comm == None

    @property
    def comm(self):
        """
        The MPI communicator.
        """
        return self._comm

    @comm.setter
    def comm(self, value):
        """
        Set the MPI communicator.
        """
        if value is None:
            self._comm = None
            self._rank = 0
            self._size = 1
        else:
            self._comm = value
            self._rank = self.comm.Get_rank()
            self._size = self.comm.Get_size()
            np.random.seed(314159 * (self.rank + 1))

    @property
    def rank(self):
        """
        Get the rank of the process owing the object.
        """
        return self._rank

    @property
    def size(self):
        """
        Get the number of processes.
        """
        return self._size

    @property
    def reduction_map_file(self):
        """
        :getter:    The reduction map file.
        :setter:    The reduction map file.
        """
        return self._reduction_map_file

    @reduction_map_file.setter
    def reduction_map_file(self, value):
        """
        Set the reduction map file.
        """
        self._reduction_map_file = value
        self._reduction_map = load_pickled_obj(self.reduction_map_file, self.comm)

    @property
    def reduction_map(self):
        """
        :getter:    The reduction map.
        """
        return self._reduction_map

    def __init__(self, reduction_map_file=None,
                 insolation_file=None,
                 lag=2, name='Base Sateliite Model',
                 comm=None, **kwargs):
        """
        Initialize model.
        """
        assert reduction_map_file is not None
        self.comm = comm
        self.reduction_map_file = reduction_map_file
        self.insolation_file = insolation_file
        assert os.path.exists(self.insolation_file)
        self.__name__ = str(name)
        self.lag = int(lag)

    def __getstate__(self):
        """
        For pickling purposes.
        """
        state = {}
        state['name'] = self.__name__
        state['lag'] = self.lag
        state['scaler'] = self.scaler
        return state

    def __setstate__(self, state):
        """
        For pickling purposes.
        """
        self.__name__ = state['name']
        self.lag = state['lag']
        self.scaler = state['scaler']
        self.comm = None

    def fit(self, start_date=None, final_date=None):
        """
        Train the model on the data we have.
        If the final date is not specified, then we use all data.
        Otherwise, we use all data up to that date. If the date does not
        contain a specific timezone, then we assume that it is in UTC.

        :param start_date:  The first date we start training on.
        :type start_date:   str or datetime
        :param final_date:  The day we stop training on.
        :type final_date:   str or datetime
        """
        # Extract the clearness index up to the final_date
        if self.rank == 0:
            f = pt.open_file(self.input_file, 'r')
            clearness_indices, dates = extract_patch_data(f, 'clearness_index',
                                                          start_date=start_date,
                                                          final_date=final_date)
            f.close()
            # Get the reduced observations
            Z = self.reduction_map.transform(clearness_indices)
            # Figure out the scaling we are going to perform
            self.scaler = StandardScaler().fit(Z)
            # Get the scaled reduced observations
            s_Z = self.scaler.transform(Z)
            lag_idx = get_good_dates_with_lag(dates, self.lag)
            print ('number of observations satisfying a lag ' +
                   str(self.lag) +  ' requirement: ' + str(lag_idx.shape[0]))
            print 'actual start date:', dates[lag_idx[0, 0]]
            print 'actual end date:', dates[lag_idx[-1, -1]]
            # Get the X and the Y on which are are going to train
            X = np.array([np.hstack([s_Z[j, :] for j in lag_idx[i, :-1]])
                          for i in xrange(lag_idx.shape[0])])
            Y = np.array([s_Z[lag_idx[i, -1], :] for i in xrange(lag_idx.shape[0])])
        else:
            self.scaler = None
            X = None
            Y = None
        if self.use_mpi:
            self.scaler = self.comm.bcast(self.scaler)
            X = self.comm.bcast(X)
            Y = self.comm.bcast(Y)
        # At this point, everybody has a scaler, X and Y
        self._do_train(X, Y)

    def _draw_initial_reduced_points(self, data, num_samples):
        """
        Draws initial reduced points that will be used to initialize the forecast.
        """
        Z = self.reduction_map.transform(data)
        if isinstance(self.reduction_map, FactorAnalysis):
            self.W = self.reduction_map.components_.T
            self.mu = self.reduction_map.mean_
            self.Psi = self.reduction_map.noise_variance_
            tmp = self.W.T / self.Psi
            self.G_inv = np.eye(self.W.shape[1]) + np.dot(tmp, self.W)
            # Now we will just take samples from the Gaussian N(Z, G_inv^{-1})
            self.L_G_inv = np.linalg.cholesky(self.G_inv)
            X = []
            for i in xrange(num_samples):
                Zr = scipy.linalg.solve_triangular(self.L_G_inv,
                                                   np.random.randn(*Z.shape).T,
                                                   trans='T', lower=True).T
                s_Z = self.scaler.transform(Z + Zr)
                X.append(np.hstack([s_Z[j, :] for j in xrange(self.lag)])[None, :])
            X = np.vstack(X)
        else:
            s_Z = self.scaler.transform(Z)
            XX = np.hstack([s_Z[j, :] for j in xrange(self.lag)])[None, :]
            X = np.vstack([XX for i in xrange(num_samples)])
        return X

    def _draw_reconstruction(self, Y):
        """
        Draw a sample from the reconstruction distribution.
        """
        Z = self.scaler.inverse_transform(Y)
        if isinstance(self.reduction_map, FactorAnalysis):
            k = np.dot(Z, self.W.T) + self.mu
            k += np.random.randn(*k.shape) * np.sqrt(self.Psi)
        else:
            k = self.reduction_map.inverse_transform(Z)
        return Z, k

    def sample(self, starting_date, num_ahead=16, num_samples=1,
               output_file=None):
        """
        Take samples from the model.

        :param starting_date:   The date to start making predictions.
        :param starting_date:   str
        :type num_ahead:        The number of steps ahead to look at.
        :type num_ahead:        int
        :param num_samples:     The number of samples to take.
        :type num_samples:      int
        :param output_file: The output file. If ``None``, no
                                output is written, but the insolation at
                                particular points is returned.
        :type output_file:  str
        """
        if self.rank == 0:
            f = pt.open_file(self.input_file, 'r')
            dates = str_to_datetime(f.root.records.cols.date)
            f.close()
        else:
            dates = None
        if self.use_mpi:
            dates = self.comm.bcast(dates)
        lag_idx = get_good_dates_with_lag(dates, self.lag - 1)
        l_num_start = get_closest_date(dates[lag_idx[:, 0]], starting_date)
        g_num_start = lag_idx[l_num_start, 0]
        # Print a message so that we now what we are doing
        if self.rank == 0:
            print 'Requested date:', starting_date
            print 'Closest date found in dataset:', dates[g_num_start]
            print 'So, as a starting point I will use the pair:'
            print '\t', dates[lag_idx[l_num_start, 0]], ',', dates[lag_idx[l_num_start, 1]]
        my_num_samples = num_samples / self.size
        if self.rank == 0:
            print 'Taking a total of ', my_num_samples * self.size, ' sample(s)'
        # Figure out the prediction dates
        dt_starting_date = dates[lag_idx[l_num_start, 0]]
        prediction_dates = pd.date_range(dt_starting_date, freq='30min',
                                         periods=num_ahead + self.lag)
        # Evaluate the clear sky model on those dates
        clear_sky_insolation = self.observer.evaluate_clear_sky_model(prediction_dates)
        # Get the data at that
        if self.rank == 0:
            f = pt.open_file(self.input_file, 'r')
            try:
                data = np.vstack([getattr(f.get_node('/data/' + g),
                                  'clearness_index')[:].flatten()
                    for g in f.root.records.cols.name[g_num_start:g_num_start+self.lag]])
            except:
                f.close()
            f.close()
        else:
            data = None
        if self.use_mpi:
            data = self.comm.bcast(data)
        my_X = self._draw_initial_reduced_points(data, my_num_samples)
        # Now let the actual model sample Y
        my_insolations = []
        my_Z = []
        my_k1 = np.zeros((num_ahead + self.lag, data.shape[1]))
        my_k2 = np.zeros(my_k1.shape)
        for i in xrange(my_num_samples):
            if self.rank == 0:
                sys.stdout.write('taking sample ' +
                            str((i + 1)* self.size).zfill(len(str(num_samples))) +
                                 ' of ' + str(num_samples) + '\n')
                sys.stdout.flush()
            Y = self._do_sample(my_X[i, :][None, :], num_ahead=num_ahead)
            Z, k = self._draw_reconstruction(Y)
            my_k1 += k
            my_k2 += k ** 2
            I = np.array(([k[i].reshape(self.patch_shape)[self.i0, self.j0]
                for i in xrange(k.shape[0])]) * clear_sky_insolation)[None, :]
            my_insolations.append(I)
            my_Z.append(Z)
        if self.rank == 0:
            sys.stdout.write('\n')
#        k1 = self.comm.reduce(my_k1)
#        k2 = self.comm.reduce(my_k2)
        k1 = np.zeros(my_k1.shape)
        k2 = np.zeros(my_k2.shape)
        import mpi4py.MPI as MPI
        self.comm.Reduce([my_k1, MPI.DOUBLE], [k1, MPI.DOUBLE])
        self.comm.Reduce([my_k2, MPI.DOUBLE], [k2, MPI.DOUBLE])
        Z = self.comm.gather(np.array(my_Z))
        insolations = self.comm.gather(np.vstack(my_insolations))
        if self.rank == 0:
            mean_k = k1 / num_samples
            std_k = np.sqrt(k2 / num_samples - mean_k ** 2)
            Z = np.concatenate(Z)
            insolations = np.vstack(insolations)
            state = {}
            state['mean_k'] = mean_k
            state['std_k'] = std_k
            state['Z'] = Z
            state['I'] = insolations
            state['dates'] = prediction_dates
            return state
        else:
            return None

    def _do_train(self, X, Y):
        """
        Train the actual surrogate on X and Y.
        """
        raise NotImplementedError('This must be implemented by deriving classes.')

    def _do_sample(self, X, num_ahead):
        """
        Do the actual sampling.
        """
        raise NotImplementedError('This must be implemented by deriving classes.')

    def __str__(self):
        """
        Get a string representation of the object.
        """
        s = 'Name: ' + self.__name__ + '\n'
        s += 'lag: ' + str(self.lag) + '\n'
        s += 'reduction_map file: ' + self.reduction_map_file
        return s

    @property
    def input_dim(self):
        """
        Get the input dimension of the recursive dynamics.
        """
        return self.num_comp * self.lag


class _SatelliteModelWithGP(SatelliteModel):

    """
    A satellite model with a polynomial basis.
    """

    def __init__(self, gp_regression_model='GPRegression',
                 kernel='rbf', kernel_ARD=True,
                 max_iters=5000, optimizer='bfgs',
                 degree=1, poly_weight_func='norm', poly_ARD=True,
                 **kwargs):
        """
        Initialize the object.
        """
        self.gp_regression_model = gp_regression_model
        self.kernel = kernel
        self.kernel_ARD = kernel_ARD
        self.max_iters = max_iters
        self.optimizer = optimizer
        self.degree = degree
        self.poly_weight_func = poly_weight_func
        self.poly_ARD = poly_ARD
        super(_SatelliteModelWithGP, self).__init__(**kwargs)
        rvs = [getattr(scipy.stats, poly_weight_func)()
               for i in xrange(self.input_dim)]
        self.poly = orthpol.ProductBasis(degree=degree, rvs=rvs)

    def _get_mean_function(self):
        """
        Construct and return the mean function.
        """
        parametrize_variance = not self.poly_ARD
        mean = pm.mean_function(self.input_dim, self.poly, ARD=self.poly_ARD,
                                parametrize_variance=parametrize_variance)
        return mean

    def _get_kernel_function(self):
        """
        Construct and return the kernel function.
        """
        return getattr(GPy.kern, self.kernel)(self.input_dim, ARD=self.kernel_ARD)

    def _get_full_kernel(self):
        """
        Returns the kernel function with the mean function.
        """
        return self._get_mean_function() + self._get_kernel_function()

    def _construct_gp_model(self, X, Y):
        """
        Construct a GP model and return it.
        """
        model = getattr(GPy.models,
                        self.gp_regression_model)(X, Y,
                                                  kernel=self._get_full_kernel())
        return model

    def _optimize_gp_model(self, model):
        """
        Optimize the GP model in place.
        """
        tmp_dir = os.path.join(os.environ['TMPDIR'], 'tmp' + str(self.rank))
        os.mkdir(tmp_dir)
        current_dir = os.getcwd()
        os.chdir(tmp_dir)
        model.optimize(optimizer=self.optimizer, max_iters=self.max_iters,
                       messages=(self.rank == 0))
        os.chdir(current_dir)

    def _construct_and_train_gp_model(self, X, Y):
        """
        Constructs and trains a GP model.
        """
        model = self._construct_gp_model(X, Y)
        self._optimize_gp_model(model)
        return model

    def __getstate__(self):
        """
        Return the state of the model.
        """
        state = super(_SatelliteModelWithGP, self).__getstate__()
        state['gp_regression_model'] = self.gp_regression_model
        state['kernel'] = self.kernel
        state['kernel_ARD'] = self.kernel_ARD
        state['max_iters'] = self.max_iters
        state['optimizer'] = self.optimizer
        state['degree'] = self.degree
        state['poly_weight_func'] = self.poly_weight_func
        state['poly'] = self.poly
        return state

    def __setstate__(self, state):
        """
        Set the state of the object.
        """
        super(_SatelliteModelWithGP, self).__setstate__(state)
        self.gp_regression_model = state['gp_regression_model']
        self.kernel = state['kernel']
        self.kernel_ARD = state['kernel_ARD']
        self.max_iters = state['max_iters']
        self.optimizer = state['optimizer']
        self.degree = state['degree']
        self.poly_weight_func = state['poly_weight_func']
        self.poly = state['poly']


class MultioutputGaussianProcess(_SatelliteModelWithGP):

    """
    A multioutput Gaussian process model.
    """

    def __init__(self, **kwards):
        """
        Initialize the model.
        """
        super(MultioutputGaussianProcess, self).__init__(**kwards)

    def _do_train(self, X, Y):
        """
        Train the model.
        """
        # Only one processor trains this model.
        if self.rank == 0:
            # The mean of the data is a simple linear function
            self.model = self._construct_and_train_gp_model(X, Y)

    def _do_sample(self, X, num_ahead):
        """
        Do the actual sampling.
        """
        num_comp = self.num_comp
        Y_p = [X[0, :num_comp][None, :], X[0, num_comp:][None, :]]
        for i in xrange(num_ahead):
            y = self.model.posterior_samples(X, 1)
            Y_p.append(y)
            X = np.hstack([X[:, num_comp:], y])
        return np.vstack(Y_p)

    def __str__(self):
        """
        Get a string representation of the object.
        """
        s = super(MultioutputGaussianProcess, self).__str__() + '\n'
        if self.rank == 0:
            s += str(self.model)
        return s

    def __getstate__(self):
        """
        Get the state for pickling purposes.
        """
        state = super(MultioutputGaussianProcess, self).__getstate__()
        state['model'] = self.model
        return state

    def __setstate__(self, state):
        """
        Set the state.
        """
        super(MultioutputGaussianProcess, self).__setstate__(state)
        self.model = state['model']


class IndependentOutputGaussianProcess(MultioutputGaussianProcess):

    """
    A multioutput Gaussian process in which each output is trained independently.
    """

    def __init__(self, **kwargs):
        """
        Initialize the object.
        """
        super(IndependentOutputGaussianProcess, self).__init__(**kwargs)

    def _do_train(self, X, Y):
        """
        Train the object.
        """
        assert self.num_comp % self.size == 0
        my_comp = self.num_comp / self.size
        my_start = self.rank * my_comp
        my_models = []
        for i in xrange(my_start, my_start + my_comp):
            my_models.append(self._construct_and_train_gp_model(X,
                                                               Y[:, i][:, None]))
        all_models = self.comm.allgather(my_models)
        self.model = [m for mm in all_models for m in mm]

    def __str__(self):
        """
        Get a string representation of the object.
        """
        s = super(IndependentOutputGaussianProcess, self).__str__() + '\n'
        if self.rank == 0:
            for i in range(self.num_comp):
                s += 'Comp: ' + str(i) + '\n'
                s += str(self.model[i]) + '\n'
        return s

    def _do_sample(self, X, num_ahead):
        """
        Do the actual sampling.
        """
        num_comp = self.num_comp
        Y_p = [X[0, :num_comp][None, :], X[0, num_comp:][None, :]]
        for i in xrange(num_ahead):
            # Fix very small variance problem
            for j in range(self.num_comp):
                if self.model[j]['noise_variance'] <= 0.:
                    self.model[j]['noise_variance'] = 1e-10
            y = np.hstack([self.model[j].posterior_samples(X, 1)
                           for j in xrange(self.num_comp)])
            Y_p.append(y)
            X = np.hstack([X[:, num_comp:], y])
        return np.vstack(Y_p)
