"""
Convert Emil's pickled data to pandas.
"""

import pandas as pd
import netCDF4 as nc
import numpy as np
from observer import Observer
from extraterrestrial_irradiance import ExtraterrestrialIrradiance
from clear_sky_model import ClearSkyIrradiance
import math
import cPickle as pickle


def read_pickled_data(filename):
    """
    Read the pickled data from ``filename``.
    :returns:   A dictionary describing the data.
    """
    time = {}
    data = {}
    with open(filename, 'rb') as fd:
        data['julian'] = np.array(pickle.load(fd), 'float64')
        data['unix'] = (data['julian'] - 2440587.5) * 86400
        time['year'] = np.array(pickle.load(fd), 'int16')
        time['month'] = np.array(pickle.load(fd), 'uint8')
        time['day'] = np.array(pickle.load(fd), 'uint8')
        time['hour'] = np.array(pickle.load(fd), 'uint16')
        time['minute'] = np.array(pickle.load(fd), 'uint8')
        data['irradiance'] = np.array(pickle.load(fd), 'float64')
        idx = (data['irradiance'] == 99999)
        data['irradiance'][idx] = 'NaN'
        data['pressure'] = np.array(pickle.load(fd), 'float64')
        idx = (data['pressure'] == 99999)
        data['pressure'][idx] = 'NaN'
        data['temperature'] = np.array(pickle.load(fd), 'float64')
        idx = (data['temperature'] == 99999)
        data['temperature'][idx] = 'NaN'
    return time, data

# Start by reading Emil's pickled data
time, data = read_pickled_data('../data/data.pcl')
# First create a pandas object
# Create the time variable
t = pd.DatetimeIndex(
        [pd.datetime(time['year'][i], time['month'][i], time['day'][i],
                      time['hour'][i], time['minute'][i])
         for i in xrange(time['year'].shape[0])], tz='Etc/GMT+6')
df = pd.DataFrame(data, index=t)
# Add some more data related to the sun
obs = Observer()
z, r = obs.compute_many(df.index)
df['zenith'] = z
df['earth_sun_dist'] = r
E_ext = ExtraterrestrialIrradiance()
E = ClearSkyIrradiance()
I0 = E_ext(r)
df['ext_irradiance'] = I0
I_g, I_bn, I_d = E(I0, z, p=df['pressure'], aod_700=0.01, w=0.21)
df['clear_sky_irradiance'] = I_g
# Write it to a file
df.to_hdf('../data/full_solar.h5', 'solar')

