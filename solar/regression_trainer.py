"""
Defines a class that facilitates in training with respect
to the irradiance data using a simple regression approach.
"""


import numpy as np
import best
import cPickle as pickle


class RegressionTrainer(object):

    # The pandas data
    _data = None

    # The number of points to train on
    _num_train = None

    @property
    def num_train(self):
        """
        Get/set the number of points to train on.
        """
        return self._num_train

    @num_train.setter
    def num_train(self, value):
        """
        Set the number of points to train on.
        """
        value = int(value)
        assert value > 0
        self._num_train = value

    @property
    def data(self):
        """
        Get/set the data.
        """
        return self._data

    @data.setter
    def data(self, value):
        self._data = value
        self.max_I = np.max(self.data.irradiance[:self.num_train + 1])
        self.max_P = np.max(self.data.pressure[:self.num_train + 1])
        self.min_P = np.min(self.data.pressure[:self.num_train + 1])
        self.max_T = np.max(self.data.temperature[:self.num_train + 1])
        self.min_T = np.min(self.data.temperature[:self.num_train + 1])

    @property
    def X(self):
        """
        Get the inputs for regression.
        """
        n = self.num_train
        x1 = self.data.irradiance[:n].interpolate() / self.max_I
        x2 = (np.cos(self.data.zenith[:n]) + 1.) / 2.
        x3 = ((self.data.pressure[:n].interpolate() - self.min_P) /
               (self.max_P - self.min_P))
        x4 = ((self.data.temperature[:n].interpolate() - self.min_T) /
              (self.max_T - self.min_T))
        X = np.hstack([np.atleast_2d(x1).T,
                       np.atleast_2d(x2).T,
                       np.atleast_2d(x3).T,
                       np.atleast_2d(x4).T])
        return X

    @property
    def Y(self):
        """
        Get the outputs for regression.
        """
        n = self.num_train
        y1 = self.data.irradiance[1:n+1].interpolate() / self.max_I
        y2 = ((self.data.pressure[1:n+1].interpolate() - self.min_P) /
              (self.max_P - self.min_P))
        y3 = ((self.data.temperature[1:n+1].interpolate() - self.min_T) /
              (self.max_T - self.min_T))
        Y = np.hstack([np.atleast_2d(y1).T,
                       np.atleast_2d(y2).T,
                       np.atleast_2d(y3).T])
        return Y

    def __init__(self):
        """
        Initialize the object.
        """
        pass

    def train(self):
        """
        Train it.
        """
        phi = best.gpc.ProductBasis(polynomials=[
                                    best.gpc.OrthogonalPolynomial(2,
                                                                  left=0,
                                                                  right=1)
                                    for i in xrange(4)])
        PHI = phi(self.X)
        rvm = best.rvm.RelevanceVectorMachine()
        rvm.set_data(PHI, self.Y)
        rvm.initialize()
        rvm.train(verbose=True)
        print str(rvm)
        for i in range(len(rvm.relevant)):
            print phi.terms[rvm.relevant[i]]
        f = rvm.get_generalized_linear_model(phi)
        with open('f.dat', 'wb') as fd:
            pickle.dump(f, fd)
        self.f = f

    def load(self, filename):
        """
        Load the surface from filename.
        """
        with open(filename, 'rb') as fd:
            self.f = pickle.load(fd)

    def _scale_x(self, x):
        """
        Scale X.
        """
        if x.ndim == 1:
            x = np.atleast_2d(x)
        x[:, 0] /= self.max_I
        x[:, 1] += 1.
        x[:, 1] /= 2.
        x[:, 2] -= self.min_P
        x[:, 2] /= (self.max_P - self.min_P)
        x[:, 3] -= self.min_T
        x[:, 3] /= (self.max_T - self.min_T)
        return x

    def _descale_y(self, y):
        """
        Descale Y.
        """
        y[0] *= self.max_I
        y[1] *= (self.max_P - self.min_P)
        y[1] += self.min_P
        y[2] *= (self.max_T - self.min_T)
        y[2] += self.min_T
        return y

    def _descale_std(self, s):
        """
        Descale std of Y.
        """
        s[0] *= self.max_I
        s[1] *= (self.max_P - self.min_P)
        s[2] *= (self.max_T - self.min_T)
        return s

    def predict(self, I0, P0, T0, zenith):
        """
        Predict the irradiance, temperature and pressure n timesteps ahead.
        """
        n = zenith.shape[0]
        I = []
        P = []
        T = []
        Y = []
        S = []
        m = self.num_train
        x = np.array([I0, np.cos(zenith[0]), P0, T0])
        x = self._scale_x(x)
        y = self.f(x)
        dy = self._descale_y(y)
        I.append(dy[0])
        P.append(dy[1])
        T.append(dy[2])
        Y.append(y)
        for i in xrange(1, n):
            x = np.array([I[-1], np.cos(zenith[i]), P[-1], T[-1]])
            x = self._scale_x(x)
            y = self.f(x)
            dy = self._descale_y(y)
            I.append(dy[0])
            P.append(dy[1])
            T.append(dy[2])
            Y.append(y)
        I = np.array(I)
        P = np.array(P)
        T = np.array(T)
        return I, P, T

    def get_predictive_covariance(self, x):
        tmp = np.atleast_2d(np.dot(self.f.sigma_sqrt, self.f.basis(x).T)).T
        C = np.dot(tmp.T, tmp) + np.eye(tmp.shape[1]) / self.f.beta
        return C

    def sample(self, I0, P0, T0, zenith):
        """
        Predict the irradiance, temperature and pressure n timesteps ahead.
        """
        n = zenith.shape[0]
        I = []
        P = []
        T = []
        Y = []
        S = []
        m = self.num_train
        x = np.array([I0, np.cos(zenith[0]), P0, T0])
        x = self._scale_x(x)
        C = self.get_predictive_covariance(x)[0]
        y = self.f(x) + np.sqrt(C) * np.random.randn()
        y[y >= 1.] = 1.
        y[y <= 0.] = 0.
        dy = self._descale_y(y)
        I.append(dy[0])
        P.append(dy[1])
        T.append(dy[2])
        Y.append(y)
        for i in xrange(1, n):
            x = np.array([I[-1], np.cos(zenith[i]), P[-1], T[-1]])
            x = self._scale_x(x)
            C = self.get_predictive_covariance(x)[0]
            y = self.f(x) + np.sqrt(C) * np.random.randn()
            y[y >= 1.] = 1.
            y[y <= 0.] = 0.
            dy = self._descale_y(y)
            I.append(dy[0])
            P.append(dy[1])
            T.append(dy[2])
            Y.append(y)
        I = np.array(I)
        P = np.array(P)
        T = np.array(T)
        return I, P, T


if __name__ == '__main__':
    import pandas as pd
    import matplotlib.pyplot as plt
    rt = RegressionTrainer()
    rt.num_train = 96 * 30 + 30
    rt.data = pd.read_hdf('../data/full_solar.h5', 'solar')
#    plt.plot(rt.X)
#    plt.show()
#    plt.plot(rt.Y)
#    plt.show()
#    rt.train()
    rt.load('f.dat')
    data = rt.data
    num_start = 50
    num_ahead = 96 * 5
    I0 = data.irradiance[num_start]
    P0 = data.pressure[num_start]
    T0 = data.temperature[num_start]
    zenith = data.zenith[num_start:num_start + num_ahead]
    I = []
    P = []
    T = []
    for i in xrange(100):
        print i
        Is, Ps, Ts = rt.sample(I0, P0, T0, zenith)
        I.append(Is)
        P.append(Ps)
        T.append(Ts)
    I = np.vstack(I).T
    P = np.vstack(P).T
    T = np.vstack(T).T
    I_m = np.mean(I, axis=1)
    I_std = np.std(I, axis=1)
    P_m = np.mean(P, axis=1)
    P_std = np.std(P, axis=1)
    T_m = np.mean(T, axis=1)
    T_std = np.std(T, axis=1)
    plt.plot(I_m, 'r')
    plt.plot(I_m + 2. * I_std, 'r')
    plt.plot(I_m - 2. * I_std, 'r')
    plt.plot(data.irradiance[num_start:num_start + num_ahead], linewidth=2)
    plt.show()
    plt.plot(P_m, 'r')
    plt.plot(P_m + 2. * P_std, 'r')
    plt.plot(P_m - 2. * P_std, 'r')
    plt.plot(data.pressure[num_start:num_start + num_ahead], linewidth=2)
    plt.show()
    plt.plot(T_m, 'r')
    plt.plot(T_m + 2. * T_std, 'r')
    plt.plot(T_m - 2. * T_std, 'r')
    plt.plot(data.temperature[num_start:num_start + num_ahead], linewidth=2)
    plt.show()
