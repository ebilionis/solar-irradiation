"""
An object that calculates the extraterrestrial irradiance
given the Sun-Earth distance.

Author:
    Ilias Bilionis
    12/17/2013
"""

__all__ = ['ExtraterrestrialIrradiance']


class ExtraterrestrialIrradiance(object):

    # The solar constant (W/m^2)
    _E0 = None

    # The average sun-earth distance (AU)
    _R_av = None

    @property
    def E0(self):
        """
        Setter/Getter of the solar constant.
        """
        return self._E0

    @E0.setter
    def E0(self, value):
        """
        Set the solar constant.
        """
        value = float(value)
        assert value > 0.
        self._E0 = value

    @property
    def R_av(self):
        """
        Setter/Getter of the average sun-earth distance.
        """
        return self._R_av

    @R_av.setter
    def R_av(self, value):
        """
        Set the average sun-earth distance.
        """
        value = float(value)
        assert value > 0.
        self._R_av = value

    def __init__(self, E0=1361., R_av=1.0000010178):
        """
        Initialize the object.
        """
        self.E0 = E0
        self.R_av = R_av

    def __call__(self, r):
        """
        Evaluate the function. ``r`` is the sun-earth distance in AU.
        """
        return self.E0 * (self.R_av / r) ** 2.


if __name__ == '__main__':
    import pandas as pd
    import matplotlib.pyplot as plt
    from observer import Observer
    data = pd.read_hdf('../data/data_w_alpha.h5', 'solar')
    obs = Observer()
    E_ext = ExtraterrestrialIrradiance()
    spa_data = obs.spa_data
    n = 96 * 365 * 5
    print 'computing'
    z, r = obs.compute_many(data.index[:n])
    plt.plot(E_ext(r))
    plt.show()
