"""
Looks up various things about a particular site on www.earthtools.org.

Author:
    Ilias Bilionis

Date:
    3/12/2014
"""


import urllib
from xml.etree import ElementTree as XMLTree


__all__ = ['look_up_offset', 'look_up_elevation']


def _look_up(lat, lon, service, variable, web='http://www.earthtools.org'):
    """
    Looks up ``service`` for variable ``variable`` at locations specified
    by latitude ``lat`` and longitude ``lon``.
    """
    url = urllib.urlopen(web + '/' + service + '/' + str(lat) + '/' + str(lon))
    print url.read()
    quit()
    tree = XMLTree.fromstring(url.read())
    return tree.find(variable).text


def look_up_offset(lat, lon):
    """
    Looks up the time offset from UTC at ``lat`` and ``lon``.
    The result is in hours.
    """
    return float(_look_up(lat, lon, 'timezone', 'offset'))


def look_up_elevation(lat, lon):
    """
    Looks up the elevation from the sea level at ``lat`` and ``lon``.
    The result is in meters.
    """
    return float(_look_up(lat, lon, 'height', 'meters'))
