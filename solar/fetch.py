"""
Functions that fetch irradiance data from the Argonne location.
"""


__all__ = ['get_days_of_month', 'fetch_month', 'fetch_year', 'fetch_year_range',
           'fetch_no_qc_data']



import pandas as pd
import urllib2
import itertools
import numpy as np
from datetime import timedelta
from datetime import tzinfo
from datetime import datetime


# The three first letters of each month
MONTH_NAMES = {
        1: 'jan',
        2: 'feb',
        3: 'mar',
        4: 'apr',
        5: 'may',
        6: 'jun',
        7: 'jul',
        8: 'aug',
        9: 'sep',
        10: 'oct',
        11: 'nov',
        12: 'dec'}


# The days of month for a regular year
DAYS_OF_MONTH = {
        1: 31,
        2: 28,
        3: 31,
        4: 30,
        5: 31,
        6: 30,
        7: 31,
        8: 31,
        9: 30,
        10: 31,
        11: 30,
        12: 31}


def get_days_of_month(month, year):
    """
    Get the days of a month and correct for the year.
    """
    if not month == 2 or not year % 4 == 0:
        return DAYS_OF_MONTH[month]
    return DAYS_OF_MONTH[2] + 1


# The meaning of each data field.
DATA_FIELDS = {
        'freq': {'id': 0, 'desc': 'ID, M for 15 min, H for hourly, D for daily',
                 'units': ''},
        'day': {'id': 1, 'desc': 'Day of the month', 'units': ''},
        'month': {'id': 2, 'desc': 'Month', 'units': ''},
        'year': {'id': 3, 'desc': 'Year', 'units': ''},
        'CST': {'id': 4, 'desc': 'Central Standard Time at the end of period',
                'units': ''},
        'temperature_60m': {'id': 5, 'desc': 'Average 60 m temperature',
                        'units': 'deg C'},
        'wind_speed_60m': {'id': 6, 'desc': 'Average 60 m wind speed',
                        'units': 'cm/s'},
        'v_wind_speed_60m': {'id': 7, 'desc': 'Vector-averaged 60 m wind speed',
                         'units': 'cm/s'},
        'v_wind_direction_60m': {'id': 8,
                              'desc': 'Vector-averaged 60 m wind direction',
                              'units': 'deg'},
        'std_v_wind_direction_60m': {'id': 9,
                                 'desc': 'Standard deviation of 60 m wind direction',
                                 'units': 'deg'},
        'precipitation': {'id': 10,
                          'desc': 'Total precipitation for period',
                          'units': 'mm'},
        'heat_flux': {'id': 11,
                      'desc': 'Estimated head flux',
                      'units': 'W/m**2'},
        'friction_velocity': {'id': 12,
                              'desc': 'Estimated friction velocity',
                              'units': 'cm/s'},
        'vapor_pressure': {'id': 13,
                           'desc': 'Average 10 m vapor pressure',
                           'units': 'kPa'},
        'dew_point_temperature': {'id': 14,
                                  'desc': 'Average 10 m dew point temperature',
                                  'units': 'deg C'},
        'temperature': {'id': 15,
                        'desc': 'Average 10 m temperature',
                        'units': 'deg C'},
        'wind_speed': {'id': 16,
                        'desc': 'Average 10 m wind speed',
                        'units': 'cm/s'},
        'v_wind_speed': {'id': 17,
                         'desc': 'Vector-averaged 10 m wind speed',
                         'units': 'cm/s'},
        'v_wind_direction': {'id': 18,
                             'desc': 'Vector-averaged 10 m wind direction',
                             'units': 'deg'},
        'std_v_wind_direction': {'id': 19,
                                 'desc': 'Standard deviation of vector-averaged 10 m wind direction',
                                 'units': 'deg'},
        'irradiation': {'id': 20,
                        'desc': 'Average global irradiation',
                        'units': 'W/m**2'},
        'net_radition': {'id': 21,
                         'desc': 'Average net radiation',
                         'units': 'W/m**2'},
        'surf_roughness': {'id': 22,
                           'desc': 'Estimated surface roughness length',
                           'units': 'cm'},
        'pressure': {'id': 23,
                     'desc': 'Average barometric pressure',
                     'units': 'kPa'},
        'rel_humidity': {'id': 24,
                         'desc': 'Average 10 m relative humidity',
                         'units': r'%'}
        }



def fetch_month(year, month,
                base_url='http://www.atmos.anl.gov/ANLMET/numeric/15Minute',
                variables=['irradiation']):
    """
    Fetch a particular month of a particular year.

    :param year:    The year.
    :type year:     int
    :param month:   The month.
    :type month:    int
    :param base_url: The base url that contains the data.
    :type base_url: str
    :param variables: A list of the variables you want to extract
    :type variables: list of str
    """
    url = base_url + '/' + str(year)
    data_file = url + '/' + MONTH_NAMES[month] + str(year)[-2:] + 'met.dat'
    print 'Fetching:', data_file
    f = urllib2.urlopen(data_file)
    data = f.readlines()
    return parse_data(data, variables)


def fix_data(data):
    fixed_data = []
    i = 0
    while i < len(data):
        tmp = data[i].split()
        if tmp[0] == 'M' or tmp[0] == 'H':
            fixed_data.append(data[i] + data[i + 1] + data[i + 2])
            i += 3
        else:
            fixed_data.append(data[i] + data[i + 1] + data[i + 2]
                              + data[i + 3])
            i += 4
    return fixed_data


def parse_raw_variables(data, variables):
    data = fix_data(data)
    k = len(variables)
    variables = variables + ['day', 'month', 'year', 'CST']
    types = [float] * k + [int] * 3 + [str]
    out = {}
    for name in variables:
        out[name] = []
    for line in data:
        tmp = line.split()
        if tmp[DATA_FIELDS['freq']['id']] == 'M':
            for name, t in itertools.izip(variables, types):
                #print name, DATA_FIELDS[name]['id']
                d = tmp[DATA_FIELDS[name]['id']]
                if d == '+99999.':
                    out[name].append(None)
                else:
                    out[name].append(t(d))
    out['hour'] = [int(s[:2]) for s in out['CST']]
    out['minute'] = [int(s[2:]) for s in out['CST']]
    for i in xrange(len(out['day'])):
        if out['hour'][i] == 24:
            out['hour'][i] = 0
            dom = get_days_of_month(out['month'][i], out['year'][i])
            out['day'][i] = out['day'][i] % dom + 1
            if out['day'][i] == 1:
                out['month'][i] = out['month'][i] % 12 + 1
                if out['month'][i] == 1:
                    out['year'][i] += 1
    out['year'] = [y + 2000 for y in out['year']]
    return out


def parse_data(data, variables):
    """
    Parse the data.
    """
    data = parse_raw_variables(data, variables)
    t = pd.DatetimeIndex(
    [pd.datetime(data['year'][i], data['month'][i], data['day'][i],
                 data['hour'][i],
                 data['minute'][i])
     for i in xrange(len(data['year']))], tz='Etc/GMT+6')
    data = np.array([data[name] for name in variables], dtype='float32').T
    df = pd.DataFrame(data, index=t, columns=variables)
    return df


def fetch_year(year):
    """
    Fetch data from a particular year.
    """
    data = []
    for i in xrange(1, 13):
        try:
            df = fetch_month(year, i)
        except:
            print 'Month', MONTH_NAMES[i], 'was not found!'
            continue
        data.append(df)
    return pd.concat(data)


def fetch_year_range(year1, year2):
    """
    Fetch data from a year range.

    year1 is inclusive.
    year2 is exclusive.
    """
    data = []
    for i in xrange(year1, year2):
        df = fetch_year(i)
        data.append(df)
    return pd.concat(data)


def fetch_no_qc_data(start_year=2014,
                     url='http://www.atmos.anl.gov/ANLMET/anltower.not_qc'):
    """
    Fetch no quality controllled data.
    """
    f = urllib2.urlopen(url)
    data = f.readlines()
    # Figure out the column corresponding to variable 'radW/m2'
    tmp = data[1].split()
    idx = tmp.index('radW/m2')
    if idx == -1:
        raise RuntimeError('Variable \'radW/m2\' not found in ' + url)
    # Collect day of the year
    doy = np.array([int(line.split()[0]) - 1 for line in data[2:-1]],
                   dtype='i')
    # Find problems with days
    # The following is very specific to the data
    # START OF PROBLEMATIC SECTION
    # ----------------------------
    idx_to_remove = []
    problem_flag = False
    for i in xrange(1, doy.shape[0]):
        if doy[i] < doy[i-1]:
            if (doy[i - 1] == 365 or doy[i - 1] == 366) and doy[i] == 1:
                problem_flag = False
            else:
                idx_to_remove.append(i)
                problem_flag = True
        elif problem_flag:
            if doy[i] == doy[idx_to_remove[-1]]:
                idx_to_remove.append(i)
            else:
                problem_flag = False
    doy = np.delete(doy, idx_to_remove)
    # ------------------------------
    # END OF PROBLEMATIC SECTION
    year = np.ndarray(doy.shape, dtype='i')
    year[0] = start_year
    for i in xrange(1, doy.shape[0]):
        if (doy[i - 1] == 365 or doy[i - 1] == 366) and doy[i] == 1:
            year[i] = year[i - 1] + 1
        else:
            year[i] = year[i - 1]
    lst_time = [line.split()[1] for line in data[2:-1]]
    hour = np.delete(np.array([int(t.split(':')[0]) for t in lst_time],
                              dtype='i'),
                     idx_to_remove)
    minute = np.delete(np.array([int(t.split(':')[1]) for t in lst_time],
                                dtype='i'),
                       idx_to_remove)
    irradiance = np.delete(np.array([float(line.split()[idx]) for line in data[2:-1]]),
                           idx_to_remove)
    # Create the index
    dates = [datetime(year[i], 1, 1) + timedelta(days=float(doy[i]),
                                                 minutes=float(minute[i]),
                                                 hours=float(hour[i]))
             for i in xrange(doy.shape[0])]
    index = pd.DatetimeIndex(dates, tz='Etc/GMT+6')
    df = pd.DataFrame(irradiance, index=index, columns=['irradiance'])
    return df


if __name__ == '__main__':
    # A small test to see if everything works
    #df = fetch_month(2013, 9)
    #df = fetch_year(2013)
    #df = fetch_year_range(2010, 2013)
    import matplotlib.pyplot as plt
    #print df.irradiation
    #df.irradiation.plot()
    #plt.show()
    df = fetch_no_qc_data()
    df.plot()
    plt.show()
