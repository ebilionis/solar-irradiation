
"""
Plot the one step ahead predictive distribution.
"""

import sys
import os
sys.path.insert(0, os.path.abspath('..'))
import matplotlib.pyplot as plt
import numpy as np
import cPickle as pickle
import solar
import hmm
import scipy.stats


assert len(sys.argv) == 2, '\nUsage:\n\t' + sys.argv[0] + ' pickled_model'
filename = sys.argv[1]
with open(filename, 'rb') as fd:
    solar_run = pickle.load(fd)
model = solar_run['model']
P = model.num_symbols
how = 'daily'
data = solar.IrradianceData(num_symbols=P,
                            how=how, date='2012')
Z = model.decode(data.discrete_deseasoned())
E = data.irradiance
I = np.arange(Z.shape[0])
#for i in xrange(model.num_states):
#    plt.plot(I[Z == i], E[Z == i], 'o', markersize=10)
#plt.xlabel('Day of Year', fontsize=16)
#plt.ylabel('Irradiance', fontsize=16)
#plt.show()
fig = plt.figure()
num_rows = model.num_states / 2
for i in xrange(model.num_states):
    ax = fig.add_subplot(num_rows, 2, i + 1)
    ax.hist(np.arange(model.num_symbols), weights=model.B[i, :])
    ax.set_xlabel('state ' + str(i + 1), fontsize=16)
plt.show()
Xt = data.discrete_deseasoned()
XP = model.one_step_ahead_prediction(Xt)
a = data.get_discrete_one_step_ahead_cdf(XP)
print scipy.stats.kstest(a, 'uniform')
plt.hist(a)
plt.show()
t = data.irradiance_bins
E0 = data.E0
alpha = data.airmass()
plt.plot(np.arange(XP.shape[0]) + 0.5,
	 E0 * alpha * t[Xt], 'm-*', linewidth=2.,
         markersize=10)
plt.plot(np.arange(XP.shape[0]) + 0.5,
	 E0 * alpha * t[data.get_discrete_ppf(0.5, XP)],
         'gd-', linewidth=2., markersize=10)
plt.plot(np.arange(XP.shape[0]) + 0.5,
	 E0 * alpha * t[data.get_discrete_ppf(0.05, XP)],
         'k', linewidth=2)
plt.plot(np.arange(XP.shape[0]) + 0.5,
	 E0 * alpha * t[data.get_discrete_ppf(0.95, XP)],
         'k', linewidth=2)
plt.xlabel('Day of Year', fontsize=16)
plt.ylabel('Irradiance', fontsize=16)
plt.legend(['True data', 'Median', '95\% error bars'], loc='best')
plt.xlim([0, 365])
plt.show()
p = plt.pcolor(XP.T)
plt.plot(Xt, '*r', linewidth=2)
plt.colorbar(p)
plt.xlim([0, 365])
plt.show()
