"""
Various input output methods/classes.

Author:
    Ilias Bilionis

Date:
    3/12/2014
"""


__all__ = ['extract_patch_data', 'load_pickled_obj', 'dump_pickled_obj']


import sys
import numpy as np
import cPickle as pickle
from netCDF4 import Dataset
import pandas as pd
from datetime import timedelta
from datetime import datetime
import pytz
from dates import get_closest_date
from dates import str_to_datetime


def extract_patch_data(f, name, start_date=None, final_date=None, verbose=False):
    """
    Extract data from an HDF5 file containing patch data up to a particular
    date.
    """
    if start_date is None:
        i_start = 0
    else:
        i_start = get_closest_date(f.root.records.cols.date[:], start_date)
    if final_date is None:
        i_end = f.root.records.nrows
    else:
        i_end = get_closest_date(f.root.records.cols.date[:], final_date)
    X = []
    dates = []
    count = 0
    for i in xrange(i_start, i_end + 1):
        g = f.root.records.cols.name[i]
        if verbose:
            sys.stdout.write('extracting: ' + g + '\r')
        try:
            x = getattr(f.get_node('/data/' + g), name)[:].flatten()
        except:
            continue
        if np.isnan(x).any() or np.isinf(x).any():
            if verbose:
                print 'found nan'
            continue
        sys.stdout.flush()
        dates.append(f.root.records.cols.date[i])
        X.append(x)
        count += 1
    if verbose:
        sys.stdout.write('\n')
        print 'final number of observations extracted:', count
    X = np.vstack(X)
    return X, str_to_datetime(dates)


def load_pickled_obj(input_filename, comm=None):
    """
    Load pickled object and make sure that everybody has it.
    """
    rank = 0 if comm is None else comm.Get_rank()
    if rank == 0:
        with open(input_filename, 'rb') as fd:
            obj = pickle.load(fd)
    else:
        obj = None
    if comm is not None:
        obj = comm.bcast(obj)
    return obj


def dump_pickled_obj(obj, output_filename, comm=None):
    """
    Dump an object to a file.
    """
    rank = 0 if comm is None else comm.Get_rank()
    if rank == 0:
        with open(output_filename, 'wb') as fd:
            pickle.dump(obj, fd, protocol=pickle.HIGHEST_PROTOCOL)


def read_netcdf_data(filename):
    """
    Read insolation data from a netcdf file.
    """
    with Dataset(filename, 'r') as fd:
        # The time is in UTC
        t0 = datetime.fromtimestamp(fd.variables['base_time'][:], pytz.utc)
        index = pd.DatetimeIndex([t0 + timedelta(seconds=dt)
                 for dt in fd.variables['time_offset'][:]])
        I_g_max = fd.variables['down_short_hemisp_max'][:][:, None]
        lat = fd.variables['lat'][:][0]
        lon = fd.variables['lon'][:][0]
        df = pd.DataFrame(I_g_max, index=index, columns=['insolation']).resample('30T',
                                                                     how='mean')
    return df, lon, lat
