"""
This files contains functions and classes that parse the numerical
weather prediction data.
"""


__all__ = ['NWPFile']


import os
from datetime import datetime
import itertools
import numpy as np
import pandas as pd


class NWPFile(object):
    """
    Parses a NWP file.
    """

    # The description of the each row of the data file
    ROW_VARS = {
            1:
        {
            'num': 4,
            'dim': {'RAP': 1, 'HRRR': 1, 'NAM': 1},
            0: {'name': 'line_id', 'type': int,
                'desc': 'Line id', 'units': ''},
            1: {'name': 'longitude', 'type': float,
                'desc': 'Lognitude in ###.#### degrees.fractional degrees',
                'units': 'deg'},
            2: {'name': 'latitude', 'type': float,
                'desc': 'Latitude in ###.#### degrees.fractional degrees',
                'units': 'deg'},
            3: {'name': 'minutes', 'type': int,
                'desc': 'Forecast minutes into the future',
                'units': 'min'}
        },
            2:
        {
            'num': 6,
            'dim': {'RAP': 1, 'HRRR': 1, 'NAM': 1},
            0: {'name': 'line_id', 'type': int,
                'desc': 'Line id', 'units': ''},
            1: {'name': 'total_column_ozone', 'type': float,
                'desc': 'Total column ozone - ozone concentration',
                'units': 'Dobson'},
            2: {'name': 'surface_albedo', 'type': float,
                'desc': 'Surface albedo', 'units': ''},
            3: {'name': 'land_mask', 'type': float,
                'desc': 'Land mask', 'units': ''},
            4: {'name': 'vegetation_cover', 'type': float,
                'desc': 'Vegetation cover', 'units': ''},
            5: {'name': 'aerosol_optical_depth', 'type': float,
                'desc': 'Aerosol optical depth', 'units': ''}
        },
            3:
        {
            'num': 8,
            'dim': {'RAP': 50, 'HRRR': 50, 'NAM': 39},
            0: {'name': 'line_id', 'type': int,
                'desc': 'Line id', 'units': ''},
            1: {'name': 'z', 'type': float,
                'desc': 'The z coordinate', 'units': 'm'},
            2: {'name': 'hp_cloud_f', 'type': float,
                'desc': 'Height profile of cloud fraction',
                'units': ''},
            3: {'name': 'cloud_water_mix', 'type': float,
                'desc': 'Cloud water mixing ratio',
                'units': ''},
            4: {'name': 'cloud_ice_mix', 'type': float,
                'desc': 'Cloud ice mixing ratio',
                'units': ''},
            5: {'name': 'water_vapor_density', 'type': float,
                'desc': 'Water vapor density',
                'units': 'kg/m**3'},
            6: {'name': 'temperature', 'type': float,
                'desc': 'Temperature',
                'units': 'K'},
            7: {'name': 'pressure', 'type': float,
                'desc': 'Pressure',
                'units': 'Pa'}
        }
        }

    def _readline(self, fd, expected_id):
        """
        Get the next line of the file and split it in fields.

        Also checks for the right id and raises an exception if it
        does not find it.
        """
        if not hasattr(self, 'c'):
            self.c = 0
        self.c += 1
        line = fd.readline()
        if line == '':
            raise EOFError('Reached the end of the file.')
        tmp = line.strip().split(',')
        line_id = int(tmp[0])
        self._check_line_id(line_id, expected_id)
        return tmp

    def _readlines(self, fd, expected_id):
        """
        Read all lines.
        """
        row_var_dict = self.ROW_VARS[expected_id]
        dim = row_var_dict['dim'][self.model_name]
        for n in xrange(dim):
            tmp = self._readline(fd, expected_id)
            if n == 0:
                for i in xrange(1, len(tmp)):
                    setattr(self, row_var_dict[i]['name'], np.zeros(dim))
            for i in xrange(1, len(tmp)):
                getattr(self, row_var_dict[i]['name'])[n] = (
                        row_var_dict[i]['type'](tmp[i]))

    def _check_line_id(self, observed_id, expected_id):
        """
        Check if the line id is correct and throw an exception if it is not.

        :param observed_id: The observed id of the line.
        :type observed_id:  int
        :param expected_id: The expected id of the line.
        :type expected_id:  int
        """
        if not observed_id == expected_id:
            raise IOError('Expected a file id \'' + str(expected_id) +
                          '\', but got a value of \'' + str(observed_id) +
                          '\'')

    def _process_row_0(self, fd):
        """
        Process the 0-th row starting at the current position of the file.

        :param fd:  An opened file.
        """
        tmp = self._readline(fd, 0)
        year = int(tmp[1][:4])
        month = int(tmp[1][4:6])
        day = int(tmp[1][6:8])
        tmp2 = tmp[1].split(':')
        hour = int(tmp2[1][:2])
        minute = int(tmp2[1][2:4])
        second = int(tmp2[1][4:6])
        self.model_name = tmp[2]
        self.date = datetime(year, month, day, hour)
        self._vars_to_str.append('model_name')
        self._vars_to_str.append('date')

    def _process_row_1(self, fd):
        """
        Process the 1-th row starting at the current position of the file.

        :param fd:  An opened file.
        """
        self._readlines(fd, 1)

    def _process_row_2(self, fd):
        """
        Process the 2-nd row starting at the current position of the file.

        :param fd:  An opened file.
        """
        self._readlines(fd, 2)

    def _process_row_3(self, fd):
        """
        Process the 3-rd row starting at the current location.
        Continue until we cannot find a line with the right tag.
        """
        self._readlines(fd, 3)

    def _process_time_step(self, fd):
        """
        Process a whole time step.
        """
        for i in xrange(1, 4):
            getattr(self, '_process_row_%d' % i)(fd)

    def _get_variable_list(self):
        """
        Return a list containing the names of all the variables in the files.
        """
        var_names = []
        for line_id in xrange(1, 4):
            row_var_dict = self.ROW_VARS[line_id]
            num = row_var_dict['num']
            for i in xrange(1, num):
                var_names.append(row_var_dict[i]['name'])
        return var_names

    def _process_all_time_steps(self, fd):
        """
        Process all time steps.
        """
        var_names = self._get_variable_list()
        tmp_var_names = ['_' + name for name in var_names]
        for tmp_name in tmp_var_names:
            setattr(self, tmp_name, [])
        try:
            while True:
                self._process_time_step(fd)
                for name, tmp_name in itertools.izip(var_names, tmp_var_names):
                    getattr(self, tmp_name).append(getattr(self, name))
        except EOFError as e:
            for name, tmp_name in itertools.izip(var_names, tmp_var_names):
                setattr(self, name, np.vstack(getattr(self, tmp_name)))

    def __init__(self, filename):
        """
        Initialize the object.

        :param filename: The filename you wish to parse.
        """
        self.filename = os.path.abspath(filename)
        basename = os.path.basename(self.filename)
        tmp = basename.split('.')
        tmp2 = tmp[1].split('_')
        self.data_set = tmp2[0]
        self.site_code_name = tmp2[1]
        self._vars_to_str = ['filename',
                             'data_set',
                             'site_code_name']
        self.fd = open(self.filename, 'r')
        self._process_row_0(self.fd)
        self._process_all_time_steps(self.fd)
        self.fd.close()

    def __str__(self):
        """
        Return a string representation of the object.
        """
        s = 'Numerical Weather Prediction File\n'
        s += '-' * 80 + '\n'
        for name in self._vars_to_str:
            desc = ' '.join(name.split('_'))
            s += '{0:20}:\t{1:40}\n'.format(desc, str(getattr(self, name)))
        return s


if __name__ == '__main__':
    import matplotlib.pyplot as plt
    f = NWPFile('data/nwp_data_anl/NAM.ANL_ARGON.130914:06.csv')
    plt.plot(f.temperature)
    plt.show()
