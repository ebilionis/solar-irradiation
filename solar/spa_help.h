// A helping function to do some calculations related to the spa library faster
// using the Python interface.
//
// Author: Ilias Bilionis, December 2013
//
#ifndef SPA_HELP_H_IS_INCLUDED
#define SPA_HELP_H_IS_INCLUDED

#include "spa.h"
#include <assert.h>
#include <stdio.h>

#define SPA_HELPER_DEBUG

int spa_calculate_many(spa_data* spa,
                       int num_years, int* years,
                       int num_months, int* months,
                       int num_days, int* days,
                       int num_hours, int* hours,
                       int num_minutes, int* minutes,
                       int num_seconds, int* seconds,
                       int num_zenith, double* zenith,
                       int num_r, double* r)
{
    const int N = num_years;
    int i;
#ifdef SPA_HELPER_DEBUG
    assert(N == num_months);
    assert(N == num_days);
    assert(N == num_hours);
    assert(N == num_minutes);
    assert(N == num_seconds);
    assert(N == num_zenith);
    assert(N == num_r);
#endif // SPA_HELPER_DEBUG
    for(i=0; i<N; i++) {
        spa->year = years[i];
        spa->month = months[i];
        spa->day = days[i];
        spa->hour = hours[i];
        spa->minute = minutes[i];
        spa->second = seconds[i];
        spa_calculate(spa);
        zenith[i] = spa->zenith;
        r[i] = spa->r;
    }
    return 0;
}


int spa_calculate_many_sunrise_and_sunset(spa_data* spa,
                                          int num_years, int* years,
                                          int num_months, int* months,
                                          int num_days, int* days,
                                          int num_hours, int* hours,
                                          int num_minutes, int* minutes,
                                          int num_seconds, int* seconds,
                                          int num_sunrise, double* sunrise,
                                          int num_sunset, double* sunset,
                                          int num_r, double* r)
{
    const int N = num_years;
    int i;
#ifdef SPA_HELPER_DEBUG
    assert(N == num_months);
    assert(N == num_days);
    assert(N == num_hours);
    assert(N == num_minutes);
    assert(N == num_seconds);
    assert(N == num_zenith);
    assert(N == num_r);
#endif // SPA_HELPER_DEBUG
    for(i=0; i<N; i++) {
        spa->year = years[i];
        spa->month = months[i];
        spa->day = days[i];
        spa->hour = hours[i];
        spa->minute = minutes[i];
        spa->second = seconds[i];
        calculate_eot_and_sun_rise_transit_set(spa);
        spa_calculate(spa);
        sunrise[i] = spa->sunrise;
        sunset[i] = spa->sunset;
        r[i] = spa->r;
    }
    return 0;
}

int spa_calculate_many_locations(spa_data* spa,
                                 int num_lons, float* lons,
                                 int num_lats, float* lats,
                                 int num_years, int* years,
                                 int num_months, int* months,
                                 int num_days, int* days,
                                 int num_hours, int* hours,
                                 int num_minutes, int* minutes,
                                 int num_seconds, int* seconds,
                                 int num_zenith, double* zenith,
                                 int num_r, double* r)
 {
    const int K = num_lons;
    const int N = num_years;
    int i, j;
#ifdef SPA_HELPER_DEBUG
    assert(K == num_lats);
    assert(N == num_months);
    assert(N == num_days);
    assert(N == num_hours);
    assert(N == num_minutes);
    assert(N == num_seconds);
    assert(N * K == num_zenith);
    assert(N * K == num_r);
#endif // SPA_HELPER_DEBUG
    for(i=0; i<N; i++) {
        spa->year = years[i];
        spa->month = months[i];
        spa->day = days[i];
        spa->hour = hours[i];
        spa->minute = minutes[i];
        spa->second = seconds[i];
        for(j = 0; j<K; j++) {
            spa->latitude = lats[j];
            spa->longitude = lons[j];
            spa_calculate(spa);
            zenith[i * K + j] = spa->zenith;
            r[i * K + j] = spa->r;
        }
    }
    return 0;
}
                                  

#endif // SPA_HELP_H_IS_INCLUDED
