#!/usr/bin/env python
"""
Plot the results.
"""

import sys
import os
sys.path.insert(0, os.path.abspath('..'))
import matplotlib.pyplot as plt
import numpy as np
import scipy
import cPickle as pickle
import solar
import hmm
import argparse

parser = argparse.ArgumentParser(description='Train an HMM to the solar data.')
parser.add_argument('--model', dest='model_filename', type=str,
                     help='Specify the model filename')
parser.add_argument('--num-steps', dest='num_steps', type=int, default=1,
                     help='Specify how many steps ahead to predict.')
parser.add_argument('--date', dest='date', type=str, default='1991',
                     help='Specify a range of dates to use in training.')
args = parser.parse_args()
tmp = args.date.split(':')
if len(tmp) == 1:
    date = tmp[0]
else:
    date = tmp

with open(args.model_filename, 'rb') as fd:
    solar_run = pickle.load(fd)
model = solar_run['model']
data = solar.IrradianceData(num_symbols=model.num_symbols,
                            inverse_airmass_class=solar_run['inverse_airmass'],
                            how=solar_run['how'], date=date, E0=solar_run['E0'])
#Z = model.decode(data.discrete_deseasoned())
#plt.plot(Z)
#plt.show()
#I = np.arange(Z.shape[0])
#E = data.irradiance
#for i in xrange(P):
#    plt.plot(I[Z == i], E[Z == i], 'o', markersize=5)
#plt.show()
Xt = data.discrete_deseasoned
XP = model.step_ahead_prediction(Xt, args.num_steps)
# Exclude from this computation points of zero irradiance
idx_nonzero = Xt >= 2
a = data.get_discrete_one_step_ahead_cdf(np.copy(XP[idx_nonzero, :]), Xt[idx_nonzero])
print scipy.stats.kstest(a, 'uniform')
plt.plot(np.sort(a), '*-', markersize=10)
plt.plot(np.arange(a.shape[0]), np.arange(a.shape[0]) / 365., 'r', linewidth=2)
plt.show()
#plt.hist(a, bins=30)
#plt.show()
t = np.linspace(0, data.E0, data.num_symbols)
inverse_airmass = data.inverse_airmass
plt.plot(np.arange(XP.shape[0]) + 0.5,
	  data.irradiance, 'm-*', linewidth=2.,
         markersize=10)
plt.plot(np.arange(XP.shape[0]) + 0.5,
	  data.get_ppf(0.5, XP),
         'gd-', linewidth=2., markersize=10)
plt.plot(np.arange(XP.shape[0]) + 0.5,
	  data.get_ppf(0.05, XP),
         'k', linewidth=2)
plt.plot(np.arange(XP.shape[0]) + 0.5,
	  data.get_ppf(0.95, XP),
         'k', linewidth=2)
plt.show()
plt.pcolor(XP.T)
plt.plot(Xt, '*r')
plt.show()
