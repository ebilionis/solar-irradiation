"""
Train an HMM using the Daily data without deseasoning.
"""

import sys
import os
sys.path.insert(0, os.path.abspath('..'))
import matplotlib.pyplot as plt
import numpy as np
import solar
import hmm
import cPickle as pickle


# Number of hidden states
K = 4
# Number of symbols
P = 40
# Number of training points
num_train = 365 * 20
E0 = 1361.
# Prior for pi
f_pi = 1.
# Prior for A
f_A = 1.
# Prior for B
f_B = 1.
# Tolerance
tol = 1e-10
# Maximum number of iterations
max_iter = 10000

# Load the data
data = solar.IrradianceData(num_symbols=P, E0=E0)
train_data, test_data = data.split(num_train)
X = train_data.discrete_deseasoned()

# Make the model
num_clones = 1
like_per_clone = []
for i in xrange(num_clones):
    pi = np.random.dirichlet(np.ones(K) * f_pi)
    A = np.random.dirichlet(np.ones(K) * f_A, size=(K,))
    B = np.random.dirichlet(np.ones(P) * f_B, size=(K,))
    model = hmm.DiscreteHMM(pi, A, B)
    loglike = model.train(X=X, tol=1e-6, max_iter=max_iter)
    plt.plot(loglike, 'b', linewidth=2)
    like_per_clone.append(loglike[-1])
    filename = 'results/solar_daily_K=' + str(K) + '_P=' + str(P) + '_N=' + str(num_train)
    filename += '_C=' + str(i) + '.pickle'
    with open(filename, 'wb') as fd:
    	results = {}
    	results['model'] = model
    	results['loglike'] = loglike
	results['airmass'] = data.airmass_class
	results['E0'] = data.E0
	results['how'] = data.how
    	pickle.dump(results, fd)
plt.show()
print 'Maximum likelihood', np.max(like_per_clone), np.argmax(like_per_clone)
