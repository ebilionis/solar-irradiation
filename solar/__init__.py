"""
The solar Python module.

Currently, it contains many different things related to forcasting solar
radiation.

Author:
    Ilias Bilionis
"""


from solar import *
from observer import *
from clear_sky_model import *
from satellite import *
from fetch import *
from extraterrestrial_irradiance import *
import spa
