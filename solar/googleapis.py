"""
Asks google for some geoinfo.

Author:
    Ilias Bilionis

Date:
    3/12/2014
"""


import urllib
import time
from xml.etree import ElementTree as XMLTree
import os
import math
import re


__all__ = ['look_up_offset', 'look_up_elevation']


# A place to store requests already made, so that it can also work offline
REQUEST_DIR = os.path.join(os.environ['HOME'], '.solar_requests')
# Make it
if not os.path.isdir(REQUEST_DIR):
    os.mkdir(REQUEST_DIR)


def _get_base_url(service,
                  web='https://maps.googleapis.com/maps/api',
                  how = 'xml'):
    """
    Get the base url for a particular service.
    """
    return web + '/' + service + '/'  + how + '?'

def _look_up(request):
    """
    Looks up the ``request``. It only goes online if it cannot find it locally.
    """
    req_file = request.replace('/', '_')
    req_file = re.sub(r'timestamp=(\d+)&', 'timestamp=0000000', req_file)
    req_file = os.path.join(REQUEST_DIR, req_file)
    if os.path.exists(req_file):
        with open(req_file, 'r') as fd:
            data = fd.read()
    else:
        url = urllib.urlopen(request)
        data = url.read()
        with open(req_file, 'w') as fd:
            fd.write(data)
    return data

def look_up_offset(lat, lon):
    """
    Looks up the time offset from UTC at ``lat`` and ``lon``.
    The result is in hours.
    """
    request = (_get_base_url('timezone') +
               'location=%f,%f&timestamp=%d&sensor=false'
                % (lat, lon, int(time.time())))
    tree = XMLTree.fromstring(_look_up(request))
    offset = float(tree.find('raw_offset').text) / 3600.
    sign = '+' if offset <= 0 else '-'
    timezone_id = 'Etc/GMT' + sign + str(int(math.fabs(offset)))
    return offset, timezone_id


def look_up_elevation(lat, lon):
    """
    Looks up the elevation at a particular at ``lat`` and ``lon``.
    The result is in meters.
    """
    request = (_get_base_url('elevation') +
               'locations=%f,%f&sensor=false'
               % (lat, lon))
    tree = XMLTree.fromstring(_look_up(request))
    return float(tree.find('./result/elevation').text)
