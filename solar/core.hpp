// Some routines for solar that are very slow and need to be done in C.
// Author: Ilias Bilionis

#include <cassert>
#include <iostream>

// Trapezoid rule
double trapz(int N_x, double* x)
{
    double s = 0.;
    for(int i=1; i<N_x; i++)
        s += x[i] + x[i - 1];
    return 0.5 * s;
}


// Get the cdf of one step ahead probability
void get_discrete_one_step_ahead_cdf(int M_XP, int N_XP, double* XP,
                                     int N_XD, int* XD,
                                     int N_CDF, double* CDF)
{
    const int num_samples = M_XP;
    const int num_symbols = N_XP;
    assert(num_samples == N_XD);
    assert(num_samples == N_CDF);
    for(int i=0; i<num_samples; i++) {
        CDF[i] = 0.;
        for(int j=0; j<=XD[i]; j++)
            CDF[i] += XP[i * num_symbols + j];
    }
}


// resample a sequence
void integrate_sequence(int N_x, double* x,
                        int every,
                        int N_y, double* y)
{
    assert(N_x % every == 0);
    const int num_every = N_x / every;
    const double scale = 1. / every;
    assert(num_every == N_y);
    for(int i=0; i<num_every; i++)
        y[i] = trapz(every, x + i * every) * scale;
}


