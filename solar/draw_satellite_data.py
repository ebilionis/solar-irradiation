"""
Draw some satellite data just to see how they look.
"""


from pyhdf.SD import SD, SDC
import matplotlib.pyplot as plt
from satellite import get_unscaled


# Open the hdf file
f = SD('../data/goes13_2013_334_0232.1km.level2.hdf', SDC.READ)

# Get lattitude and longtitude
lat_sd = f.select('latitude')
lon_sd = f.select('longitude')
# Scale them properly
lat = get_unscaled(lat_sd)
lon = get_unscaled(lon_sd)
# Get the cloud fraction
k_sd = f.select('cloud_fraction')
k = get_unscaled(k_sd)

c = plt.imshow(k)
plt.colorbar(c)
plt.show()
