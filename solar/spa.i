/* File: spa.i */
%module spa

%{
#define SWIG_FILE_WITH_INIT
#include "spa.h"
#include "spa_help.h"
%}

%include "numpy.i"
%init %{
import_array();
%}

%apply (int DIM1, double* INPLACE_ARRAY1)
       {(int num_zenith, double* zenith),
        (int num_r, double* r),
        (int num_sunrise, double* sunrise),
        (int num_sunset, double* sunset)};
%apply (int DIM1, float* INPLACE_ARRAY1)
       {(int num_lons, float* lons),
        (int num_lats, float* lats)};
%apply (int DIM1, int* IN_ARRAY1)
       {(int num_years, int* years),
        (int num_months, int* months),
        (int num_days, int* days),
        (int num_hours, int* hours),
        (int num_minutes, int* minutes),
        (int num_seconds, int* seconds)};


%include "spa.h"
%include "spa_help.h"
