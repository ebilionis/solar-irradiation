
"""
Plot the one step ahead predictive distribution.
"""

import sys
import os
sys.path.insert(0, os.path.abspath('..'))
import matplotlib.pyplot as plt
import numpy as np
import scipy
import cPickle as pickle
import solar
import hmm


assert len(sys.argv) == 2, '\nUsage:\n\t' + sys.argv[0] + ' pickled_model'
filename = sys.argv[1]
with open(filename, 'rb') as fd:
    solar_run = pickle.load(fd)
model = solar_run['model']
E0 = solar_run['E0']
P = model.num_symbols
data = solar.IrradianceData(num_symbols=P, airmass_class=solar_run['airmass'],
                            how=solar_run['how'], date='2012', E0=E0)
#Z = model.decode(data.discrete_deseasoned())
#plt.plot(Z)
#plt.show()
#I = np.arange(Z.shape[0])
#E = data.irradiance
#for i in xrange(P):
#    plt.plot(I[Z == i], E[Z == i], 'o', markersize=5)
#plt.show()
Xt = data.discrete()
XP = model.one_step_ahead_prediction(Xt)
a = data.get_discrete_one_step_ahead_cdf(XP)
print scipy.stats.kstest(a, 'uniform')
plt.plot(np.sort(a), '*-', markersize=10)
plt.plot(np.arange(a.shape[0]), np.arange(a.shape[0]) / 365., 'r', linewidth=2)
plt.show()
plt.hist(a, bins=30)
plt.show()
t = np.linspace(0, data.E0, data.num_symbols)
alpha = data.airmass()
plt.plot(np.arange(XP.shape[0]) + 0.5,
	  t[Xt], 'm-*', linewidth=2.,
         markersize=10)
plt.plot(np.arange(XP.shape[0]) + 0.5,
	  t[data.get_discrete_ppf(0.5, XP)],
         'gd-', linewidth=2., markersize=10)
plt.plot(np.arange(XP.shape[0]) + 0.5,
	  t[data.get_discrete_ppf(0.05, XP)],
         'k', linewidth=2)
plt.plot(np.arange(XP.shape[0]) + 0.5,
	  t[data.get_discrete_ppf(0.95, XP)],
         'k', linewidth=2)
#plt.pcolor(XP.T + 1e-10)
#plt.show()
#plt.plot(np.arange(XP.shape[0]) + 0.5,
#	 data. data.airmass() * (Xt + 0.5), 'm-*', linewidth=2.,
#         markersize=10)
#plt.plot(np.arange(XP.shape[0]) + 0.5,
#	 data. data.airmass() * (data.get_discrete_ppf(0.5, XP) + 0.5),
#         'gd-', linewidth=2., markersize=10)
#plt.plot(np.arange(XP.shape[0]) + 0.5,
#	 data. data.airmass() * (data.get_discrete_ppf(0.05, XP) + 0.5),
#         'k', linewidth=2)
#plt.plot(np.arange(XP.shape[0]) + 0.5,
#	 data. data.airmass() * (data.get_discrete_ppf(0.95, XP) + 0.5),
#         'k', linewidth=2)
#plt.xlim([0, 365 * 96])
plt.show()
plt.pcolor(XP.T)
plt.plot(Xt, '*r')
plt.show()
