"""
Make the NWP regression data.
Loop over all the nwp data files, read the data of the first timestep
and correlate them to the observed irradiance data.
"""


import tarfile
import os
from nwp import NWPFile
import cPickle as pickle


if __name__ == '__main__':
    # Extract the nwp data in the local directory if required
    nwp_data_dir = os.path.abspath('./nwp_data_anl')
    zipped_nwp_data = os.path.abspath('../data/nwp_data_anl.tgz')
    output_file = os.path.abspath('../data/nwp_regression_X.pcl')
    if not os.path.isdir(nwp_data_dir):
        print 'Extracting nwp data...'
        zf = tarfile.open(os.path.abspath(zipped_nwp_data))
        print 'Done!'
        zf.extractall('.')
    # Loop over all the files and extract the first time step
    data = {'RAP':{}, 'HRRR':{}, 'NAM':{}}
    for filename in os.listdir(nwp_data_dir):
        filename = os.path.join(nwp_data_dir, filename)
        print 'Reading', filename
        f = NWPFile(filename)
        if f.minutes[0, 0] > 0:
            # Something is wrong with the data in this case
            continue
        d = {}
        for name in f._get_variable_list():
            d[name] = getattr(f, name)[0, :]
        data[f.model_name][f.date] = d
    with open(output_file, 'wb') as fd:
        pickle.dump(data, fd)
