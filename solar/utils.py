"""
Some utility functions used through out.

Author:
    Ilias Bilionis
"""


__all__ = ['make_lag']


import numpy as np
import spa
import math
from scipy import weave


def compute_crps(X, x):
    """
    Compute the Continuous Rank Probability Score (CRPS) of the ensemble ``X``
    when the observation was ``x``.

    I am assuming here that ``X`` is a 1D vector and that ``x`` is just a number.
    """
    M = float(X.shape[0])
    mad = np.linalg.norm(X - x, np.inf) / M
    cross = 0.5 * np.linalg.norm(
                        np.tile(X, (M, 1)) - np.repeat(X, M).reshape((M, M)),
                           np.inf) / M ** 2
    crps = mad - cross
    return mad, cross

def make_lag(t, L):
    """
    Make X, Y from a time series ``t`` using a lag ``L``.
    """
    n = t.shape[0] - L
    q = t.shape[1]
    X = np.zeros((n, L * q))
    Y = np.zeros((n, q))
    for i in xrange(L + 1, n):
        X[i, :] = t[i-L:i, :].flatten()
        Y[i, :] = t[i, :]
    return X, Y


def sample_single_prediction(X0, num_ahead, m, z):
    """
    Sample a single prediction.
    """
    k = X0.shape[1]
    Y = []
    X_current = np.hstack([X0, z[:1, :]])
    for i in xrange(num_ahead):
        y = m.posterior_samples(X_current, 1)
        Y.append(y)
        X_current[0, :-2] = X_current[:, 1:-1]
        X_current[0, -2] = y[0, 0]
        X_current[0, -1] = z[i, 0]
    return np.vstack(Y)


def sample_prediction(X0, num_ahead, m, z, size=1):
    """
    Sample ``size`` predictions.
    """
    Y = []
    for i in xrange(size):
        Y.append(sample_single_prediction(X0, num_ahead, m, z))
    return Y


def get_error_bars(Y_s):
    """
    Get error bars from sample.
    """
    m = np.mean(Y_s, axis=0)
    s = np.std(Y_s, axis=0)
    return m, m - 2. * s, m + 2. * s


def astro_compute_many(lon, lat, dates, comm=None):
    """
    Compute the solar zenith and the solar latitude at many locations and times.
    """
    if comm is not None:
        rank = comm.Get_rank()
        size = comm.Get_size()
    else:
        rank = 0
        size = 1
    K = np.prod(lon.shape)
    N = dates.shape[0]
    my_K = K / size
    my_z = np.ndarray(N * my_K)
    my_r = np.ndarray(N * my_K)
    my_lon = lon.flatten()[my_K * rank:my_K * (rank + 1)]
    my_lat = lat.flatten()[my_K * rank:my_K * (rank + 1)]
    spa_data = spa.spa_data()
    spa.spa_calculate_many_locations(spa_data, my_lon, my_lat,
                           dates.year, dates.month, dates.day,
                           dates.hour, dates.minute, dates.second, my_z, my_r)
    my_z = my_z / 360. * (2. * math.pi)
    if comm is not None:
        z = np.hstack(comm.allgather(my_z.reshape((N, my_K))))
        r = np.hstack(comm.allgather(my_r.reshape((N, my_K))))
    else:
        z = my_z
        r = my_r
    z = z.reshape((N,) + lat.shape)
    r = r.reshape((N,) + lon.shape)
    return z, r
