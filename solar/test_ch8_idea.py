"""
Test the idea of chapter 8.
"""

import numpy as np
import pandas as pd
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import best
from sklearn.preprocessing import Scaler
import cPickle as pickle


# Load data
data = pd.read_hdf('../data/full_solar.h5', 'solar')
max_I = np.max(data.irradiance.interpolate())
max_P = np.max(data.pressure.interpolate())
min_P = np.min(data.pressure.interpolate())
max_T = np.max(data.temperature.interpolate())
min_T = np.min(data.temperature.interpolate())

n = 96 * 30


y = data.irradiance[1:n+1].interpolate() / max_I
x1 = data.irradiance[:n].interpolate() / max_I
x2 = (np.cos(data.zenith[:n]).interpolate() - 1.) / 2.
x3 = (data.pressure[:n].interpolate() - min_P) / (max_P - min_P)
x4 = (data.temperature[:n].interpolate() - min_T) / (max_T - min_T)

phi = best.gpc.ProductBasis(polynomials=[
    best.gpc.OrthogonalPolynomial(4, left=0, right=1) for i in xrange(4)])
X = np.hstack([np.atleast_2d(x1).T,
               np.atleast_2d(x2).T,
               np.atleast_2d(x3).T,
               np.atleast_2d(x4).T])
PHI = phi(X)

rvm = best.rvm.RelevanceVectorMachine()
rvm.set_data(PHI, y)
rvm.initialize()
rvm.train(verbose=True)
f = rvm.get_generalized_linear_model(phi)
with open('f.dat', 'wb') as fd:
    pickle.dump(f, fd)
print str(f)

quit()
PHI = phi(X)
quit()
fig = plt.figure()
ax = Axes3D(fig)
ax.plot(x1, x3, y, '.')
plt.show()

plt.plot(x1, y, '.')
plt.show()

plt.plot(x2, y, 'r.')
plt.show()

plt.plot(x3, y, 'g.')
plt.show()

plt.plot(x4, y, 'm.')
plt.show()
