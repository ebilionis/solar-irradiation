"""
Using this:
    Gaussian Process Priors with Uncertain Inputs - Application to multiple-
    Step Ahead Time Series Forecasting, A. Girard, C. E. Rasmussen,
    J. Q. Candel and R. Murray-Smith

We test the prediction of just the temperature data from the solar irradiance
project.

Author:
    Ilias Bilionis

Requires:
    pydelay to solve the delay equation and collect the Mackey-Glass chaotic
    time series used in the example.

"""


import numpy as np
from pydelay import dde23
from utils import make_lag
from utils import sample_prediction
from utils import get_error_bars
import pandas as pd
import GPy


def generate_timeseries_data(num_days=365):
    """
    Generate the time series data.

    """
    from datetime import timedelta
    import matplotlib.pyplot as plt
    from extraterrestrial_irradiance import ExtraterrestrialIrradiance
    from clear_sky_model import ClearSkyIrradiance
    import math
    data = pd.read_hdf('../data/full_solar.h5', 'solar')
    z = np.cos(data).zenith.resample('H', how='mean')[:24*num_days, None]
    I = data.irradiance.interpolate().resample('H')[:24*num_days, None]
    return np.log(I + 1), z


if __name__ == '__main__':
    import matplotlib.pyplot as plt

    # The lag to be used
    L = 2

    # The variance of the corruption noise
    noise_var = 0.001

    # The number of samples to be used for the GP
    num_train = 100

    # The number of samples from the forecast
    num_samples = 100

    # The number of days to use
    num_days = 100

    # Generate the timeseries data
    t, z = generate_timeseries_data(num_days)

    # Normalize the series
    t_m = np.mean(t)
    t_s = np.std(t)
    t = (t - t_m) / t_s

    #plt.plot(t)
    #plt.show()
    #a = raw_input('ts')


    # Turn the data into the appropriate form
    X, Y = make_lag(t, L)
    # Add the cosine of the solar zenith as a extra dimension
    X = np.hstack([X, z[:X.shape[0], :]])

    # Downsample the data
    idx = np.arange(X.shape[0])
    np.random.shuffle(idx)
    X = X[idx[:num_train], :]
    Y = Y[idx[:num_train], :]

    # Corrupt the targets with some small noise
    Y += np.sqrt(noise_var) * np.random.randn(*Y.shape)

    # Construct and train the GP
    #k = GPy.kern.rbf(L, ARD=True)
    #k = GPy.kern.exponential(L + z.shape[1], ARD=True)
    k = (GPy.kern.linear(L + z.shape[1], ARD=True)
         + GPy.kern.exponential(L + z.shape[1], ARD=True))
    m = GPy.models.SparseGPRegression(X, Y, kernel=k)
    m.optimize_restarts(num_restarts=1, messages=True, max_iters=1000)
    print m

    # Make predictions
    num_start = 100 + L + 30
    num_ahead = 24 * 3

    # The predict_steps we are actually looking at
    predict_steps = np.arange(num_start, num_start + num_ahead)[:, None]

    X0 = t[num_start - L: num_start, :].T
    Y_s = sample_prediction(X0, num_ahead, m, z[num_start+1:, :], size=num_samples)
    mu, lower, upper = get_error_bars(Y_s)

    # Plot the mean with error bars
    GPy.util.plot.gpplot(predict_steps, mu, lower, upper)

    # Plot the truth
    Y_t = t[num_start:num_start + num_ahead, :]
    plt.plot(predict_steps, Y_t, '--r', linewidth=2)
    plt.legend(['Model mean', 'lower error bar', 'upper error bar', 'observed']
                ,loc='best')
    plt.show()
    a = raw_input('Press Enter...')
