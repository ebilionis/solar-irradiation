"""
This module contains various functions/classes that are related to the solar
irradiance data.

Author:
    Ilias Bilionis
    Mathematics and Computer Science Division
    Argonne National Laboratory
    December 2013
"""

__all__ = ['PlaneEarthInverseAirMass', 'YoungInverseAirMass', 'IrradianceData']


import numpy as np
import pandas as pd
import scipy.stats
import _core as core


class _BaseInverseAirMass(object):
    """
    This is the base class of all classes representing the calculation of
    the air mass (i.e. the distance a sunlight beam travels within the
    atmosphere) given the solar altitude. This is not supposed to be initialized
    on each one. It is to serve as a base class for special types of inverse_airmass
    calculations. The only functionality it offers is the ability to specify
    and store the number of parameters of the air mass function (if any).

    :param num_param:   The number of parameters of the inverse_airmass model.
    :type num_param:    int
    :param param:       The parameters (in case you want them to be fixed)
    :type param:        float or 1D numpy.ndarray
    :param __name__:    A name for the inverse_airmass model.
    :type __name__:     str
    """

    # The number of parameters (if any)
    _num_param = None

    # The parameters of the inverse_airmass function
    _param = None

    @property
    def num_param(self):
        """Get the number of parameters."""
        return self._num_param

    @property
    def param(self):
        """Get the parameters of the inverse_airmass function."""
        return self._param

    @param.setter
    def param(self, value):
        """Set the parameters of the inverse_airmass function."""
        if value is None:
            self._param = None
            return
        value = np.array(value)
        assert value.ndim == 1
        assert value.shape[0] == self.num_param
        self._param = value

    def __init__(self, num_param, param=None, name='BaseInverseAirMass'):
        """
        Initialize the object.
        """
        num_param = int(num_param)
        assert num_param >= 0
        self._num_param = num_param
        self.param = param
        self.__name__ = name

    def __str__(self):
        """
        Return a string representation of the object."""
        s = self.__name__ + ', num_param: ' + str(self.num_param)
        s += ', param: ' + str(self.param)
        return s

    def __call__(self, alpha, param=None):
        """
        Evaluate the inverse_airmass at solar altitude ``alpha`` using the parameters
        ``param``.

        :param alpha:   The solar altitude.
        :type alpha:    float or 1D numpy.ndarray
        :param param:   The parameters of the inverse_airmass function. If not specified
                        then we attempt to look at the parameters stored in
                        :attr:`self.param`.
        :returns:       The inverse_airmass.
        :raises:        :class:`ValueError` if the parameters are not properly
                        specified.
        """
        if param is None:
            param = self.param
        if param is None and self.num_param > 0:
            raise ValueError('The function you are trying to evaluate requires'
                             ' parameters and they are not fixed.')
        elif param is not None:
            param = np.array(param)
            assert param.ndim == 1
            assert param.shape[0] == self.num_param
        res = self._eval(alpha, param)
        res[res < 0.] = 0.
        return res

    def _eval(self, alpha, param):
        """
        Evaluate the inverse_airmass at ``alpha`` using ``param``.

        This function must be overloaded by the children of this class.
        """
        raise NotImplementedError('Overload this in your child class!')


class PlaneEarthInverseAirMass(_BaseInverseAirMass):
    """
    Computes the inverse_airmass using the plain Earth approximation.

    It requires no parameters.
    """

    def __init__(self, name='Plane Airmass Approximation'):
        """
        Initialize the object.
        """
        super(PlaneEarthInverseAirMass, self).__init__(0, name=name)

    def _eval(self, alpha, param):
        """
        Overloading :meth:`_BaseInverseAirMass._eval()`.
        """
        return np.sin(alpha)


class YoungInverseAirMass(_BaseInverseAirMass):
    """
    The inverse_airmass described in [Young 1994] with arbitrary parameters.

    Use the static method :meth:`YoungInverseAirMass.get_fixed()` to
    get the parametrized version using the parameters in the afforementioned
    paper.
    """

    def __init__(self, param=None, name='Young 1994 Airmass Approximation'):
        """
        Initialize the object.
        """
        super(YoungInverseAirMass, self).__init__(6, param=param, name=name)

    def _eval(self, alpha, param):
        """
        Overloading :meth:`_BaseInverseAirMass._eval()`.
        """
        t = np.sin(alpha)
        tmp1 = param[0] * t ** 2. + param[1] * t + param[2]
        tmp2 = t ** 3. + param[3] * t ** 2. + param[4] * t + param[5]
        return tmp2 / tmp1

    @staticmethod
    def get_fixed(name='Young 1994 Airmass Approximation (fixed)'):
        return YoungInverseAirMass(param=[1.002432, 0.149864, 0.0096467, 0.149864,
                                   0.0102963, 0.000303978], name=name)

class YoungInrvineInverseAirMass(_BaseInverseAirMass):
    def __init__(self, param=None, name='Young-Irvine Air Mass'):
        super(YoungInrvineInverseAirMass, self).__init__(0, param=param, name=name)

    def _eval(self, alpha, param):
        t = np.sin(alpha)
        tmp = (1. - 0.0012 * (t ** (-2.) - 1.))
        return t * tmp

class RozenbergInverseAirMass(_BaseInverseAirMass):
    def __init__(self, param=None, name='Rozenberg Inverse Air Mass'):
        super(RozenbergInverseAirMass, self).__init__(0, param=param, name=name)
    def _eval(self, alpha, param):
        t = np.sin(alpha)
        return t + 0.025 * np.exp(-11. * t)

class IdentityInverseAirMass(_BaseInverseAirMass):
    """
    A fake inverse_airmass equal to 1.
    """

    def __init__(self, name='Identity Airmass.'):
        """
        Initialize the object.
        """
        super(IdentityInverseAirMass, self).__init__(0, name=name)

    def _eval(self, alpha, param):
        """
        Overloading :meth:`_BaseInverseAirMass._eval()`.
        """
        tmp = alpha.copy()
        tmp[:] = 1.
        return tmp


class IrradianceData(object):
    """
    A class that represents the irradiance data.
    """

    # The actual data (panda HDF5 object)
    _data = None

    # The class that calculates the InverseAirMass.
    _inverse_airmass_class = None

    # How do you want the data served when using the generric attributes
    _how = None

    # The extraterstrial irradiance
    _E0 = None

    # The number of symbols used to discretize the deseasoned irradiance data
    _num_symbols = None

    @property
    def num_symbols(self):
        """Get the number of symbols."""
        return self._num_symbols

    @num_symbols.setter
    def num_symbols(self, value):
        """Set the number of symbols."""
        value = int(value)
        assert value > 1
        self._num_symbols = value

    @property
    def data(self):
        """Get the data."""
        return self._data

    @property
    def how(self):
        """Get the ``how`` attribute."""
        return self._how

    @how.setter
    def how(self, value):
        """Set the ``how`` attribute."""
        if not (value == 'quarter' or value == 'hourly' or value == 'daily'):
            raise ValueError('The ``how`` attribute must be `quarter`, `hourly`'
                             ' or `daily`.')
        self._how = value

    @property
    def E0(self):
        """Get the extraterrestrial irradiance."""
        return self._E0

    @E0.setter
    def E0(self, value):
        """Set the extraterrestrial irradiance."""
        value = float(value)
        assert value > 0.
        self._E0 = value

    @property
    def inverse_airmass_class(self):
        """Get the class that calculates the inverse_airmass."""
        return self._inverse_airmass_class

    @inverse_airmass_class.setter
    def inverse_airmass_class(self, value):
        """Set the class that calculates the inverse_airmass."""
        assert isinstance(value, _BaseInverseAirMass)
        self._inverse_airmass_class = value

    @property
    def num_samples(self):
        """Get the number of samples."""
        return self.data.shape[0]

    def integrate_sequence(self, x, every):
        """
        Integrate sequence according to every.
        """
        num_every = x.shape[0] / every
        res = np.zeros(num_every)
        core.integrate_sequence(np.array(x, dtype='float64'), every, res)
        return res

    def get_inverse_airmass_daily(self, param=None):
        """Get the inverse_airmass on a daily basis."""
        tmp = self.inverse_airmass_class(self.data['alpha'], param=param)
        #tmp[tmp <= 0.] = 0.
        #tmp *= 1. / 96.
        #alpha = tmp.resample('D', how='sum')
        alpha = tmp.resample('D', how='max')
        alpha[alpha <= 0.] = None
        return alpha

    @property
    def irradiance(self):
        """Get the irradiance as specified by ``how``."""
        return self._irradiance

    @property
    def inverse_airmass(self):
        """Get the inverse_airmass as specified by ``how``."""
        return self._inverse_airmass

    def __init__(self, data=None, data_filename='../data/data_w_alpha.h5',
                 inverse_airmass_class=YoungInverseAirMass.get_fixed(), E0=1361.,
                 how='daily', num_symbols=50, date=None):
        """Initialize the object."""
        if data is not None:
            assert isinstance(data, pd.DataFrame)
        else:
            data = pd.read_hdf(data_filename, 'solar')
        if date is not None:
            if isinstance(date, list) or isinstance(date, tuple):
                data = data[date[0]:date[1]]
            else:
                data = data[date]
        self._data = data
        self.inverse_airmass_class = inverse_airmass_class
        self.how = how
        self.E0 = E0
        self.num_symbols = num_symbols
        # Fix the data once and for all
        tmp_irradiance = data.Irradiance
        tmp_inverse_airmass = self.inverse_airmass_class(data.alpha)
        tmp_max_irradiance = E0 * tmp_inverse_airmass
        # Make sure that irradiance is well-behaved
        cut = 0.
        tmp_irradiance[tmp_irradiance <= cut] = 0.
        max_idx = tmp_irradiance > tmp_max_irradiance
        tmp_irradiance[max_idx] = np.array(tmp_max_irradiance[max_idx], dtype='float32')
        null_idx = ~tmp_irradiance.notnull()
        tmp_irradiance[null_idx] = np.array(tmp_max_irradiance[null_idx], dtype='float32')
        if how == 'hourly' or how == 'daily':
            import core
            x = np.arange(10, dtype='float64')
            tmp_irradiance = self.integrate_sequence(tmp_irradiance, self.num_how)
            tmp_inverse_airmass = self.integrate_sequence(tmp_inverse_airmass, self.num_how)
            tmp_max_irradiance = E0 * tmp_inverse_airmass
        self._irradiance = tmp_irradiance
        self._max_irradiance = tmp_max_irradiance
        self._inverse_airmass = tmp_inverse_airmass

    @property
    def max_irradiance(self):
        """
        The maxium irradiance that is allowed under the inverse_airmass model.
        """
        return self._max_irradiance

    @property
    def deseasoned(self):
        """Get the deseasoned irradiance data according to ``how``."""
        tmp = self.irradiance / self.max_irradiance
        tmp[self.irradiance <= 0.] = 0.
        tmp[tmp >= 1.] = 1.
        return tmp

    @property
    def min_irradiance(self):
        """
        The minimum irradiance we observe.
        """
        return 0.

    @property
    def irradiance_bins(self):
        """
        Get the bins of discretized irradiances.
        """
        return np.linspace(self.irradiance.min(), self.irradiance.max(), self.num_symbols)

    @property
    def deseasoned_bins(self):
        return np.linspace(0, 1, self.num_symbols)

    def binify(self, x, t):
        """
        Binify sequence ``x`` using bins ``t``.
        """
        res = np.ndarray(x.shape[0], dtype='int32')
        for i in xrange(1, t.shape[0]):
            l = t[i - 1]
            u = t[i]
            res[(l <= x) * (x <= u)] = i
        res[x <= 0] = 0
        return res

    @property
    def discrete_deseasoned(self):
        """Get the discrete deseasoned irradiance data."""
        E_deseasoned = self.deseasoned
        t = self.deseasoned_bins
        return self.binify(E_deseasoned, t)

    @property
    def discrete(self):
        """Get the discrete deseasoned irradiance data."""
        E = self.irradiance
        t = np.linspace(0, np.max(self.max_irradiance), self.num_symbols)
        return self.binify(E, t)

    def get_ppf(self, p, XP):
        """
        Get the p-th percentile of the predictive probabilities XP.
        """
        assert p >= 0. and p <= 1.
        XP = np.array(XP)
        assert XP.ndim == 2
        assert XP.shape[1] == self.num_symbols
        symbols = np.arange(self.num_symbols)
        inverse_airmass = self.inverse_airmass
        t = self.deseasoned_bins
        XD = self.get_discrete_ppf(p, XP)
        res = self.E0 * inverse_airmass * t[XD]
        return res

    def get_discrete_ppf(self, p, XP):
        """
        Get the p-th percentile of the predictive probabilities XP.
        """
        assert p >= 0. and p <= 1.
        XP = np.array(XP)
        assert XP.ndim == 2
        assert XP.shape[1] == self.num_symbols
        symbols = np.arange(self.num_symbols)
        res = np.ndarray(XP.shape[0], dtype='int32')
        for i in xrange(XP.shape[0]):
            rv = scipy.stats.rv_discrete(values=(symbols, XP[i, :]))
            res[i] = int(rv.ppf(p))
        return res

    @property
    def num_how(self):
        """
        Return the number of original samples that correspond to one sample,
        when using ``how`` to resample the data.
        """
        if self.how == 'quarter':
            return 1
        elif self.how == 'hourly':
            return 4
        elif self.how == 'daily':
            return 96

    def split(self, num_train):
        """Split the samples in two.

        Splits the samples in two so that one component can be used for training
        and the other for testing purposes.

        :param num_train:   The number of samples to be used for training. If
                            this is a float between 0. and 1. then it is assumed
                            that num_train per cent of the samples will be
                            for training and the rest for testing. If it is an
                            integer, we assume that you are counting quarters,
                            hours or days according to the value of ``how``.
        :type num_train:    int or float
        """
        if isinstance(num_train, float):
            assert num_train >= 0. and num_train <= 1.
            num_train = int(num_train * self.num_samples)
        else:
            num_train = int(num_train)
            num_train *= self.num_how
        train_data = self.data[:num_train]
        test_data = self.data[num_train:]
        train_data = IrradianceData(data=self.data[:num_train],
                                    inverse_airmass_class=self.inverse_airmass_class,
                                    E0=self.E0,
                                    how=self.how,
                                    num_symbols=self.num_symbols)
        test_data = IrradianceData(data=self.data[num_train:],
                                   inverse_airmass_class=self.inverse_airmass_class,
                                   E0=self.E0,
                                   how=self.how,
                                   num_symbols=self.num_symbols)
        return train_data, test_data

    def get_discrete_one_step_ahead_cdf(self, XP, XD):
        """
        Get the p-th percentile of the predictive probabilities XP.
        """
        XP = np.array(XP)
        assert XP.ndim == 2
        assert XP.shape[1] == self.num_symbols
        symbols = np.arange(self.num_symbols)
        res = np.ndarray(XP.shape[0])
        core.get_discrete_one_step_ahead_cdf(XP, XD, res)
        return res
        for i in xrange(XP.shape[0]):
            rv = scipy.stats.rv_discrete(values=(symbols, XP[i, :]))
            res[i] = rv.cdf(XD[i])
        return res





if __name__ == '__main__':
    """
    Just run some small tests.
    """
    import matplotlib.pyplot as plt
    data = IrradianceData(how='daily', date=['1991','2013'], E0=1361.)
    #I_monthly_max = data.data.Irradiance.resample('m', how='max')
    #plt.plot(I_monthly_max, '-*', linewidth=2)
    #plt.show()
    #plt.plot(data.max_irradiance, 'r', linewidth=2)
    plt.plot(data.deseasoned, '.')
    plt.show()
    #fig, ax1 = plt.subplots()
    #ax1.plot(data.irradiance, 'r')
    #ax2 = ax1.twinx()
    #ax2.plot(data.deseasoned)
    #plt.show()
    #plt.plot(data.discrete_deseasoned)
    #plt.show()
