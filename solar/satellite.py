"""
Some routines for reading and manipulating satellite data.
"""

from pyhdf.SD import SD, SDC
import numpy as np
from observer import Observer
from datetime import datetime, timedelta


__all__ = ['get_unscaled', 'CLAVRxFile']


def get_unscaled(data, i=None, j=None):
    """
    Assuming that data is an HDF data set.

    :param data:    An HDF data set.
    :param i:       Index for line scans. If ``None`` then all lines are
                    returned.
    :type i:        NoneType, int or list of int
    :param j:       Index for columns. If ``None`` then all columns are
                    returned.
    :type j:        NoneType, int or list of int
    """
    if data.attributes()['SCALED']:
        scale_factor = np.array(data.attributes()['scale_factor'], dtype='float32')
        add_offset = np.array(data.attributes()['add_offset'], dtype='float32')
    else:
        scale_factor = 1
        add_offset = 0
    x = data.get()
    if i is None and j is None:
        return scale_factor * x + add_offset
    elif i is None:
        return scale_factor * x[:, j] + add_offset
    elif j is None:
        return scale_factor * x[i, :] + add_offset
    else:
        return scale_factor * x[i, j] + add_offset


class CLAVRxFile(object):

    """
    Object representing a CLAVR-x file.

    The user can select to view, only part of the file by specifying a patch
    of latitudes, longtitudes as well as part of the data by specifying
    only some variables to be viewed.

    The patch is basically a disk around a particular pair of longtitude
    and latitude. It is specified by an observer.

    :param filename:    The satellite file.
    :type filename:     str
    :param observer:    The observer.
    :type observer:     :class:`solar.Observer`
    :param box_width_rows:  The width of the box surrounding the observer (in
                            pixels). Remember that pixels correspond to
                            approximately 1 km^2. This parameter specifies the
                            number of rows of the box.
    :type box_width_rows:   int
    :param box_width_cols:  The number of columns of the box surrounding the
                            observer. If this is ``None``, then its value is
                            assumed to be the same as `box_width_row`.
    :type box_width_cols:   int
    """

    def __init__(self, filename, observer=Observer(), box_width_rows=400,
                 box_width_cols=None):
        """
        Initialize the object.
        """
        # Open the file
        self.f = SD(filename, SDC.READ)
        self.observer = observer
        self.box_width_rows = int(box_width_rows)
        if box_width_cols is None:
            box_width_cols = self.box_width_rows
        self.box_width_cols = int(box_width_cols)
        self.full_latitude = self.get('latitude')
        self.full_longitude = self.get('longitude')
        self.i0, self.j0 = self.get_observer_indices()
        # Get the indices inside the box
        num_rows = self.full_latitude.shape[0]
        num_cols = self.full_latitude.shape[1]
        self.i_l = max(self.i0 - self.box_width_rows / 2, 0)
        self.i_u = min(self.i0 + self.box_width_rows / 2, num_rows)
        self.j_l = max(self.j0 - self.box_width_cols / 2, 0)
        self.j_u = min(self.j0 + self.box_width_cols / 2, num_rows)

    def get(self, name):
        """
        Get an **unscaled** version of a variable called ``name``.

        :param name:    The name of a variable.
        :type name:     str
        """
        data = self.f.select(name)
        return get_unscaled(data)

    def get_at_observer(self, name):
        """
        Get the variable at the location of the observer.

        :param name:    The name of a variable.
        :type name:     str
        """
        data = self.f.select(name)
        return get_unscaled(data, self.i0, self.j0)

    def get_patch(self, name):
        """
        Get the variable at a the patch.

        :param name:    The name of a variable.
        :type name:     str
        """
        data = self.f.select(name)
        data = get_unscaled(data)
        return data[self.i_l:self.i_u, self.j_l:self.j_u]

    @property
    def datetime(self):
        """
        Get a datetime object representing the time of the measurement.
        """
        y = self.f.attributes()['START_YEAR']
        doy = self.f.attributes()['START_DAY'] - 1
        fh = self.f.attributes()['END_TIME']
        t = datetime(y, 1, 1)
        dt = timedelta(doy + fh / 24.)
        return t + dt

    def get_observer_indices(self):
        """
        Return the indices of the pixels closer to the position of the
        observer.
        """
        n1 = self.full_latitude.shape[0]
        n2 = self.full_latitude.shape[1]
        n = n1 * n2
        x1 = np.array([[self.observer.latitude, self.observer.longitude]])
        x2 = np.hstack([self.full_latitude.reshape((n, 1), order='C'),
                        self.full_longitude.reshape((n, 1), order='C')])
        d = cdist(x1, x2)
        k = np.argmin(d[0, :])
        i0 = k / n2
        j0 = k % n2
        return i0, j0

    def swap_file(self, new_filename):
        """
        Swap the current file with ``new_filename``.
        This is done under the assumption that the new data files contains exactly the
        same scan type as the old one. This assumption is not checked! We do it this way
        in order to speed up things a little bit.

        :param new_filename: The new satelite filename.
        :type new_filename:  str
        """
        self.f = SD(new_filename, SDC.READ)

    def plot(self, name):
        """
        Plot a property.
        """
        import mpl_toolkits.basemap as bm
        data = self.get_patch(name)
        lon = self.get_patch('longitude')
        lat = self.get_patch('latitude')
        m = bm.Basemap(projection='mill',
                       llcrnrlon=lon.min(), urcrnrlon=lon.max(),
                       llcrnrlat=lat.min(), urcrnrlat=lat.max(),
                       resolution='l')
        m.drawcoastlines()
        m.drawstates()
        m.drawcountries()
        x, y = m(lon, lat)
        m.pcolormesh(x, y, data)
        m.colorbar()
        i_c = self.box_width_rows / 2
        j_c = self.box_width_cols / 2
        m.scatter(x[i_c, j_c], y[i_c, j_c], 50, marker='o', color='k')


if __name__ == '__main__':
    f = CLAVRxFile('./data/goes13_2013_334_0232.1km.level2.hdf', box_width_rows=200)
    import matplotlib.pyplot as plt
    f.plot('cloud_phase')
    plt.show()
