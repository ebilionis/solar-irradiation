"""
Train an HMM using the Daily data without deseasoning.
"""

import sys
import os
sys.path.insert(0, os.path.abspath('..'))
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import cPickle as pickle
import solar
import hmm


# Number of hidden states
K = 24
# Number of symbols
P = 200
# Number of training points
num_train = 365 * 20
# Prior for pi
f_pi = 1.
# Prior for A
f_A = 1.
# Prior for B
f_B = 1.
# Tolerance
tol = 1e-6
# Maximum number of iterations
max_iter = 10000
E0 = 350.

# Load the data
airmass = solar.IdentityAirMass()
data = solar.IrradianceData(num_symbols=P, E0=E0,
                            how='daily')
data, tmp = data.split(num_train)
X = data.discrete()
print X
plt.plot(data.irradiance)
plt.show()
plt.plot(X)
plt.show()
E = data.irradiance


# Make the model
pi = np.random.dirichlet(np.ones(K) * f_pi)
A = np.random.dirichlet(np.ones(K) * f_A, size=(K,))
B = np.random.dirichlet(np.ones(P) * f_B, size=(K,))
model = hmm.DiscreteHMM(pi, A, B)
loglike = model.train(X=X, tol=1e-6, max_iter=max_iter)
solar_run = {}
solar_run['model'] = model
solar_run['loglike'] = loglike
solar_run['how'] = data.how
solar_run['airmass'] = data.airmass_class
solar_run['E0'] = E0
filename = 'results/solar_' + data.how + '_K=' + str(K) + '_P=' + str(P) + '.pickle'
print 'Writing:', filename
with open(filename, 'wb') as fd:
    pickle.dump(solar_run, fd)
