/* File: core.i */
%module core

%{
#define SWIG_FILE_WITH_INIT
#include "core.hpp"
%}

%include "numpy.i"
%init %{
import_array();
%}

%apply (int DIM1, double* INPLACE_ARRAY1)
       {(int N_CDF, double* CDF),
        (int N_y, double* y)};
%apply (int DIM1, double* IN_ARRAY1)
       {(int N_x, double* x)};
%apply (int DIM1, int DIM2, double* IN_ARRAY2)
       {(int M_XP, int N_XP, double* XP)};
%apply (int DIM1, int* IN_ARRAY1)
       {(int N_XD, int* XD)};

%include "core.hpp"
