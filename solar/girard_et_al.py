"""
Reproduction of:
    Gaussian Process Priors with Uncertain Inputs - Application to multiple-
    Step Ahead Time Series Forecasting, A. Girard, C. E. Rasmussen,
    J. Q. Candel and R. Murray-Smith

Author:
    Ilias Bilionis

Requires:
    pydelay to solve the delay equation and collect the Mackey-Glass chaotic
    time series used in the example.

"""


import numpy as np
from pydelay import dde23
from utils import make_lag
from utils import sample_prediction
from utils import get_error_bars
import GPy


def generate_timeseries_data():
    """
    Generate the time series data.

    The time series is the Mackey-Glass chaotic time series. We return exactly
    what is described in the paper.
    """
    eqns = { 'x' : '0.2 * x(t-tau) / (1.0 + pow(x(t-tau),10.0)) -0.1*x' }
    dde = dde23(eqns=eqns, params={'tau': 17})
    dde.set_sim_params(tfinal=9000, dtmax=1.0, AbsTol=10**-6, RelTol=10**-3)
    histfunc = {'x': lambda t: 0.5 }
    dde.hist_from_funcs(histfunc, 51)
    dde.run()
    sol = dde.sample(1000, 9000, 1)
    x = sol['x']
    return x[:, None]


if __name__ == '__main__':
    import matplotlib.pyplot as plt

    # The lag to be used
    L = 16

    # The variance of the corruption noise
    noise_var = 0.001

    # The number of samples to be used for the GP
    num_train = 100

    # The number of samples from the forecast
    num_samples = 100

    # Generate the timeseries data
    t = generate_timeseries_data()

    # Normalize the series
    t_m = np.mean(t)
    t_s = np.std(t)
    t = (t - t_m) / t_s

    # Turn the data into the appropriate form
    X, Y = make_lag(t, L)

    # Downsample the data
    idx = np.arange(X.shape[0])
    np.random.shuffle(idx)
    X = X[idx[:num_train], :]
    Y = Y[idx[:num_train], :]

    # Corrupt the targets with some small noise
    Y += np.sqrt(noise_var) * np.random.randn(*Y.shape)

    # Construct and train the GP
    k = GPy.kern.rbf(L, ARD=True)
    m = GPy.models.GPRegression(X, Y, kernel=k)
    m.optimize_restarts(num_restarts=1, messages=True, max_iters=1000)

    # Make predictions
    num_start = 100 + L
    num_ahead = 100

    # The predict_steps we are actually looking at
    predict_steps = np.arange(num_start, num_start + num_ahead)[:, None]

    X0 = t[num_start - L: num_start, :].T
    Y_s = sample_prediction(X0, num_ahead, m, num_samples)
    mu, lower, upper = get_error_bars(Y_s)

    # Plot the mean with error bars
    GPy.util.plot.gpplot(predict_steps, mu, lower, upper)

    # Plot the truth
    Y_t = t[num_start:num_start + num_ahead, :]
    plt.plot(predict_steps, Y_t, '--r', linewidth=2)
    plt.legend(['Model mean', 'lower error bar', 'upper error bar', 'observed']
                ,loc='best')
    plt.show()
    a = raw_input('Press Enter...')
