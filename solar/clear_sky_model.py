"""
A collection of clear sky models to play with.
Each one has its own intricacies.
"""


__all__ = ['ClearSkyIrradiance']


import numpy as np


class ClearSkyIrradiance(object):
    """
    The only model I have implemented here is P. Ineichen (2008) which is
    supposed to be a surrogate of Radiative Transfer Model (RTM) calculations.

    It requires the following inputs to be evaluated:
        + the extraterrestrial irradiance (W / m^2)
        + solar zenith angle (in radiants) (zenith)
        + aerosol optical depth at 700 nm (aod_700) (0. to 0.45)
        + water vapor column in cm (w). (0.2 cm to 10 cm)
        + the atmospheric pressure at sea level (p0)
        + the atmoshperic pressure at the considered altitude (p)
          (the two pressures must be of the same units).
    """

    def __init__(self):
        """
        Initialize the obect.
        """
        pass

    def __call__(self, I_o, zenith, aod_700=0.15, w=.3, p=99., p0=101.325):
        """
        Evaluate the function. It computes the global irradiance at the
        observered site.

        The values I have put there for aod_700 and w are a little bit
        arbitrary.
        """
        # Some preliminary computations
        cos_z = np.cos(zenith)
        # Compute enhanced extraterrestrial irradiance
        I_o0 = 1.08 * w ** 0.0051
        I_o1 = 0.97 * w ** 0.032
        I_o2 = 0.12 * w ** 0.56
        I_o_prime = I_o * (I_o2 * aod_700 ** 2. + I_o1 * aod_700 + I_o0 + 0.071 * np.log(p / p0))
        # Compute the normal beam irradiance
        tau_b1 = 1.82 + 0.056 * np.log(w) + 0.0071 * np.log(w) ** 2.
        tau_b0 = 0.33 + 0.045 * np.log(w) + 0.0096 * np.log(w) ** 2.
        tau_bp = 0.0089 * w + 0.13
        tau_b = tau_b1 * aod_700 + tau_b0 + tau_bp * np.log(p / p0)
        b1 = 0.00925 * aod_700 ** 2. + 0.0148 * aod_700 - 0.0172
        b0 = -0.7565 * aod_700 ** 2. + 0.5057 * aod_700 + 0.4557
        b = b1 * np.log(w) + b0
        I_bn = I_o_prime * np.exp(-tau_b / cos_z ** b) * cos_z
        I_bn[cos_z <= 0.] = 0.
        # Compute the global irradiance
        tau_g1 = 1.24 + 0.047 * np.log(w) + 0.0061 * np.log(w) ** 2.
        tau_g0 = 0.27 + 0.043 * np.log(w) + 0.0090 * np.log(w) ** 2.
        tau_gp = 0.0079 * w + 0.1
        tau_g = tau_g1 * aod_700 + tau_g0 + tau_gp * np.log(p / p0)
        g = -0.0147 * np.log(w) - 0.3079 * aod_700 ** 2. + 0.2846 * aod_700 + 0.3798
        I_g = I_o_prime * np.exp(-tau_g / cos_z ** g) * cos_z
        I_g[cos_z <= 0.] = 0.
        # Compute the diffusive irradiance
        if isinstance(cos_z, float):
            cos_z = np.array([cos_z])
        if isinstance(aod_700, float):
            aod_700 = np.ones(cos_z.shape[0]) * aod_700
        if isinstance(w, float):
            w = np.ones(cos_z.shape[0]) * w
        i_l = aod_700 < 0.05
        i_u = ~i_l
        tau_d4 = np.ndarray(cos_z.shape[0])
        tau_d3 = np.ndarray(cos_z.shape[0])
        tau_d2 = np.ndarray(cos_z.shape[0])
        tau_d1 = np.ndarray(cos_z.shape[0])
        tau_d0 = np.ndarray(cos_z.shape[0])
        tau_dp = np.ndarray(cos_z.shape[0])
        # Case 1 (aod_700 < 0.05)
        tau_d4[i_l] = 86 * w[i_l]- 13800.
        tau_d3[i_l] = -3.11 * w[i_l] + 79.4
        tau_d2[i_l] = -0.23 * w[i_l] + 74.8
        tau_d1[i_l] = 0.092 * w[i_l] - 8.86
        tau_d0[i_l] = 0.0042 * w[i_l] + 3.12
        tau_dp[i_l] = -0.83 * (1. + aod_700[i_l]) ** (-17.2)
        # Case 2 (aod_700 >= 0.05)
        tau_d4[i_u] = -0.21 * w[i_u] + 11.6
        tau_d3[i_u] = 0.27 * w[i_u] - 20.7
        tau_d2[i_u] = -0.134 * w[i_u] + 15.5
        tau_d1[i_u] = 0.0554 * w[i_u] - 5.71
        tau_d0[i_u] = 0.0057 * w[i_u] + 2.94
        tau_dp[i_u] = -0.71 * (1. + aod_700[i_u]) ** (-15.0)
        # Finallize the computation of the diffusive part
        tau_d = (tau_d4 * aod_700 ** 4. + tau_d3 * aod_700 * 3. +
                 tau_d2 * aod_700 * 2. + tau_d1 * aod_700 +
                 tau_d0 + tau_dp * np.log(p / p0))
        d_p = 1. / (18. + 152. * aod_700)
        d = -0.337 * aod_700 * 2. + 0.63 * aod_700 + 0.116 + d_p * np.log(p / p0)
#        I_d = I_o_prime * np.exp(-tau_d / cos_z ** d)
#        I_d[cos_z <= 0.] = 0.
#        return I_g, I_bn, I_d
        return I_g,


if __name__ == '__main__':
    import pandas as pd
    from datetime import timedelta
    import matplotlib.pyplot as plt
    from observer import Observer
    from extraterrestrial_irradiance import ExtraterrestrialIrradiance
    import math
    data = pd.read_hdf('./data/data_w_alpha.h5', 'solar')
    n = 96 * 31
    m = n + 96 * 365 * 5
    I = data.Irradiance[n:m].resample('H', how='mean')
    obs = Observer()
    z, r = obs.compute_many(I.index)
    E_ext = ExtraterrestrialIrradiance()
    E = ClearSkyIrradiance()
    I0 = E_ext(r)
    I_g= E(I0, z, p=88.5, aod_700=0.01, w=0.21)[0]
    plt.plot(I)
    plt.plot(I_g)
    plt.show()
    k = I / np.array(I_g)
    k[z * 360 / (2. * math.pi) >= 75] = None
    plt.plot(k, '.')
    plt.show()
