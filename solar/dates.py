"""
Utilities that have to do with dates.

Author:
    Ilias Bilionis

Date:
    3/12/2014
"""


__all__ = ['get_good_dates_with_lag']


import pandas as pd
import numpy as np
import pytz
import math
from datetime import timedelta
from datetime import datetime


def _str_to_datetime(date, timezone='UTC'):
    """
    Convert the string representation of time to a datetime object.

    This version works only with one string.
    It gets rid of the miliseconds and the seconds.

    .. note::

        The timezone must be the real timezone of the date. Not the one you
        want to convert it to.

    """
    if isinstance(date, str):
        # Get rid of seconds
        date = ':'.join(date.split(':')[:2])
        date = datetime.strptime(date.split('.')[0], '%Y-%m-%d %H:%M')
    if date.tzinfo is None:
        tz = pytz.timezone(timezone)
        return tz.localize(date)
    else:
        return date


def str_to_datetime(date, timezone='UTC'):
    """
    Convert many dates to datetime objects.

    .. note::

        The timezone must be the real timezone of the date(s). Not the one you
        want to convert it to.
    """
    if isinstance(date, str):
        return _str_to_datetime(date, timezone=timezone)
    else:
        d = pd.DatetimeIndex([_str_to_datetime(s, timezone=timezone) for s in date])
        return d


def get_good_dates_with_lag(dates, lag):
    """
    Get the indices of the dates forming good lags.

    TODO: EXTEND DOCUMENTTION
    """
    if not isinstance(dates, pd.DatetimeIndex):
        dates = str_to_datetime(dates)
    dts = np.array([(dates[i + 1] - dates[i]).total_seconds()
                    for i in xrange(dates.shape[0] - 1)])
    # Concert differences to minutes
    dts /= 60.
    # The good indices are the ones with a dt of 30 minutes
    idx_flags = dts == 30.
    lag_idx_flags = np.array([np.all([idx_flags[j] for j in xrange(i, i + lag)])
                              for i in xrange(dates.shape[0] - lag)])
    lag_idx = []
    for i in xrange(dates.shape[0] - lag):
        if lag_idx_flags[i]:
            lag_idx.append(np.arange(i, i + lag + 1))
    lag_idx = np.vstack(lag_idx)
    return lag_idx


def get_closest_date(dates, date):
    """
    Get the index of the closest date to date.
    """
    if not isinstance(date, datetime):
        date = str_to_datetime(date)
    if not isinstance(dates, pd.DatetimeIndex):
        dates = str_to_datetime(dates)
    dts = np.array([math.fabs((dates[i] - date).total_seconds())
                    for i in xrange(dates.shape[0])])
    return np.argmin(dts)
