"""
General implementation of ground-based models.

This is an effort to organize things a little bit so that we can try lots of
different things fast.

Author:
    Ilias Bilionis

Date:
    3/12/2014

"""

import warnings
warnings.filterwarnings("ignore")
import GPy
from sklearn.preprocessing import StandardScaler
import numpy as np
import pandas as pd
import sys
from observer import Observer
from input_output import read_netcdf_data
from dates import get_good_dates_with_lag
from dates import get_closest_date


class GroundModel(object):

    """
    The base ground model.
    """

    # An observer object situated at the desired location
    _observer = None

    # The actual observations
    _I_ground = None

    # The netcdf filename containing the data
    _netcdf_filename = None

    # The lag you want to use
    _lag = None

    @property
    def observer(self):
        """
        Get the observer.
        """
        return self._observer

    @property
    def I_ground(self):
        """
        Get the actual observation.
        """
        return self._I_ground

    @property
    def dates(self):
        """
        Get the observed dates.
        """
        return self.I_ground.index

    @property
    def netcdf_filename(self):
        """
        Get the netcdf filename containing the observations.
        """
        return self._netcdf_filename

    @netcdf_filename.setter
    def netcdf_filename(self, value):
        """
        Set the netcdf filename.
        """
        self._netcdf_filename = value
        self._I_ground, lon, lat = read_netcdf_data(value)
        self._observer = Observer(latitude=lat, longitude=lon)
        good_dates = self.observer.is_inside_day(self.I_ground.index,
                                        hours_after_sunrise=self.hours_after_sunrise,
                                        hours_before_sunset=self.hours_before_sunset)
        self._I_ground = self.I_ground[good_dates]
        self._I_ground.dropna()

    @property
    def lag(self):
        """
        Get the lag.
        """
        return self._lag

    def __init__(self, netcdf_filename, lag=2,
                 hours_after_sunrise=2.,
                 hours_before_sunset=2.,
                 kernel='rbf',
                 max_iter=1000):
        """
        Initialize the object.
        """
        self.hours_after_sunrise = hours_after_sunrise
        self.hours_before_sunset = hours_before_sunset
        self.netcdf_filename = netcdf_filename
        self._lag = lag
        self.kernel = kernel
        self.max_iter = max_iter

    def fit(self, num_obs, start_date=None, final_date=None, output_filename=None):
        """
        Fit the model.
        """
        i_start = get_closest_date(self.dates, start_date) if start_date is not None else 0
        i_end = get_closest_date(self.dates, final_date) if final_date is not None else -1
        I_ground = self.I_ground[i_start:i_end]
        lag_idx = get_good_dates_with_lag(I_ground.index, self.lag)
        clear_sky_model = self.observer.evaluate_clear_sky_model(I_ground.index)
        insolation = I_ground.values.flatten()
        k = insolation / clear_sky_model
        self.scaler = StandardScaler().fit(k)
        s_k = self.scaler.transform(k)
        X = np.array([np.hstack([s_k[j] for j in lag_idx[i, :-1]])
                      for i in xrange(lag_idx.shape[0])])
        Y = np.array([s_k[lag_idx[i, -1]] for i in xrange(lag_idx.shape[0])])[:, None]
        sub_idx = np.random.permutation(np.arange(X.shape[0]))[:num_obs]
        X = X[sub_idx, :]
        Y = Y[sub_idx, :]
        self.B = np.linalg.lstsq(X, Y)[0]
        Y = Y - np.dot(X, self.B)
        kernel = getattr(GPy.kern, self.kernel)(X.shape[1], ARD=True)
        self.model = GPy.models.GPRegression(X, Y, kernel=kernel)
        self.model.optimize(messages=True, max_iters=1000)

    def sample(self, starting_date, num_ahead=16, num_samples=1,
               output_filename=None):
        """
        Take samples from the model.
        """
        lag_idx = get_good_dates_with_lag(self.dates, self.lag - 1)
        l_num_start = get_closest_date(self.dates[lag_idx[:, 0]], starting_date)
        g_num_start = lag_idx[l_num_start, 0]
        # Print a message so that we now what we are doing
        print 'Requested date:', starting_date
        print 'Closest date found in dataset:', self.dates[g_num_start]
        print 'So, as a starting point I will use the pair:'
        print '\t', self.dates[lag_idx[l_num_start, 0]], ',', self.dates[lag_idx[l_num_start, 1]]
        dt_starting_date = self.dates[lag_idx[l_num_start, 0]]
        prediction_dates = pd.date_range(dt_starting_date, freq='30min',
                                         periods=num_ahead + self.lag)
        data = self.I_ground[g_num_start:g_num_start+self.lag]
        clear_sky_insolation = self.observer.evaluate_clear_sky_model(data.index)
        k = data.values.flatten() / clear_sky_insolation
        X = self.scaler.transform(k[None, :])
        predicted_clear_sky_insolation = self.observer.evaluate_clear_sky_model(prediction_dates)
        my_insolations = []
        for i in xrange(num_samples):
            sys.stdout.write('taking sample ' +
                                 str(i * 1).zfill(len(str(num_samples))) +
                                 ' of ' + str(num_samples) + '\r')
            sys.stdout.flush()
            Y = self._do_sample(X, num_ahead=num_ahead)
            k = self.scaler.inverse_transform(Y)
            I = (k.flatten() * predicted_clear_sky_insolation)[None, :]
            my_insolations.append(I)
        sys.stdout.write('\n')
        state = {}
        state['I'] = np.vstack(my_insolations)
        state['dates'] = prediction_dates
        return state

    def _do_sample(self, X, num_ahead):
        """
        Do the actual sampling.
        """
        num_comp = X.shape[1] / self.lag
        Y_p = [X[0, :num_comp][None, :], X[0, num_comp:][None, :]]
        for i in xrange(num_ahead):
            y = self.model.posterior_samples(X, 1) + np.dot(X, self.B)
            Y_p.append(y)
            X = np.hstack([X[:, num_comp:], y])
        return np.vstack(Y_p)


if __name__ == '__main__':
    import cPickle as pickle
    with open('data/ground_ok.pcl', 'rb') as fd:
        model = pickle.load(fd)
    I_g, prediction_dates = model.sample('2014-02-26 16:00')
#    model = GroundModel('data/ok_ground.nc', kernel='exponential')
#    model.fit(300, final_date='2014-02-25 18:30')
#    with open('data/ground_ok.pcl', 'wb') as fd:
#        pickle.dump(model, fd, protocol=pickle.HIGHEST_PROTOCOL)
