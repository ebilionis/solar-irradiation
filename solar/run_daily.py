#!/usr/bin/env python
"""
Reproduce the results in the paper for the daily model.
"""
import os
import sys
import imp
import contextlib
import numpy as np


@contextlib.contextmanager
def train_context(K, P, dates, prefix, outdir):
    sys._argv = sys.argv[:]
    sys.argv = ['./train.py',
                '--num-states=' + str(K),
                '--num-symbols=' + str(P),
                '--date=\'' + dates[0] + ':' + dates[1] + '\'',
                '--prefix=' + prefix + '',
                '--out-dir=' + outdir + '',
                '--verbose=0']
    yield
    sys.argv = sys._argv

# Number of hidden states
num_states = [2, 4, 6, 8, 16, 32, 64]
# Number of symbols
num_symbols = 100
# Training dates
training_dates=['1991', '2000']
# Number of random initializations before maximizing likelihood
num_init = 100
# Output directory
outdir = os.path.abspath('results_id')
# Train the model for each hidden state
for k in num_states:
    print 'Training for', k, 'hidden states'
    all_loglike = []
    all_filenames = []
    for i in xrange(num_init):
        prefix = 'solar_' + 'C=' + str(i)
        fp, pathname, description = imp.find_module('train')
        with train_context(k, num_symbols, training_dates, prefix, outdir):
            imp.load_module('__main__', fp, pathname, description)
            all_loglike.append(loglike[-1])
            all_filenames.append(filename)
    i_max = np.argmax(all_loglike)
    max_like_filename = 'solar_max_K=' + str(k) + '_P=' + str(num_symbols) + '.pickle'
    symlink_filename = os.path.join(outdir, max_like_filename)
    if os.path.exists(symlink_filename):
        os.unlink(symlink_filename)
    os.symlink(all_filenames[i_max], symlink_filename)
