"""
Convert the pandas observed data to NetCDF data.

Remember that the data are measured every 15 minutes.
"""

import pandas as pd
import netCDF4 as nc
import numpy as np
import math

def write_nc_file(filename, df, hdf_var_name, nc_var_name):
    """
    Write ``filename`` that includes ``var_name``.
    """
    df_wna = df[hdf_var_name].dropna()
    nr = df_wna.shape[0]
    nc_data = nc.Dataset(filename, 'w')
    nc_data.createDimension('nr_' + nc_var_name, size=nr)
    nc_data.createVariable('time_' + nc_var_name, 'd',
                           dimensions=('nr_' + nc_var_name, ))
    nc_data.createVariable(nc_var_name, 'd',
                           dimensions=('nr_' + nc_var_name, ))
    nc_data.variables['time_' + nc_var_name][:] = np.array(df['t'][df_wna.index])
    nc_data.variables[nc_var_name][:] = np.array(df_wna)

# Read the HDF data and produce netcdf files droping unobserved values
df = pd.read_hdf('../data/full_solar.h5', 'solar')
print df
df['t'] = np.arange(df.shape[0])
write_nc_file('./nc_data/irradiance_obs.nc', df, 'irradiance',
              'irradiance_obs')
write_nc_file('./nc_data/pressure_obs.nc', df, 'pressure',
              'pressure_obs')
write_nc_file('./nc_data/temperature_obs.nc', df, 'temperature',
              'temperature_obs')
write_nc_file('./nc_data/zenith.nc', df, 'zenith',
              'zenith')
write_nc_file('./nc_data/clear_sky_irradiance.nc', df,
              'clear_sky_irradiance',
              'clear_sky_irradiance')
k = df['irradiance'] / df['clear_sky_irradiance']
k[df['zenith'] * 360. / (2. * math.pi) >= 75] = None
df['clearness_index'] = k
write_nc_file('./nc_data/clearness_index_obs.nc', df,
              'clearness_index',
              'clearness_index_obs')
