"""
Defines an Observer class that stores information about the observation site.
It knows how to compute the position of the sun using the SPA C library.

Author:
    Ilias Bilionis
    12/17/2013
"""


__all__ = ['Observer']


import spa
import numpy as np
import math
import urllib
from xml.etree import ElementTree as XMLElementTree
import googleapis
import time
from extraterrestrial_irradiance import ExtraterrestrialIrradiance
from clear_sky_model import ClearSkyIrradiance


class Observer(object):

    """
    A class storing the details of the observer.
    """

    # Longtitue
    _longtitue = None

    # Latitude
    _latitude = None

    # Timezone
    _timezone = None

    # Difference between Earth rotation time and terrestrial time (in seconds, float)
    _delta_t = None

    # The elevation of the site (in meters, float)
    _elevation = None

    # Annual average pressure (in mbar, float)
    _pressure = None

    # Annual average temperature (in degrees Celsius, float)
    _temperature = None

    # Atmospheric refraction (in deg, float)
    _atmos_refract = None

    @property
    def longitude(self):
        """
        Setter/Getter.

        Observer longitude (negative west of Greenwich)
        valid range: -180  to  180 degrees, error code: 9
        """
        return self._longitude

    @longitude.setter
    def longitude(self, value):
        """
        Set the longitude.
        """
        value = float(value)
        self._longitude = value

    @property
    def latitude(self):
        """
        Setter/Getter.

        Observer latitude (negative south of equator)
        valid range: -90   to   90 degrees, error code: 10
        """
        return self._latitude

    @latitude.setter
    def latitude(self, value):
        """
        Set the latitude.
        """
        value = float(value)
        self._latitude = value

    @property
    def timezone(self):
        """
        Setter/Getter.

        Observer time zone (negative west of Greenwich)
        valid range: -18   to   18 hours,   error code: 8
        """
        return self._timezone

    @timezone.setter
    def timezone(self, value):
        """
        Set the timezone.
        """
        value = float(value)
        assert value >= -18. and value <= 18.
        self._timezone = value

    @property
    def delta_t(self):
        """
        Setter/Getter.

        Difference between earth rotation time and terrestrial time
        It is derived from observation only and is reported in this
        bulletin: http://maia.usno.navy.mil/ser7/ser7.dat,
        where delta_t = 32.184 + (TAI-UTC) - DUT1
        valid range: -8000 to 8000 seconds, error code: 7
        """
        return self._delta_t

    @delta_t.setter
    def delta_t(self, value):
        """
        Set delta_t (in seconds)
        """
        value = float(value)
        assert value > -8000. and value < 8000.
        self._delta_t = value

    @property
    def pressure(self):
        """
        Setter/Getter.

        Annual average local pressure [millibars]
        valid range:    0 to 5000 millibars,       error code: 12
        """
        return self._pressure

    @pressure.setter
    def pressure(self, value):
        """
        Set pressure (in mbar)
        """
        value = float(value)
        assert value > 0. and value < 5000.
        self._pressure = value

    @property
    def elevation(self):
        """
        Setter/Getter.

        Observer elevation [meters]
        valid range: -6500000 or higher meters,    error code: 11
        """
        return self._elevation

    @elevation.setter
    def elevation(self, value):
        """
        Set the elevation.
        """
        value = float(value)
        assert value > -6500000
        self._elevation = value

    @property
    def temperature(self):
        """
        Setter/Getter.

        Annual average local temperature [degrees Celsius]
        valid range: -273 to 6000 degrees Celsius, error code; 13
        """
        return self._temperature

    @temperature.setter
    def temperature(self, value):
        """
        Set the temperature.
        """
        value = float(value)
        assert value > -273. and value < 6000
        self._temperature = value

    @property
    def atmos_refract(self):
        """
        Setter/Getter.
        Atmospheric refraction at sunrise and sunset (0.5667 deg is typical)
        valid range: -5   to   5 degrees, error code: 16

        TODO: See how important this is. It is possible to calculate it quite
        accurately if we have the temperature and the pressure. See this:
        http://en.wikipedia.org/wiki/Atmospheric_refraction
        """
        return self._atmos_refract

    @atmos_refract.setter
    def atmos_refract(self, value):
        """
        Set the atmospheric refraction.
        """
        value = float(value)
        assert value > -5. and value < 5.
        self._atmos_refract = value

    def __init__(self,
                 longitude=-87.976492,
                 latitude=41.711841,
                 temperature=10.83,
                 pressure=99.,
                 atmos_refract=0.5667,
                 delta_t=65.7768):
        """
        Initialize the object.

        The particular values are for Argonne, IL
        """
        self.longitude = longitude
        self.latitude = latitude
        self._lookup_site_details()
        self.temperature = temperature
        self.pressure = pressure
        self.atmos_refract = atmos_refract
        self.delta_t = delta_t


    def _lookup_site_details(self):
        """
        Looks up on the web the details about the site,
        i.e., timezone, elevation, etc.
        """
        offset, timezone_id = googleapis.look_up_offset(self.latitude, self.longitude)
        self.timezone = offset
        self.timezone_id = timezone_id
        self.elevation = googleapis.look_up_elevation(self.latitude, self.longitude)

    @property
    def spa_data(self):
        """
        Prepaper and return an spa_struct with the current parameters.
        The date will be the only thing missing.
        """
        res = spa.spa_data()
        res.longitude = self.longitude
        res.latitude = self.latitude
        res.elevation = self.elevation
        res.temperature = self.temperature
        res.pressure = self.pressure * 10.
        res.atmos_refract = self.atmos_refract
        res.delta_t = self.delta_t
        res.delta_ut1 = 0.
        res.slope = 0.
        res.azm_rotation = 0.
        res.timezone = self.timezone
        res.function = spa.SPA_ZA
        return res

    def compute_single(self, date):
        """
        Run SPA on a single date.

        The date is whatever the pandas object for a date is.
        """
        spa_data = self.spa_data
        spa_data.year = date.year
        spa_data.month = date.month
        spa_data.day = date.day
        spa_data.hour = date.hour
        spa_data.minute = date.minute
        spa_data.second = date.second
        spa.spa_calculate(spa_data)
        return spa_data

    def zenith(self, date):
        """
        Get the zenith at a particular date (in radiants).
        """
        return self.compute_single(date).zenith / 360. * 2. * math.pi

    def compute_many(self, dates):
        """
        Run SPA on many dates. The dates must be at the right timezone.
        """
        dates = dates.tz_convert(self.timezone_id)
        z = np.ndarray(dates.shape[0])
        r = np.ndarray(dates.shape[0])
        spa_data = self.spa_data
        spa.spa_calculate_many(spa_data, dates.year, dates.month, dates.day,
                               dates.hour, dates.minute, dates.second, z, r)
        return z / 360. * (2. * math.pi), r

    def compute_sunrise_and_sunset(self, dates):
        """
        Runs SPA on many dates to compute sunrise and sunset.

        The sunrise and sunset are in fractional hours from midnight local time
        of each particular day.

        """
        # Convert dates to local timezone
        dates = dates.tz_convert(self.timezone_id)
        sunrise_ftime = np.ndarray(dates.shape[0])
        sunset_ftime = np.ndarray(dates.shape[0])
        r = np.ndarray(dates.shape[0])
        spa_data = self.spa_data
        spa.spa_calculate_many_sunrise_and_sunset(spa_data, dates.year, dates.month,
                                                  dates.day, dates.hour, dates.minute,
                                                  dates.second, sunrise_ftime,
                                                  sunset_ftime, r)
        return sunrise_ftime, sunset_ftime

    def is_inside_day(self, dates, hours_after_sunrise=2., hours_before_sunset=2.):
        """
        Return the a bool numpy array with a ``True`` value for at index ``i``
        if ``dates[i]`` is between sunrise and sunset and ``False`` otherwise.
        """
        dates = dates.tz_convert(self.timezone_id)
        dates_ftime = dates.hour + dates.minute / 60.
        sunrise_ftime, sunset_ftime = self.compute_sunrise_and_sunset(dates)
        good_dates = ((dates_ftime >= sunrise_ftime + hours_after_sunrise) *
                      (dates_ftime < sunset_ftime - hours_before_sunset))
        return good_dates

    def evaluate_clear_sky_model(self, dates=None,
                                 solar_zenith_angle=None,
                                 sun_earth_distance=None):
        """
        Evaluate the clear sky model, given the solar zenith angle (in radians)
        and the sun earth distance (in AU) or given a list of dates.

        You may specify the dates or the solar_zenith_angle, sun_earth_distance pair.
        Not all of them!
        """
        # Initialize the model for extraterrestrial insolation
        ext_I = ExtraterrestrialIrradiance()
        # Initialize the clear sky model
        cls_model = ClearSkyIrradiance()
        if dates is not None:
            solar_zenith_angle, sun_earth_distance = self.compute_many(dates)
        I_ext = ext_I(sun_earth_distance)
        return cls_model(I_ext, solar_zenith_angle)[0]


if __name__ == '__main__':
    import pandas as pd
    import matplotlib.pyplot as plt
    obs = Observer()
