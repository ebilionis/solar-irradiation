#!/usr/bin/env python

"""
setup.py file for SWIG example
"""

from distutils.core import setup, Extension
import os
try:
    from numpy.distutils.misc_util import get_numpy_include_dirs
except:
    print 'You need to have Numpy installed!'
    quit()


include_dirs = [os.path.abspath('extern')] + get_numpy_include_dirs()
define_macros = []

# Check if we have MKL
try:
    mkl_root = os.environ['MKLROOT']
except:
    mkl_root = None
if mkl_root is not None:
    include_dirs.append(os.path.join(mkl_root, 'include'))
    define_macros.append(('HAS_MKL', '1'))


solar_module = Extension('solar._core',
                         sources=['solar/core_wrap.cxx'],
                         define_macros=define_macros,
                         include_dirs=include_dirs)
spa_module = Extension('solar._spa',
                       sources=['solar/spa.c', 'solar/spa_wrap.c'],
                       define_macros=define_macros,
                       include_dirs=include_dirs)

setup (name = 'PySolar',
       version = '0.1',
       author      = "Ilias Bilionis",
       description = """Satelite based solar irradiance""",
       ext_modules = [solar_module, spa_module],
       py_modules = ['solar'],
       )
