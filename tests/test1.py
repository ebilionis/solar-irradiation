#!/usr/bin/env python

"""
Test how astronomical data can be used to predict the irradiance.
"""


import pandas as pd
import matplotlib.pyplot as plt
import ephem as em
import math
import numpy as np


def get_solar_alt_at_time(t, g):
    d = pd.to_datetime(t, unit='s')
    g.date = d
    # Calculate the position of the sun
    s = em.Sun(g)
    return float(s.alt)


def get_solar_alt_path(t, g, n):
    y = np.linspace(0., 60., n)
    times = t + 60 * y
    z = np.array([get_solar_alt_at_time(times[i], g) for i in xrange(n)])
    return times, z


if __name__ == '__main__':
    # The position of Lemont, IL
    g = em.Observer()
    g.lat = '41.6688'
    g.lon = '-87.9888'
    g.elev = 219.456
    h = g.elev / 1000.
    data = pd.read_hdf('../data/data.h5', 'solar')
    # Let's just look at the data of August 2008
    #df = data['2010']
    data = data.resample('H', how='mean')
    data['unix'] = (data['julian'] - 2440587.5) * 86400 + 6 * 60 * 60 - 60 * 25
    n = 24 * 10
    df = data['2012'][:n]
    y = []
    A = 1370
    y = np.zeros(n)
    count = 0
    for (i, E) in df.iterrows():
        utime = E['unix']
        t, z = get_solar_alt_path(utime, g, 60)
        y[count] = np.trapz(z, t) / (t[-1] - t[0])
        print g.date, get_solar_alt_at_time(utime, g), y[count]
        count += 1
    x = df['Irradiance']
    plt.plot(x)
    plt.plot(A * y)
    plt.show()
    x /= (A * y)
    x[y <= 0.12] = None
    plt.plot(x)
    plt.show()
