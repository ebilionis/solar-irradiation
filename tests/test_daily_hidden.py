#!/usr/bin/env python
"""
Test a simple hidden model.
"""


import pandas as pd
import numpy as np
import best
import matplotlib.pyplot as plt
import statsmodels.api as sm
from statsmodels.graphics.api import qqplot
from statsmodels.tsa.arima_model import _arma_predict_out_of_sample as arma_predict
from scipy.stats import beta
import hmm as thmm
from sklearn import hmm
from sklearn import cluster


def s(x, beta=1.):
    return 1. / (1. + np.exp(-beta * x))


def get_process(n, sigma=0.1):
    res = np.zeros(n)
    for i in xrange(1, n):
        res[i] += res[i] + sigma * np.random.randn()
    return res


def P(w, d, t):
    return (d * np.cos(w * t)).sum()


def Q(w, d, t):
    return (d * np.sin(w * t)).sum()


def C(w, d, t):
    N = d.shape[0]
    return (P(w, d, t) ** 2. + Q(w, d, t) ** 2.) / N


def C_all(w, d, t):
    res = np.zeros(w.shape[0])
    for i in xrange(w.shape[0]):
        res[i] = C(w[i], d, t)
    return res


def Stud_t(w, d, t):
    msd = (d ** 2.).mean()
    N = d.shape[0]
    return (1. - 2. * C_all(w, d, t) / (N * msd)) ** (0.5 * (2. - N))


def young_1994(alpha):
    t = np.sin(alpha)
    tmp1 = 1.002432 * t ** 2. + 0.149864 * t + 0.0096467
    tmp2 = t ** 3. + 0.149864 * t ** 2. + 0.0102963 * t + 0.000303978
    return tmp2 / tmp1


def pp_air_mass(alpha):
    return np.sin(alpha)



if __name__ == '__main__':
    data = pd.read_hdf('../data/data_w_alpha.h5', 'solar')
    #data = data['1992']
    E = data['Irradiance'].resample('D', how='mean').interpolate()
    air_mass = young_1994
    tmp = air_mass(data['alpha'])
    tmp[tmp <= 0.] = 0.
    tmp *= 1. / 96.
    alpha = tmp.resample('D', how='sum')
    alpha[alpha <= 0] = None
    A = 1361
    Z = E / alpha
    Z /= A
    Z[Z >= 1.] = None
    Z[Z <= 0.] = None

    beta = 0.001
    Y = (np.log(Z) - np.log(1. - Z)).interpolate() / beta
    covars = np.tile(np.identity(1), (2, 1, 1))
    n_c = 2
    pi = np.ones(n_c) / n_c
    transmat = np.zeros((n_c, n_c))
    transmat[0, 0] = 0.5
    transmat[0, 1] = 0.5
    for i in xrange(1, n_c - 1):
        transmat[i, i - 1] = 0.25
        transmat[i, i] = 0.5
        transmat[i, i + 1] = 0.25
    transmat[-1, -1] = 0.5
    transmat[-1, -2] = 0.5
    Y = np.atleast_2d(Y).T
    Y = Y[:365 * 7]
    model = hmm.GaussianHMM(n_c, 'full', pi, transmat, n_iter=1000)
    model.fit([Y])
    print model.means_
    print model.covars_
    print model.transmat_
    #plt.contourf(model.transmat_)
    #plt.show()
    print model.startprob_
    H = model.predict(Y)
    print H
    n = 365 * 3
    fig = plt.figure()
    ax1 = fig.add_subplot(211)
    ax2 = fig.add_subplot(212)
    ax1.plot(H[:n], linewidth=2)
    ax1.set_ylabel('$z_n$', fontsize=16)
    #ax1.set_ylabel('$E_t (W/m^2)$', fontsize=16)
    ax1.set_xlim([0, n])
    idx = np.arange(n)
    H = H[:n]
    Y = Y[:n]
    #ax2.plot(E, 'g', linewidth=2)
    for i in xrange(n_c):
        idx_c = H == i
        ax2.plot(idx[idx_c], Y[idx_c], 'o', markersize=10)
    #ax2.plot(Y[:n], 'r', linewidth=2)
    ax2.set_xlabel('Day of Year', fontsize=16)
    ax2.set_ylabel('$x_n$', fontsize=16)
    ax2.set_xlim([0, n])
    plt.show()
    quit()

    (Ys, Zs) = model.sample(10)
    plt.plot(Ys)
    plt.show()
    for i in xrange(n_c):
        idx_c = H == i
        plt.hist(Y[idx_c])
        plt.show()
    quit()
