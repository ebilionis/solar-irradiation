#!/usr/bin/env python

"""
Tests with PCA.
"""


import pandas as pd
import matplotlib.pyplot as plt
import ephem as em
import numpy as np
from sklearn import mixture
from sklearn import preprocessing
from sklearn.decomposition import PCA
import math


if __name__ == '__main__':
    # The position of Lemont, IL
    g = em.Observer()
    g.lat = '41.6688'
    g.lon = '-87.9888'
    g.elev = 219.456
    data = pd.read_hdf('../data/data.h5', 'solar')
    # Let's just look at July
    y = []
    for year in range(1990, 2000):
        #df = pd.concat([data.loc['8/1/%d' % year], data.loc['8/2/%d' % year],
        #                data.loc['8/3/%d' % year]])
        for month in range(3, 12):
            for day in range(1, 31):
                df = data['%d/%d/%d' % (month, day, year)]
                #print df['Irradiance'].get_loc(None)
                A = 1100.
                z = []
                gamma = []
                for (i, E) in df.iterrows():
                    g.date = i.tz_convert('UTC')
                    print g.date
                    # Calculate the position of the sun
                    s = em.Sun(g)
                    r = s.earth_distance
                    gamma_s = float(s.alt)
                    if gamma_s <= 0.:
                        z.append(0.)
                    else:
                        z.append(A / r ** 2 * math.sin(gamma_s))
                    gamma.append(gamma_s)
                z = np.array(z)
                df['z'] = gamma
                y.append(np.hstack([np.array(df['Irradiance']) / A,
                                    np.array(df['z'])]))
    y = np.vstack(y)
    plt.plot(y.T)
    plt.show()
    imputer = preprocessing.Imputer()
    imputer.fit(y)
    y_c = imputer.transform(y)
    pca = PCA(n_components=0.999)
    pca.fit(y_c)
    #print pca.explained_variance_
    r = pca.transform(y_c)
    plt.plot(r[:, 0], r[:, 1], 'o')
    plt.show()
    plt.plot(y_c[0, :], 'r')
    plt.plot(pca.inverse_transform(r[0, :]).T, 'b')
    plt.show()
    for i in range(4):
        plt.plot(pca.components_[i, :], 'b')
    plt.show()
    gmm = mixture.GMM(n_components=10, covariance_type='full', n_iter=100)
                      #  verbose=True)
    m = gmm.fit(r)
    m.covars_ = m._get_covars()
    for i in range(5):
        plt.plot(pca.inverse_transform(m.sample())[0, :96], 'b', linewidth=2)
        plt.show()
    #plt.plot(np.log(y).T)
    #np.savetxt('covar.dat', m.covars_[0])
    #c = plt.imshow(m.covars_[0])
    #plt.colorbar(c)
    #plt.show()
    #print m.sample()
    #for i in range(10):
    #    plt.plot((m.sample() + z).T)
    #plt.show()
