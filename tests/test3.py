#!/usr/bin/env python

"""
Make some predictions using the simple empirical model, we have.
"""


import pandas as pd
import matplotlib.pyplot as plt
import ephem as em
import numpy as np
from sklearn import mixture
from sklearn import preprocessing
import math
import Pysolar as ps


def predict(obs_n, df, C, g):
    """
    Make prediction.
    """
    A = 1100
    z = []
    for (i, E) in df.iterrows():
        g.date = i.tz_convert('UTC')
        s = em.Sun(g)
        r = s.earth_distance
        gamma_s = float(s.alt)
        if gamma_s <= 0.:
            z.append(0.)
        else:
            z.append(A / r ** 2 * math.sin(gamma_s))
        alt_deg = ps.GetAltitude(g.lat, g.lon, i)
    obs_idx = np.arange(obs_n)
    pred_idx = np.arange(obs_n, df.shape[0])
    z = np.array(z)
    obs_z = z[obs_idx]
    obs_y = np.array(df['Irradiance'])[obs_idx]
    obs_C = C[obs_idx, :][:, obs_idx]
    obs_L = np.linalg.cholesky(obs_C)
    tmp = np.linalg.solve(obs_L, obs_y - obs_z)
    tmp = np.linalg.solve(obs_L.T, tmp)
    mu = np.dot(C[pred_idx, :][:, obs_idx], tmp) + z[pred_idx]
    mu[mu < 0.] = 0.
    tmp = np.linalg.solve(obs_L, C[obs_idx, :][:, pred_idx])
    print tmp.shape
    post_C = C[pred_idx, :][:, pred_idx] - np.dot(tmp.T, tmp)
    post_L = np.linalg.cholesky(post_C)
    df['mean'] = np.hstack([obs_y, mu])
    for i in range(500):
        df['sample_%d' % i] = np.hstack([obs_y,
                                         mu + np.dot(post_L,
                                                np.random.randn(post_L.shape[0]))])
        df['sample_%d' % i][df['sample_%d' % i] < 0.] = 0.


if __name__ == '__main__':
    # The position of Lemont, IL
    g = em.Observer()
    g.lat = '41.6688'
    g.lon = '-87.9888'
    g.elev = 219.456
    data = pd.read_hdf('../data/data.h5', 'solar')
    # The covariance matrix
    C = np.loadtxt('covar.dat')
    # Fit an AR process to the of a year
    from statsmodels.tsa.ar_model import AR
    df = pd.concat([data[str(y)] for y in range(1991, 2010)])
    ar = AR(df['Irradiance'], dates=df.index)
    results = ar.fit(maxlag=10)
    # Pick a date:
    df_p = data['1/1/2011']
    obs_n = 45
    predict(obs_n, df_p, C, g)
    ar_p = AR(df_p['Irradiance'][:obs_n], dates=df_p.index[:obs_n])
    ar_p.fit(maxlag=10)
    print df_p.index[-1]
    R = ar_p.predict(results.params,
                     end=96)
    #df.plot()
    for i in range(5):
        df_p['sample_%d' % i].plot(style='g', linewidth=3)
    df_p['mean'].plot(style='b', linewidth=3)
    df_p['Irradiance'].plot(style='r', linewidth=3)
    df_r = pd.Series(R, index=df_p.index[:R.shape[0]])
    df_r.plot(style='m', linewidth=3)
    plt.show()
