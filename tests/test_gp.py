#!/usr/bin/env python
"""
Test a simple hidden model.
"""


import pandas as pd
import numpy as np
import best
import matplotlib.pyplot as plt
import pymc as pm
import math


def make_model(data):
    """
    Make the model.
    """
    # The singal strength
    s = pm.Exponential('s', 1. / 1000., value=1000.)
    # The noise of the model
    #sigma = pm.Exponential('sigma', 1., value=0.0001)
    sigma = 1e-4
    # The length scale
    ell = pm.Exponential('ell', 1. / 10., value=10.)
    # The observed data
    y = np.array(data['Irradiance'])
    # The airmass
    airmass = np.sin(np.array(data['alpha']))
    airmass[airmass <= 0.122] = 0.
    # The coefficient of the mean
    A = pm.Uniform('A', 0., 2000., value=1361.)
    #A = 0.
    rho = 1.
    # The observed inputs
    t = np.arange(35, 35 + y.shape[0])
    # Compute the covariance function
    @pm.deterministic(plot=False)
    def comp_C(t=t, s=s, ell=ell):
        n = t.shape[0]
        C = np.zeros((n, n))
        for i in xrange(n):
            for j in xrange(n):
                C[i, j] = s * math.exp(-(np.abs((t[i] - t[j])) / ell) ** rho)
        return C

    @pm.deterministic(plot=False)
    def comp_Cpg(C=comp_C, sigma=sigma):
        n = C.shape[0]
        return C + sigma ** 2. * np.eye(n)

    @pm.deterministic(plot=False)
    def comp_L(Cpg=comp_Cpg):
        L = np.linalg.cholesky(Cpg)
        return L

    @pm.deterministic(plot=False)
    def comp_m(A=A, airmass=airmass):
        return A * airmass

    @pm.stochastic(observed=True)
    def obs(value=y, L=comp_L, m=comp_m):
        n = y.shape[0]
        det_L = np.log(np.diag(L)).sum()
        z = y - m
        a = np.linalg.solve(L, z)
        return -0.5 * np.dot(a, a) - det_L - 0.5 * n * math.log(2. * math.pi)

    return locals()


def predict(model, data):
    L = model['comp_L'].value
    n = L.shape[0]
    t = model['t']
    m = data.shape[0] - n - 35 - 96
    tp = np.arange(t[-1], t[-1] + m)
    s = model['s'].value
    ell = model['ell'].value
    k = np.zeros((n, m))
    A = model['A'].value
    rho = model['rho']
    #A = model['A']
    airmass = np.sin(np.array(data['alpha'][(35 + n):(35 + n + m)]))
    airmass[airmass <= 0.] = 0.
    for i in xrange(n):
        for j in xrange(m):
            k[i, j] = s * math.exp(-(np.abs((t[i] - tp[j])) / ell) ** rho)
    y = model['y']
    m = model['comp_m'].value
    a = np.linalg.solve(L, y - m)
    tmp = np.linalg.solve(L, k)
    f = np.dot(a, tmp) + A * airmass
    plt.plot(data['Irradiance'], '*')
    plt.plot(tp, f, 'r', linewidth=2)
    plt.show()


if __name__ == '__main__':
    data = pd.read_hdf('../data/data_w_alpha.h5', 'solar')
    #data = data.resample('D', how='mean')
    #data = data['2002']
    n = 12
    model = make_model(data[96 + 35:96 + 35 + n])
    mcmc = pm.MCMC(model)
    mcmc.sample(100000., thin=1, burn=0, verbose=1, progress_bar=True)
    pm.Matplot.plot(mcmc)
    plt.show()
    predict(model, data[0:(96 + 35 + n + 96 * 3)])
