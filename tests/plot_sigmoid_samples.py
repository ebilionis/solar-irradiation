#!/usr/bin/env python
"""
Test a simple hidden model.
"""


import pandas as pd
import numpy as np
import best
import matplotlib.pyplot as plt
import math


def get_process(n, s, sigma, ell, rho):
    C = np.zeros((n, n))
    for i in xrange(n):
        for j in xrange(n):
            C[i, j] = math.exp(-(math.fabs(i - j) / ell) ** rho)
    C *= s
    C += np.eye(n) * sigma
    L = np.linalg.cholesky(C)
    z = np.random.randn(n)
    return np.dot(L, z)


def S(x, beta=1.):
    return 1. / (1. + np.exp(-beta * x))


def young_1994(alpha):
    t = np.sin(alpha)
    tmp1 = 1.002432 * t ** 2. + 0.149864 * t + 0.0096467
    tmp2 = t ** 3. + 0.149864 * t ** 2. + 0.0102963 * t + 0.000303978
    return tmp2 / tmp1


if __name__ == '__main__':
    data = pd.read_hdf('../data/data_w_alpha.h5', 'solar')
    E_0 = 1361
    n = 96 * 4
    data = data[:n]
    airmass = lambda x: np.sin(x)
    s = 1.
    ell = 45.
    sigma = 1e-10
    rho = 1.1
    Y = get_process(n, s, sigma, ell, rho)
    alpha = data['alpha']
    E = E_0 * airmass(alpha) * S(Y)
    E[alpha <= 0.] = 0.
    fig = plt.figure()
    ax1 = fig.add_subplot(211)
    ax1.plot(Y, 'r', linewidth=2)
    ax1.set_title(r'Exp. Cov.: $\rho=%1.2f,\;s=%1.2f,\;\sigma=%1.2f,\;\ell=%1.2f$' %
                  (rho, s, sigma, ell), fontsize=16)
    ax1.set_ylabel('$Y_t$', fontsize=16)
    ax2 = fig.add_subplot(212)
    ax2.plot(data['Irradiance'], 'b', linewidth=2)
    ax2.plot(E, 'r', linewidth=2)
    ax2.legend(['Observation', 'Synthetic'], loc='best', fontsize=16)
    ax2.set_xlabel('$t$ (time step)', fontsize=16)
    ax2.set_ylabel('$E_t (W / m^2)$', fontsize=16)
    plt.show()
    quit()
    theta = [.0, 0.]
    epsilon = 2 * A * np.random.beta(50., 50., n) - A
    # Try one step ahead prediction
    plt.show()
    plt.hist(epsilon)
    plt.show()
    x = np.sin(data['alpha'][:n])
    E = data['Irradiance'][:n]
    Z = np.zeros(n)
    E_p = np.zeros(n)
    E_p[:2] = E[:2]
    for i in xrange(2, n):
        if x[i] < 0.:
            Zim1 = 0.
        else:
            Zim1 = E[i - 1] / x[i - 1] - A
        if x[i] < 0.:
            Zim2 = E[i - 2] / x[i - 2] - A
        else:
            Zim2 = 0.
        Z[i] = theta[0] * Zim1 + theta[1] * Zim2
        print Zim1, Z[i]
        E_p[i] = (A + Z[i]) * x[i]
    E_p[x <= 0.] = 0.
    T = (E - E_p) / x
    T[x <= 0.] = 0.
    plt.plot(T, 'b', linewidth=2)
    plt.plot(E)
    plt.plot(E_p)
    #plt.plot(E_p, '--r', linewidth=2)
    plt.show()

    E_p[:2] = E[:2]
    for i in xrange(2, n):
        if x[i] < 0.:
            Zim1 = 0.
        else:
            Zim1 = E_p[i - 1] / x[i - 1] - A
        if x[i] < 0.:
            Zim2 = E_p[i - 2] / x[i - 2] - A
        else:
            Zim2 = 0.
        Z[i] = theta[0] * Zim1 + theta[1] * Zim2
        print Zim1, Z[i]
        E_p[i] = (A + Z[i] + epsilon[i]) * x[i]
    E_p[x <= 0.] = 0.

    plt.plot(E)
    plt.plot(data['Irradiance'][:n])
    plt.show()
