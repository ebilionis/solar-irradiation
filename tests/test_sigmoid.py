#!/usr/bin/env python
"""
Test a simple hidden model.
"""


import pandas as pd
import numpy as np
import best
import matplotlib.pyplot as plt


def s(x, beta=1.):
    return 1. / (1. + np.exp(-beta * x))


def get_process(n, sigma=0.1):
    res = np.zeros(n)
    for i in xrange(1, n):
        res[i] += res[i] + sigma * np.random.randn()
    return res


def P(w, d, t):
    return (d * np.cos(w * t)).sum()


def Q(w, d, t):
    return (d * np.sin(w * t)).sum()


def C(w, d, t):
    N = d.shape[0]
    return (P(w, d, t) ** 2. + Q(w, d, t) ** 2.) / N


def C_all(w, d, t):
    res = np.zeros(w.shape[0])
    for i in xrange(w.shape[0]):
        res[i] = C(w[i], d, t)
    return res


def Stud_t(w, d, t):
    msd = (d ** 2.).mean()
    N = d.shape[0]
    return (1. - 2. * C_all(w, d, t) / (N * msd)) ** (0.5 * (2. - N))


def young_1994(alpha):
    t = np.sin(alpha)
    tmp1 = 1.002432 * t ** 2. + 0.149864 * t + 0.0096467
    tmp2 = t ** 3. + 0.149864 * t ** 2. + 0.0102963 * t + 0.000303978
    return tmp2 / tmp1


if __name__ == '__main__':
    data = pd.read_hdf('../data/data_w_alpha.h5', 'solar')
    #data = data.resample('D', how='mean')
    #data = data['2002']
    A = 1370
    n = 365
    #n = 96 * 356 * 2
    sigma = 0.5
    alpha = young_1994(np.array(data['alpha'][:n]))
    X = np.array(data['Irradiance'][:n])
    plt.plot(np.sin(alpha), X, '.')
    plt.show()
    Z = np.hstack([np.atleast_2d(alpha).T, np.atleast_2d(X).T,
                   np.atleast_2d(X / alpha).T])
    Z[alpha <= 0.12, :] = None
    plt.plot(A * alpha)
    plt.show()
    #plt.plot(np.arange(n), Z[:, 1], 'b', linewidth=2)
   # plt.plot(np.arange(n), Z[:, 2], 'r', linewidth=2)
    plt.show()
    Z[:, 2] /= A
    Z[Z[:, 2] >= 1.] = None
    Z[Z[:, 2] <= 0.] = None
    #plt.plot(np.arange(n), Z[:, 2], 'r', linewidth=2)
    Y = np.log(Z[:, 2]) - np.log(1. - Z[:, 2])
    # Now, we must investigate if there is any periodicity in this data
    plt.plot(Y, 'b', linewidth=2)
    plt.show()
    quit()
    Z = get_process(n, sigma)
    E = A * alpha * s(Z)
    E[alpha <= 0.] = 0.
    plt.plot(E)
    plt.plot(data['Irradiance'][:n])
    plt.show()
    quit()
    theta = [.0, 0.]
    epsilon = 2 * A * np.random.beta(50., 50., n) - A
    # Try one step ahead prediction
    plt.show()
    plt.hist(epsilon)
    plt.show()
    x = np.sin(data['alpha'][:n])
    E = data['Irradiance'][:n]
    Z = np.zeros(n)
    E_p = np.zeros(n)
    E_p[:2] = E[:2]
    for i in xrange(2, n):
        if x[i] < 0.:
            Zim1 = 0.
        else:
            Zim1 = E[i - 1] / x[i - 1] - A
        if x[i] < 0.:
            Zim2 = E[i - 2] / x[i - 2] - A
        else:
            Zim2 = 0.
        Z[i] = theta[0] * Zim1 + theta[1] * Zim2
        print Zim1, Z[i]
        E_p[i] = (A + Z[i]) * x[i]
    E_p[x <= 0.] = 0.
    T = (E - E_p) / x
    T[x <= 0.] = 0.
    plt.plot(T, 'b', linewidth=2)
    plt.plot(E)
    plt.plot(E_p)
    #plt.plot(E_p, '--r', linewidth=2)
    plt.show()

    E_p[:2] = E[:2]
    for i in xrange(2, n):
        if x[i] < 0.:
            Zim1 = 0.
        else:
            Zim1 = E_p[i - 1] / x[i - 1] - A
        if x[i] < 0.:
            Zim2 = E_p[i - 2] / x[i - 2] - A
        else:
            Zim2 = 0.
        Z[i] = theta[0] * Zim1 + theta[1] * Zim2
        print Zim1, Z[i]
        E_p[i] = (A + Z[i] + epsilon[i]) * x[i]
    E_p[x <= 0.] = 0.

    plt.plot(E)
    plt.plot(data['Irradiance'][:n])
    plt.show()
