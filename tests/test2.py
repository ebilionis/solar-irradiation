#!/usr/bin/env python

"""
Test how astronomical data can be used to predict the irradiance.
"""


import pandas as pd
import matplotlib.pyplot as plt
import ephem as em
import numpy as np
from sklearn import mixture
from sklearn import preprocessing
import math


if __name__ == '__main__':
    # The position of Lemont, IL
    g = em.Observer()
    g.lat = '41.6688'
    g.lon = '-87.9888'
    g.elev = 219.456
    h = g.elev / 1000.
    data = pd.read_hdf('../data/data.h5', 'solar')
    # Let's just look at July
    y = []
    for year in range(1990, 2010):
        #df = pd.concat([data.loc['8/1/%d' % year], data.loc['8/2/%d' % year],
        #                data.loc['8/3/%d' % year]])
        for month in range(8, 9):
            for day in range(1, 31):
                df = data['%d/%d/%d' % (month, day, year)]
                #print df['Irradiance'].get_loc(None)
                A = 1353.
                z = []
                for (i, E) in df.iterrows():
                    g.date = i.tz_convert('UTC')
                    print g.date
                    # Calculate the position of the sun
                    s = em.Sun(g)
                    r = s.earth_distance
                    gamma_s = float(s.alt)
                    AM = 1. / (math.sin(gamma_s) +
                               0.50572 * (96.07995 - (math.pi / 2 - s.alt) * (0.5 * 360 / math.pi)) ** (-1.6364))
                    ID = A * ((1 - 0.14 * h) * 0.7 ** (AM ** 0.678) + 0.14 * h)
                    z.append(1.1 * ID)
                    #if AM <= 0.:
                    #    z.append(0.)
                    #else:
                    #    z.append(A / math.sin(gamma_s))
                z = np.array(z)
                y.append((np.array(df['Irradiance']) / z))
    y = np.vstack(y)
    plt.plot(y.T)
    plt.show()
    imputer = preprocessing.Imputer()
    imputer.fit(y)
    y_c = imputer.transform(y)
    gmm = mixture.GMM(n_components=1, covariance_type='full', n_iter=100,
                      thresh=1e-6)
    m = gmm.fit(y_c)
    m.covars_ = m._get_covars()
    #plt.plot(np.log(y).T)
    np.savetxt('covar.dat', m.covars_[0])
    c = plt.imshow(m.covars_[0])
    plt.colorbar(c)
    plt.show()
    #print m.sample()
    #for i in range(10):
    #    plt.plot((m.sample() + z).T)
    #plt.show()
