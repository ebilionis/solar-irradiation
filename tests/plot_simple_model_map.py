from mpl_toolkits.basemap import Basemap
import matplotlib.pyplot as plt
import numpy as np
import ephem as em
import datetime
import itertools
import math


# set up orthographic map projection with
# perspective of satellite looking down at 50N, 100W.
# use low resolution coastlines.
map = Basemap(projection='ortho',lat_0=45,lon_0=-100,resolution='l')
# draw coastlines, country boundaries, fill continents.
map.drawcoastlines(linewidth=0.25)
map.drawcountries(linewidth=0.25)
map.fillcontinents(color='coral',lake_color='aqua')
# draw the edge of the map projection region (the projection limb)
map.drawmapboundary(fill_color='aqua')
# draw lat/lon grid lines every 30 degrees.
map.drawmeridians(np.arange(0,360,30))
map.drawparallels(np.arange(-90,90,30))
# make up some data on a regular lat/lon grid.
nlats = 73; nlons = 145; delta = 2.*np.pi/(nlons-1)
lats = (0.5*np.pi-delta*np.indices((nlats,nlons))[0,:,:])
lons = (delta*np.indices((nlats,nlons))[1,:,:])
#wave = 0.75*(np.sin(2.*lats)**8*np.cos(4.*lons))
mean = 0.5*np.cos(2.*lats)*((np.sin(2.*lats))**2 + 2.)
# compute native map projection coordinates of lat/lon grid.
x, y = map(lons*180./np.pi, lats*180./np.pi)
g = em.Observer()
g.date = datetime.datetime(2013, 8, 14, 20, 0, tzinfo=datetime.tzinfo('UTC'))
z = np.zeros(x.shape)
for i in range(x.shape[0]):
    for j in range(y.shape[0]):
        g.lat = str(lats[i, j])
        g.lon = str(lons[i, j])
        s = em.Sun(g)
        gamma_s = float(s.alt)
        print gamma_s
        if gamma_s <= 0.:
            z[i, j] = 0.
        else:
            z[i, j] = 1. / math.sin(gamma_s)
# contour data over the map.
cs = map.contour(x,y,z,100,linewidths=1.5)
plt.title('contour lines over filled continent background')
plt.show()
