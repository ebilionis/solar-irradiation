#!/usr/bin/env python
"""
Test a simple hidden model.
"""


import pandas as pd
import numpy as np
import best
import matplotlib.pyplot as plt
import statsmodels.api as sm
from statsmodels.graphics.api import qqplot
from statsmodels.tsa.arima_model import _arma_predict_out_of_sample as arma_predict
from scipy.stats import beta


def s(x, beta=1.):
    return 1. / (1. + np.exp(-beta * x))


def get_process(n, sigma=0.1):
    res = np.zeros(n)
    for i in xrange(1, n):
        res[i] += res[i] + sigma * np.random.randn()
    return res


def P(w, d, t):
    return (d * np.cos(w * t)).sum()


def Q(w, d, t):
    return (d * np.sin(w * t)).sum()


def C(w, d, t):
    N = d.shape[0]
    return (P(w, d, t) ** 2. + Q(w, d, t) ** 2.) / N


def C_all(w, d, t):
    res = np.zeros(w.shape[0])
    for i in xrange(w.shape[0]):
        res[i] = C(w[i], d, t)
    return res


def Stud_t(w, d, t):
    msd = (d ** 2.).mean()
    N = d.shape[0]
    return (1. - 2. * C_all(w, d, t) / (N * msd)) ** (0.5 * (2. - N))


def young_1994(alpha):
    t = np.sin(alpha)
    tmp1 = 1.002432 * t ** 2. + 0.149864 * t + 0.0096467
    tmp2 = t ** 3. + 0.149864 * t ** 2. + 0.0102963 * t + 0.000303978
    return tmp2 / tmp1


def pp_air_mass(alpha):
    return np.sin(alpha)


if __name__ == '__main__':
    data = pd.read_hdf('../data/data_w_alpha.h5', 'solar')
    #data = data['1992']
    E = data['Irradiance'].resample('D', how='mean').interpolate()
    air_mass = young_1994
    tmp = air_mass(data['alpha'])
    tmp[tmp <= 0.] = 0.
    tmp *= 1. / 96.
    alpha = tmp.resample('D', how='sum')
    alpha[alpha <= 0] = None
    A = 1361
    Z = E / alpha
    Z /= A
    Z[Z >= 1.] = None
    Z[Z <= 0.] = None

    Y = (np.log(Z) - np.log(1. - Z)).interpolate()
    plt.hist(Y, bins=100)
    plt.show()
    quit()

    fig = plt.figure()
    ax1 = fig.add_subplot(211)
    ax2 = fig.add_subplot(212)
    ax1.plot(E, linewidth=2)
    ax1.set_ylabel('$E_t (W/m^2)$', fontsize=16)
    ax1.set_xlim([0, 365])
    ax2.plot(Y, 'r', linewidth=2)
    ax2.set_xlabel('Day of Year', fontsize=16)
    ax2.set_ylabel('$Y_t$', fontsize=16)
    ax2.set_xlim([0, 365])
    plt.show()
    quit()

    sm.graphics.tsa.plot_acf(P, lags=60)
    plt.show()
    #plt.savefig(prefix + '_standarized_E_sacf.eps')
    #plt.clf()
    # Plot the SPACF
    sm.graphics.tsa.plot_pacf(P, lags=60)
    #plt.savefig(prefix + '_standarized_E_spacf.eps')
    plt.show()
    #plt.clf()
    # Fit an AR model
    r_d_E_arma = sm.tsa.ARMA(P, (2, 0)).fit()
    print r_d_E_arma.params
    # Do we obey theory
    plt.plot(r_d_E_arma.resid)
    #plt.savefig(prefix + '_residuals_E.eps')
    #plt.clf()
    plt.show()
    qqplot(r_d_E_arma.resid, line='q', fit=True)
    #plt.savefig(prefix + '_standarized_E_qqplot.eps')
    #plt.clf()
    plt.show()
    # Make one step ahead forcast
    # get what you need for predicting one-step ahead
    params = r_d_E_arma.params
    residuals = r_d_E_arma.resid
    p = r_d_E_arma.k_ar
    q = r_d_E_arma.k_ma
    k_exog = r_d_E_arma.k_exog
    k_trend = r_d_E_arma.k_trend
    steps = 1
    d_E_test = P#d_E_all['2012']
    y_all = (d_E_test - m) / s
    p_E_test = np.zeros(y_all.shape[0])
    p_E_test[0] = d_E_test[0]
    for i in xrange(1, y_all.shape[0]):
        y_obs = np.array(y_all[:i])
        y_n_p  = arma_predict(params, steps, residuals, p, q, k_trend, k_exog,
                 endog=y_obs, exog=None, start=y_obs.shape[0])[0]
        p_E_test[i] = y_n_p# * m + s
    d_E_test = A * alpha * 1. / (1. + np.exp(-(d_E_test * s + m)))
    p_E_test = A * alpha * 1. / (1. + np.exp(-(p_E_test * s + m)))
    plt.plot(d_E_test)
    plt.plot(p_E_test, 'r', linewidth=2)
    plt.xlabel('Day of Year', fontsize=16)
    plt.ylabel('Solar Irradiance', fontsize=16)
    plt.legend(['Data', 'Forecast'], loc='best')
    #plt.savefig(prefix + '_one_step_prediction.eps')
    #plt.clf()
    plt.show()

