"""
Tests RTM calculations.

Author:
    Ilias Bilionis
"""


from datetime import datetime
from datetime import timedelta
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from solar import fetch_year
import os
from glob import glob


def read_rtm_file(filename):
    """
    Reads an rtm file and returns a pandas object containing the irradiance
    and the date.
    """
    print 'Processing:', filename
    with open(filename, 'r') as fd:
        data = fd.read().splitlines()
        i = 11
        index = []
        I = []
        columns = None
        while i < len(data):
            s1 = data[i+1].find('[') + 1
            s2 = data[i+1].find(']')
            tmp = data[i+1][s1:s2].split(':')
            date = datetime.strptime(tmp[0], '%Y%m%d') + timedelta(hours=int(tmp[1][:2]))
            I.append(np.array([float(data[i+4].split()[j]) for j in [2, 3, 4, 5, 6]]))
            index.append(date)
            i += 5
    I = np.array(I)
    index = pd.DatetimeIndex(index, tz='UTC').tz_convert('Etc/GMT+6')
    df = pd.DataFrame(I, index=index, columns=['RTM Up. Ins.', 'RTM Dif. Ins.', 'RTM Dir. Ins.',
                                               'RTM Glob. Ins.', 'RTM Net Ins.'])
    return df


def read_all_rtm(dir):
    """
    Read all the RTM files.
    """
    assert os.path.isdir(dir), 'Directory ' + dir + ' does not exist!'
    lof = glob(dir + '/*')
    lof.sort()
    return pd.concat([read_rtm_file(file) for file in lof])


if __name__ == '__main__':
    output_dir = '/home/ebilionis/Solar/Output'
    df = read_all_rtm(output_dir)
    df_ground = fetch_year(2013)
    I_ground = df_ground.irradiation.resample('1H', how='mean')
    df_ground = pd.DataFrame(I_ground, columns=['Ground Insolation'])
    ax = df_ground.plot(linewidth=2)
    df.plot(ax=ax, linewidth=2)
    plt.show()
