"""
Boland's analysis of daily sums of solar irradiance.
"""


import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from numpy.fft import rfft, irfft
import statsmodels.api as sm
from statsmodels.graphics.api import qqplot
from statsmodels.tsa.arima_model import _arma_predict_out_of_sample as arma_predict
from scipy.stats import beta


def get_periodogram(nu):
    n = nu.shape[0]
    A0 = np.real(nu[0]) / (2. * n)
    A = 2. * np.real(nu[1:]) / n
    B = -2. * np.imag(nu[1:]) / n
    rho2 = 0.5 * (A ** 2. + B ** 2.)
    V = np.sum(rho2)
    return rho2 / V


if __name__ == '__main__':
    # A prefix for the output files
    prefix = 'hourly_1'
    # Load the data
    data = pd.read_hdf('../data/data_w_alpha.h5', 'solar')
    # Get the Irradiance series
    E_all = data['Irradiance']
    # Compute the daily sum of soloar irradiance
    d_E_all = E_all.resample('h', how='sum').interpolate()
    # Use irradiance of only one year for fourier expansion
    d_E = d_E_all['1991']
    # Compute the FFT of the data
    nu = rfft(d_E)
    rho2 = get_periodogram(nu)
    left = np.arange(1, rho2.shape[0] + 1)
    plt.bar(left[:3*354], rho2[:3*354], width=0.5)
    plt.xlabel('Frequency', fontsize=16)
    plt.ylabel('Power', fontsize=16)
    print nu
    plt.savefig(prefix + '_fft_E.eps')
    plt.clf()
#    plt.show()
    # Truncate the FFT
    nu_t = np.zeros(nu.shape, dtype='complex128')
    idx = np.argsort(rho2)[::-1]
    nu_t[:1] = nu[:1]
    num_terms = 20
    print idx[:num_terms]
    print rho2[idx]
    idx_fft = np.array([0, 1, 2, 3, 363, 364, 365, 729, 730, 731, 1093, 1094, 1095])
    plt.bar(left[idx_fft], rho2[idx_fft], width=0.5)
    plt.xlabel('Frequency', fontsize=16)
    plt.ylabel('Power', fontsize=16)
    plt.savefig(prefix + '_fft_E.eps')
    plt.clf()
    #plt.show()
    nu_t[idx_fft + 1] = nu[idx_fft + 1]
    # Plot the data vs the the season component
    s_E = irfft(nu_t)
    idx = np.arange(160 * 24, 164 * 24)
    plt.plot(idx, d_E[idx])
    plt.plot(idx, s_E[idx], 'g--', linewidth=2)
    plt.xlabel('Hour of Year', fontsize=16)
    plt.ylabel('Solar Irradiance', fontsize=16)
    plt.legend(['Hourly Data', 'Hourly Component'], loc='best')
    plt.savefig(prefix + '_hourly_E.eps')
    plt.clf()
    #plt.show()
    # The unseasoned data
    d_E_us = d_E[:365 * 24] - s_E
    plt.plot(idx, d_E_us[idx])
    plt.xlabel('Hour of Year', fontsize=16)
    plt.ylabel('Detrended Solar Irradiance', fontsize=16)
    plt.savefig(prefix + '_detrended_E.eps')
    plt.clf()
    #plt.show()
    # Compute the standard deviation per hour for all years
    t_d_E_all = pd.concat([d_E_all[str(y)] for y in range(1990, 2012)])
    # The observed standard deviation of each day
    d_std = np.zeros(365 * 24)
    for i in range(1, d_std.shape[0] + 1):
        # Get the day of the year corresponding to this hour
        day = (i -1) / 24 + 1
        hour = (i - 1) % 24
        print 'day:', day, 'hour:', hour
        # Get the data for that day
        d_d_E = t_d_E_all[t_d_E_all.index.dayofyear == day]# - s_E[i - 1]
        # Get the data for that particular hour
        h_d_d_E = d_d_E[d_d_E.index.hour == hour] - s_E[i - 1]
        d_std[i - 1] = h_d_d_E.std()
    # Save this because it takes some time to compute it.
    np.savetxt('hourly_1990_2012_std_E.txt', d_std)
    # Load the standard deviation per hour
    d_std = np.loadtxt('hourly_1990_2012_std_E.txt')
    nu_std = rfft(d_std)
    rho2_std = get_periodogram(nu_std)
    left = np.arange(1, rho2.shape[0] + 1)
    plt.clf()
    plt.bar(left, rho2_std, width=0.5)
    plt.xlabel('Frequency', fontsize=16)
    plt.ylabel('Power', fontsize=16)
    plt.savefig(prefix + '_fft_std_E.eps')
    plt.clf()
    #plt.show()
    # The model standard deviation per day
    nu_t_std = np.zeros(nu_std.shape, dtype='complex128')
    idx_fft0 = np.argsort(rho2_std)[::-1]
    num_terms_std = 2
    nu_t_std[0] = nu_std[0]
    #nu_t_std[idx[:num_terms_std] + 1] = nu_std[idx[:num_terms_std] + 1]
    idx_fft = idx_fft0[:num_terms_std]
    nu_t_std[idx_fft + 1] = nu_std[idx_fft + 1]
    m_d_std = irfft(nu_t_std) + 1.
    plt.plot(idx, d_std[idx], linewidth=2)
    plt.plot(idx, m_d_std[idx], '-g', linewidth=2)
    plt.xlabel('Hour of Year', fontsize=16)
    plt.ylabel('Standard Deviation of Solar Irradiance', fontsize=16)
    plt.legend(['Standard Deviation', 'Model'], loc='best')
    plt.savefig(prefix + '_seasonal_std_E.eps')
    plt.clf()
    #plt.show()
    # Now compute the residuals for the desired year
    #m_d_std[m_d_std <= 1e-6] = 1.
    #m_d_std = np.ones(m_d_std.shape[0])
    r_d_E_us = d_E_us / m_d_std
    plt.plot(r_d_E_us)
    #plt.show()
    plt.clf()
    plt.plot(idx, r_d_E_us[idx])
    plt.xlabel('Hour of Year', fontsize=16)
    plt.ylabel('Standarised Residuals', fontsize=16)
    plt.savefig(prefix + '_standarized_E.eps')
    plt.clf()
    #plt.show()
    # Plot the SACF
    sm.graphics.tsa.plot_acf(r_d_E_us, lags=60)
    plt.savefig(prefix + '_standarized_E_sacf.eps')
    plt.clf()
    #plt.show()
    # Plot the SPACF
    sm.graphics.tsa.plot_pacf(r_d_E_us, lags=60)
    plt.savefig(prefix + '_standarized_E_spacf.eps')
    plt.clf()
    #plt.show()
    # Fit an AR model
    r_d_E_arma = sm.tsa.ARMA(np.array(r_d_E_us), (2, 0)).fit()
    print r_d_E_arma.params
    # Do we obey theory
    plt.plot(idx, r_d_E_arma.resid[idx])
    plt.savefig(prefix + '_residuals_E.eps')
    plt.clf()
    #plt.show()
    qqplot(r_d_E_arma.resid, line='q', fit=True)
    #plt.show()
    plt.savefig(prefix + '_standarized_E_qqplot.eps')
    plt.clf()
    #plt.show()
    # Make one step ahead forcast
    # get what you need for predicting one-step ahead
    params = r_d_E_arma.params
    residuals = r_d_E_arma.resid
    from scipy import stats
    print 'Test:', stats.shapiro(residuals[:1000])
    p = r_d_E_arma.k_ar
    q = r_d_E_arma.k_ma
    k_exog = r_d_E_arma.k_exog
    k_trend = r_d_E_arma.k_trend
    steps = 1
    d_E_test = d_E_all['2012']
    y_all = (d_E_test[:365 * 24] - s_E) / m_d_std
    p_E_test = np.zeros(365 * 24)
    p_E_test[0] = d_E_test[0]
    for i in xrange(10, 365 * 24):
        y_obs = np.array(y_all[:i])
        y_n_p  = arma_predict(params, steps, residuals, p, q, k_trend, k_exog,
                 endog=y_obs, exog=None, start=y_obs.shape[0])[0]
        p_E_test[i] = y_n_p * m_d_std[i] + s_E[i]
    alpha = data['2012']['alpha'].resample('h', how='first')
    p_E_test[np.sin(alpha[:365 * 24]) <= 0.] = 0.
    idx = np.arange(190 * 24, 194 * 24)
    plt.plot(idx, d_E_test[idx], 'b*', markersize=5)
    plt.plot(idx, p_E_test[idx], '-r', linewidth=2)
    plt.xlabel('Hour of Year', fontsize=16)
    plt.ylabel('Solar Irradiance', fontsize=16)
    plt.legend(['Data', 'Forecast'], loc='best')
    plt.savefig(prefix + '_one_step_prediction.eps')
    plt.clf()
    #plt.show()
    # Estimating the noise
    loc = residuals.min()
    scale = residuals.max() - loc - 2.
    z = (residuals - loc) / scale
    z = z[z <= 1.]
    import scipy.stats
    dist = scipy.stats.beta
    param = dist.fit(z[:1000], 10., 10., floc=loc, fscale=scale)
    print param
    z = np.linspace(loc, loc + scale, 100)
    z_pdf = dist.pdf(z, param[0], param[1], loc=loc, scale=scale)#*params[:-2], loc=param[-2], scale=param[-1])
    plt.hist(residuals, normed=True, bins=100)
    plt.plot(z, z_pdf, 'r', linewidth=2)
    plt.xlabel('Standarized Residual ($(Z_t)$)', fontsize=16)
    plt.ylabel('$p(Z_t)$', fontsize=16)
    plt.savefig(prefix + '_noise_pdf.')
    plt.clf()
    # Take a sample of a year
    Z = dist.rvs(param[0], param[1], loc=loc, scale=scale, size=365 * 24)
    d_E_test = d_E_all['2012']
    y_all = (d_E_test[:365 * 24] - s_E) / m_d_std
    p_E_test = np.zeros(365 * 24)
    p_E_test[0] = d_E_test[0]
    for i in xrange(1, 365 * 24):
        y_obs = np.array(y_all[:i])
        y_n_p  = arma_predict(params, steps, residuals, p, q, k_trend, k_exog,
                 endog=y_obs, exog=None, start=y_obs.shape[0])[0]
        y_all[i] = y_n_p + Z[i]
        p_E_test[i] = y_all[i]* m_d_std[i] + s_E[i]
    plt.clf()
    p_E_test[np.sin(alpha[:365 * 24]) <= 0.] = 0.
    plt.plot(idx, d_E_test[idx])
    plt.plot(idx, p_E_test[idx], '-r', linewidth=2)
    plt.xlabel('Hour of Year', fontsize=16)
    plt.ylabel('Solar Irradiance', fontsize=16)
    plt.legend(['Real Year', 'Synthetic Year'], loc='best')
    plt.savefig(prefix + '_one_sample.eps')
