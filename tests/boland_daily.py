"""
Boland's analysis of daily sums of solar irradiance.
"""


import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from numpy.fft import rfft, irfft
import statsmodels.api as sm
from statsmodels.graphics.api import qqplot
from statsmodels.tsa.arima_model import _arma_predict_out_of_sample as arma_predict
from scipy.stats import beta


def get_periodogram(nu):
    n = nu.shape[0]
    A0 = np.real(nu[0]) / (2. * n)
    A = 2. * np.real(nu[1:]) / n
    B = -2. * np.imag(nu[1:]) / n
    rho2 = 0.5 * (A ** 2. + B ** 2.)
    V = np.sum(rho2)
    return rho2 / V


if __name__ == '__main__':
    # A prefix for the output files
    prefix = 'daily_arma_20'
    # Load the data
    data = pd.read_hdf('../data/data_w_alpha.h5', 'solar')
    # Get the Irradiance series
    E_all = data['Irradiance']
    # Compute the daily sum of soloar irradiance
    d_E_all = E_all.resample('D', how='sum')
    # Use irradiance of only one year for fourier expansion
    d_E = d_E_all['1991']
    # Compute the FFT of the data
    nu = rfft(d_E)
    rho2 = get_periodogram(nu)
    left = np.arange(1, rho2.shape[0] + 1)
    plt.bar(left, rho2, width=0.5)
    plt.xlabel('Frequency', fontsize=16)
    plt.ylabel('Power', fontsize=16)
    plt.savefig(prefix + '_fft_E.eps')
    plt.clf()
    #plt.show()
    # Truncate the FFT
    nu_t = np.zeros(nu.shape, dtype='complex128')
    nu_t[:2] = nu[:2]
    # Plot the data vs the the season component
    s_E = irfft(nu_t)
    plt.plot(d_E[:364])
    plt.plot(s_E, 'g', linewidth=2)
    plt.xlabel('Day of Year', fontsize=16)
    plt.ylabel('Solar Irradiance', fontsize=16)
    plt.legend(['Daily Data', 'Seasonal Component'], loc='best')
    plt.savefig(prefix + '_seasonal_E.eps')
    plt.clf()
    #plt.show()
    # The unseasoned data
    d_E_us = d_E[:364] - s_E
    plt.plot(d_E_us)
    plt.xlabel('Day of Year', fontsize=16)
    plt.ylabel('Detrended Solar Irradiance', fontsize=16)
    plt.savefig(prefix + '_detrended_E.eps')
    plt.clf()
    #plt.show()
    # Compute the variance per day for all years
    t_d_E_all = pd.concat([d_E_all[str(y)] for y in range(1990, 2012)])
    # The observed standard deviation of each day
    d_std = np.zeros(364)
    for i in range(1, d_std.shape[0] + 1):
        d_d_E = t_d_E_all[t_d_E_all.index.dayofyear == i] - s_E[i - 1]
        d_std[i - 1] = d_d_E.std()
    nu_std = rfft(d_std)
    rho2_std = get_periodogram(nu_std)
    left = np.arange(1, rho2.shape[0] + 1)
    plt.clf()
    plt.bar(left, rho2_std, width=0.5)
    plt.xlabel('Frequency', fontsize=16)
    plt.ylabel('Power', fontsize=16)
    plt.savefig(prefix + '_fft_std_E.eps')
    plt.clf()
    #plt.show()
    # The model standard deviation per day
    nu_t_std = np.zeros(nu_std.shape, dtype='complex128')
    idx = np.argsort(rho2_std)[::-1]
    num_terms_std = 1
    nu_t_std[0] = nu_std[0]
    nu_t_std[idx[:num_terms_std] + 1] = nu_std[idx[:num_terms_std] + 1]
    m_d_std = irfft(nu_t_std)
    plt.plot(d_std)
    plt.plot(m_d_std, 'g', linewidth=2)
    plt.xlabel('Day of Year', fontsize=16)
    plt.ylabel('Standard Deviation of Solar Irradiance', fontsize=16)
    plt.legend(['Standard Deviation', 'Model'], loc='best')
    plt.savefig(prefix + '_seasonal_std_E.eps')
    plt.clf()
    #plt.show()
    # Now compute the residuals for the desired year
    r_d_E_us = d_E_us / m_d_std
    plt.plot(r_d_E_us)
    plt.xlabel('Day of Year', fontsize=16)
    plt.ylabel('Standarised Residuals', fontsize=16)
    plt.savefig(prefix + '_standarized_E.eps')
    plt.clf()
    #plt.show()
    # Plot the SACF
    sm.graphics.tsa.plot_acf(r_d_E_us, lags=60)
    plt.savefig(prefix + '_standarized_E_sacf.eps')
    plt.clf()
    #plt.show()
    # Plot the SPACF
    sm.graphics.tsa.plot_pacf(r_d_E_us, lags=60)
    plt.savefig(prefix + '_standarized_E_spacf.eps')
    plt.clf()
    #plt.show()
    # Fit an AR model
    r_d_E_arma = sm.tsa.ARMA(np.array(r_d_E_us), (3, 0)).fit()
    print r_d_E_arma.params
    # Do we obey theory
    plt.plot(r_d_E_arma.resid)
    plt.savefig(prefix + '_residuals_E.eps')
    plt.clf()
    #plt.show()
    qqplot(r_d_E_arma.resid, line='q', fit=True)
    plt.savefig(prefix + '_standarized_E_qqplot.eps')
    plt.clf()
    #plt.show()
    # Make one step ahead forcast
    # get what you need for predicting one-step ahead
    params = r_d_E_arma.params
    residuals = r_d_E_arma.resid
    p = r_d_E_arma.k_ar
    q = r_d_E_arma.k_ma
    k_exog = r_d_E_arma.k_exog
    k_trend = r_d_E_arma.k_trend
    steps = 1
    d_E_test = d_E_all['2012']
    y_all = (d_E_test[:364] - s_E) / m_d_std
    p_E_test = np.zeros(364)
    p_E_test[0] = d_E_test[0]
    for i in xrange(1, 364):
        y_obs = np.array(y_all[:i])
        y_n_p  = arma_predict(params, steps, residuals, p, q, k_trend, k_exog,
                 endog=y_obs, exog=None, start=y_obs.shape[0])[0]
        p_E_test[i] = y_n_p * m_d_std[i] + s_E[i]
    plt.plot(d_E_test)
    plt.plot(p_E_test, 'r', linewidth=2)
    plt.xlabel('Day of Year', fontsize=16)
    plt.ylabel('Solar Irradiance', fontsize=16)
    plt.legend(['Data', 'Forecast'], loc='best')
    plt.savefig(prefix + '_one_step_prediction.eps')
    plt.clf()
    #plt.show()
    # Estimating the noise
    loc = residuals.min()
    scale = residuals.max() - loc - 2.
    z = (residuals - loc) / scale
    z = z[z <= 1.]
    import scipy.stats
    dist = scipy.stats.beta
    param = dist.fit(residuals, 1.89, 2.64, floc=loc, fscale=scale)
    print param
    z = np.linspace(loc, loc + scale, 100)
    z_pdf = dist.pdf(z, param[0], param[1], loc=loc, scale=scale)#*params[:-2], loc=param[-2], scale=param[-1])
    plt.hist(residuals, normed=True)
    plt.plot(z, z_pdf, 'r', linewidth=2)
    plt.xlabel('Standarized Residual ($(Z_t)$)', fontsize=16)
    plt.ylabel('$p(Z_t)$', fontsize=16)
    plt.savefig(prefix + '_noise_pdf.')
    plt.clf()
    # Take a sample of a year
    Z = dist.rvs(param[0], param[1], loc=loc, scale=scale, size=364)
    d_E_test = d_E_all['2012']
    y_all = (d_E_test[:364] - s_E) / m_d_std
    p_E_test = np.zeros(364)
    p_E_test[0] = d_E_test[0]
    for i in xrange(1, 364):
        y_obs = np.array(y_all[:i])
        y_n_p  = arma_predict(params, steps, residuals, p, q, k_trend, k_exog,
                 endog=y_obs, exog=None, start=y_obs.shape[0])[0]
        y_all[i] = y_n_p + Z[i]
        p_E_test[i] = y_all[i]* m_d_std[i] + s_E[i]
    plt.clf()
    plt.plot(d_E_test)
    plt.plot(p_E_test, 'r', linewidth=2)
    plt.xlabel('Day of Year', fontsize=16)
    plt.ylabel('Solar Irradiance', fontsize=16)
    plt.legend(['Real Year', 'Synthetic Year'], loc='best')
    plt.savefig(prefix + '_one_sample.eps')
