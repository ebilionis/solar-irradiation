#!/usr/bin/env python
"""
Calibrate a simple model to the data.
"""


import pandas as pd
import numpy as np
import best
import matplotlib.pyplot as plt


if __name__ == '__main__':
    data = pd.read_hdf('../data/data_w_alpha.h5', 'solar')
    #tmp = data['alpha']
    #tmp = np.hstack([[0.], tmp[:-1]])
    #data['alpha'] = tmp
    df = data['1999']
    #df = data
    x = np.atleast_2d(df['alpha']).T
    n = 1
    tmp = []
    for i in xrange(1, n + 1):
        tmp.append(np.sin(float(i) * x))
    PHI = np.hstack(tmp)
    #print PHI
    idx = PHI[:, 0] <= 0.
    PHI[idx, :] = 0.
    y = np.atleast_2d(df['Irradiance']).T
    #rvm = best.rvm.RelevanceVectorMachine()
    #rvm.set_data(PHI, y)
    #rvm.initialize(beta=100000.)
    #rvm.train(verbose=True)
    #print rvm.weights
    B, res, rank, s = np.linalg.lstsq(PHI, y)
    print B
    #m = np.zeros(365 * 96)
    #v = np.zeros(365 * 96)
    #for i in xrange(365 * 96):
    #    tmp = []
    #    for y in xrange(1991, 2010)
    #        tmp.append(df[str(y)]['Irradiance'][i])
    #    tmp = np.array(tmp)
    #    m[i] = np.mean(tmp)
    #    v[i] = np.var(tmp)
    #    print i, i / 365, m[i], v[i]
    #np.savetxt('m.dat', m)
    #np.savetxt('v.dat', v)
    #m = np.loadtxt('m.dat')
    #v = np.loadtxt('v.dat')
    #plt.plot(m)
    #plt.show()
#    df = pd.concat([data[str(y)] for y in range(2011, 2012)])
#    x = np.sin(df['alpha'])
#    y = np.cos(df['alpha'])
#    z = df['Irradiance']
#    c = -(1. / 24.) * 10.
#    t = df['julian'] + c - np.array(df['julian'] + c, dtype='int32')
    #x1 = np.linspace(x.shape[0], x.shape[1], 64)
    #x2 = np.linspace(0., 1., 64)
    #X1, X2 = np.meshgrid(x1, x2)
#    H, X1, X2 = np.histogram2d(x, t, weights=z)
#    plt.imshow(H, interpolation='nearest',
#               extent=[X1[0], X1[-1], X2[0], X2[-1]])
#    plt.show()
#    from mpl_toolkits.mplot3d import Axes3D
#    fig = plt.figure()
#    ax = fig.add_subplot(111, projection='3d')
#    ax.plot(x, t * 24., z, '.')
#    plt.show()
#    quit()
#    for i in range(1, 12):
#        df = pd.concat([data['%d/2011' % i]])
#        x = np.sin(df['alpha'])
#        y = np.cos(df['alpha'])
#        z = df['Irradiance']
#        c = -(1. / 24.) * 10.
#        t = df['julian'] + c - np.array(df['julian'] + c, dtype='int32')
#        ax.plot(x, t * 24., z, '.')
#    plt.show()
#    x = np.atleast_2d(df['alpha']).T
#    tmp = []
#    for i in xrange(1, n + 1):
#        tmp.append(np.sin(float(i) * x))
#    PHI = np.hstack(tmp)
#    idx = PHI[:, 0] <= 0.
#    PHI[idx, :] = 0.
    #plt.plot(df['Irradiance'])
    s = df['Irradiance'] - np.dot(PHI, B).flatten()
    from statsmodels.tsa.ar_model import AR
    ar = AR(s, dates=df.index)
    results = ar.fit(maxlag=3)
    print results.params
    #plt.plot(np.sin(df['alpha']), s, '.')
    #plt.show()
    #x = np.linspace(0., 1., 100)
    #X1, X2 = np.meshgrid(x, x)
    #plt.hist(df['alpha'])
    #plt.show()
