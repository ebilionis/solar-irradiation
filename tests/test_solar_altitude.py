#!/usr/bin/env python
"""
Plot the solar altitude as a function of time.
"""


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt



if __name__ == '__main__':
    data = pd.read_hdf('../data/data_w_alpha.h5', 'solar')
    df = data['1992']
    tmp = df['julian']
    times = tmp - np.array(tmp, dtype='i')
#    plt.plot(times, np.sin(df['alpha']))
#    plt.show()
    from mpl_toolkits.mplot3d import Axes3D
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    idx = np.sin(df['alpha']) > 0.
    ax.plot(df['julian'][idx], df['alpha'][idx], df['Irradiance'][idx], '.')
    plt.show()
