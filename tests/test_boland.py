#!/usr/bin/env python

"""
Test how astronomical data can be used to predict the irradiance.
"""


import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from sklearn import preprocessing
import math
from scipy import stats
import statsmodels.api as sm
from statsmodels.graphics.api import qqplot


if __name__ == '__main__':
    data = pd.read_hdf('../data/data.h5', 'solar')
    #years = ['1991', '1992', '1993', '1994', '1995', '1996', '1997',
    #         '1998', '1999', '2000', '2001']
    df = pd.concat([data[str(y)] for y in range(1991, 1992)])
    #df = data['1991']
    n = df.shape[0]
    t = df['julian']
    ts = t[1] - t[0]
    f = df['Irradiance']
    nu = np.fft.rfft(f)
    A0 = np.real(nu[0]) / (2. * n)
    A = 2. * np.real(nu[1:]) / n
    B = -2. * np.imag(nu[1:]) / n
    rho2 = A ** 2. + B ** 2.
    rho2 = rho2
    V = 0.5 * rho2.sum()
    left = np.arange(rho2.shape[0])
    #plt.bar(left[:1000], rho2[:1000], width=0.5)
    #plt.show()
    idx = np.argsort(rho2)[::-1]
    #print idx[:50]
    #print 0.5 * rho2[idx].cumsum()[:50] / V
    num_terms = 5
    new_nu = np.zeros(nu.shape, dtype='complex128')
    new_nu[0] = nu[0]
    new_nu[idx[:num_terms] + 1] = nu[idx[:num_terms] + 1]
    from statsmodels.tsa.ar_model import AR
    R = df['Irradiance'] - np.fft.irfft(new_nu)
    fig = plt.figure(figsize=(12, 8))
    ax1 = fig.add_subplot(211)
    fig = sm.graphics.tsa.plot_acf(R, lags=500, ax=ax1)
    ax2 = fig.add_subplot(212)
    fig = sm.graphics.tsa.plot_pacf(R, lags=500, ax=ax2)
    plt.show()
    ar = AR(R, dates=df.index).fit()
    ar.resid.plot()
    plt.show()
    print stats.normaltest(ar.resid)
    qqplot(ar.resid, line='q', fit=True)
    plt.show()
    #df_p = data['1992']
    #predict = ar.predict()
    #predict.plot()
    #plt.show()
    df_p = data['1/1/1992']
    R_u = df_p['Irradiance'] - np.fft.irfft(new_nu)[:96]
    ar_p = AR(R_u[:45], dates=df_p.index[:45]).fit(start_params=ar.params,
                                                   maxiter=1)
    R_p = ar_p.predict(end=96)
    R_u.plot(style='b', linewidth=3)
    R_p.plot(style='r', linewidth=3)
    plt.show()
