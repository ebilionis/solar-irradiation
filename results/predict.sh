kernel="lin_exp"
for i in 19 20 21 22 23 24; 
do 
    python scripts/make_prediction_comparison.py \
    -i /pvfs/ebilionis/data/ok/results_16_${kernel}/insolation_ok_400x400_pca_16_gp_${kernel}_predict_2014_02_${i}_1600.h5,\
/pvfs/ebilionis/data/ok/insolation_ok_ground_2_500_exponential_predict_2014_02_${i}_1600.h5 \
--sat-data-filename=/pvfs/ebilionis/data/insolation_ok_400x400.h5 \
--netcdf-filename=/pvfs/ebilionis/data/ground_insolation_ok.nc
done
